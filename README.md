Community Windows CE 7 BSP
===================

Windows CE 7 for BeagleBone baseboards.
This project is a fork of the existing BSP for **BeagleBone Black**[^bbb].

----------

### Old but gold

This project is a fork of the existing BSP for **BeagleBone Black**[^bbb]. It has been used for demo purpose and received some improvements that I would like to share:
* LAN initialization bug fixed : the hardware initialization timings are not perfect and it sometimes causes PHY detection error which result with no connectivity.
* 'Enable' signal toggling is now properly done with RS485 : in the previous version, this signal was not staying at HIGH during transmission. It was set back to LOW right after data buffer copy to internal FIFO.
* Battery support fixed: in the previous version, the board was not shutting down properly with a battery connected
* Capacitive touchscreen support : thanks to toradex code snippet, this BSP is now compatible with FT5x04 chips
* New drivers : LT2984 ADC, DS1307 RTC, MCP23017 GPIO, ...
* Probably other things that I do not recall right now :)

### Tools

Your workstation must have the following tools installed:

* Visual Studio 2008
* Visual Studio 2008 SP1 (if not installed with the package above)
* .NET Compact Framework 3.5 (if not installed with the package above)
* Windows Embedded Compact 7 

### How to get started ?

* Copy setup/platform/LniBSP folder to C:/WINCE700/platform/
* Copy setup/Public/PowerVR folder to C:/WINCE700/public/
* Install AutoLaunch v310 for compact7[^autolaunch]
* Apply patch in tools/AutoLaunch_patch.bat. If WINCE is not installed on C:/, edit both patches and script.
* Open LniBSP.sln

> **Note:**
> - Replace C:/WINCE700 with your Windows Embedded Compact 7 installation directory

### Technical Notes

Several technical notes have been written on my blog[^blog]. They can be helpful to 
whoever wants to get familiar with Windows CE internals. 


[1] https://beaglebonebsp.codeplex.com/
[2] https://autolaunch4ce.codeplex.com/releases
[3] http://myembeddedlinux.blogspot.fr/p/blog-page_7.html
