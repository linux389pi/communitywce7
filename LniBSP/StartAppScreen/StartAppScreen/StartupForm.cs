﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using Microsoft.Win32;

namespace StartAppScreen
{
    public partial class StartupForm : Form
    {
        const int DEFAULT_SLEEP_TIME = 1000;
        System.Windows.Forms.Timer _sleepTimer;

        public StartupForm()
        {
            InitializeComponent();
            _sleepTimer = new System.Windows.Forms.Timer();
            // Fullscreen mode
            this.Size = new Size(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
        }

        void StartupForm_Load(object sender, System.EventArgs e)
        {
            // Check if registry key exists
            RegistryKey key = Registry.LocalMachine.OpenSubKey("StartAppScreen");
            if (key != null)
            {
                int sleepTime = (int)key.GetValue("SleepTime", DEFAULT_SLEEP_TIME);

                // Sleep for 3 seconds
                _sleepTimer.Interval = sleepTime;
                _sleepTimer.Tick += new EventHandler(_sleepTimer_Tick);
                _sleepTimer.Enabled = true;
            }
            else
            {
                // Auto-Kill Now
                this.Close();
            }
            
        }

        void _sleepTimer_Tick(object sender, EventArgs e)
        {
            _sleepTimer.Enabled = false;
            // Auto-Kill
            this.Close();
        }
    }
}