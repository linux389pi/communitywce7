@echo off

:: Constants (modify if necessary)
SET WINCEDIR="C:/WINCE700/"
SET FILE_TO_BE_PATCHED= "%WINCEDIR%3rdParty/AutoLaunch_v310_Compact7/postlink.bat"

:: First of all check if the file to be patched exists
if exist %FILE_TO_BE_PATCHED% (
    goto version_check
) else (
    echo File not found. Either AutoLaunch is not installed or Windows CE is not installed on C: drive
    goto exit
)

:version_check
wmic os get version | find "5.1." > nul
if %ERRORLEVEL% == 0 goto ver_xp

:: else this is a recent windows
goto ver_7


:ver_7
:ver_8
:ver_10
echo Platform: Windows 7 or later
call "patch/bin/patch.exe" -p0 -i AutoLaunch_win7.patch
goto space_support

:ver_xp
echo Platform: Windows XP
call "patch/bin/patch.exe" -p0 -i AutoLaunch_xp.patch
goto space_support

:: Space support in name of the executable to run. If the registry key is something like
:: /Storage Card/myApp.exe, the default behavior of AppLaunch is to split the string in two and to consider
:: that /Storage is the executable name and Card/myApp.exe some parameters that will be given to it.
:: With this patch, we disable the argument support but we allow for spaces in key names
:space_support
call "patch/bin/patch.exe" -p0 -i AutoLaunch_SupportSpace.patch
goto exit

:exit
pause