// All rights reserved ADENEO EMBEDDED 2010
// Copyright (c) 2007 BSQUARE Corporation. All rights reserved.

/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/
//------------------------------------------------------------------------------
//
//  File:  trans.c
//
#pragma warning (push)
#pragma warning (disable: 4115 4201 4214)
#include <windows.h>
#include <oal.h>
#include <bsp_def.h>

#include "soc_cfg.h"
#pragma warning (pop)

//-------------------------------------------------------------------------
// T2 HSUSB Transceiver Register Values
//-------------------------------------------------------------------------

#define FUNC_CTRL_SUSPENDM                      (1 << 6)
#define FUNC_CTRL_RESET                         (1 << 5)
#define FUNC_CTRL_OPMODE_MASK                   (3 << 3)
#define FUNC_CTRL_OPMODE_NORMAL                 (0 << 3)
#define FUNC_CTRL_OPMODE_NONDRIVING             (1 << 3)
#define FUNC_CTRL_OPMODE_DISABLE_BIT_NRZI       (2 << 3)
#define FUNC_CTRL_TERMSELECT                    (1 << 2)
#define FUNC_CTRL_XCVRSELECT_MASK               (3 << 0)
#define FUNC_CTRL_XCVRSELECT_HS                 (0 << 0)
#define FUNC_CTRL_XCVRSELECT_FS                 (1 << 0)
#define FUNC_CTRL_XCVRSELECT_LS                 (2 << 0)
#define FUNC_CTRL_XCVRSELECT_FS4LS              (3 << 0)
 
#define OTG_CTRL_VBUS_DRV                       (1 << 5)
#define OTG_CTRL_VBUS_CHRG                      (1 << 4)
#define OTG_CTRL_VBUS_DISCHRG                   (1 << 3)
#define OTG_CTRL_DM_PULLDOWN                    (1 << 2)
#define OTG_CTRL_DP_PULLDOWN                    (1 << 1)
#define OTG_CTRL_ID_PULLUP                      (1 << 0)

#define IFC_CTRL_INTERFACE_PROTECT_DISABLE      (1 << 7)
#define IFC_CTRL_AUTORESUME                     (1 << 4)
#define IFC_CTRL_CLOCK_SUSPENDM                 (1 << 3)
#define IFC_CTRL_CARKITMODE                     (1 << 2)
#define IFC_CTRL_FSLSSERIALMODE_3PIN            (1 << 1)

#define OTHER_FUNC_CTRL_DM_PULLUP               (1 << 7)
#define OTHER_FUNC_CTRL_DP_PULLUP               (1 << 6)
#define OTHER_FUNC_CTRL_BDIS_ACON_EN            (1 << 4)
#define OTHER_FUNC_CTRL_FIVEWIRE_MODE           (1 << 2)


#define OTHER_IFC_CTRL_OE_INT_EN                (1 << 6)
#define OTHER_IFC_CTRL_CEA2011_MODE             (1 << 5)
#define OTHER_IFC_CTRL_FSLSSERIALMODE_4PIN      (1 << 4)
#define OTHER_IFC_CTRL_HIZ_ULPI_60MHZ_OUT       (1 << 3)
#define OTHER_IFC_CTRL_HIZ_ULPI                 (1 << 2)
#define OTHER_IFC_CTRL_ALT_INT_REROUTE          (1 << 0)

#define OTHER_IFC_CTRL2_ULPI_STP_LOW            (1 << 4)
#define OTHER_IFC_CTRL2_ULPI_TEXN_POL           (1 << 3)
#define OTHER_IFC_CTRL2_ULPI_4PIN_2430          (1 << 2)
#define OTHER_IFC_CTRL2_ULPI_INT_OUTSEL_INT2N   (1 << 0)

#define PHY_CLK_CTRL_CLOCKGATING_EN             (1 << 2)
#define PHY_CLK_CTRL_CLK32_EN                   (1 << 1)
#define PHY_CLK_CTRL_REQ_PHY_DPLL_CLK           (1 << 0)

#define PHY_CLK_CTRL_STS_PHY_DPLL_LOCK          (1 << 0)

#define PHY_PWR_CTRL_PHYPWD                     (1 << 0)

#define POWER_CTRL_OTG_EN                       (1 << 5)

#define CARKIT_CTRL_CARKITPWR                   (1 << 0)
#define CARKIT_CTRL_TXDEN                       (1 << 2)
#define CARKIT_CTRL_RXDEN                       (1 << 3)
#define CARKIT_CTRL_SPKLEFTEN                   (1 << 4)
#define CARKIT_CTRL_MICEN                       (1 << 6)


#define USB_INT_EN_MASK                         0xFF

// Debug message 
#define OAL_RNDIS                               FALSE


//------------------------------------------------------------------------------
//
//  Function:  RequestPHYAccess()
//

BOOL 
RequestPHYAccess( 
        HANDLE hTWL
    )
{
	UNREFERENCED_PARAMETER(hTWL);

    return TRUE;
}

//------------------------------------------------------------------------------
//
//  Function:  PowerOnT2USBTransceiver()
//

BOOL 
PowerOnT2USBTransceiver( 
        HANDLE hTWL
    )
{
    BOOL  rc = FALSE;

	UNREFERENCED_PARAMETER(hTWL);


    return rc;
}

//------------------------------------------------------------------------------
//
//  Function:  InitTransceiver
//
BOOL 
InitTransceiver( 
        HANDLE hTWL
    )
{
    BOOL    rc = FALSE;

	UNREFERENCED_PARAMETER(hTWL);


    return rc;
}

//------------------------------------------------------------------------------
//
//  Function:  InitializeHardware
//

BOOL InitializeHardware()
{
    BOOL   rc = FALSE;


    return rc;
}

//------------------------------------------------------------------------------
//
//  Function:  ConnectHardware
//

void ConnectHardware()
{

}

//------------------------------------------------------------------------------
//
//  Function:  DisconnectHardware
//

void DisconnectHardware()
{

}

//------------------------------------------------------------------------------
//
//  Function:  GetUniqueDeviceID
//

DWORD GetUniqueDeviceID()
{
    DWORD code;
    DWORD *pDieId = (DWORD *)OALPAtoUA(SOCGetIDCodeAddress());

    // Create unique part of name from SoC ID
    code  = INREG32(pDieId);

    OALMSG(1, (L"+USBFN::Device ID = 0x%x\r\n", code));

    return BSP_USB_DEFAULT_DEVICE_ID;  // hardcoded id
}

//------------------------------------------------------------------------------

