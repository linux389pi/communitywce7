//========================================================================
//   Copyright (c) Texas Instruments Incorporated 2011-2012
//
//   Use of this software is controlled by the terms and conditions found
//   in the license agreement under which this software has been supplied
//   or provided.
//========================================================================

#include <windows.h>
#include <oal.h>
#include <oal_memory.h>
#include "cpsw3g.h"

#define AM33X_CPSW_MDIO_BASE          0x4A101000

void MdioWaitForAccessComplete(int channel);

UINT32 g_cpmdio_base=0;
#define CPMDIO_BASE				g_cpmdio_base
#define CPMDIO_ALIVE			(CPMDIO_BASE + 0x08)

#define MDIO_USRACCESS(channel) (*(volatile unsigned int *)(CPMDIO_BASE + 0x80 + (8*(channel))))
#define MDIO_ALIVE              (*(volatile unsigned int *)(CPMDIO_BASE + 0x08))
#define MDIO_USERPHYSEL0        (*(volatile unsigned int *)(CPMDIO_BASE + 0x84))

#define MDIO_UA_GO              (1 << 31)
#define MDIO_UA_READ            0x00000000
#define MDIO_UA_WRITE           (1 << 30)
#define MDIO_UA_ACK             (1 << 29)
#define MDIO_UA_DATA_MASK       0x0000ffff
#define MDIO_CTRL_ENABLE        (1 << 30)
#define MDIO_CTRL_IDLE          (1 << 31)
#define MII_STATUS_REG          1

#define MDIO_CTRL               (( volatile unsigned int *) (CPMDIO_BASE + 0x04))
#define MDIO_ACK                (*(volatile UINT32       *) (CPMDIO_BASE + 0x08))
#define MDIO_REGADR             (21)
#define MDIO_PHYADR             (16)
#define MDIO_DATA               (0)

#define MDIO_PREAMBLE           (1 << 20)
#define MDCLK_DIVIDER           (0x255)


void MdioWaitForAccessComplete(int channel)
{
    while ((MDIO_USRACCESS(channel) & MDIO_UA_GO) != 0);
}

int MdioRd(UINT16 PhyAddr, UINT16 RegNum, int channel, UINT16 *pData)
{
    MdioWaitForAccessComplete(channel);

    MDIO_USRACCESS(channel) = MDIO_UA_GO | 
                              MDIO_UA_READ | 
                              ((RegNum  & 0x1F) << MDIO_REGADR) |
                              ((PhyAddr & 0x1F) << MDIO_PHYADR );

    MdioWaitForAccessComplete(channel);
    
    if(MDIO_USRACCESS(channel) & MDIO_UA_ACK)
    {
        /* Return reg value on successful ACK */
        *pData = (UINT16)(MDIO_USRACCESS(channel) & MDIO_UA_DATA_MASK);  
        return 0;
    }

    return (-1);
}

void MdioWr(UINT16 phyAddr, UINT16 regNum, int channel, UINT16 data)
{
    MdioWaitForAccessComplete(channel);

    MDIO_USRACCESS(channel) = MDIO_UA_GO | 
                              MDIO_UA_WRITE |
                              ((regNum & 0x1F) << MDIO_REGADR) |
                              ((phyAddr & 0x1F) << MDIO_PHYADR ) | 
                              (data & 0xFFFF);

    MdioWaitForAccessComplete(channel);
}

void MdioSetBits(UINT16 PhyAddr, UINT16 RegNum, UINT16 BitMask, int channel)
{
    UINT16 val;

    if (0 == MdioRd(PhyAddr,RegNum,channel, &val))
    {
        MdioWr((PhyAddr & 0xffff), RegNum, channel, (val | BitMask));
    }
}

WORD MdioEnable(void)
{
    DWORD r = 0;
	WORD address = 0;

    g_cpmdio_base = (UINT32)OALPAtoUA(AM33X_CPSW_MDIO_BASE);

    /* Clearing MDIOCONTROL register */
	*(MDIO_CTRL) = 0;
	/* Configure the PREAMBLE and CLKDIV in the MDIO control register */
	*(MDIO_CTRL) &= ~MDIO_PREAMBLE; /* CLKDIV default */
	/* Enable sending MDIO frame preambles */
	*(MDIO_CTRL) |= (MDCLK_DIVIDER | MDIO_CTRL_ENABLE);
	/* Enable the MDIO module by setting the ENABLE bit in MDIOCONTROL */
    while (!(r = (MDIO_ALIVE)));

	/* Get PHY address 
	 * Although it is supposed to be 0, the reset line of LAN8710 is poor and the chip's
	 * registers are not always initialized properly. Hence, when read phy mask, we are sometimes
	 * surprised to see that PHY is not 0 but 2. In this scenario, we have to use the phy_id
	 * that matches the mask that we read at init.*/
	while (r >>= 1) {
		++address;
    }

	OALMSGS(OAL_ERROR, (L"mdio_alive = %d\r\n", r));

    // @@CSo assumption : channel = 0
	(MDIO_USERPHYSEL0) |= address; 

	return address;
}
