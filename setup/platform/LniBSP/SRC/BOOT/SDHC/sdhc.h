/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/
//
//  File: SDHC.H
//
//  SDHC controller driver public API
//

#ifndef _SDHC_H
#define _SDHC_H

#include "SDHCRegs.h"

//
// SHDC controller handler
//
typedef struct
{
    DWORD            dwSlot;
    DWORD            dwSDIOCard;
    DWORD            dwFlags;
    DWORD            dwMaxClockRate;              // host controller's clock base
    BOOL             fInitialized;                // driver initialized
    BOOL             fAppCmdMode;                 // if true, the controller is in App Cmd mode
    BOOL             fCardPresent;                // a card is inserted and initialized
    UINT16           TransferClass;
    AM33X_MMCHS_REGS *pbRegisters;                // SDHC controller registers
}SDHC_Port_t;

DWORD GetBootMMCSlotConfig();

VOID SendInitSequence(SDHC_Port_t* pPort);

VOID SdhcHandleInsertion(SDHC_Port_t* pPort);

VOID SdhcSetClockRate(SDHC_Port_t* pPort, PDWORD pdwRate);

VOID SdhcSetInterface(SDHC_Port_t* pPort, DWORD mode);

VOID SdhcSoftwareReset(SDHC_Port_t* pPort, DWORD dwResetBits);

SD_API_STATUS SdhcInitialize(SDHC_Port_t* pPort);

SD_API_STATUS SdhcBusRequestHandler(SDHC_Port_t* pPort, PSD_BUS_REQUEST pRequest);

SD_API_STATUS SdhcControllerIstThread(SDHC_Port_t* pPort, PSD_BUS_REQUEST pRequest);

#endif


