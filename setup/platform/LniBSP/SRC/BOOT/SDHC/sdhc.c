/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/
//
//  File: SDHC.C
//  SDHC controller driver implementation (xldr and Eboot loader version)

#include <SDCardDDK.h>
#include "soc_cfg.h"
#include "bsp_padcfg.h"
#include "sdk_gpio.h"
#include "gpio.h"
#include "sdhc.h"


// set to true to enable more OALMSGs
// Caution: leave FALSE for XLOADER builds to keep size down
#define OALMSG_ENABLE   FALSE
//#define OALMSG_ENABLE   TRUE

#if OALMSG_ENABLE
    #define OALMSGX(z, m)   OALMSG(z, m)
#else
    #define OALMSGX(z, m)   {}
#endif

#ifndef OAL_FUNC
#define OAL_FUNC 1
#endif

#ifndef OAL_INFO
#define OAL_INFO 1
#endif

#ifndef OAL_ERROR
#define OAL_ERROR 1
#endif

#define    MMC_SEND_EXT_CSD         8
#define    DATACMD_TIMEOUT_VALUE               10000
#define MMC_INT_EN_MASK                     0x00330033

DWORD g_bootSlot = 0;

// forward declarations
VOID SdhcSetClockRate(SDHC_Port_t* pPort, PDWORD pdwRate);
VOID SdhcSetInterface(SDHC_Port_t* pPort, DWORD mode);
VOID SdhcSoftwareReset(SDHC_Port_t* pPort, DWORD dwResetBits);
static SD_API_STATUS GetCommandResponse(SDHC_Port_t* pPort, PSD_BUS_REQUEST pRequest);
static BOOL SDIPollingReceive(SDHC_Port_t* pPort, PBYTE pBuff, DWORD dwLen);
static BOOL SDIPollingTransmit(SDHC_Port_t* pPort, PBYTE pBuff, DWORD dwLen);
static SD_API_STATUS  CommandCompleteHandler(SDHC_Port_t* pPort, PSD_BUS_REQUEST pRequest);
static VOID SetSDVSVoltage(SDHC_Port_t* pPort);
#ifdef DEBUG
    VOID DumpRegisters(SDHC_Port_t* pPort);
#endif
    
#define DEFAULT_TIMEOUT_VALUE               20000
#define START_BIT                           0x00
#define TRANSMISSION_BIT                    0x00
#define START_RESERVED                      0x3F
#define END_RESERVED                        0xFE
#define END_BIT                             0x01
#define SDIO_MAX_LOOP                       0x0080000

#define TRANSFER_SIZE(pRequest)            ((pRequest)->BlockSize * (pRequest)->NumBlocks)

#if defined(DEBUG)
    #define HEXBUFSIZE 1024
    char szHexBuf[HEXBUFSIZE];
#endif

typedef struct cmd_t
{
    BYTE Cmd;           // 1 - this is a known SD CMD; 2 - this is a known SDIO CMD
    BYTE ACmd;          // 1 - this is a known ACMD
    BYTE MMCCmd;        // 1 - this is a known MMC CMD
    DWORD flags;
} CMD;

//-----------------------------------------------------------------------------
//
//  InitializeHardware
//
static BOOL InitializeHardware(SDHC_Port_t* pPort)
{
    DWORD               dwCurrentTickCount;
    DWORD               dwTimeout;
    DWORD               dwCountStart;
    BOOL                fTimeoutOverflow = FALSE;
	HANDLE				hGpio = NULL;

    pPort->dwMaxClockRate = STD_HC_MAX_CLOCK_FREQUENCY;

    pPort->pbRegisters = OALPAtoUA(GetAddressByDevice(SOCGetSDHCDeviceBySlot(pPort->dwSlot)));
    if(!pPort->pbRegisters)
    {
        OALMSG(1, (TEXT("SDHC:Init HW: No SDHC device found\r\n")));
        return FALSE;
    }
             
    // Reset the controller
    OALMSG(OAL_INFO, (TEXT("SDHC:Init HW: controller RST %d\r\n"),pPort->dwSlot));
    OUTREG32(&pPort->pbRegisters->MMCHS_SYSCONFIG, MMCHS_SYSCONFIG_SOFTRESET);

    // calculate timeout conditions
    dwCountStart = OALGetTickCount();
    dwTimeout = dwCountStart + DEFAULT_TIMEOUT_VALUE;
    if ( dwTimeout < dwCountStart )
        fTimeoutOverflow = TRUE;

    // Verify that reset has completed.
    while (!(INREG32(&pPort->pbRegisters->MMCHS_SYSSTATUS) & MMCHS_SYSSTATUS_RESETDONE))
    {
        OALMSGX(OAL_INFO, (TEXT("SDHC:Init HW: MMCHS_SYSSTATUS = 0x%X\r\n"), INREG32(&pPort->pbRegisters->MMCHS_SYSSTATUS)));

        // check for a timeout
        dwCurrentTickCount = OALGetTickCount();
        if ( fTimeoutOverflow ? ( dwTimeout < dwCurrentTickCount && dwCurrentTickCount < dwCountStart )
            : ( dwTimeout < dwCurrentTickCount || dwCurrentTickCount < dwCountStart ) )
        {
            OALMSG(1, (TEXT("SDHC:Init HW: TIMEOUT\r\n")));
            return FALSE;
        }
    }

	if (2 == pPort->dwSlot)
	{
		hGpio = GPIOOpen();
 	
		if (hGpio == NULL)
		{
			return FALSE;
		}
        GPIOSetMode(hGpio, GPIO1_20, GPIO_DIR_OUTPUT );
        GPIOSetBit(hGpio, GPIO1_20);
		OALStall(1000);
        GPIOClrBit(hGpio, GPIO1_20);
		GPIOClose(hGpio);
		OALStall(10 * 1000);
		OALMSG(1, (TEXT("SDHC:Init HW:eMMC reset\r\n")));
	}

    return TRUE;
}


DWORD GetBootMMCSlotConfig()
{
	DWORD pad0,pad1,bootCfg;
    AM33X_SYSC_PADCONFS_REGS *pPadRegs;
	DWORD slot = 0;

	const PAD_INFO MMC1Pads[] = {MMC1_PADS END_OF_PAD_ARRAY};

	pPadRegs = (AM33X_SYSC_PADCONFS_REGS *)OALPAtoUA(AM33X_SYSC_PADCONFS_REGS_PA);

	bootCfg = 0x1f & INREG32(OALPAtoUA(AM33X_DEVICE_BOOT_REGS_PA));

	pad0 = INREG32(&(pPadRegs->CONF_MMC0_CMD));
	// check GPMC_CSN2 which is used as MMC1_CMD pad
	pad1 = INREG32(&(pPadRegs->CONF_GPMC_CSN2));

	// user button pressed and MMC0 pads configured by firmware
	if (0x18 == bootCfg && 0x30 == pad0)
	{
		slot = 1;
		goto cleanUp;
	}

	if (0x30 == pad0)
	{
		slot = 1;
		goto cleanUp;
	}

	// normal boot MMC1 only some pads configured by firmware
	// we configure for 8 bit
	if (0x1c == bootCfg && 0x32 == pad1)
	{
		ConfigurePadArray(MMC1Pads);
		slot = 2;
		goto cleanUp;
	}

	slot = 1;

cleanUp:
    OALMSG(1,(TEXT("SDHC: Slot %d boot\r\n"), slot)); 
	return slot;
}


//-----------------------------------------------------------------------------
//
//  SdhcControllerInit
//
static void SdhcControllerInit(SDHC_Port_t* pPort)
{
    DWORD dwClockRate;

    pPort->fAppCmdMode = FALSE;

    pPort->pbRegisters = NULL;
    pPort->fCardPresent = FALSE;

    // save boot device as first location at bottom of xldr stack
	*((DWORD *)OALPAtoUA(IMAGE_XLDR_STACK_PA)) = g_bootSlot;

    pPort->dwSlot = g_bootSlot;
    pPort->dwSDIOCard = 0;

    pPort->fInitialized = FALSE;
    pPort->TransferClass = 0;

    InitializeHardware(pPort);

    dwClockRate = MMCSD_CLOCK_INIT;
    SdhcSetClockRate(pPort, &dwClockRate);
    
    // use 1 bit MMC mode ...to start
    SdhcSetInterface(pPort, SD_INTERFACE_SD_MMC_1BIT);
    
    // Initialize the slot
    SdhcSoftwareReset(pPort, SOFT_RESET_ALL);
    OALStall(10 * 1000); // Allow time for card to power down after a device reset

#ifdef DEBUG
    DumpRegisters(pPort);
#endif
    pPort->dwSDIOCard = 0;
}


// Set up the controller according to the interface parameters.
VOID SdhcSetInterface(SDHC_Port_t* pPort, DWORD mode)
{
    if (SD_INTERFACE_SD_4BIT == mode)
    {
        OALMSGX(OAL_INFO, (TEXT("SDHC: 4 bit mode\r\n")));
        SETREG32(&pPort->pbRegisters->MMCHS_HCTL, MMCHS_HCTL_DTW);
    }
    else if (SD_INTERFACE_MMC_8BIT == mode)
    {
        OALMSGX(OAL_INFO, (TEXT("SDHC: 8 bit mode\r\n")));
        CLRREG32(&pPort->pbRegisters->MMCHS_HCTL, MMCHS_HCTL_DTW);
        SETREG32(&pPort->pbRegisters->MMCHS_CON, MMCHS_CON_DW8);		// MMC 8bit
    }
    else //SD_INTERFACE_SD_MMC_1BIT
    {
        OALMSGX(OAL_INFO, (TEXT("SDHC: 1 bit mode\r\n")));
        CLRREG32(&pPort->pbRegisters->MMCHS_HCTL, MMCHS_HCTL_DTW);
    }
}

//-----------------------------------------------------------------------------
//
//  SdhcSetClockRate
//
VOID SdhcSetClockRate(SDHC_Port_t* pPort, PDWORD pdwRate)
{
    DWORD dwRegValue;
	DWORD dwTimeout;
    DWORD dwDiv;
    DWORD dwClockRate = *pdwRate;

    OALMSGX(OAL_FUNC, (TEXT("SdhcSetClockRate %d\r\n"), *pdwRate));

    if (dwClockRate > pPort->dwMaxClockRate)
        dwClockRate = pPort->dwMaxClockRate;

    // calculate the register value
    dwDiv = (DWORD)((MMCSD_CLOCK_INPUT + dwClockRate - 1) / dwClockRate);

    //OALMSGX(OAL_INFO, (TEXT("actual wDiv = 0x%x  requested:0x%x"), dwDiv, *pdwRate));
    // Only 10 bits available for the divider, so mmc base clock / 1024 is minimum.
    if ( dwDiv > 0x03FF )
        dwDiv = 0x03FF;

    // Program the divisor, but leave the rest of the register alone.
    dwRegValue = INREG32(&pPort->pbRegisters->MMCHS_SYSCTL);

    dwRegValue = (dwRegValue & ~MMCHS_SYSCTL_CLKD_MASK) | MMCHS_SYSCTL_CLKD(dwDiv);
    dwRegValue = (dwRegValue & ~MMCHS_SYSCTL_DTO_MASK) | MMCHS_SYSCTL_DTO(0x0e); // DTO
    dwRegValue &= ~MMCHS_SYSCTL_CEN;
    dwRegValue &= ~MMCHS_SYSCTL_ICE;

    CLRREG32(&pPort->pbRegisters->MMCHS_SYSCTL, MMCHS_SYSCTL_CEN);

    OUTREG32(&pPort->pbRegisters->MMCHS_SYSCTL, dwRegValue);

    SETREG32(&pPort->pbRegisters->MMCHS_SYSCTL, MMCHS_SYSCTL_ICE); // enable internal clock

    dwTimeout = 500;
    while (((INREG32(&pPort->pbRegisters->MMCHS_SYSCTL) & MMCHS_SYSCTL_ICS) != MMCHS_SYSCTL_ICS) && (dwTimeout>0))
    {
        dwTimeout--;
    }

    SETREG32(&pPort->pbRegisters->MMCHS_SYSCTL, MMCHS_SYSCTL_CEN);
    SETREG32(&pPort->pbRegisters->MMCHS_HCTL, MMCHS_HCTL_SDBP); // power up the card

    dwTimeout = 500;
    while (((INREG32(&pPort->pbRegisters->MMCHS_SYSCTL) & MMCHS_SYSCTL_CEN) != MMCHS_SYSCTL_CEN) && (dwTimeout>0))
    {
        dwTimeout--;
    }

    *pdwRate = MMCSD_CLOCK_INPUT / dwDiv;
    OALMSGX(OAL_INFO, (TEXT("SDHC: clock = %d\r\n"), *pdwRate));
}


//  Reset the controller.
VOID SdhcSoftwareReset( SDHC_Port_t* pPort, DWORD dwResetBits )
{
    DWORD               dwCurrentTickCount;
    DWORD               dwTimeout;
    DWORD               dwCountStart;
    BOOL                fTimeoutOverflow = FALSE;

    dwResetBits &= (MMCHS_SYSCTL_SRA | MMCHS_SYSCTL_SRC | MMCHS_SYSCTL_SRD);

    // Reset the controller
    SETREG32(&pPort->pbRegisters->MMCHS_SYSCTL, dwResetBits);

    // calculate timeout conditions
    dwCountStart = OALGetTickCount();

    dwTimeout = dwCountStart + DEFAULT_TIMEOUT_VALUE;
    if ( dwTimeout < dwCountStart )
        fTimeoutOverflow = TRUE;

    // Verify that reset has completed.
    while ((INREG32(&pPort->pbRegisters->MMCHS_SYSCTL) & dwResetBits))
    {
        // check for a timeout
        dwCurrentTickCount = OALGetTickCount();
        if ( fTimeoutOverflow ? ( dwTimeout < dwCurrentTickCount && dwCurrentTickCount < dwCountStart )
            : ( dwTimeout < dwCurrentTickCount ) )
        {
            OALMSG(1, (TEXT("SDHC: MMC Reset timeout\r\n")));
            break;
        }
    }
    // enable autoidle, disable wakeup, enable smart-idle, ClockActivity (interface and functional clocks may be switched off)
    OUTREG32(&pPort->pbRegisters->MMCHS_SYSCONFIG, MMCHS_SYSCONFIG_AUTOIDLE | MMCHS_SYSCONFIG_SIDLEMODE(SIDLE_SMART));
}


//-----------------------------------------------------------------------------
//
// SetSDVSVoltage
//
static VOID SetSDVSVoltage(SDHC_Port_t* pPort)
{
    UINT32 val1, val2;

	val1 = MMCHS_CAPA_VS30;
	val2 = MMCHS_HCTL_SDVS_3V0;

    if ( pPort->dwSlot == MMCSLOT_1 )
    {
#ifdef MMCHS1_LOW_VOLTAGE
      val1 = MMCHS_CAPA_VS18;
      val2 = MMCHS_HCTL_SDVS_1V8;
#endif
    }
    else if (pPort->dwSlot == MMCSLOT_2)
    {
#ifdef MMCHS2_LOW_VOLTAGE
      val1 = MMCHS_CAPA_VS18;
      val2 = MMCHS_HCTL_SDVS_1V8;
#endif
    }
		
	SETREG32(&pPort->pbRegisters->MMCHS_CAPA, val1);
	SETREG32(&pPort->pbRegisters->MMCHS_HCTL, val2);
}


// Send command without response
static SD_API_STATUS GoIdleState(SDHC_Port_t* pPort)
{
    DWORD MMC_CMD = 0;
    DWORD dwTimeout;

    OUTREG32(&pPort->pbRegisters->MMCHS_STAT, 0xFFFFFFFF);
    dwTimeout = 80000;
    while (((INREG32(&pPort->pbRegisters->MMCHS_PSTATE) & MMCHS_PSTATE_CMDI)) && (dwTimeout>0))
    {
        dwTimeout--;
        if(!dwTimeout)
        {
			OALMSG(1, (TEXT("SDHC:GoIdleState : ERROR: Cmd Line in use!\r\n")));
            return SD_API_STATUS_RESPONSE_TIMEOUT;
        }
    }

    MMC_CMD = MMCHS_INDX(SD_CMD_GO_IDLE_STATE);
    MMC_CMD |= MMCHS_RSP_NONE;

    // Program the argument into the argument registers
    OUTREG32(&pPort->pbRegisters->MMCHS_ARG, 0xFFFFFFFF);
    // Issue the command.
    OUTREG32(&pPort->pbRegisters->MMCHS_CMD, MMC_CMD);

    dwTimeout = 5000;
    while (dwTimeout > 0)
    {
        dwTimeout --;
        if (INREG32(&pPort->pbRegisters->MMCHS_STAT) & MMCHS_STAT_CC ) 
        {
            break;
        }
        if (INREG32(&pPort->pbRegisters->MMCHS_STAT) & (MMCHS_STAT_CTO | MMCHS_STAT_CERR)) 
        {
            OALMSG(1, (TEXT("SDHC: GoIdleState: ERROR: transmission failed (STAT=0x%08X)\r\n"), INREG32(&pPort->pbRegisters->MMCHS_STAT)));
            return SD_API_STATUS_CRC_ERROR;
        }
        else if(!dwTimeout)
        {
            OALMSG(1, (TEXT("SDHC: GoIdleState: ERROR: transmission failed (STAT=0x%08X)\r\n"), INREG32(&pPort->pbRegisters->MMCHS_STAT)));
            return SD_API_STATUS_RESPONSE_TIMEOUT;
        }
    }

    OUTREG32(&pPort->pbRegisters->MMCHS_STAT, INREG32(&pPort->pbRegisters->MMCHS_STAT));
    // always return 0 if no response needed
    return SD_API_STATUS_SUCCESS;
}


//  Send init sequence to card
VOID SendInitSequence(SDHC_Port_t* pPort)
{
    DWORD dwCount;

    // set SD CLK slow enough so init sequence (80 CLK cycles is at least 1msec)
    dwCount = 10000;
    SdhcSetClockRate(pPort, &dwCount);

    //OUTREG32(&pPort->pbRegisters->MMCHS_IE,  0xFFFFFEFF);
    // Enable interrupts
    OUTREG32(&pPort->pbRegisters->MMCHS_ISE, MMC_INT_EN_MASK);
    OUTREG32(&pPort->pbRegisters->MMCHS_IE,  MMC_INT_EN_MASK);

    SETREG32(&pPort->pbRegisters->MMCHS_CON, MMCHS_CON_INIT);

    //for (dwCount = 0; dwCount < 10; dwCount ++)
    //{
        GoIdleState(pPort);
    //}
    OUTREG32(&pPort->pbRegisters->MMCHS_STAT, 0xFFFFFFFF);
    CLRREG32(&pPort->pbRegisters->MMCHS_CON, MMCHS_CON_INIT);
}

//  Issues the specified SDI command
static SD_API_STATUS SendCommand(SDHC_Port_t* pPort, PSD_BUS_REQUEST pRequest)
{
    DWORD MMC_CMD;
    DWORD dwCountStart;
    DWORD Cmd = pRequest->CommandCode;
    DWORD Arg = pRequest->CommandArgument;
	DWORD Flags = pPort->dwFlags;
    DWORD dwRegVal;

    pPort->TransferClass = pRequest->TransferClass;

    // Clear all transmission interrupt flags
    OUTREG32(&pPort->pbRegisters->MMCHS_STAT,0xFFFFFFFF);
    
    dwCountStart = OALGetTickCount();
    // Fill command index
    MMC_CMD = MMCHS_INDX(Cmd);
    // Add flags (response type, data, ...)
    MMC_CMD |= Flags;
    // Disable DMA, we are not using it here
    MMC_CMD &= ~MMCHS_CMD_DE;

    if ( (Cmd == MMC_SEND_EXT_CSD) && (pRequest->HCParam == 0x5 ) )
    {
        // Asking an MMC card to send its extended CSD which means some of the args below need tweaking.
        // Again, like the call in sdcard.c, this is to account for the 1 command that is handled differently
        // in MMC vs SD so as not to rewrite the entire command table at the top of the file.        
        pPort->TransferClass = SD_READ;
        pRequest->TransferClass = SD_READ; 
        pRequest->HCParam = 0;
    }

    if ( pPort->TransferClass == SD_READ || pPort->TransferClass == SD_WRITE )
    {
        dwRegVal = (DWORD)(pRequest->BlockSize & 0xFFFF);
        dwRegVal |= (((DWORD)(pRequest->NumBlocks & 0xFFFF)) << 16);
        OUTREG32(&pPort->pbRegisters->MMCHS_BLK, dwRegVal); // block de-counter for multiblock transmission
    }

    //OALMSG(1, (TEXT("C=0x%x, A= 0x%x\r\n"), MMC_CMD,Arg));
    // Program the argument into the argument registers
    OUTREG32(&pPort->pbRegisters->MMCHS_ARG, Arg);
    // Issue the command.
    OUTREG32(&pPort->pbRegisters->MMCHS_CMD, MMC_CMD);

    return SD_API_STATUS_SUCCESS;
}

///////////////////////////////////////////////////////////////////////////////////////////////

// Initialize the card
VOID SdhcHandleInsertion(SDHC_Port_t* pPort)
{
    DWORD dwClockRate = SD_DEFAULT_CARD_ID_CLOCK_RATE;
    DWORD dwTimeout;

    pPort->fCardPresent = TRUE;
    pPort->dwSDIOCard = 0;

    OALMSGX(1, (TEXT("SDHC: HandleInsertion\r\n")));

    SdhcSoftwareReset(pPort, SOFT_RESET_ALL);

    // Check for debounce stable
    dwTimeout = 5000;
    while (((INREG32(&pPort->pbRegisters->MMCHS_PSTATE) & 0x00020000)!= 0x00020000) && (dwTimeout>0))
    {
        dwTimeout--;
    }

    SetSDVSVoltage(pPort);

    SdhcSetClockRate(pPort, &dwClockRate);
}

//#ifdef DEBUG

//-----------------------------------------------------------------------------
//
//  DumpRegisters
//
//  Reads from SD Standard Host registers and writes them to the debugger.
//
VOID DumpRegisters(SDHC_Port_t* pPort)
{
    OALMSGX(OAL_INFO, (TEXT("+DumpStdHCRegs-------------------------\r\n")));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_CMD       0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_CMD)    ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_ARG       0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_ARG)  ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_CON       0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_CON)   ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_PWCNT     0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_PWCNT)   ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_STAT      0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_STAT)  ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_PSTATE    0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_PSTATE)  ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_IE        0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_IE)  ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_ISE       0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_ISE)  ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_BLK       0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_BLK)  ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_REV       0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_REV)    ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_RSP10     0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_RSP10)  ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_RSP32     0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_RSP32)  ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_RSP54     0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_RSP54)  ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_RSP76     0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_RSP76)  ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_HCTL      0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_HCTL)  ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_SYSCTL    0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_SYSCTL)  ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_SYSCONFIG 0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_SYSCONFIG) ));
    OALMSGX(OAL_INFO, (TEXT("MMCHS_CAPA      0x%08X \r\n"), INREG32(&pPort->pbRegisters->MMCHS_CAPA) ));
    OALMSGX(OAL_INFO, (TEXT("-DumpStdHCRegs-------------------------\r\n")));
}

//#endif

///////////////////////////////////////////////////////////////////////////////
//  SDHCControllerIstThread - implementation of SDIO/controller IST thread
//                                for driver
//  Input:
//  Output:
//  Return: Thread exit status
//  Notes:
///////////////////////////////////////////////////////////////////////////////
SD_API_STATUS SdhcControllerIstThread(SDHC_Port_t* pPort, PSD_BUS_REQUEST pRequest)
{
    DWORD dwStat;
    SD_API_STATUS Status = SD_API_STATUS_PENDING;
    
    // check for interrupt pending
    dwStat = INREG32(&pPort->pbRegisters->MMCHS_STAT);
    dwStat &= INREG32(&pPort->pbRegisters->MMCHS_IE);
    if ( dwStat & (MMCHS_STAT_CC|MMCHS_STAT_CERR|MMCHS_STAT_CCRC|MMCHS_STAT_CTO|MMCHS_STAT_DTO|MMCHS_STAT_DCRC) )
    {
        Status = CommandCompleteHandler(pPort, pRequest);
    }

    return Status;
}

///////////////////////////////////////////////////////////////////////////////
//  SDHCInitialize - Initialize the the controller
//  Input:
//  Output:
//  Return: SD_API_STATUS
//  Notes:
//
///////////////////////////////////////////////////////////////////////////////
SD_API_STATUS SdhcInitialize(SDHC_Port_t* pPort)
{
    SD_API_STATUS status = SD_API_STATUS_INSUFFICIENT_RESOURCES; // intermediate status

    OALMSGX(OAL_INFO, (TEXT("SDHC init\r\n")));

    SdhcControllerInit(pPort);

    status = SD_API_STATUS_SUCCESS;

    return status;
}


///////////////////////////////////////////////////////////////////////////////
//  SdhcBusRequestHandler - bus request handler
//  Input:  pRequest - the request
//
//  Output:
//  Return: SD_API_STATUS
//  Notes:  The request passed in is marked as uncancelable, this function
//          has the option of making the outstanding request cancelable
//          returns status pending
///////////////////////////////////////////////////////////////////////////////
SD_API_STATUS SdhcBusRequestHandler(SDHC_Port_t* pPort, PSD_BUS_REQUEST pRequest)
{
    SD_API_STATUS   status;

    status = SendCommand(pPort, pRequest);

    if (!SD_API_SUCCESS(status))
    {
        goto cleanUp;
    }

    // we will handle the command response interrupt on another thread
    status = SD_API_STATUS_PENDING;

cleanUp:

    return status;
}

//-----------------------------------------------------------------------------
//  CommandCompleteHandler
//  Input:
//  Output:
//  Notes:
static SD_API_STATUS CommandCompleteHandler(SDHC_Port_t* pPort, PSD_BUS_REQUEST pRequest)
{
    DWORD               dwCurrentTickCount;
    DWORD               dwTimeout;
    DWORD               dwCountStart;
    BOOL                fTimeoutOverflow = FALSE;
    SD_API_STATUS       status = SD_API_STATUS_PENDING;
    DWORD MMC_STAT;
    DWORD MmcPstateRegValue;
    DWORD MmcStatBits;

    MMC_STAT = INREG32(&pPort->pbRegisters->MMCHS_STAT);
    MmcPstateRegValue = INREG32(&pPort->pbRegisters->MMCHS_PSTATE);

    /* Check if the card is still busy processing data */
    if ( MmcPstateRegValue & MMCHS_PSTATE_DATI )
    {
        if ( pRequest->CommandResponse.ResponseType == ResponseR1b )
        {
            OALMSGX(1, (TEXT("SDHC: Card busy after command\r\n")));
            // calculate timeout conditions
            dwCountStart = OALGetTickCount();
            dwTimeout = dwCountStart + DEFAULT_TIMEOUT_VALUE;
            if ( dwTimeout < dwCountStart )
                fTimeoutOverflow = TRUE;

            MMC_STAT = INREG32(&pPort->pbRegisters->MMCHS_STAT);
            MmcPstateRegValue = INREG32(&pPort->pbRegisters->MMCHS_PSTATE);

            while ( (MmcPstateRegValue & MMCHS_PSTATE_DATI) && !( MMC_STAT & ( MMCHS_STAT_CCRC | MMCHS_STAT_CTO | MMCHS_STAT_DCRC | MMCHS_STAT_DTO )) )
            {
                OALStall(2 * 1000);

                MMC_STAT = INREG32(&pPort->pbRegisters->MMCHS_STAT);
                MmcPstateRegValue = INREG32(&pPort->pbRegisters->MMCHS_PSTATE);

                // check for a timeout
                dwCurrentTickCount = OALGetTickCount();
                if ( fTimeoutOverflow ? ( dwTimeout < dwCurrentTickCount && dwCurrentTickCount < dwCountStart )
                    : ( dwTimeout < dwCurrentTickCount ) )
                {
                    OALMSG(1, (TEXT("SDHC: Card BUSY timeout!\r\n")));
                    status = SD_API_STATUS_RESPONSE_TIMEOUT;
                    goto TRANSFER_DONE;
                }
            }
        }
    }

    MmcStatBits = 0;

    if ( MMC_STAT & MMCHS_STAT_CCRC ) // command CRC error
    {
        OALMSG(1, (TEXT("SDHC: Cmd CRC Err\r\n")));
        status = SD_API_STATUS_CRC_ERROR;
        MmcStatBits |= MMCHS_STAT_CCRC;
    }
    if ( MMC_STAT & MMCHS_STAT_DTO ) // data timeout
    {
        OALMSG(1, (TEXT("SDHC: Data timeout\r\n")));
        status = SD_API_STATUS_RESPONSE_TIMEOUT;
        MmcStatBits |= MMCHS_STAT_DTO;
    }
    if ( MMC_STAT & MMCHS_STAT_DCRC ) // data CRC error
    {
        OALMSG(1, (TEXT("SDHC: Data CRC\r\n")));
        status = SD_API_STATUS_RESPONSE_TIMEOUT;
        MmcStatBits |= MMCHS_STAT_DCRC;
    }
    if ( MMC_STAT & MMCHS_STAT_CTO ) // command response timeout
    {
        OALMSG(1, (TEXT("SDHC: Cmd timeout\r\n")));
        status = SD_API_STATUS_RESPONSE_TIMEOUT;
        MmcStatBits |= MMCHS_STAT_CTO;
    }
    if ( MmcStatBits )
    {
        OALMSG(1, (TEXT("SDHC: SendCommand: ERROR : STAT=0x%08x\r\n"), MMC_STAT));
        // clear the status error bits
        OUTREG32(&pPort->pbRegisters->MMCHS_STAT,MmcStatBits);
        goto TRANSFER_DONE;
    }

    // get the response information
    if (pRequest->CommandResponse.ResponseType == NoResponse)
    {
        status = SD_API_STATUS_SUCCESS;
        goto TRANSFER_DONE;
    }
    else
    {
        status =  GetCommandResponse(pPort, pRequest);
        if (!SD_API_SUCCESS(status))
        {
            goto TRANSFER_DONE;
        }
    }

    if (SD_COMMAND != pRequest->TransferClass) // data transfer
    {
        DWORD cbTransfer = TRANSFER_SIZE(pRequest);
        BOOL     fRet;

        switch (pRequest->TransferClass)
        {
        case SD_READ:
            fRet = SDIPollingReceive(pPort, pRequest->pBlockBuffer, cbTransfer);
            if (!fRet)
            {
                status = SD_API_STATUS_DATA_ERROR;
                goto TRANSFER_DONE;
            }
            break;

        case SD_WRITE:
            fRet = SDIPollingTransmit(pPort, pRequest->pBlockBuffer, cbTransfer);

            if( !fRet )
            {
                OALMSG (1,(TEXT("Transmit failed to send %d bytes\r\n"),cbTransfer));
                status = SD_API_STATUS_DATA_ERROR;
                goto TRANSFER_DONE;
            }
            else
            {
                OALMSGX(1,(TEXT("Transmit succesfully sent %d bytes\r\n"),cbTransfer));
            }

            break;
            default:
                break;
        }

        if (!pPort->fCardPresent)
            status = SD_API_STATUS_DEVICE_REMOVED;
        else
            status = SD_API_STATUS_SUCCESS;
    }

TRANSFER_DONE:

    if ( status == SD_API_STATUS_SUCCESS )
    {
        if ( pPort->fAppCmdMode )
        {
            pPort->fAppCmdMode = FALSE;
        }
        else if ( pRequest && pRequest->CommandCode == 55 )
        {
            pPort->fAppCmdMode = TRUE;
        }
    } 

    // Clear the MMC_STAT register
    MMC_STAT = INREG32(&pPort->pbRegisters->MMCHS_STAT);
    OUTREG32(&pPort->pbRegisters->MMCHS_STAT,MMC_STAT);

    return status;
}

//-----------------------------------------------------------------------------
//  Function:     GetCommandResponse()
//  Description:  Retrieves the response info for the last SDI command
//              issues.
//  Notes:
//  Returns:      SD_API_STATUS status code.
static SD_API_STATUS GetCommandResponse(SDHC_Port_t* pPort, PSD_BUS_REQUEST pRequest)
{
    DWORD  dwRegVal;
    PUCHAR  respBuff;       // response buffer
    DWORD dwRSP;

    dwRegVal = INREG32(&pPort->pbRegisters->MMCHS_STAT);


    if ( dwRegVal & (MMCHS_STAT_CC | MMCHS_STAT_CERR | MMCHS_STAT_CCRC))
    {
        respBuff = pRequest->CommandResponse.ResponseBuffer;

        switch (pRequest->CommandResponse.ResponseType)
        {
            case NoResponse:
                break;

            case ResponseR1:
            case ResponseR1b:
                //--- SHORT RESPONSE (48 bits total)---
                // Format: { START_BIT(1) | TRANSMISSION_BIT(1) | COMMAND_INDEX(6) | CARD_STATUS(32) | CRC7(7) | END_BIT(1) }
                // NOTE: START_BIT and TRANSMISSION_BIT = 0, END_BIT = 1
                //
                // Dummy byte needed by calling function.
                *respBuff = (BYTE)(START_BIT | TRANSMISSION_BIT | pRequest->CommandCode);

                dwRSP = INREG32(&pPort->pbRegisters->MMCHS_RSP10);

                *(respBuff + 1) = (BYTE)(dwRSP & 0xFF);
                *(respBuff + 2) = (BYTE)(dwRSP >> 8);
                *(respBuff + 3) = (BYTE)(dwRSP >> 16);
                *(respBuff + 4) = (BYTE)(dwRSP >> 24);


                *(respBuff + 5) = (BYTE)(END_RESERVED | END_BIT);

                break;

            case ResponseR3:
            case ResponseR4:
            case ResponseR7:
                //--- SHORT RESPONSE (48 bits total)---
                // Format: { START_BIT(1) | TRANSMISSION_BIT(1) | RESERVED(6) | CARD_STATUS(32) | RESERVED(7) | END_BIT(1) }
                //
                *respBuff = (BYTE)(START_BIT | TRANSMISSION_BIT | START_RESERVED);

                dwRSP = INREG32(&pPort->pbRegisters->MMCHS_RSP10);

                *(respBuff + 1) = (BYTE)(dwRSP & 0xFF);
                *(respBuff + 2) = (BYTE)(dwRSP >> 8);
                *(respBuff + 3) = (BYTE)(dwRSP >> 16);
                *(respBuff + 4) = (BYTE)(dwRSP >> 24);

                *(respBuff + 5) = (BYTE)(END_RESERVED | END_BIT);

                break;

            case ResponseR5:
            case ResponseR6:
                //--- SHORT RESPONSE (48 bits total)---
                // Format: { START_BIT(1) | TRANSMISSION_BIT(1) | COMMAND_INDEX(6) | RCA(16) | CARD_STATUS(16) | CRC7(7) | END_BIT(1) }
                //
                *respBuff = (BYTE)(START_BIT | TRANSMISSION_BIT | pRequest->CommandCode);

                dwRSP = INREG32(&pPort->pbRegisters->MMCHS_RSP10);

                *(respBuff + 1) = (BYTE)(dwRSP & 0xFF);
                *(respBuff + 2) = (BYTE)(dwRSP >> 8);
                *(respBuff + 3) = (BYTE)(dwRSP >> 16);
                *(respBuff + 4) = (BYTE)(dwRSP >> 24);

                *(respBuff + 5) = (BYTE)(END_BIT);
                break;

            case ResponseR2:
                dwRSP = INREG32(&pPort->pbRegisters->MMCHS_RSP10);

                *(respBuff + 0) = (BYTE)(dwRSP & 0xFF);
                *(respBuff + 1) = (BYTE)(dwRSP >> 8);
                *(respBuff + 2) = (BYTE)(dwRSP >> 16);
                *(respBuff + 3) = (BYTE)(dwRSP >> 24);

                dwRSP = INREG32(&pPort->pbRegisters->MMCHS_RSP32);

                *(respBuff + 4) = (BYTE)(dwRSP & 0xFF);
                *(respBuff + 5) = (BYTE)(dwRSP >> 8);
                *(respBuff + 6) = (BYTE)(dwRSP >> 16);
                *(respBuff + 7) = (BYTE)(dwRSP >> 24);

                dwRSP = INREG32(&pPort->pbRegisters->MMCHS_RSP54);

                *(respBuff + 8) = (BYTE)(dwRSP & 0xFF);
                *(respBuff + 9) = (BYTE)(dwRSP >> 8);
                *(respBuff + 10) = (BYTE)(dwRSP >> 16);
                *(respBuff + 11) = (BYTE)(dwRSP >> 24);


                dwRSP = INREG32(&pPort->pbRegisters->MMCHS_RSP76);

                *(respBuff + 12) = (BYTE)(dwRSP & 0xFF);
                *(respBuff + 13) = (BYTE)(dwRSP >> 8);
                *(respBuff + 14) = (BYTE)(dwRSP >> 16);
                *(respBuff + 15) = (BYTE)(dwRSP >> 24);
                break;

            default:
                break;
        }
    }
    return SD_API_STATUS_SUCCESS;
}

static BOOL SDIPollingTransmit(SDHC_Port_t* pPort, PBYTE pBuff, DWORD dwLen)
{
    DWORD blockLengthW; // Almost Full level and block length
    DWORD dwCount1, dwCount2 = 0;
    DWORD *pbuf = (DWORD *) pBuff; // short* of buffer
    DWORD dwCountStart;
    DWORD dwCurrentTickCount;
    DWORD dwTimeout;
    BOOL fTimeoutOverflow = FALSE;

    // calculate timeout conditions
    dwCountStart = OALGetTickCount();
    dwTimeout = dwCountStart + DEFAULT_TIMEOUT_VALUE;
    if ( dwTimeout < dwCountStart )
        fTimeoutOverflow = TRUE;

    blockLengthW = dwLen / 512;
    // Start writing
    for (dwCount1 = 0; dwCount1 < blockLengthW; dwCount1++)
    {
        // poll on write ready here
        while((INREG32(&pPort->pbRegisters->MMCHS_STAT) & MMCHS_STAT_BWR) != MMCHS_STAT_BWR)
        {
            // check for a timeout
            dwCurrentTickCount = OALGetTickCount();
            if ( fTimeoutOverflow ? ( dwTimeout < dwCurrentTickCount && dwCurrentTickCount < dwCountStart )
                : ( dwTimeout < dwCurrentTickCount || dwCurrentTickCount < dwCountStart ) )
            {
                OALMSG(1, (TEXT("SDHC: SDIPollingTransmit: ERROR : Card not ready for write\r\n")));
                goto WRITE_ERROR;
            }
        }
        SETREG32(&pPort->pbRegisters->MMCHS_STAT,MMCHS_STAT_BWR); // clear Block Write Ready interrupt

        for (dwCount2 = 0; dwCount2 < (512 /sizeof(DWORD)); dwCount2++) // write data to DATA buffer
        {
            OUTREG32(&pPort->pbRegisters->MMCHS_DATA, *pbuf++);
            OALStall(0);
        }
    }

    // recalculate timeout conditions
    dwCountStart = OALGetTickCount();
    dwTimeout = dwCountStart + DEFAULT_TIMEOUT_VALUE;
    if ( dwTimeout < dwCountStart )
        fTimeoutOverflow = TRUE;
    else
        fTimeoutOverflow = FALSE;

    /* Wait for transfer completion */
    while (((INREG32(&pPort->pbRegisters->MMCHS_STAT)&MMCHS_STAT_TC) != MMCHS_STAT_TC))
    {
        // check for a timeout
        dwCurrentTickCount = OALGetTickCount();
        if ( fTimeoutOverflow ? ( dwTimeout < dwCurrentTickCount && dwCurrentTickCount < dwCountStart )
           : ( dwTimeout < dwCurrentTickCount || dwCurrentTickCount < dwCountStart ) )
        {
            OALMSG(1, (TEXT("SDHC: SDIPollingTransmit: ERROR : Transmission never completed\r\n")));
            goto WRITE_ERROR;
        }
    }

    // Check if there is no CRC error
    if (!(INREG32(&pPort->pbRegisters->MMCHS_STAT) & MMCHS_STAT_DCRC))
    {
        return TRUE;
    }
    else
    {
        OALMSG(1, (TEXT("SDHC: SDIPollingTransmit: CRC ERROR\r\n")));
        return FALSE;
    }

WRITE_ERROR:
    SETREG32(&pPort->pbRegisters->MMCHS_STAT,MMCHS_STAT_BWR); // clear Block Write Ready interrupt
    return FALSE;
}

//-----------------------------------------------------------------------------
//  Function:     SDIPollingReceive()
//  Description:
//  Notes:        This routine assumes that the caller has already locked
//                the current request and checked for errors.
//  Returns:      SD_API_STATUS status code.
static BOOL SDIPollingReceive(SDHC_Port_t* pPort, PBYTE pBuff, DWORD dwLen)
{
    DWORD fifoSizeW, blockLengthW; // Almost Full level and block length
    DWORD dwCount1, dwCount2;
    DWORD MMC_STAT;
    DWORD MmcPstateRegValue;
    DWORD __unaligned *pbuf2 = (DWORD *) pBuff;
    DWORD dwCurrentTickCount;
    DWORD dwTimeout;
    DWORD dwCountStart;
    BOOL fTimeoutOverflow = FALSE;

    OALMSGX(OAL_IO, (TEXT("SDIPollingReceive(0x%x, %d)\r\n"), pBuff, dwLen));
    //check the parameters

    OALMSGX(OAL_IO, (TEXT("SDIPollingReceive reading MMC_STAT\r\n")));
    MMC_STAT = INREG32(&pPort->pbRegisters->MMCHS_STAT);
    OALMSGX(OAL_IO, (TEXT("SDIPollingReceive reading MMCHS_PSTATE\r\n")));
    MmcPstateRegValue = INREG32(&pPort->pbRegisters->MMCHS_PSTATE);

    // calculate timeout conditions
    OALMSGX(OAL_IO, (TEXT("SDIPollingReceive OALGetTickCount\r\n")));
    dwCountStart = OALGetTickCount();
    dwTimeout = dwCountStart + DEFAULT_TIMEOUT_VALUE;
    if ( dwTimeout < dwCountStart )
        fTimeoutOverflow = TRUE;

    if (dwLen % SDMMC_DEFAULT_BLOCK_LEN || pPort->dwSDIOCard)
    {
        while ((INREG32(&pPort->pbRegisters->MMCHS_STAT) & MMCHS_STAT_BRR) != MMCHS_STAT_BRR)
        {
           // check for a timeout
           dwCurrentTickCount = OALGetTickCount();
           if ( fTimeoutOverflow ? ( dwTimeout < dwCurrentTickCount && dwCurrentTickCount < dwCountStart )
             : ( dwTimeout < dwCurrentTickCount || dwCurrentTickCount < dwCountStart ) )
           {
              goto READ_ERROR;
           }
        }
        SETREG32(&pPort->pbRegisters->MMCHS_STAT,MMCHS_STAT_BRR);
        fifoSizeW = dwLen / sizeof(DWORD);
        if (dwLen % sizeof(DWORD)) fifoSizeW++;
        for (dwCount2 = 0; dwCount2 < fifoSizeW; dwCount2++)
        {
            *pbuf2 = INREG32(&pPort->pbRegisters->MMCHS_DATA);
            pbuf2++;
        }
    } 
    else
    {
      fifoSizeW = INREG32(&pPort->pbRegisters->MMCHS_BLK) & 0xFFFF;
      blockLengthW = dwLen / fifoSizeW;
      for (dwCount1 = 0; dwCount1 < blockLengthW; dwCount1++)
      {
        OALMSGX(OAL_IO, (TEXT("SDIPollingReceive set MMCHS_STAT BBR\r\n")));
        // Wait for Block ready for read
        while ((INREG32(&pPort->pbRegisters->MMCHS_STAT) & MMCHS_STAT_BRR) != MMCHS_STAT_BRR)
        {
          // check for a timeout
          dwCurrentTickCount = OALGetTickCount();
          if ( fTimeoutOverflow ? ( dwTimeout < dwCurrentTickCount && dwCurrentTickCount < dwCountStart )
            : ( dwTimeout < dwCurrentTickCount || dwCurrentTickCount < dwCountStart ) )
          {
            OALMSGX(OAL_ERROR, (TEXT("SDIPollingReceive: TIMEOUT1\r\n")));
            goto READ_ERROR;
          }
        }
        SETREG32(&pPort->pbRegisters->MMCHS_STAT,MMCHS_STAT_BRR);

        // Get all data from DATA register and write in user buffer
        for (dwCount2 = 0; dwCount2 < (fifoSizeW/sizeof(DWORD)); dwCount2++)
        {
            *pbuf2 = INREG32(&pPort->pbRegisters->MMCHS_DATA) ;
            pbuf2++;
        }
      }
    }
    // recalculate timeout conditions
    dwCountStart = OALGetTickCount();
    dwTimeout = dwCountStart + DEFAULT_TIMEOUT_VALUE;
    if ( dwTimeout < dwCountStart )
        fTimeoutOverflow = TRUE;
    else
        fTimeoutOverflow = FALSE;

    while (((INREG32(&pPort->pbRegisters->MMCHS_STAT)&MMCHS_STAT_TC) != MMCHS_STAT_TC))
    {
        // check for a timeout
        dwCurrentTickCount = OALGetTickCount();
        if ( fTimeoutOverflow ? ( dwTimeout < dwCurrentTickCount && dwCurrentTickCount < dwCountStart )
           : ( dwTimeout < dwCurrentTickCount || dwCurrentTickCount < dwCountStart ) )
        {
            OALMSGX(OAL_ERROR, (TEXT("SDIPollingReceive: TIMEOUT3\r\n")));
            goto READ_ERROR;
        }
    }

    SETREG32(&pPort->pbRegisters->MMCHS_STAT,MMCHS_STAT_TC);
    // Check if there is no CRC error
    if (!(INREG32(&pPort->pbRegisters->MMCHS_STAT) & MMCHS_STAT_DCRC))
    {
        MMC_STAT = INREG32(&pPort->pbRegisters->MMCHS_STAT);
        OUTREG32(&pPort->pbRegisters->MMCHS_STAT,MMC_STAT);
    }
    else
    {
        MMC_STAT = INREG32(&pPort->pbRegisters->MMCHS_STAT);
        OUTREG32(&pPort->pbRegisters->MMCHS_STAT,MMC_STAT);
        goto READ_ERROR;
    }

    return TRUE;

READ_ERROR:

    OALMSGX(OAL_IO, (TEXT("SDIPollingReceive error\r\n")));
    return FALSE;
}
