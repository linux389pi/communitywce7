/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/
//===================================================================
//
//   Module Name:   BOOTLOADER
//
//   File Name:      fileio.h
//
//   Description:   FAT12/16/32 file system i/o for block devices
//
//===================================================================

#ifndef _FILEIO_H
#define _FILEIO_H

//----------------------------------------------------------------------------
//
// File IO Definitions
//
//----------------------------------------------------------------------------

// sector size is currently fixed to 512 bytes
#define SECTOR_SIZE                                         512
#define CURRENT_SECTOR_EOF_VALUE                            0xffff

// FAT Values

// Bootstrap jump instruction
#define FAT_BOOTSTRAP_SHORT_JUMP_INSTRUCTION                0xEB
#define FAT_BOOTSTRAP_NEAR_JUMP_INSTRUCTION                 0xEA
// Media descriptor types
#define FAT_MEDIA_TYPE_FLOPPY                               0xF0
#define FAT_MEDIA_TYPE_HARD_DISK                            0xF8
// Partition state
#define FAT_PARTITION_ACTIVE                                0x80
#define FAT_PARTITION_INCATIVE                              0x00
// Partition type
#define FAT_PARTITION_TYPE_UNKNOWN                          0x00
#define FAT_PARTITION_TYPE_FAT12                            0x01
#define FAT_PARTITION_TYPE_FAT16_SMALL                      0x04 // max partition size < 32 Mb
#define FAT_PARTITION_TYPE_EXT_MSDOS                        0x05 
#define FAT_PARTITION_TYPE_FAT16_BIG                        0x06 // max partition size > 32 Mb
#define FAT_PARTITION_TYPE_FAT32                            0x0B // partition up to 2048 Go
#define FAT_PARTITION_TYPE_FAT32_LBA_EXT                    0x0C // FAT32 with LBA extensions
#define FAT_PARTITION_TYPE_FAT16_LBA_EXT                    0x0E // FAT16 (>32 Mb) with LBA extensions
#define FAT_PARTITION_TYPE_MSDOS_LBA_EXT                    0x0F // Extended MSDOS with LBA extensions
// File entry attributes
#define FAT_FILE_ATTRIBUTE_READ_ONLY                        0x01
#define FAT_FILE_ATTRIBUTE_HIDDEN                           0x02
#define FAT_FILE_ATTRIBUTE_SYSTEM_FILE                      0x04
#define FAT_FILE_ATTRIBUTE_DISK_LABEL                       0x08 // special entry. contains disk label instead of a file
#define FAT_FILE_ATTRIBUTE_SUBDIRECTORY                     0x10
#define FAT_FILE_ATTRIBUTE_ARCHIVE                          0x20
// File name status
// These are special case first bytes of the filename 
// Usually, the first byte contains a character
#define FAT_FILE_STATUS_NEVER_USED                          0x00
#define FAT_FILE_STATUS_USED_BUT_DELETED                    0xE5
#define FAT_FILE_STATUS_E5_FIRST_CHAR                       0x05 // 0xE5 is the first character of the name
#define FAT_FILE_STATUS_DIRECTORY_RESERVED                  0x2E
// File entry limits
#define FAT_FILE_FILENAME_SIZE                              0x08
#define FAT_FILE_EXTENSION_SIZE                             0x03

// FAT Boot sector Indexes

#define FAT_INDEX_JUMP_BOOTSTRAP                            0x00
#define FAT_INDEX_OEM_NAME                                  0x03
#define FAT_INDEX_BYTES_PER_SECTOR                          0x0B
#define FAT_INDEX_SECTORS_PER_CLUSTER                       0x0D
#define FAT_INDEX_RESERVED_SECTOR_NB                        0x0E
#define FAT_INDEX_FATS_NB                                   0x10
#define FAT_INDEX_ROOT_DIR_ENTRY_NB                         0x11
#define FAT_INDEX_TOTAL_SECTORS_IN_PARTITION                0x13
#define FAT_INDEX_MEDIA_DESCRIPTOR                          0x15
#define FAT_INDEX_SECTORS_PER_FAT                           0x16
#define FAT_INDEX_SECTORS_PER_TRACK                         0x18
#define FAT_INDEX_HEADS_NB                                  0x1A

// FAT Defaults

#define FAT_FORMAT_OEM_NAME                                 "LNIFAT"
#define FAT_FORMAT_FAT32_RESERVED_SECTORS                   32
#define FAT_FORMAT_FATS_NB                                  2
#define FAT_FORMAT_TYPE                                     "FAT32   "
#define FAT_FORMAT_VOLUME_LABEL                             "BOOT"
#define FAT_FORMAT_SECTOR_PER_CLUSTER                       8
#define FAT_FORMAT_TOTAL_SECTOR_NB                          0x766623 // for a 4Gb MMC

// FAT32 Bootstrap indexes (first 64 bytes are bpb)
#define FAT_BOOTSTRAP_INDEX_EXTENDED_SIGNATURE              0
#define FAT_BOOTSTRAP_INDEX_VOLUME_LABEL                    7
#define FAT_BOOTSTRAP_INDEX_FS_TYPE                         18
#define FAT_BOOTSTRAP_INDEX_MBR_SIGNATURE                   446

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//
// File IO Data Structures
//
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

typedef struct _FILEHANDLE {
    char name[9];           // root of 8.3 file name, with terminating null character.
                            // This field is typically initialized with FileNameToDirEntry()
    char extension[4];      // extension of 8.3 file name, with terminating null character.
                            // This field is typically initialized with FileNameToDirEntry()
    UINT32 file_size;       // Size of the file, in bytes

    // The following variables are used internally by the fileio routines
    UINT32 current_sector;             // sector count
    UINT32 current_cluster;
    UINT16 current_sector_in_cluster;  // next sector in cluster to read
    int bytes_in_buffer;
    UINT8  buffer[SECTOR_SIZE];
} FILEHANDLE, * PFILEHANDLE;

// S_FILEIO_OPERATIONS
// This data structure contains a set of pointers to functions that provide
// access to a block device.  This structure needs to be initialized with the
// correct functions before calling FileIoInit.  The fileio routines use
// these function pointers to access the block device.
// Note that the implementation of these functions is custom for each device.
typedef struct fileio_operations_t {
    // Pointer to an initialization function for the block device.  Function
    // takes a pointer to a data structure that describes the device.  
    // This function should initialize the device, making it ready for read
    // access.
    int (*init)(DWORD Slot);

    // Pointer to a diagnostic function that can return information about the
    // block device.  This function takes a pointer to the device information
    // data structure, and a pointer to a user buffer with size equal to the 
    // sector size.  Note that the use of the user buffer is not specified.  
    // NOTE - This diagnostic function may not be called by all versions of 
    // the fileio library.
    int (*identify)(DWORD Slot, void *Sector);

    // Pointer to a function that reads the specified logical sector into
    // the provided sector buffer.  Function also takes a pointer to the 
    // device specific information data structure.
    int (*read_sector)(DWORD Slot, UINT32 LogicalSector, void *pSector);

    // Pointer to a function that reads the specified logical sector into
    // the provided sector buffer.  Function also takes a pointer to the 
    // device specific information data structure.
    int (*read_multi_sectors)(DWORD Slot, UINT32 LogicalSector, void *pBuffer, UINT16 numSectors);

   // Pointer to a function that writes the specified logical sector with data in
   // the provided sector buffer.  Function also takes a pointer to the
   // device specific information data structure.
   int (*write_sector)(DWORD Slot, UINT32 LogicalSector, void *pSector);

   // Pointer to a function that writes the specified buffer into
    // the specified logical sector.  Function also takes a pointer to the 
    // device specific information data structure.
    int (*write_multi_sectors)(DWORD Slot, UINT32 LogicalSector, void *pBuffer, UINT16 numSectors);
 
   // Slot is the device information data handle.
    DWORD Slot;

    // Checks if FileIo is initialized
    BOOL isInit;
} S_FILEIO_OPERATIONS, *S_FILEIO_OPERATIONS_PTR;

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//
// File IO Function Return Codes
//
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

#define FILEIO_STATUS_OK            0
#define FILEIO_STATUS_INIT_FAILED   1
#define FILEIO_STATUS_OPEN_FAILED   2
#define FILEIO_STATUS_READ_FAILED   3
#define FILEIO_STATUS_READ_EOF      4
#define FILEIO_STATUS_WRITE_FAILED   5

//----------------------------------------------------------------------------
//----------------------------------------------------------------------------
//
// File IO Public Functions
//
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//
// NAME: FileIoInit()
//
// DESCRIPTION: Initializes fileio subsystem on any FAT12/16/32 formatted block
// device.
//
// PARAMETERS:
//   fileio_ops   Pointer to a preinitialized S_FILEIO_OPERATIONS structure
//
// RETURNS:    FILEIO_STATUS_OK on success, error code on failure
//
//----------------------------------------------------------------------------

int FileIoInit(S_FILEIO_OPERATIONS_PTR fileio_ops);

//----------------------------------------------------------------------------
//
// NAME: FileNameToDirEntry()
//
// DESCRIPTION: create 8+3 FAT file system directory entry strings from 8+3
// file name.  This function must be used to create the proper filename root
// and extension strings for use by FileIoOpen().  This function is typically
// used to initialize the 'name' and 'extension' fields of a FILEHANDLE structure.
//
// PARAMETERS:
//   pFileName   Pointer to WCHAR string containing original file name
//   pName      Pointer to buffer that will contain resulting filename root.
//            This pointer is typically the 'name' field of a FILEHANDLE
//            structure.
//   pExtension   Pointer to buffer that will contain resulting filename extension.
//            This pointer is typically the 'extension' field of a FILEHANDLE
//            structure.
//
// RETURNS:    Nothing
//
//----------------------------------------------------------------------------

void FileNameToDirEntry(LPCWSTR pFileName, PCHAR pName, PCHAR pExtension);

//----------------------------------------------------------------------------
//
// NAME: FileIoOpen()
//
// DESCRIPTION: Opens the specified file for sequential read access.
//
// PARAMETERS:  
//  pFileio_ops Pointer to S_FILEIO_OPERATIONS structure
//  pFile       Pointer to FILEHANDLE structure with 'name' and 'extension' 
//              fields already initialized using FileNameToDirEntry function.
//
// RETURNS:     FILEIO_STATUS_OK on success, error code on failure
//
//----------------------------------------------------------------------------

int FileIoOpen(S_FILEIO_OPERATIONS_PTR pFileio_ops, PFILEHANDLE pFile);

//----------------------------------------------------------------------------
//
// NAME: FileIoRead()
//
// DESCRIPTION: Reads specified number of bytes from file into user buffer. 
//              File read pointer is saved in the FILEHANDLE structure, subsequent
//              calls to this function will continue reading from the previous
//              location.
//
// PARAMETERS:  
//  pFileio_ops Pointer to S_FILEIO_OPERATIONS structure
//  pFile       Pointer to FILEHANDLE structure with 'name' and 'extension' 
//              fields already initialized using FileNameToDirEntry function.
//  pDest       Pointer to destination for data
//  Count       Number of bytes to read
//
// RETURNS:     FILEIO_STATUS_OK on success, error code on failure
//
//----------------------------------------------------------------------------

int FileIoRead(S_FILEIO_OPERATIONS_PTR pFileio_ops, PFILEHANDLE pFile, void *pDest, DWORD Count);

//----------------------------------------------------------------------------
//
// NAME: FileIoWrite()
//
// DESCRIPTION: Writes specified number of bytes from user buffer to specified file.
//            File write pointer is saved in the FILEHANDLE structure, subsequent
//            calls to this function will continue writing from the previous
//            location.
//
// PARAMETERS:
//   pFileio_ops   Pointer to S_FILEIO_OPERATIONS structure
//   pFile      Pointer to FILEHANDLE structure with 'name' and 'extension'
//            fields already initialized using FileNameToDirEntry function.
//  pSrc       Pointer to source of data
//  Count       Number of bytes to write
//
// RETURNS:    FILEIO_STATUS_OK on success, error code on failure
//
//----------------------------------------------------------------------------

int FileIoWrite(S_FILEIO_OPERATIONS_PTR pFileio_ops, PFILEHANDLE pFile, void *pSrc, DWORD Count);

int FormatDiskFAT32(S_FILEIO_OPERATIONS_PTR pfileio_ops);

#endif
