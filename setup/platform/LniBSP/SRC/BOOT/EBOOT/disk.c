//
//  File:  disk.c
//
//  This file implements disk features
//  

#include "eboot.h"
#include <StoreMgr.h>
#include <Diskio.h>
#include <intsafe.h>
#include "fatutil.h"

VOID BL_DisplayDisks()
{
    STOREINFO si = {0};
    HANDLE hFind = INVALID_HANDLE_VALUE;
    DWORD dwCurrentSel = 0;
                HANDLE hStore;
               
    si.cbSize = sizeof(STOREINFO);
    // enumerate first store
	OALLog(L"find first store");
    hFind = FindFirstStore(&si);
   
    if(INVALID_HANDLE_VALUE != hFind)
    {
        do
        {
            OALLog(L"in");
			RETAILMSG( 1, (TEXT("Device Name %s\n"), si.szDeviceName ));
            RETAILMSG( 1, (TEXT("Name %s\n"), si.szStoreName ));
            RETAILMSG( 1, (TEXT("Class %s\n"), si.dwDeviceClass == STORAGE_DEVICE_CLASS_BLOCK ?
                            TEXT("STORAGE_DEVICE_CLASS_BLOCK") :
                            TEXT("STORAGE_DEVICE_CLASS_MULTIMEDIA") ));
            RETAILMSG( 1, (TEXT("Type ")));
            switch( si.dwDeviceType )
            {
                case STORAGE_DEVICE_TYPE_PCIIDE:
                                RETAILMSG( 1, (TEXT("STORAGE_DEVICE_TYPE_PCIIDE\n")));
                                break;
                case STORAGE_DEVICE_TYPE_FLASH:
                                RETAILMSG( 1, (TEXT("STORAGE_DEVICE_TYPE_FLASH\n")));
                                break;
                case STORAGE_DEVICE_TYPE_ATA:
                                RETAILMSG( 1, (TEXT("STORAGE_DEVICE_TYPE_ATA\n")));
                                break;
                case STORAGE_DEVICE_TYPE_ATAPI:
                                RETAILMSG( 1, (TEXT("STORAGE_DEVICE_TYPE_ATAPI\n")));
                                break;
                //case STORAGE_DEVICE_TYPE_PCCARD:
                //                RETAILMSG( 1, (TEXT("STORAGE_DEVICE_TYPE_PCCARD\n")));
                //                break;
                //case STORAGE_DEVICE_TYPE_CFCARD:
                //                RETAILMSG( 1, (TEXT("STORAGE_DEVICE_TYPE_CFCARD\n")));
                //                break;
                case STORAGE_DEVICE_TYPE_SRAM:
                                RETAILMSG( 1, (TEXT("STORAGE_DEVICE_TYPE_SRAM\n")));
                                break;
                case STORAGE_DEVICE_TYPE_DVD:
                                RETAILMSG( 1, (TEXT("STORAGE_DEVICE_TYPE_DVD\n")));
                                break;
                case STORAGE_DEVICE_TYPE_CDROM:
                                RETAILMSG( 1, (TEXT("STORAGE_DEVICE_TYPE_CDROM\n")));
                                break;
                case STORAGE_DEVICE_TYPE_USB:
                                RETAILMSG( 1, (TEXT("STORAGE_DEVICE_TYPE_USB\n")));
                                break;
                case STORAGE_DEVICE_TYPE_1394:
                                RETAILMSG( 1, (TEXT("STORAGE_DEVICE_TYPE_1394\n")));
                                break;
                case STORAGE_DEVICE_TYPE_DOC:
                                RETAILMSG( 1, (TEXT("STORAGE_DEVICE_TYPE_DOC\n")));
                                break;
                case STORAGE_DEVICE_TYPE_UNKNOWN:
                                RETAILMSG( 1, (TEXT("STORAGE_DEVICE_TYPE_UNKNOWN\n")));
                                break;
                case STORAGE_DEVICE_TYPE_REMOVABLE_DRIVE:
                                RETAILMSG( 1, (TEXT("STORAGE_DEVICE_TYPE_REMOVABLE_DRIVE\n")));
                                break;
                case STORAGE_DEVICE_TYPE_REMOVABLE_MEDIA:
                                RETAILMSG( 1, (TEXT("STORAGE_DEVICE_TYPE_REMOVABLE_MEDIA\n")));
                                break;
                default:
                                RETAILMSG( 1, (TEXT("Unkown device type 0x%X\n"), si.dwDeviceType));
                                break;

            }
            RETAILMSG( 1, (TEXT("Flags")));
            if(si.dwDeviceFlags & STORAGE_DEVICE_FLAG_READWRITE )
                            RETAILMSG( 1, (TEXT(" STORAGE_DEVICE_FLAG_READWRITE")));
            if(si.dwDeviceFlags & STORAGE_DEVICE_FLAG_READONLY )
                            RETAILMSG( 1, (TEXT(" STORAGE_DEVICE_FLAG_READONLY")));
            if(si.dwDeviceFlags & STORAGE_DEVICE_FLAG_TRANSACTED )
                            RETAILMSG( 1, (TEXT(" STORAGE_DEVICE_FLAG_TRANSACTED")));
            if(si.dwDeviceFlags & STORAGE_DEVICE_FLAG_MEDIASENSE )
                            RETAILMSG( 1, (TEXT(" STORAGE_DEVICE_FLAG_MEDIASENSE")));
            if(si.dwDeviceFlags == 0 )
                            RETAILMSG( 1, (TEXT(" None")));
            RETAILMSG( 1, (TEXT("\n")));

            RETAILMSG( 1, (TEXT("Bytes per Sector %d\n"), si.dwBytesPerSector));
            RETAILMSG( 1, (TEXT("Attributes")));
            if(si.dwAttributes & STORE_ATTRIBUTE_READONLY )
                            RETAILMSG( 1, (TEXT(" STORE_ATTRIBUTE_READONLY")));
            if(si.dwAttributes & STORE_ATTRIBUTE_REMOVABLE )
                            RETAILMSG( 1, (TEXT(" STORE_ATTRIBUTE_REMOVABLE")));
            if(si.dwAttributes & STORE_ATTRIBUTE_UNFORMATTED )
                            RETAILMSG( 1, (TEXT(" STORE_ATTRIBUTE_UNFORMATTED")));
            if(si.dwAttributes & STORE_ATTRIBUTE_AUTOFORMAT )
                            RETAILMSG( 1, (TEXT(" STORE_ATTRIBUTE_AUTOFORMAT")));
            if(si.dwAttributes & STORE_ATTRIBUTE_AUTOPART )
                            RETAILMSG( 1, (TEXT(" STORE_ATTRIBUTE_AUTOPART")));
            if(si.dwAttributes & STORE_ATTRIBUTE_AUTOMOUNT )
                            RETAILMSG( 1, (TEXT(" STORE_ATTRIBUTE_AUTOMOUNT")));
            if(si.dwAttributes == 0 )
                            RETAILMSG( 1, (TEXT(" None")));
            RETAILMSG( 1, (TEXT("\n")));
            
            RETAILMSG( 1, (TEXT("Partition Count %d\n"), si.dwPartitionCount));
            RETAILMSG( 1, (TEXT("Mount Count %d\n"), si.dwMountCount));

            
            hStore = OpenStore( si.szDeviceName );             

            if( hStore )
            {
                            BL_DisplayPartions( hStore );
                            CloseHandle(hStore);
            }
            else
                            RETAILMSG(1, (TEXT("OpenSelectedStore failed\n")));
        }
        while(FindNextStore(hFind, &si));
        FindClose(hFind);
    }
}

VOID BL_DisplayPartions( HANDLE hStore )
{
    HANDLE hFind = INVALID_HANDLE_VALUE;
    HANDLE hPartition = INVALID_HANDLE_VALUE;
    PARTINFO partInfo = {0};

    partInfo.cbSize = sizeof(PARTINFO);
    hFind = FindFirstPartition(hStore, &partInfo);

    if(INVALID_HANDLE_VALUE != hFind)
    {             
        do
        {
            if(PARTITION_ATTRIBUTE_MOUNTED & partInfo.dwAttributes)
            {
                RETAILMSG( 1, (TEXT("\t\tPartition Name %s\n"), partInfo.szPartitionName ));
                RETAILMSG( 1, (TEXT("\t\tSize %d\n"), partInfo.cbSize ));
                RETAILMSG( 1, (TEXT("\t\tFile System %s\n"), partInfo.szFileSys ));
                RETAILMSG( 1, (TEXT("\t\tVolume Name %s\n"), partInfo.szVolumeName ));
                RETAILMSG( 1, (TEXT("\t\tNumber of Sectors %d\n"), partInfo.snNumSectors ));
                RETAILMSG( 1, (TEXT("\t\tAttributes")));
                if(partInfo.dwAttributes & PARTITION_ATTRIBUTE_EXPENDABLE )
                                RETAILMSG( 1, (TEXT(" PARTITION_ATTRIBUTE_EXPENDABLE")));
                if(partInfo.dwAttributes & PARTITION_ATTRIBUTE_READONLY )
                                RETAILMSG( 1, (TEXT(" PARTITION_ATTRIBUTE_READONLY")));
                if(partInfo.dwAttributes & PARTITION_ATTRIBUTE_BOOT )
                                RETAILMSG( 1, (TEXT(" PARTITION_ATTRIBUTE_BOOT")));
                if(partInfo.dwAttributes & PARTITION_ATTRIBUTE_AUTOFORMAT )
                                RETAILMSG( 1, (TEXT(" PARTITION_ATTRIBUTE_AUTOFORMAT")));
                if(partInfo.dwAttributes & PARTITION_ATTRIBUTE_MOUNTED )
                                RETAILMSG( 1, (TEXT(" PARTITION_ATTRIBUTE_MOUNTED")));
                if(partInfo.dwAttributes == 0 )
                                RETAILMSG( 1, (TEXT(" None")));
                RETAILMSG( 1, (TEXT("\n")));
                {
                    TCHAR *PartTypeString = NULL;
                    DWORD Result;
                    HKEY hKey;
                    DWORD NumBytes = 0;
                    DWORD Type;
                    TCHAR PartType[3];

                    // Convert the partion type value to a string
                    wsprintf( PartType, TEXT("%02X"), partInfo.bPartType );

                    // Open the Registry Key
                    Result = RegOpenKeyEx(HKEY_LOCAL_MACHINE, (LPCWSTR)TEXT("System\\StorageManager\\PartitionTable"), 0, 0, &hKey);

                    if( ERROR_SUCCESS == Result )
                    {
                                    // This is a fake read, all it does is fill in NumBytes with the number of
                                    // bytes in the string value plus the null character.
                                    Result = RegQueryValueEx( hKey, PartType, NULL, &Type, NULL, &NumBytes );
                                    if( NumBytes > 0 )
                                    {
                                                    // Now we know how big the string is allocate and read it
                                                    PartTypeString = (TCHAR *)malloc( NumBytes );
                                                    if( PartTypeString != NULL )
                                                                    Result = RegQueryValueEx( hKey, PartType, NULL, &Type, (LPBYTE)PartTypeString, &NumBytes );
                                                    RETAILMSG( 1, (TEXT("\t\tPartion Type %s\n"), PartTypeString ));
                                                    free( PartTypeString );
                                    }
                                    else
                                                    RETAILMSG( 1, (TEXT("\t\tPartion Type not found\n")));
                                    RegCloseKey( hKey );
                    }
                }
            }
        }
        while(FindNextPartition(hFind, &partInfo));
        FindClose(hFind);
    }
    CloseHandle(hStore);
}