#ifndef __SDCARD_DEFS_H
    #define __SDCARD_DEFS_H

// SDHC interface methods
unsigned int INTF_MMCReadResponse(struct MMC_command * pMMC_command);
unsigned int INTF_MMCSendCommand(SDHC_Port_t* pPort, struct MMC_command * pMMC_command);
// Utils methods
unsigned int UTIL_csd_get_sectorsize(struct MMC_command * pMMCcmd);
unsigned int UTIL_csd_get_devicesize(struct MMC_command * pMMCcmd);
unsigned int UTIL_csd_get_tran_speed(struct MMC_command * pMMCcmd);
//--------------------------------------------------------------------
// MMC / SD Operations
//--------------------------------------------------------------------
#define MMC_SUCCESS     0
#define MMC_FAILURE     1

// MMC specific
DWORD MMCUpdateExtCsd(SDCARD_t* pCard,BYTE index, BYTE value);
DWORD MMCGetExtCsd(SDCARD_t* pCard);
DWORD MMCSendOpCond(SDCARD_t* pCard);
DWORD MMCSetRelativeAddress(SDCARD_t* pCard);
// SD specific
DWORD SDGetConfigRegister(SDCARD_t* pCard, BYTE* scr);
DWORD SDSetBusWidth(SDCARD_t* pCard, int width);
DWORD SDSendIfCond(SDCARD_t* pCard);
DWORD SDSendOpCode(SDCARD_t* pCard, BOOL isHC);
DWORD SDSendRelativeAddress(SDCARD_t* pCard);
// Both SD & MMC
DWORD MMCGetStatus(SDCARD_t* pCard);
DWORD MMCSetBlockLen(SDCARD_t* pCard);
DWORD MMCGoIdle(SDCARD_t* pCard);
DWORD MMCAllSendCID(SDCARD_t* pCard);
DWORD MMCSendCID(SDCARD_t* pCard);
DWORD MMCSendCSD(SDCARD_t* pCard);
DWORD MMCWaitForReady(SDCARD_t* pCard);
DWORD MMCSelectCard(SDCARD_t* pCard);
DWORD MMCReadMultiSectors(SDCARD_t* pCard, UINT32 LogicalSector, void *pBuffer, UINT16 numSectors);
DWORD MMCRead(SDCARD_t* pCard, UINT32 LogicalSector, void *pSectorBuffer);
DWORD MMCWriteMultiSectors(SDCARD_t* pCard, UINT32 LogicalSector, void *pBuffer, UINT16 numSectors);
DWORD MMCWrite(SDCARD_t* pCard, UINT32 LogicalSector, void *pSectorBuffer);
//--------------------------------------------------------------------
// SDHC interaction
//--------------------------------------------------------------------
unsigned int MMCCommandResponse(SDCARD_t* pCard);
#endif