/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/
//
//  File:  sdcard.c
//
//  This file implements bootloader functions related to sdcard download
//  

#include <SDCardDDK.h>
#include <diskio.h>
#include <blcommon.h>
#include "SDHCD.h"
#include "oal.h"
#include "oal_alloc.h"

#include "sdcard.h"
#include "sdcard_defs.h"

// set to true to enable more OALMSGs
// Caution: leave FALSE for XLOADER builds to keep size down
#define OALMSG_ENABLE   FALSE
//#define OALMSG_ENABLE   TRUE

#define DL_SUCCESS      0
#define DL_MMC_ERROR    -1

#ifndef OAL_FUNC
#define OAL_FUNC 1
#endif

#ifndef OAL_INFO
#define OAL_INFO 1
#endif

#ifndef OAL_ERROR
#define OAL_ERROR 1
#endif

#if OALMSG_ENABLE
    #define OALMSGX(z, m)   OALMSG(z, m)
#else
    #define OALMSGX(z, m)   {}
#endif

BOOL isDiskConfiguredforMMC = FALSE;
FILEHANDLE   File;
PFILEHANDLE   pFile;
extern DWORD g_bootSlot;
SDCARD_t m_SdCards[3] = {0,};// One for each slot

//--------------------------------------------------------------------------
// 
// Interface method with SDHC to exchange data with cards
//
//--------------------------------------------------------------------------
unsigned int MMCCommandResponse(SDCARD_t* pCard)
{
    if ( INTF_MMCSendCommand(&pCard->Sdhc, &pCard->MMCcmd) )
    {
        OALMSGX(OAL_ERROR, (TEXT("MMC::MMCCommandResponse: MMCSendCommand error, command = %d\r\n"), pCard->MMCcmd->command));
        goto CommandResponseError;
    }

    INTF_MMCReadResponse(&pCard->MMCcmd);

    // check response for errors
    if ( MMC_STATUS_CMD_ERROR(pCard->MMCcmd.status) )
    {
        OALMSGX(OAL_ERROR,(TEXT("MMC::MMCCommandResponse: Command = %d: Response Status Error = 0x%x\r\n"), pCard->MMCcmd->command, pCard->MMCcmd->status));
        goto CommandResponseError;
    }

    return 0;

CommandResponseError:

    OALMSGX(OAL_ERROR, (TEXT("MMC::MMCCommandResponse: Command Response Error\r\n")));
    return 1;
}

//----------------------------------------------------------------------------
//
// HostSetTranSpeed()
//
//----------------------------------------------------------------------------
static DWORD HostSetTranSpeed(DWORD bootSlot, DWORD speed)
{
    static DWORD CurrentSpeed = 0;
    DWORD ActualSpeed = speed;

    if (CurrentSpeed != speed)
	{
        SdhcSetClockRate(&m_SdCards[bootSlot].Sdhc, &ActualSpeed);
	    OALMSG(1, (TEXT("SDCARD: Reqested speed %d, actual speed %d\r\n"), speed, ActualSpeed));
        CurrentSpeed = speed;
    }		
    return ActualSpeed;
}

//----------------------------------------------------------------------------
//
// SD Configuration routine
//----------------------------------------------------------------------------
static DWORD ConfigureSD(DWORD bootSlot)
{
    DWORD result = 1;
    BYTE scr[8];

    if ( !SDGetConfigRegister(&m_SdCards[bootSlot], scr) )
    {
        //OALMSGX(OAL_ERROR, (TEXT("SEND_SCR failed\r\n")));
        // most significant byte arrives first at scr[0]
        // scr[1] has bits 55:48, bit 50 is set for 4 bit capable card
        m_SdCards[bootSlot].Disk.d_Supports4Bit = scr[1] & 0x04 ? TRUE : FALSE;
    }
    // else, in case of failure continue

    if (&m_SdCards[bootSlot].Disk.d_Supports4Bit)
    {
        OALMSGX(OAL_FUNC, (TEXT("SDCARD: 4 bit mode\r\n")));

        result = SDSetBusWidth(&m_SdCards[bootSlot], SD_BUS_WIDTH_4);
        if (result)
        {
            OALMSGX(OAL_WARN, (TEXT("MMC: unable to set wide bus mode for SD/SDHC card.\r\n")));
        }
        else
        {
            OALMSGX(1, (TEXT("SDCARD:Using 4 bit mode\r\n")));
            SdhcSetInterface(&m_SdCards[bootSlot].Sdhc, SD_INTERFACE_SD_4BIT);
        }
    }

    return result;
}

//----------------------------------------------------------------------------
//
// Try to attach SD card
//----------------------------------------------------------------------------
static DWORD AttachSD(DWORD bootSlot)
{
    DWORD StartTime = OALGetTickCount();
    BOOL bTimeout = FALSE, bCardSupportsSD2 = FALSE;
    DWORD result = 1;
    /*
	 * To support SD 2.0 cards, we must always invoke SD_SEND_IF_COND
	 * before SD_APP_OP_COND. This command will harmlessly fail for
	 * SD 1.0 cards.
	 */
    if ( !SDSendIfCond(&m_SdCards[bootSlot]) )
    {
        // Need to verify voltage support and check pattern
        bCardSupportsSD2 = TRUE;
    }
    
    // SD spec says timeout for ACMD41 should be 1 second
    for(;;)
    {
        if (OALGetTickCount() - StartTime > 1000)
            bTimeout = TRUE;

        if ( SDSendOpCode(&m_SdCards[bootSlot], bCardSupportsSD2) )
            break;
        
        if ( (!(MMC_OCR_BUSY(m_SdCards[bootSlot].MMCcmd.ocr))) || bTimeout)
            break;

        OALStall(100 * 1000);
    }

    if ( !MMC_OCR_BUSY(m_SdCards[bootSlot].MMCcmd.ocr) )
    {
        OALMSG(1, (TEXT("SDCARD: ScanCards: SD%s card detected\r\n"), m_SdCards[bootSlot].MMCcmd.card_type == CARDTYPE_SDHC ? TEXT(" HC") : TEXT("")));    
        // check for high capacity SD memory card, OCR bit 30 == 1: high capacity card
        if (bCardSupportsSD2 && (m_SdCards[bootSlot].MMCcmd.ocr & (1 << 30)))
        {
            m_SdCards[bootSlot].MMCcmd.card_type = CARDTYPE_SDHC;
            m_SdCards[bootSlot].Disk.d_CardType = CARDTYPE_SDHC;
        }
        else
        {
            m_SdCards[bootSlot].Disk.d_CardType = CARDTYPE_SD;
        }
        result = 0;
    }
    else
    {
        OALMSG(1, (TEXT("SDCARD: ScanCards: busy bit never deactivated -- no sd card detected, ocr = 0x%X\r\n"), m_SdCards[bootSlot].MMCcmd.ocr));
    }
    
    return result;
}

static DWORD AttachMMC(DWORD bootSlot)
{
    DWORD StartTime = OALGetTickCount();
    BOOL bTimeout = FALSE, bCommandFailed = FALSE;
    DWORD result = 1;

    for(;;)
    {
        if (OALGetTickCount() - StartTime > 1000)
            bTimeout = TRUE;

        bCommandFailed = MMCSendOpCond(&m_SdCards[bootSlot]);

        if (bTimeout || !(MMC_OCR_BUSY(m_SdCards[bootSlot].MMCcmd.ocr)) || bCommandFailed)
            break;
    }

    if ( !bCommandFailed && !(MMC_OCR_BUSY(m_SdCards[bootSlot].MMCcmd.ocr)) )
    {
        m_SdCards[bootSlot].Disk.d_CardType = CARDTYPE_MMC;
        m_SdCards[bootSlot].Disk.d_RelAddress = 1;
		OALMSGX(1, (TEXT("ScanCards: eMMC%s detected\r\n"), m_SdCards[bootSlot].MMCcmd.ocr == 0xC0FF8080 ? TEXT(" HC") : TEXT("")));
		if (m_SdCards[bootSlot].MMCcmd.ocr == 0xC0FF8080)
			m_SdCards[bootSlot].Disk.d_MMC_HC = TRUE;

        result = 0;
	}
    else
    {
        OALMSG(1, (TEXT("SDCARD: ScanCards: busy bit never deactivated -- probably no mmc card, ocr = 0x%X\r\n"), m_SdCards[bootSlot].MMCcmd.ocr));
    }
    return result;
}

//----------------------------------------------------------------------------
//
// MMC Configuration routine
//----------------------------------------------------------------------------
static DWORD ConfigureMMC(DWORD bootSlot)
{
    DWORD result = 1;
	// assume its our eMMC!
    if (MMCUpdateExtCsd(&m_SdCards[bootSlot], EXT_CSD_HS_TIMING, EXT_CSD_TIMING_HS))
    {
        OALMSGX(OAL_FUNC, (TEXT("SDCARD: unable to set high speed mode for MMC card.\r\n")));
    }
    else
    {
        OALMSGX(1, (TEXT("SDCARD:Using High Speed mode\r\n")));
        m_SdCards[bootSlot].Disk.MaxClkFreq = 52000000;
		// needed for Micron part...slow controller!
		MMCWaitForReady(&m_SdCards[bootSlot]);

		OALMSGX(1, (TEXT("SDCARD: SET 8bit eMMC\r\n")));
		if (MMCUpdateExtCsd(&m_SdCards[bootSlot], EXT_CSD_BUS_WIDTH, EXT_CSD_BUS_WIDTH_8))
		{
			OALMSG(1, (TEXT("SDCARD: unable to set 8 bitbus for MMC card.\r\n")));
		}
		else
		{
			OALMSG(1, (TEXT("SDCARD: Using 8bit mode\r\n")));
			SdhcSetInterface(&m_SdCards[bootSlot].Sdhc, SD_INTERFACE_MMC_8BIT);
			result = 0;
			// needed for Micron part...slow controller!
			MMCWaitForReady(&m_SdCards[bootSlot]);
		}
	}
	return result;
}

//----------------------------------------------------------------------------
// Configuration routine for the detected card type
// 
//----------------------------------------------------------------------------
static DWORD ConfigureCard(DWORD bootSlot)
{
    UINT32 StartTime;
    int bTimeout;

    StartTime = OALGetTickCount();
    bTimeout = FALSE;
    // Poll card until it is in transmission state
    for(;;)
    {
        if (OALGetTickCount() - StartTime > 1000)
        {
            bTimeout = TRUE;
        }

        if (MMCGetStatus(&m_SdCards[bootSlot]))
        {
            goto select_card_error;
        }
        
        if (MMC_STATUS_POLL_ERROR(m_SdCards[bootSlot].MMCcmd.status))
        {
            OALMSGX(OAL_ERROR, (TEXT("ConfigureCard: poll status error, status = 0x%X\r\n"), m_SdCards[bootSlot].MMCcmd.status));
            goto select_card_error;
        }

        // Check if card is already in transmission state
        if ( MMC_STATUS_STATE(m_SdCards[bootSlot].MMCcmd.status) == MMC_STATUS_STATE_TRAN )
        {
            break;
        }

        OALMSGX(OAL_FUNC, (TEXT("ConfigureCard: not in tran state, status = 0x%X\r\n"), m_SdCards[bootSlot].MMCcmd.status));
        OALMSGX(OAL_FUNC, (TEXT("SELECT_DESELECT_CARD\r\n")));
        
        if ( MMCSelectCard(&m_SdCards[bootSlot]) )
        {
            OALMSGX(OAL_ERROR, (TEXT("SELECT_DESELECT_CARD failed\r\n")));
            goto select_card_error;
        }

        // check for bTimeout
        if (bTimeout)
        {
            OALMSGX(OAL_ERROR, (TEXT("ConfigureCard: timeout waiting for card to get into tran state, status = 0x%X\r\n"), m_SdCards[bootSlot].MMCcmd.status));
            goto select_card_error;
        }
    }

    if (MMCSetBlockLen(&m_SdCards[bootSlot]))
	{
        OALMSGX(OAL_ERROR, (L"ConfigureCard: MMCSetBlockLen failed!\r\n"));
        goto select_card_error;
	}


    if (m_SdCards[bootSlot].Disk.d_CardType == CARDTYPE_SD || m_SdCards[bootSlot].Disk.d_CardType == CARDTYPE_SDHC)
    {
        // Type of Card : SD
        if (ConfigureSD(bootSlot))
        {
            OALMSGX(OAL_WARN, (TEXT("MMC: unable to set wide bus mode for SD/SDHC card.\r\n")));
            goto select_card_error;
        }
    }
	else if (m_SdCards[bootSlot].Disk.d_CardType == CARDTYPE_MMC)
	{
		// Type of Card : MMC
        if (ConfigureMMC(bootSlot))
        {
            OALMSGX(OAL_WARN, (TEXT("MMC: unable to configure MMC card.\r\n")));
            goto select_card_error;
        }
	}
	else
	{
        OALMSGX(OAL_FUNC, (TEXT("SDCARD: no support for 4/8 bit mode\r\n")));
	}

    return 0;
        
select_card_error:
    return 1;
}

//----------------------------------------------------------------------------
//
//  ScanCards
//
static BOOL ScanCards(DWORD bootSlot)
{    
    OALMSG(1, (TEXT("SDCARD: ScanCards: Test of SD (slot=%d)... \r\n"), bootSlot));

    // send init sequence to card at beginning of card ID process as specified in TRM 
    SendInitSequence(&m_SdCards[bootSlot].Sdhc);
    // set clock to reasonable rate
	HostSetTranSpeed(bootSlot, 1000000);
    // Start with SD card detection
    m_SdCards[bootSlot].Disk.d_CardType = CARDTYPE_SD;
    // Force reset for available SD cards
    MMCGoIdle(&m_SdCards[bootSlot]);
    // Set default values
	m_SdCards[bootSlot].Disk.d_MMC_HC = FALSE;

    /* Order's important: probe SD, then MMC */
    if ( AttachSD(bootSlot) )
    {
        OALMSG(1, (TEXT("SDCARD: ScanCards: No SD, Test of MMC... \r\n")));
        m_SdCards[bootSlot].Disk.d_CardType = CARDTYPE_MMC;
        // Force reset for available eMMC cards
        MMCGoIdle(&m_SdCards[bootSlot]);
        if( AttachMMC(bootSlot) )
        {
            OALMSG(1, (TEXT("SDCARD: ScanCards: No MMC either\r\n")));
            return FALSE;
        }
    }
    
    m_SdCards[bootSlot].Disk.d_MMCState = MMC_STATE_READY;

    // CMD2: ALL_SEND_CID - all cards send CID data    
    if ( MMCAllSendCID(&m_SdCards[bootSlot]) )
        goto command_error;

    m_SdCards[bootSlot].Disk.d_MMCState = MMC_STATE_IDENT;

    // Set Relative address
    OALMSGX(1, (TEXT("SET_RELATIVE_ADDR\r\n")));
    if (m_SdCards[bootSlot].Disk.d_CardType == CARDTYPE_MMC)
    {
        if(MMCSetRelativeAddress(&m_SdCards[bootSlot]))
            goto command_error;
    }
    else if (m_SdCards[bootSlot].Disk.d_CardType == CARDTYPE_SD || m_SdCards[bootSlot].Disk.d_CardType == CARDTYPE_SDHC)
    {
        if ( SDSendRelativeAddress(&m_SdCards[bootSlot]) )
            goto command_error;

        m_SdCards[bootSlot].Disk.d_RelAddress = m_SdCards[bootSlot].MMCcmd.relative_address;
        OALMSGX(1, (TEXT("SD card relative address is %d\r\n"), m_SdCards[bootSlot].Disk.d_RelAddress));
    }
    m_SdCards[bootSlot].Disk.d_MMCState = MMC_STATE_STBY;
    
    OALMSGX(1, (TEXT("SEND_CID\r\n")));
    if ( MMCSendCID(&m_SdCards[bootSlot]) )
        goto command_error;
        
	OALMSGX(1, (TEXT("SEND_CSD\r\n")));
	if ( MMCSendCSD(&m_SdCards[bootSlot]) )
		goto command_error;

	if (m_SdCards[bootSlot].Disk.d_MMC_HC)
	{
		OALMSGX(1, (TEXT("SELECT_DESELECT_CARD\r\n")));
		if ( MMCSelectCard(&m_SdCards[bootSlot]) )
		{
			OALMSGX(OAL_ERROR, (TEXT("SELECT_DESELECT_CARD failed\r\n")));
			goto command_error;
		}

		if (MMCGetExtCsd(&m_SdCards[bootSlot]))
		{
			OALMSGX(OAL_ERROR, (TEXT("Failed to read EXT_CSD\r\n")));
			goto command_error;
		}

		if ( MMCGetStatus(&m_SdCards[bootSlot]) )
		{
			OALMSGX(OAL_ERROR, (TEXT("Failed to read card status\r\n")));
			goto command_error;
		}
	}

	OALMSGX(1, (TEXT("reported block size = %d\r\n"), UTIL_csd_get_sectorsize(&m_SdCards[bootSlot].MMCcmd)));
    OALMSGX(1, (TEXT("Card size is = %u 512 byte sectors\r\n"), UTIL_csd_get_devicesize(&m_SdCards[bootSlot].MMCcmd)));
    OALMSGX(1, (TEXT("max clock freq = %d\r\n"), UTIL_csd_get_tran_speed(&m_SdCards[bootSlot].MMCcmd)));

    // set up data in DiskInfo data structure
    // some SD (not SDHC) cards will report 1024 or 2048 byte sectors, this is only to 
    // allow >1GB capacity to be reported, bootloader FAT file system only supports 512 byte sectors.
    m_SdCards[bootSlot].Disk.d_DiskInfo.di_bytes_per_sect = SECTOR_SIZE;
    m_SdCards[bootSlot].Disk.d_DiskInfo.di_total_sectors = UTIL_csd_get_devicesize(&m_SdCards[bootSlot].MMCcmd);

	if (m_SdCards[bootSlot].Disk.d_CardType == CARDTYPE_MMC)
	{
		m_SdCards[bootSlot].Disk.MaxClkFreq = UTIL_csd_get_tran_speed(&m_SdCards[bootSlot].MMCcmd) * 2;
	}
	else
	{
		m_SdCards[bootSlot].Disk.MaxClkFreq = UTIL_csd_get_tran_speed(&m_SdCards[bootSlot].MMCcmd);
	}

	OALMSGX(1, (TEXT("SDCARD: Card identification complete\r\n")));
    return TRUE;

command_error:
    OALMSG(1, (TEXT("SDCARD: ScanCards: ERROR\r\n")));
    return FALSE;
}

//==============================================================================
// IO_xxx functions
//==============================================================================
/*
 * Used to check if media is available
 */
static BOOL CardDetect(DWORD bootSlot)
{
    // check if card is preset in connector
    if (m_SdCards[bootSlot].Disk.d_MMCState == MMC_STATE_IDLE)
    {
        // card present so handle card entry actions
        SdhcHandleInsertion(&m_SdCards[bootSlot].Sdhc);
        return ScanCards(bootSlot);
    }
    return TRUE;
}

/*
 * InitDisk
 */
static BOOL InitDisk(DWORD bootSlot)
{
    DWORD res = 1;
    OALMSGX(OAL_INFO, (TEXT("InitDisk\r\n")));
    // default is for all IDE devices to support 16 bit data transfers (older drives don't support 8 bit transfers)
    m_SdCards[bootSlot].Sdhc.dwSlot = bootSlot;

    res = SdhcInitialize(&m_SdCards[bootSlot].Sdhc);
    if (!res)
    {
        m_SdCards[bootSlot].Disk.d_DiskInfo.di_total_sectors = 0;
        m_SdCards[bootSlot].Disk.d_DiskInfo.di_bytes_per_sect = SECTOR_SIZE;
        m_SdCards[bootSlot].Disk.d_DiskInfo.di_cylinders = 0;
        m_SdCards[bootSlot].Disk.d_DiskInfo.di_heads = 0;
        m_SdCards[bootSlot].Disk.d_DiskInfo.di_sectors = 0;
        m_SdCards[bootSlot].Disk.d_Supports4Bit = FALSE;
        m_SdCards[bootSlot].Disk.isInit = TRUE;
    }

    return (res == 0);
} 


//==============================================================================
// sdcardxxx functions
//==============================================================================

/*----------------------------------------------------------------------------
 * SDCardInit
 *
 */
static int SDCardInit(DWORD bootSlot)
{
    OALMSGX(1, (L"SDCardInit: Init device ...\r\n"));

    m_SdCards[bootSlot].Disk.d_MMCState = MMC_STATE_IDLE;
    m_SdCards[bootSlot].Disk.d_DiskCardState = STATE_INITING;

    if (!InitDisk(bootSlot))
    {
        OALMSGX(1, (L"SDCardInit: InitDisk failed!\r\n"));
        return DL_MMC_ERROR;
    }

    if (CardDetect(bootSlot) == FALSE) {
        OALMSGX(1, (L"SDCardInit: No media found!\r\n"));
        return DL_MMC_ERROR;
    }

    m_SdCards[bootSlot].Disk.d_DiskCardState = STATE_OPENED;

    if (MMCWaitForReady(&m_SdCards[bootSlot]))
    {
        OALMSGX(1, (L"SDCardInit: MMCWaitForReady failed!\r\n"));
        return DL_MMC_ERROR;
    }
    
    if (ConfigureCard(bootSlot))
    {
        OALMSGX(1, (L"SDCardInit: ConfigureCard failed!\r\n"));
        return DL_MMC_ERROR;
    }       

    OALMSGX(1, (L"SDCardInit: Setting clock ...\r\n"));
    HostSetTranSpeed(bootSlot, m_SdCards[bootSlot].Disk.MaxClkFreq);

    return DL_SUCCESS;  
}

/*
 * SDCardIdentify
 *
 */
static int SDCardIdentify(DWORD bootSlot, void *pSector)
{
    UNREFERENCED_PARAMETER(pSector);
    UNREFERENCED_PARAMETER(bootSlot);
    OALMSGX(OAL_INFO, (L"SDCardIdentify: Identify device ...\r\n"));
    
    return DL_SUCCESS;
}


/*----------------------------------------------------------------------------
 * SDCardReadSector
 *
 */
static int SDCardReadSector(DWORD bootSlot, UINT32 LogicalSector, void *pSector)
{
    int retry = 2;      //allow 3 attempts to read sector correctly
    DWORD Status;
    
    OALMSGX(OAL_FUNC, (L"SDCardReadSector %d\r\n", LogicalSector));

    do
    {
        Status = MMCRead(&m_SdCards[bootSlot], LogicalSector, pSector);
        if (Status != MMC_SUCCESS)
        {
            if (retry > 0)
                OALMSGX(OAL_ERROR, (L"SDCardReadSector: Error reading file, retry (sector %d)\r\n", LogicalSector));
        }
    } 
    while (Status != MMC_SUCCESS && retry--);
    
    if (Status != MMC_SUCCESS)
    {
        OALMSGX(OAL_ERROR, (L"SDCardReadSector: Error reading file! (sector %d)\r\n", LogicalSector));
        return DL_MMC_ERROR;
    }

    return DL_SUCCESS;
}

/*
 * SDCardReadMultiSectors
 *
 */
static int SDCardReadMultiSectors(DWORD bootSlot, UINT32 LogicalSector, void *pBuffer, UINT16 numSectors)
{
    int retry = 0;      //allow 3 attempts to read sector correctly
    DWORD Status;
    
    OALMSGX(OAL_FUNC, (L"SDCardReadMultiSectors %d, num sec %d\r\n", LogicalSector, numSectors));

    do
    {
        Status = MMCReadMultiSectors(&m_SdCards[bootSlot], LogicalSector, pBuffer, numSectors);
        if (Status != MMC_SUCCESS)
        {
            if (retry > 0)
					OALMSGX(OAL_ERROR, (L"SDCardReadMultiSectors: Error reading file, retry (sector %d)\r\n", LogicalSector));
        }
    } 
    while (Status != MMC_SUCCESS && retry--);
    
    if (Status != MMC_SUCCESS)
    {
			OALMSGX(OAL_ERROR, (L"SDCardReadMultiSectors: Error reading file! (sector %d, num sec %d)\r\n", LogicalSector, numSectors));
        return DL_MMC_ERROR;
    }

    return DL_SUCCESS;
}

 /* SDCardWriteSector
 *
 */
static int SDCardWriteSector(DWORD bootSlot, UINT32 LogicalSector, void *pSector)
{
    int retry = 2;      //allow 3 attempts to write sector correctly
    DWORD Status;
    
    OALMSGX(OAL_FUNC, (L"SDCardWriteSector %d\r\n", LogicalSector));

    do
    {
        //Status = MMCWrite(&m_SdCards[bootSlot], LogicalSector, pSector);
        Status = MMCWriteMultiSectors(&m_SdCards[bootSlot], LogicalSector, pSector, 1);
        if (Status != MMC_SUCCESS)
        {
            if (retry > 0)
                OALMSGX(OAL_ERROR, (L"SDCardWriteSector: Error writing file, retry (sector %d)\r\n", LogicalSector));
        }
    } 
    while (Status != MMC_SUCCESS && retry--);
    
    if (Status != MMC_SUCCESS)
    {
        OALMSGX(OAL_ERROR, (L"SDCardWriteSector: Error writing file! (sector %d)\r\n", LogicalSector));
        return DL_MMC_ERROR;
    }

    return DL_SUCCESS;
}

/*
 * SDCardWriteMultiSectors
 *
 */
static int SDCardWriteMultiSectors(DWORD bootSlot, UINT32 LogicalSector, void *pBuffer, UINT16 numSectors)
{
    int retry = 0;      //allow 3 attempts to write sector correctly
    DWORD Status;
    
    OALMSGX(OAL_FUNC, (L"SDCardWriteMultiSectors %d, num sec %d\r\n", LogicalSector, numSectors));

    do
    {
        Status = MMCWriteMultiSectors(&m_SdCards[bootSlot], LogicalSector, pBuffer, numSectors);
        if (Status != MMC_SUCCESS)
        {
            if (retry > 0)
					OALMSGX(OAL_ERROR, (L"SDCardWriteMultiSectors: Error writing file, retry (sector %d)\r\n", LogicalSector));
        }
    } 
    while (Status != MMC_SUCCESS && retry--);
    
    if (Status != MMC_SUCCESS)
    {
			OALMSGX(OAL_ERROR, (L"SDCardWriteMultiSectors: Error writing file! (sector %d, num sec %d)\r\n", LogicalSector, numSectors));
        return DL_MMC_ERROR;
    }

    return DL_SUCCESS;
}


//==============================================================================
// Public Functions
//==============================================================================
static void BLInitFileIo()
{
    memset(&m_SdCards[g_bootSlot].FileIO,0,sizeof(S_FILEIO_OPERATIONS));
    m_SdCards[g_bootSlot].FileIO.init = &SDCardInit;
    m_SdCards[g_bootSlot].FileIO.identify = &SDCardIdentify;
    m_SdCards[g_bootSlot].FileIO.read_sector = &SDCardReadSector;
    m_SdCards[g_bootSlot].FileIO.write_sector = &SDCardWriteSector;
    m_SdCards[g_bootSlot].FileIO.read_multi_sectors = &SDCardReadMultiSectors;
    m_SdCards[g_bootSlot].FileIO.write_multi_sectors = &SDCardWriteMultiSectors;
    m_SdCards[g_bootSlot].FileIO.Slot = g_bootSlot;
    m_SdCards[g_bootSlot].FileIO.isInit = TRUE;
}

static BOOL BLInitDisk()
{
    memset(&m_SdCards[g_bootSlot].Disk,0,sizeof(DISK));

    return InitDisk(g_bootSlot);
}
//------------------------------------------------------------------------------
//
//  Function:  BLSDCardDownload
//
//  This function initialize SDCard controller and call download function from
//  bootloader common library.
//
UINT32 BLSDCardDownload(WCHAR *szFilename)
{
    pFile = &File;
    if(g_bootSlot == 0)
    {
        g_bootSlot = GetBootMMCSlotConfig();
    }
    if(m_SdCards[g_bootSlot].Disk.isInit == FALSE)
    {
        if(!BLInitDisk())
        {
            OALMSGX(1, (L"Failed to init disk\r\n"));    
        }
    }

	if (m_SdCards[g_bootSlot].FileIO.isInit == FALSE)
	{
        BLInitFileIo();

        // initialize file system driver
        if (FileIoInit(&m_SdCards[g_bootSlot].FileIO) != FILEIO_STATUS_OK)
        {
            OALMSGX(1, (L"BLSDCardDownload:  fileio init failed\r\n"));
            return (UINT32) BL_ERROR;
        }
    }

    OALMSG(1, (L"SDCARD: BLSDCardDownload: Filename %s\r\n", szFilename));
    // fill in file name (8.3 format)
    FileNameToDirEntry(szFilename, pFile->name, pFile->extension);

    // try to open file specified by pConfig->filename, return BL_ERROR on failure
    if (FileIoOpen(&m_SdCards[g_bootSlot].FileIO, pFile) != FILEIO_STATUS_OK)
    {
        OALMSG(1, (L"SDCARD: BLSDCardDownload:  cannot open file\r\n"));
        return (UINT32) BL_ERROR;
    }

    // return BL_DOWNLOAD, BootloaderMain will then call OEMReadData
    // (which calls BLSDCardReadData) to get image data.
    return BL_DOWNLOAD;
}


//------------------------------------------------------------------------------
//
//  Function:   BLSDCardReadData
//
//  This function is called to read data from the transport during
//  the download process.
//
BOOL BLSDCardReadData(
                 ULONG size,
                 UCHAR *pData
                 )
{
    // called to read data from MMC/SD card as stream data, not as block data
    OALMSGX(OAL_FUNC, (L"BLSDCardReadData: address 0x%x, %d bytes\r\n", pData, size));

    if(g_bootSlot == 0)
    {
        g_bootSlot = GetBootMMCSlotConfig();
    }

    if (FileIoRead(&m_SdCards[g_bootSlot].FileIO, pFile, (PVOID)pData, size) != FILEIO_STATUS_OK)
        return FALSE;
    else
        return TRUE;
}

//------------------------------------------------------------------------------
//
//  Function:   BLSDCardReadLogo
//
//  This function is called to read the splash screen bitmap from SDCard
//  If called pData NULL it will attempt to get the file size
//  and return it in the size parameter field.
//
//
BOOL BLSDCardReadLogo(
    WCHAR *filename,
    UCHAR *pData,
	DWORD *size
	)
{
	FILEHANDLE logoFile;
	WORD	   wSignature = 0;
	DWORD	   dwOffset = 0;
	BYTE*	   pTmpBuf = NULL;
	DWORD	   dwCursor = 0;
	DWORD datasize;

    if(g_bootSlot == 0)
    {
        g_bootSlot = GetBootMMCSlotConfig();
    }

	if (m_SdCards[g_bootSlot].FileIO.isInit == FALSE)
	{
        BLInitFileIo();
        
        // initialize file system driver
        if (FileIoInit(&m_SdCards[g_bootSlot].FileIO) != FILEIO_STATUS_OK)
        {
            OALMSGX(OAL_ERROR, (L"BLSDCardReadLogo:  fileio init failed\r\n"));
            return FALSE;
        }
    }

    // fill in file name (8.3 format)
    FileNameToDirEntry(filename, logoFile.name, logoFile.extension);
	
    // try to open file specified by pConfig->filename, return BL_ERROR on failure
    if (FileIoOpen(&m_SdCards[g_bootSlot].FileIO, &logoFile) != FILEIO_STATUS_OK)
    {
        OALMSGX(OAL_ERROR, (L"BLSDCardReadLogo:  cannot open %s\r\n", filename));
        return FALSE;
    }

	// Read signature
    if (FileIoRead(&m_SdCards[g_bootSlot].FileIO, &logoFile, (PVOID)&wSignature, sizeof(wSignature)) != FILEIO_STATUS_OK)
	{
        OALMSGX(OAL_ERROR, (L"BLSDCardReadLogo:  cannot read file signature\r\n"));
        return FALSE;
	}

	dwCursor += sizeof(wSignature);

    if( wSignature != 0x4D42 )  /* BMP file header */
    {
        OALMSGX(OAL_ERROR, (L"BLSDCardReadLogo:  Invalid file signature\r\n"));
        return FALSE;
	}

	// Read dummy data
	pTmpBuf = (BYTE*)OALLocalAlloc(0, 2*sizeof(DWORD));
    if (FileIoRead(&m_SdCards[g_bootSlot].FileIO, &logoFile, (PVOID)pTmpBuf, 2*sizeof(DWORD)) != FILEIO_STATUS_OK)
	{
        OALMSGX(OAL_ERROR, (L"BLSDCardReadLogo:  cannot read dummy file header\r\n"));
		OALLocalFree((HLOCAL)pTmpBuf);
        return FALSE;
	}

	OALLocalFree((HLOCAL)pTmpBuf);
	dwCursor += 2*sizeof(DWORD);

	// Read pixel data offset
    if (FileIoRead(&m_SdCards[g_bootSlot].FileIO, &logoFile, (PVOID)&dwOffset, sizeof(dwOffset)) != FILEIO_STATUS_OK)
	{
        OALMSGX(OAL_ERROR, (L"BLSDCardReadLogo:  cannot read pixel data offset\r\n"));
        return FALSE;
	}

	dwCursor += sizeof(dwOffset);

	// Read dummy data before pixel data offset
	pTmpBuf = (BYTE*)OALLocalAlloc(0, dwOffset - dwCursor);
    if (FileIoRead(&m_SdCards[g_bootSlot].FileIO, &logoFile, (PVOID)pTmpBuf, dwOffset - dwCursor) != FILEIO_STATUS_OK)
	{
        OALMSGX(OAL_ERROR, (L"BLSDCardReadLogo:  cannot read file\r\n"));
		OALLocalFree((HLOCAL)pTmpBuf);
        return FALSE;
	}

	OALLocalFree((HLOCAL)pTmpBuf);

	datasize = logoFile.file_size - dwOffset;
	if (datasize != *size)
	{
        OALMSGX(OAL_ERROR, (L"BLSDCardReadLogo: file too big\r\n"));
        return FALSE;
	}

	// Read pixel data
    if (FileIoRead(&m_SdCards[g_bootSlot].FileIO, &logoFile, (PVOID)pData, datasize) != FILEIO_STATUS_OK)
	{
        OALMSGX(OAL_ERROR, (L"BLSDCardReadLogo:  cannot read pixel data\r\n"));
        return FALSE;
	}

	return TRUE;
}
    
//------------------------------------------------------------------------------

WCHAR *BLSDCardCfgLineGetField(WCHAR *Line)
{
    WCHAR *pwc = Line;

    if (*pwc == ';')
    {
        // a comment line
        return NULL;
    }

    while((*pwc != ':') && (*pwc != 0))
        ++pwc;

    if(*pwc == ':')
    {
        return (Line);
    }
    else
    {
        return NULL;
    }
}


WCHAR *BLSDCardCfgLineGetValue(WCHAR *Line)
{
    WCHAR *pwc = Line;
    WCHAR *pVal;

    // skip to ':'
    while((*pwc != ':') && (*pwc != 0))
        ++pwc;

    // no ':' exists
    if(*pwc == 0)
        return NULL;

    // found ':', nullify it and skip to next wchar
    *pwc = 0;
    ++pwc;

    // skip white space after ':'
    while (*pwc == ' ')
        ++pwc;

    if (*pwc == 0)
        // No value found
        return NULL;

    pVal = pwc;

    // remove white spce after value
    while (*pwc != ' ' && *pwc != 0)
        ++pwc;
    
    *pwc = 0;

    return (pVal);
}


BOOL BLSDCardCfgGetLine(
    WCHAR    *CfgBuf,
    DWORD    *pos,
    WCHAR    *Line,
	DWORD    size
)
{
    WCHAR  *pwc;
    DWORD i;
    BOOL  bCompleteLine = FALSE;
    DWORD skipped = 0;

	pwc = CfgBuf + *pos;

    i = 0;
    while (*pwc != 0 && (i < size-1))
    {
        Line[i] = *pwc;

        if (Line[i] == '\n' && Line[i-1] == '\r')
        {
            Line[i-1] = 0;
            *pos = *pos+(i+1); // update pos for next getline call
            bCompleteLine = TRUE;
            break;
        }
        ++i;
        ++pwc;
    }

    if (bCompleteLine == TRUE)
    {
        // a complete line (with \r\n and less than (size-1) chars is found
        if (i < size -1)
            Line[i+1] = 0;
        else
            Line[size-1] = 0;
        return TRUE;
    }

    if (*pwc == 0)
    {
        // line ends without a "\r\n"
        *pos = *pos+i;
        Line[i] = 0;
        return TRUE;
    }

    // Line is longer than size chars
    // skip the remaining of the line and update with the correct pos
    while (*pwc != 0)
    {
        ++pwc;
        ++skipped;

        if(skipped > 1)
        {
            if ((*(pwc-2) == '\r') && (*(pwc-1) == '\n'))
            {
                //enough
                break;
            }
        }
    }

    *pos = *pos + (size -1) + skipped;
    Line[size-1] = 0;

    return (TRUE);
}


//------------------------------------------------------------------------------
//
//  Function:   BLSDCardReadCfg
//
//  This function is called to read the bootloader configuration file from the SDCard
//
//
BOOL BLSDCardReadCfg(
    WCHAR *filename,
    UCHAR *pData,
	DWORD size
)
{
	FILEHANDLE cfgFile;

	if(g_bootSlot == 0)
    {
        g_bootSlot = GetBootMMCSlotConfig();
    }

    if (m_SdCards[g_bootSlot].FileIO.isInit == FALSE)
	{
        BLInitFileIo();

        // initialize file system driver
        if (FileIoInit(&m_SdCards[g_bootSlot].FileIO) != FILEIO_STATUS_OK)
        {
            OALMSGX(OAL_ERROR, (L"BLSDCardReadCfg:  fileio init failed\r\n"));
            return FALSE;
        }
    }

    // fill in file name (8.3 format)
    FileNameToDirEntry(filename, cfgFile.name, cfgFile.extension);
	
    // try to open file specified by pConfig->filename, return BL_ERROR on failure
    if (FileIoOpen(&m_SdCards[g_bootSlot].FileIO, &cfgFile) != FILEIO_STATUS_OK)
    {
        OALMSGX(OAL_ERROR, (L"BLSDCardReadCfg:  cannot open %s\r\n", filename));
        return FALSE;
    }

    if (FileIoRead(&m_SdCards[g_bootSlot].FileIO, &cfgFile, (PVOID)pData, size) != FILEIO_STATUS_OK)
	{
        OALMSGX(OAL_ERROR, (L"BLSDCardReadCfg:  cannot read file\r\n"));
        return FALSE;
	}

    OALMSGX(OAL_ERROR, (L"read %s\r\n", filename));

	return TRUE;
}
    
BOOL BLSDCardFormatFAT32()
{
	if(g_bootSlot == 0)
    {
        g_bootSlot = GetBootMMCSlotConfig();
    }

    if (m_SdCards[g_bootSlot].FileIO.isInit == FALSE)
	{
        BLInitFileIo();
        // initialize file system driver
        if (m_SdCards[g_bootSlot].FileIO.init(g_bootSlot))
        {
            OALMSG(OAL_FUNC, (L"SDCardInit failed \r\n"));
            return FALSE;
        }
    }

    if (FormatDiskFAT32(&m_SdCards[g_bootSlot].FileIO) != FILEIO_STATUS_OK)
	{
	    OALMSGX(OAL_ERROR, (L"BLSDCardFormatFAT32:  fileio format failed\r\n"));
		return FALSE;
	}

	return TRUE;
}

VOID DumpMMCCmd(struct MMC_command * pMMCcmd)
{
    OALMSG(1, (TEXT("card_type = %d\r\n"), pMMCcmd->card_type));
    OALMSG(1, (TEXT("command = %02X\r\n"), pMMCcmd->command));
    OALMSG(1, (TEXT("command = %d\r\n"), pMMCcmd->argument));
    OALMSG(1, (TEXT("num_blocks = %d\r\n"), pMMCcmd->num_blocks));
    OALMSG(1, (TEXT("block_len = %d\r\n"), pMMCcmd->block_len));
    OALMSG(1, (TEXT("status = %d\r\n"), pMMCcmd->status));
    OALMSG(1, (TEXT("ocr = %d\r\n"), pMMCcmd->ocr));
    OALMSG(1, (TEXT("RCA = %d\r\n"), pMMCcmd->relative_address));
    OALMSG(1, (TEXT("Raw response = %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X\r\n"), pMMCcmd->response[0], pMMCcmd->response[1], pMMCcmd->response[2], pMMCcmd->response[3], pMMCcmd->response[4],
                                                     pMMCcmd->response[5], pMMCcmd->response[6], pMMCcmd->response[7], pMMCcmd->response[8], pMMCcmd->response[9],
                                                     pMMCcmd->response[10], pMMCcmd->response[11], pMMCcmd->response[12], pMMCcmd->response[13], pMMCcmd->response[14],
                                                     pMMCcmd->response[15], pMMCcmd->response[16]));
    OALMSG(1, (TEXT("CID[mid] = %02X CID[oid] = %02X CID[pn] = %c%c%c%c%c%c%c \r\n"), pMMCcmd->cid.mmc_cid.mid, pMMCcmd->cid.mmc_cid.oid, pMMCcmd->cid.mmc_cid.pnm[0], pMMCcmd->cid.mmc_cid.pnm[1],
                    pMMCcmd->cid.mmc_cid.pnm[2],pMMCcmd->cid.mmc_cid.pnm[3],pMMCcmd->cid.mmc_cid.pnm[4],pMMCcmd->cid.mmc_cid.pnm[5],pMMCcmd->cid.mmc_cid.pnm[6],pMMCcmd->cid.mmc_cid.pnm[7]));
    OALMSG(1, (TEXT("CSD[struct] = %d\r\nCSD[version] = %d\r\nCSD[tr_speed] = %d\r\nCSD[class] = %d\r\nCSD[RdBkLen] = %d\r\nCSD[PtRdPart] = %d\r\nCSD[size] = %d\r\nCSD[fmt] = %d\r\nCSD[perm_wr_prot] = %d\r\nCSD[tmp_wr_prot] = %d\r\n"), pMMCcmd->csd.mmc_csd.csd_struct, pMMCcmd->csd.mmc_csd.spec_vers, pMMCcmd->csd.mmc_csd.tr_speed, pMMCcmd->csd.mmc_csd.ccc,pMMCcmd->csd.mmc_csd.rd_bl_len, pMMCcmd->csd.mmc_csd.rd_bl_part, pMMCcmd->csd.mmc_csd.c_size,  pMMCcmd->csd.mmc_csd.file_fmt, pMMCcmd->csd.mmc_csd.perm_wr_prot, pMMCcmd->csd.mmc_csd.tmp_wr_prot));    

    OALMSG(1, (TEXT("OALSec_Bad_Blk_Mgmt = %02X,\r\n\n\
Enh_Start_Addr = %02X %02X %02X %02X,\r\n\
Ehn_Size_Mult = %02X %02X %02X,\r\n\
Gp_Size_Mult = %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X %02X,\r\n\
Part_Setting_Cmplt = %02X,\r\n\
Part_Attrib = %02X,\r\n\
Max_Enh_Size_Mult = %02X %02X %02X,\r\n\
Part_Support = %02X,\r\n\
Rst_N_Function = %02X,\r\n\
Rpmb_Size_Mult = %02X,\r\n\
Fw_Config = %02X,\r\n\
User_Wp = %02X,\r\n\
Boot_Wp = %02X,\r\n\
Erase_Grp_Def = %02X\r\n"),
    pMMCcmd->mmc_extcsd.Sec_Bad_Blk_Mgmt,
    pMMCcmd->mmc_extcsd.Enh_Start_Addr[0],
    pMMCcmd->mmc_extcsd.Enh_Start_Addr[1],
    pMMCcmd->mmc_extcsd.Enh_Start_Addr[2],
    pMMCcmd->mmc_extcsd.Enh_Start_Addr[3],
    pMMCcmd->mmc_extcsd.Ehn_Size_Mult[0],
    pMMCcmd->mmc_extcsd.Ehn_Size_Mult[1],
    pMMCcmd->mmc_extcsd.Ehn_Size_Mult[2],
    pMMCcmd->mmc_extcsd.Gp_Size_Mult[0],
    pMMCcmd->mmc_extcsd.Gp_Size_Mult[1],
    pMMCcmd->mmc_extcsd.Gp_Size_Mult[2],
    pMMCcmd->mmc_extcsd.Gp_Size_Mult[3],
    pMMCcmd->mmc_extcsd.Gp_Size_Mult[4],
    pMMCcmd->mmc_extcsd.Gp_Size_Mult[5],
    pMMCcmd->mmc_extcsd.Gp_Size_Mult[6],
    pMMCcmd->mmc_extcsd.Gp_Size_Mult[7],
    pMMCcmd->mmc_extcsd.Gp_Size_Mult[8],
    pMMCcmd->mmc_extcsd.Gp_Size_Mult[9],
    pMMCcmd->mmc_extcsd.Gp_Size_Mult[10],
    pMMCcmd->mmc_extcsd.Gp_Size_Mult[11],
    pMMCcmd->mmc_extcsd.Part_Setting_Cmplt,
    pMMCcmd->mmc_extcsd.Part_Attrib,
    pMMCcmd->mmc_extcsd.Max_Enh_Size_Mult[0],
    pMMCcmd->mmc_extcsd.Max_Enh_Size_Mult[1],
    pMMCcmd->mmc_extcsd.Max_Enh_Size_Mult[2],
    pMMCcmd->mmc_extcsd.Part_Support,
    pMMCcmd->mmc_extcsd.Rst_N_Function,
    pMMCcmd->mmc_extcsd.Rpmb_Size_Mult,
    pMMCcmd->mmc_extcsd.Fw_Config,
    pMMCcmd->mmc_extcsd.User_Wp,
    pMMCcmd->mmc_extcsd.Boot_Wp,
    pMMCcmd->mmc_extcsd.Erase_Grp_Def));

OALMSG(1, (TEXT("Boot_Bus_Wdth = %02X,\r\n\
Boot_Cfg = %02X,\r\n\
Erased_Mem_Cont = %02X,\r\n\
Bus_Width_Mde = %02X,\r\n\
Hs_Timing = %02X,\r\n\
Pwr_Class = %02X,\r\n\
Cmd_Set_Rev = %02X,\r\n\
Cmd_Set = %02X,\r\n\
Ext_Csd_Rev = %02X,\r\n\
Csd_Struct = %02X,\r\n\
Card_Type = %02X,\r\n\
Pwr_Cl_52_195 = %02X,\r\n\
Pwr_Cl_26_195 = %02X,\r\n\
Pwr_Cl_52_360 = %02X,\r\n\
Pwr_Cl_26_360 = %02X,\r\n\
Min_Perf_R_4_26 = %02X,\r\n\
Min_Perf_W_4_26 = %02X,\r\n\
Min_Perf_R_8_26 = %02X,\r\n\
Min_Perf_W_8_26 = %02X,\r\n\
Min_Perf_R_8_52 = %02X,\r\n\
Min_Perf_W_8_52 = %02X,\r\n"),
    pMMCcmd->mmc_extcsd.Boot_Bus_Wdth,
    pMMCcmd->mmc_extcsd.Boot_Cfg,
    pMMCcmd->mmc_extcsd.Erased_Mem_Cont,
    pMMCcmd->mmc_extcsd.Bus_Width_Mde,
    pMMCcmd->mmc_extcsd.Hs_Timing,
    pMMCcmd->mmc_extcsd.Pwr_Class,
    pMMCcmd->mmc_extcsd.Cmd_Set_Rev,
    pMMCcmd->mmc_extcsd.Cmd_Set,
    pMMCcmd->mmc_extcsd.Ext_Csd_Rev,
    pMMCcmd->mmc_extcsd.Csd_Struct,
    pMMCcmd->mmc_extcsd.Card_Type,
    pMMCcmd->mmc_extcsd.Pwr_Cl_52_195,
    pMMCcmd->mmc_extcsd.Pwr_Cl_26_195,
    pMMCcmd->mmc_extcsd.Pwr_Cl_52_360,
    pMMCcmd->mmc_extcsd.Pwr_Cl_26_360,
    pMMCcmd->mmc_extcsd.Min_Perf_R_4_26,
    pMMCcmd->mmc_extcsd.Min_Perf_W_4_26,
    pMMCcmd->mmc_extcsd.Min_Perf_R_8_26,
    pMMCcmd->mmc_extcsd.Min_Perf_W_8_26,
    pMMCcmd->mmc_extcsd.Min_Perf_R_8_52,
    pMMCcmd->mmc_extcsd.Min_Perf_W_8_52));

OALMSG(1, (TEXT("Sec_Cnt = %02X %02X %02X %02X,\r\n\
S_A_Timeout = %02X,\r\n\
S_C_Vccq = %02X,\r\n\
S_C_Vcc = %02X,\r\n\
Hc_Wp_Grp_Sze = %02X,\r\n\
Rel_Wr_Sec_C = %02X,\r\n\
Erase_Timeout_Mult = %02X,\r\n\
Hc_Erase_Grp_Sze = %02X,\r\n\
Acc_Sze = %02X,\r\n\
Boot_Sze_Mult = %02X,\r\n\
Boot_Info = %02X,\r\n\
S_Cmd_Set = %02X\r\n"),
    pMMCcmd->mmc_extcsd.Sec_Cnt[0],
    pMMCcmd->mmc_extcsd.Sec_Cnt[1],
    pMMCcmd->mmc_extcsd.Sec_Cnt[2],
    pMMCcmd->mmc_extcsd.Sec_Cnt[3],
    pMMCcmd->mmc_extcsd.S_A_Timeout,
    pMMCcmd->mmc_extcsd.S_C_Vccq,
    pMMCcmd->mmc_extcsd.S_C_Vcc,
    pMMCcmd->mmc_extcsd.Hc_Wp_Grp_Sze,
    pMMCcmd->mmc_extcsd.Rel_Wr_Sec_C,
    pMMCcmd->mmc_extcsd.Erase_Timeout_Mult,
    pMMCcmd->mmc_extcsd.Hc_Erase_Grp_Sze,
    pMMCcmd->mmc_extcsd.Acc_Sze,
    pMMCcmd->mmc_extcsd.Boot_Sze_Mult,
    pMMCcmd->mmc_extcsd.Boot_Info,
    pMMCcmd->mmc_extcsd.S_Cmd_Set));
}

BOOL BLSDCardScan()
{ 
    if(g_bootSlot == 0)
    {
        g_bootSlot = GetBootMMCSlotConfig();
    }
    
    memset(&m_SdCards[g_bootSlot].FileIO,0,sizeof(S_FILEIO_OPERATIONS));
	memset(&m_SdCards[g_bootSlot].Disk,0,sizeof(DISK));

    if( SDCardInit(g_bootSlot) )
    {
        OALMSG(1, (TEXT("SDCARD: BLSDCardScan: Operation failed\r\n")));
    }

    DumpMMCCmd(&m_SdCards[g_bootSlot].MMCcmd);

	isDiskConfiguredforMMC = TRUE;
	return TRUE;
}

BOOL BLSDEraseBootPartition()
{
    BYTE buffer[512] = {0x00,};
    BYTE data = 0x00;

    if(isDiskConfiguredforMMC)
    {
        // Set ERASE_GROUP_DEF
        data = 0x01;
        OALMSGX(1, (TEXT("SDCARD: BLSDEraseBootPartition: set erase_group_def\r\n")));
        if (MMCUpdateExtCsd(&m_SdCards[g_bootSlot], EXT_CSD_ERASE_GROUP_DEF, data))
        {
            OALMSGX(1, (TEXT("SDCARD: BLSDEraseBootPartition: set erase_group_def\r\n")));
        }
        else
        {
            OALMSGX(1, (TEXT("SDCARD: Done.\r\n")));  
        }
        // Wait for command command completion
        if(MMCWaitForReady(&m_SdCards[g_bootSlot]))
        {
            OALMSGX(1, (TEXT("SDCARD: ERROR: Could not set Erase Group Def \r\n"), data));
            return FALSE;
        }

        // Set Partition access
        OALMSGX(1, (TEXT("SDCARD: BLSDEraseBootPartition: enable partition access\r\n")));
        data = (m_SdCards[g_bootSlot].MMCcmd.mmc_extcsd.Boot_Cfg & (~EXT_CSD_PART_CONFIG_ACC_MASK) | EXT_CSD_PART_CONFIG_ACC_BOOT0 | EXT_CSD_PART_CONFIG_ACC_BOOT1);
        if (MMCUpdateExtCsd(&m_SdCards[g_bootSlot],EXT_CSD_PART_CONFIG, data))
        {
            OALMSG(1, (TEXT("SDCARD: BLSDEraseBootPartition: unable to enable partition access\r\n")));
            return FALSE;
        }
        // Wait for command command completion
        if(MMCWaitForReady(&m_SdCards[g_bootSlot]))
        {
            OALMSG(1, (TEXT("SDCARD: ERROR: command never completed \r\n"), data));
            return FALSE;
        }
        /*
        if(MMCGetExtCsd(&m_SdCards[g_bootSlot]))
        {
            OALMSG(1, (TEXT("BLSDEraseBootPartition: unable to enable partition access (2)\r\n")));
            return FALSE;
        }
        // Wait for command command completion
        if(MMCWaitForReady(&m_SdCards[g_bootSlot]))
        {
            OALMSG(1, (TEXT("Error : command never completed (2)\r\n"), data));
            return FALSE;
        }
        else
        {
            OALMSGX(1, (TEXT("BLSDEraseBootPartition : boot partition 0 RW enabled\r\n")));
            OALMSGX(1, (TEXT("(register value is now %02X)\r\n"), m_SdCards[g_bootSlot].MMCcmd.mmc_extcsd.Boot_Cfg));  
        }
*/
        // Fill boot partition 0 with 0s
        if(!SDCardWriteSector(g_bootSlot, 0, buffer))
        {
            OALMSGX(1, (L"SDCARD: Wrote boot 0.\r\n"));    
        }
        else
        {
            OALMSG(1, (L"SDCARD: ERROR: Failed to write boot sector\r\n"));    
        }
        MMCWaitForReady(&m_SdCards[g_bootSlot]);

        // Fill boot partition 1 with 0s
        if(!SDCardWriteSector(g_bootSlot, 1, buffer))
        {
            OALMSGX(1, (L"SDCARD: Wrote boot 1.\r\n"));    
        }
        else
        {
            OALMSG(1, (L"SDCARD: ERROR: Failed to erase boot sector\r\n"));    
        }

        // Reset boot partition access & Set partition 0 as boot partition
        data = (m_SdCards[g_bootSlot].MMCcmd.mmc_extcsd.Boot_Cfg & (~EXT_CSD_PART_CONFIG_ACC_MASK)) | (0x01 << 3);

        // Clear Partition access
        if (MMCUpdateExtCsd(&m_SdCards[g_bootSlot], EXT_CSD_PART_CONFIG, data))
        {
            OALMSG(1, (TEXT("SDCARD: BLSDEraseBootPartition: ERROR: failed to reset partition access register\r\n")));
        }
        else
        {
            OALMSGX(1, (TEXT("SDCARD: BLSDEraseBootPartition : Done.\r\n")));
        }
        // Wait for command command completion
        if(MMCWaitForReady(&m_SdCards[g_bootSlot]))
        {
            OALMSG(1, (TEXT("SDCARD: ERROR: command never completed \r\n"), data));
            return FALSE;
        }
    }
    else
    {
        OALMSG(1, (L"SDCARD: Use scan first\r\n"));
    }

	return TRUE;
}

BOOL BLSDCardDumpBlocks(DWORD number)
{
    int i = 0, j=0;
    BYTE buffer[512];
    
    if(g_bootSlot == 0)
    {
        g_bootSlot = GetBootMMCSlotConfig();
    }
    if(m_SdCards[g_bootSlot].Disk.isInit == FALSE)
    {
        if(!BLInitDisk())
        {
            OALMSGX(1, (TEXT("SDCARD: BLSDCardDumpBlocks: ERROR: Init failed \r\n"), i));
            return FALSE;
        }
    }

    // Perform a full init
    if(SDCardInit(g_bootSlot))
    {
        OALMSG(1, (TEXT("SDCARD: BLSDCardDumpBlocks: ERROR: Card init failed \r\n"), i));
        return FALSE;
    }

    for(i = 0; i < number; i++)
    {
        OALMSG(1, (TEXT("Block %d\r\n"), i));
        if(!MMCRead(&m_SdCards[g_bootSlot], i, buffer))
        {
            for(j = 0; j < 512; j++)
            {
                OALMSG(1, (TEXT("%02X"), buffer[j]));
                if((j+1) % 51 == 0)
                {
                    OALMSG(1, (L"\r\n"));
                }
            }
            OALMSG(1, (L"\r\n"));
        }
        else
        {
            OALMSG(1, (L"SDCARD: BLSDCardDumpBlocks: ERROR: Read failed\r\n"));        
        }
    }
	return TRUE;
}

BOOL BLSDCardInitFilesystem()
{
    if(isDiskConfiguredforMMC)
    {
		if (m_SdCards[g_bootSlot].FileIO.isInit == FALSE)
        {
            BLInitFileIo();
            if (FileIoInit(&m_SdCards[g_bootSlot].FileIO) != FILEIO_STATUS_OK)
            {
                OALMSG(1, (L"SDCARD: BLSDCardInitFilesystem: ERROR: fileio init failed\r\n"));
                return FALSE;
            }
        }

        // Try to open "ebootsd.nb0"
        // fill in file name (8.3 format)
        FileNameToDirEntry(L"ebootsd.nb0", File.name, File.extension);
        if (FileIoOpen(&m_SdCards[g_bootSlot].FileIO, &File) != FILEIO_STATUS_OK)
        {
            OALMSG(1, (L"SDCARD: BLSDCardInitFilesystem: ERROR: cannot open file\r\n"));
			return FALSE;
        }

        OALMSG(1, ((L"SDCARD: Successfuly open ebootsd.nb0 (cluster = %d, size = %d)\r\n"), File.current_cluster, File.file_size));
    }
    else
    {
        OALMSG(1, (L"SDCARD: Use scan first\r\n"));
    }
    return TRUE;
}


