#ifndef _SDCARD_H_
#define _SDCARD_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "fileio.h"
#include "mmcdisk.h"
#include "sdhc.h"

typedef struct 
{
    // SD controller handle
    SDHC_Port_t Sdhc;
    // Disk handle
    DISK Disk;
    // Card RW operations handle
    S_FILEIO_OPERATIONS FileIO;
    // IO commands operations
    struct MMC_command MMCcmd;
}SDCARD_t;

#ifdef __cplusplus
}
#endif

#endif