#include <SDCardDDK.h>
#include <diskio.h>
#include "fileio.h"
#include "oal.h"

#include "mmcdisk.h"

// set to true to enable more OALMSGs
// Caution: leave FALSE for XLOADER builds to keep size down
#define OALMSG_ENABLE   FALSE
//#define OALMSG_ENABLE   TRUE

#ifndef OAL_FUNC
#define OAL_FUNC 1
#endif

#ifndef OAL_INFO
#define OAL_INFO 1
#endif

#ifndef OAL_ERROR
#define OAL_ERROR 1
#endif

#if OALMSG_ENABLE
    #define OALMSGX(z, m)   OALMSG(z, m)
#else
    #define OALMSGX(z, m)   {}
#endif

//==============================================================================
// UTIL_xxx functions
//==============================================================================

unsigned int UTIL_csd_get_sectorsize(struct MMC_command * pMMCcmd)
{
    if (pMMCcmd->card_type == CARDTYPE_MMC)
        return 1 << pMMCcmd->csd.mmc_csd.rd_bl_len;
    else
        return 1 << pMMCcmd->csd.sd_csd.sdrd_bl_len;
}

// returns number of 512 byte sectors on card
unsigned int UTIL_csd_get_devicesize(struct MMC_command * pMMCcmd)
{
    if (pMMCcmd->card_type == CARDTYPE_MMC)
    {
        OALMSGX(1, (TEXT("UTIL_csd_get_devicesize: MMC c_size = 0x%X\r\n"), pMMCcmd->csd.mmc_csd.c_size));
		if (pMMCcmd->csd.mmc_csd.c_size == 0xFFF)
		{
			return ((pMMCcmd->mmc_extcsd.Sec_Cnt[0]) |
				(pMMCcmd->mmc_extcsd.Sec_Cnt[1] << 8) |
				(pMMCcmd->mmc_extcsd.Sec_Cnt[2] << 16) |
				(pMMCcmd->mmc_extcsd.Sec_Cnt[3] << 24));
		}
		if (UTIL_csd_get_sectorsize(pMMCcmd) == SECTOR_SIZE)
            return (pMMCcmd->csd.mmc_csd.c_size  + 1) * (1 << (pMMCcmd->csd.mmc_csd.c_size_mult  + 2));
        else if (UTIL_csd_get_sectorsize(pMMCcmd) == 1024)
            return 2 * (pMMCcmd->csd.mmc_csd.c_size  + 1) * (1 << (pMMCcmd->csd.mmc_csd.c_size_mult  + 2));
        else
            return 0;
    }
    else if (pMMCcmd->card_type == CARDTYPE_SD)
    {
        if (UTIL_csd_get_sectorsize(pMMCcmd) == SECTOR_SIZE)
            return (pMMCcmd->csd.sd_csd.sdc_size + 1) * (1 << (pMMCcmd->csd.sd_csd.sdc_size_mult + 2));
        else if (UTIL_csd_get_sectorsize(pMMCcmd) == 1024)
            return 2 * (pMMCcmd->csd.sd_csd.sdc_size + 1) * (1 << (pMMCcmd->csd.sd_csd.sdc_size_mult + 2));
        else if (UTIL_csd_get_sectorsize(pMMCcmd) == 2048)
            return 4 * (pMMCcmd->csd.sd_csd.sdc_size + 1) * (1 << (pMMCcmd->csd.sd_csd.sdc_size_mult + 2));
        else
            return 0;
    }
    else if (pMMCcmd->card_type == CARDTYPE_SDHC)
    {
        // for SDHC, c_size is in 512K units, we want it in 512 byte sectors
        return (pMMCcmd->csd.sd_csd.sdhcc_size + 1) * 1024;
    }
    else
        return 0;
}

unsigned int UTIL_csd_get_tran_speed(struct MMC_command * pMMCcmd)
{
    unsigned int temp, mant, exp;

    temp = (unsigned int)(pMMCcmd->card_type == CARDTYPE_MMC) ? pMMCcmd->csd.mmc_csd.tr_speed : pMMCcmd->csd.sd_csd.sdtr_speed;

    /* get exponent factor */
    switch(temp & 0x7)
    {
        case 0:
            exp = 100000;       // 100khz
            break;
        case 1:
            exp = 1000000;      // 1MHz
            break;
        case 2:
            exp = 10000000;     // 10MHz
            break;
        case 3:
            exp = 100000000;    // 100MHz
            break;
        default:
            exp = 0;
    }

    /* get mantissa factor (10x so we can use integer math) */
    switch((temp >> 3) & 0xf)
    {
            case 1:
                mant = 10;      // 1.0
                break;
            case 2:
                mant = 12;      // 1.2
                break;
            case 3:
                mant = 13;      // 1.3
                break;
            case 4:
                mant = 15;      // 1.5
                break;
            case 5:
                mant = 20;      // 2.0
                break;
            case 6:
                mant = 25;      // 2.5
                break;
            case 7:
                mant = 30;      // 3.0
                break;
            case 8:
                mant = 35;      // 3.5
                break;
            case 9:
                mant = 40;      // 4.0
                break;
            case 0xa:
                mant = 45;      // 4.5
                break;
            case 0xb:
                mant = 50;      // 5.0
                break;
            case 0xc:
                mant = 55;      // 5.5
                break;
            case 0xd:
                mant = 60;      // 6.0
                break;
            case 0xe:
                mant = 70;      // 7.0
                break;
            case 0xf:
                mant = 80;      // 8.0
                break;
            default:
                mant = 0;
    }

    return (exp * mant)/10;
}