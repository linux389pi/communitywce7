#include <SDCardDDK.h>
#include <diskio.h>
#include "oal.h"

#include "mmcdisk.h"

/*
 * macro that can be used to instantiate a response table
 */

#define MMC_RESPONSE_NONE   0
#define MMC_RESPONSE_R1     1
#define MMC_RESPONSE_R2     2
#define MMC_RESPONSE_R3     3
		
#define RESPONSE_TABLE \
{ \
	MMC_RESPONSE_NONE,       /* CMD0 */      \
	MMC_RESPONSE_R3,         /* CMD1 */      \
	MMC_RESPONSE_R2,         /* CMD2 */      \
	MMC_RESPONSE_R1,         /* CMD3 */      \
	MMC_RESPONSE_NONE,       /* CMD4 */      \
	MMC_RESPONSE_NONE,       /* 5 - res */   \
	MMC_RESPONSE_NONE,       /* 6 - res */   \
	MMC_RESPONSE_R1,         /* CMD7 */      \
	MMC_RESPONSE_R3,         /* CMD8 - SD 2.0 only, 6 byte response */   \
	MMC_RESPONSE_R2,         /* CMD9 */       \
	MMC_RESPONSE_R2,         /* CMD10 */      \
	MMC_RESPONSE_R1,         /* CMD11 */      \
	MMC_RESPONSE_R1,         /* CMD12 */      \
	MMC_RESPONSE_R1,         /* CMD13 */      \
	MMC_RESPONSE_NONE,       /* 14 - res */   \
	MMC_RESPONSE_NONE,       /* CMD15 */      \
	MMC_RESPONSE_R1,         /* CMD16 */      \
	MMC_RESPONSE_R1,         /* CMD17 */      \
	MMC_RESPONSE_R1,         /* CMD18 */      \
	MMC_RESPONSE_NONE,       /* 19 - res */   \
	MMC_RESPONSE_R1,         /* CMD20 */      \
	MMC_RESPONSE_NONE,       /* 21 - res */   \
	MMC_RESPONSE_NONE,       /* 22 - res */   \
	MMC_RESPONSE_NONE,       /* 23 - res */   \
	MMC_RESPONSE_R1,         /* CMD24 */      \
	MMC_RESPONSE_R1,         /* CMD25 */      \
	MMC_RESPONSE_R1,         /* CMD26 */      \
	MMC_RESPONSE_R1,         /* CMD27 */      \
	MMC_RESPONSE_R1,         /* CMD28 */      \
	MMC_RESPONSE_R1,         /* CDM29 */      \
	MMC_RESPONSE_R1,         /* CMD30 */      \
   MMC_RESPONSE_R1,         /* CMD31 */      \
	MMC_RESPONSE_R1,         /* CMD32 */      \
	MMC_RESPONSE_R1,         /* CMD33 */      \
	MMC_RESPONSE_R1,         /* CMD34 */      \
	MMC_RESPONSE_R1,         /* CMD35 */      \
	MMC_RESPONSE_R1,         /* CMD36 */      \
	MMC_RESPONSE_R1,         /* CMD37 */      \
	MMC_RESPONSE_R1,         /* CMD38 */      \
	MMC_RESPONSE_NONE,       /* 39 - not supported */   \
	MMC_RESPONSE_NONE,       /* 40 - not supported */   \
	MMC_RESPONSE_NONE,       /* 41 - not supported */   \
	MMC_RESPONSE_R1,         /* CMD42 */   \
	MMC_RESPONSE_NONE,       /* 43 - not supported */   \
	MMC_RESPONSE_NONE,       /* 44 - not supported */   \
	MMC_RESPONSE_NONE,       /* 45 - not supported */   \
	MMC_RESPONSE_NONE,       /* 46 - not supported */   \
	MMC_RESPONSE_NONE,       /* 47 - not supported */   \
	MMC_RESPONSE_NONE,       /* 48 - not supported */   \
	MMC_RESPONSE_NONE,       /* 49 - not supported */   \
	MMC_RESPONSE_NONE,       /* 50 - not supported */   \
	MMC_RESPONSE_NONE,       /* 51 - not supported */   \
	MMC_RESPONSE_NONE,       /* 52 - not supported */   \
	MMC_RESPONSE_NONE,       /* 53 - not supported */   \
	MMC_RESPONSE_NONE,       /* 54 - not supported */   \
	MMC_RESPONSE_R1,         /* CMD55 */   \
	MMC_RESPONSE_R1          /* CMD56 */   \
}

// Useful for working with SD/SDIO cards:
#define ALT_RESPONSE_TABLE \
{ \
	MMC_RESPONSE_NONE,       /* 0 (res) */   \
	MMC_RESPONSE_NONE,       /* 1 (res) */   \
	MMC_RESPONSE_NONE,       /* 2 (res) */   \
	MMC_RESPONSE_NONE,       /* 3 (res) */   \
	MMC_RESPONSE_NONE,       /* 4 (res) */   \
	MMC_RESPONSE_NONE,       /* 5 (res) */   \
	MMC_RESPONSE_R1,         /* ACMD6 (SET BUS WIDTH) */   \
	MMC_RESPONSE_NONE,       /* 7 (res) */   \
	MMC_RESPONSE_NONE,       /* 8 (res) */   \
	MMC_RESPONSE_NONE,       /* 9 (res) */   \
	MMC_RESPONSE_NONE,       /* 10 (res) */   \
	MMC_RESPONSE_NONE,       /* 11 (res) */   \
	MMC_RESPONSE_NONE,       /* 12 (res) */   \
	MMC_RESPONSE_R1,         /* ACMD13 (SD STATUS) */ \
	MMC_RESPONSE_NONE,       /* 14 (res) */   \
	MMC_RESPONSE_NONE,       /* 15 (res) */   \
	MMC_RESPONSE_NONE,       /* 16 (res) */   \
	MMC_RESPONSE_NONE,       /* 17 (res) */   \
	MMC_RESPONSE_R1,         /* ACMD18 (secure read multi block) */   \
	MMC_RESPONSE_NONE,       /* 19 (res) */   \
	MMC_RESPONSE_NONE,       /* 20 (res) */   \
	MMC_RESPONSE_NONE,       /* 21 (res) */   \
	MMC_RESPONSE_R1,         /* ACMD22 (send num wr blocks) */   \
	MMC_RESPONSE_R1,         /* ACMD23 (set wr block erase count) */   \
	MMC_RESPONSE_NONE,       /* 24 (res) */      \
	MMC_RESPONSE_R1,         /* ACMD25 ( secure write multi block) */ \
	MMC_RESPONSE_R1,         /* ACMD26 ( secure write mkb) */ \
	MMC_RESPONSE_NONE,       /* 27 (res) */   \
	MMC_RESPONSE_NONE,       /* 28 (res) */   \
	MMC_RESPONSE_NONE,       /* 29 (res) */   \
	MMC_RESPONSE_NONE,       /* 30 (res) */   \
	MMC_RESPONSE_NONE,       /* 31 (res) */   \
	MMC_RESPONSE_NONE,       /* 32 (res) */   \
	MMC_RESPONSE_NONE,       /* 33 (res) */   \
	MMC_RESPONSE_NONE,       /* 34 (res) */   \
	MMC_RESPONSE_NONE,       /* 35 (res) */  \
	MMC_RESPONSE_NONE,       /* 36 (res) */   \
	MMC_RESPONSE_NONE,       /* 37 (res) */   \
	MMC_RESPONSE_R1,         /* ACMD38 (secure erase) */   \
	MMC_RESPONSE_NONE,       /* 39 (res) */   \
	MMC_RESPONSE_NONE,       /* 40 (res) */   \
	MMC_RESPONSE_R3,         /* ACMD41 (sd send op code */   \
	MMC_RESPONSE_R1,         /* ACMD42 (set clr card detect pu resistor*/ \
	MMC_RESPONSE_R1,         /* ACMD43 (get mkb) */   \
	MMC_RESPONSE_R1,         /* ACMD44 (get mid) */   \
	MMC_RESPONSE_R1,         /* ACMD45 (set cer RN1) */   \
	MMC_RESPONSE_R1,         /* ACMD46 (get_cer_RN2) */   \
	MMC_RESPONSE_R1,         /* ACMD47 (set cer_rs2) */   \
	MMC_RESPONSE_R1,         /* ACMD48 (get cer_rs2) */   \
	MMC_RESPONSE_NONE,       /* ACMD49 (change secure area) */   \
	MMC_RESPONSE_NONE,       /* 50 (res) */   \
	MMC_RESPONSE_R1,         /* ACMD51 (send scr) */   \
	MMC_RESPONSE_NONE,       /* 52 (padding to make table */   \
	MMC_RESPONSE_NONE,       /* 53 (same length as base table */   \
	MMC_RESPONSE_NONE,       /* 54 */   \
	MMC_RESPONSE_NONE,       /* 55 */   \
	MMC_RESPONSE_NONE        /* 56 */   \
}

#define MMC_TC_COMMAND  0
#define MMC_TC_WRITE    1
#define MMC_TC_READ     2

#define TRANSFER_CLASS_TABLE \
{ \
	MMC_TC_COMMAND,      /* CMD0 */      \
	MMC_TC_COMMAND,      /* CMD1 */      \
	MMC_TC_COMMAND,      /* CMD2 */      \
	MMC_TC_COMMAND,      /* CMD3 */      \
	MMC_TC_COMMAND,      /* CMD4 */      \
	MMC_TC_COMMAND,      /* 5 - res */   \
	MMC_TC_COMMAND,      /* 6 - res */   \
	MMC_TC_COMMAND,      /* CMD7 */      \
	MMC_TC_COMMAND,      /* CMD8 - SD 2.0 only */   \
	MMC_TC_COMMAND,      /* CMD9 */       \
	MMC_TC_COMMAND,      /* CMD10 */      \
	MMC_TC_COMMAND,      /* CMD11 */      \
	MMC_TC_COMMAND,      /* CMD12 */      \
	MMC_TC_COMMAND,      /* CMD13 */      \
	MMC_TC_COMMAND,      /* 14 - res */   \
	MMC_TC_COMMAND,      /* CMD15 */      \
	MMC_TC_COMMAND,      /* CMD16 */      \
	MMC_TC_READ,         /* CMD17 */      \
	MMC_TC_READ,         /* CMD18 */      \
	MMC_TC_COMMAND,      /* 19 - res */   \
	MMC_TC_COMMAND,      /* CMD20 */      \
	MMC_TC_COMMAND,      /* 21 - res */   \
	MMC_TC_COMMAND,      /* 22 - res */   \
	MMC_TC_COMMAND,      /* 23 - res */   \
	MMC_TC_WRITE,        /* CMD24 */      \
	MMC_TC_WRITE,        /* CMD25 */      \
	MMC_TC_COMMAND,      /* CMD26 */      \
	MMC_TC_COMMAND,      /* CMD27 */      \
	MMC_TC_COMMAND,      /* CMD28 */      \
	MMC_TC_COMMAND,      /* CDM29 */      \
	MMC_TC_COMMAND,      /* CMD30 */      \
	MMC_TC_COMMAND,      /* CMD31 */      \
	MMC_TC_COMMAND,      /* CMD32 */      \
	MMC_TC_COMMAND,      /* CMD33 */      \
	MMC_TC_COMMAND,      /* CMD34 */      \
	MMC_TC_COMMAND,      /* CMD35 */      \
	MMC_TC_COMMAND,      /* CMD36 */      \
	MMC_TC_COMMAND,      /* CMD37 */      \
	MMC_TC_COMMAND,      /* CMD38 */      \
	MMC_TC_COMMAND,      /* 39 - not supported */   \
	MMC_TC_COMMAND,      /* 40 - not supported */   \
	MMC_TC_COMMAND,      /* 41 - not supported */   \
	MMC_TC_COMMAND,      /* CMD42 */   \
	MMC_TC_COMMAND,      /* 43 - not supported */   \
	MMC_TC_COMMAND,      /* 44 - not supported */   \
	MMC_TC_COMMAND,      /* 45 - not supported */   \
	MMC_TC_COMMAND,      /* 46 - not supported */   \
	MMC_TC_COMMAND,      /* 47 - not supported */   \
	MMC_TC_COMMAND,      /* 48 - not supported */   \
	MMC_TC_COMMAND,      /* 49 - not supported */   \
	MMC_TC_COMMAND,      /* 50 - not supported */   \
	MMC_TC_COMMAND,      /* 51 - not supported */   \
	MMC_TC_COMMAND,      /* 52 - not supported */   \
	MMC_TC_COMMAND,      /* 53 - not supported */   \
	MMC_TC_COMMAND,      /* 54 - not supported */   \
	MMC_TC_COMMAND,      /* CMD55 */   \
	MMC_TC_COMMAND       /* CMD56 is read or write TC depending on RW bit */   \
}

// Useful for working with SD/SDIO cards:
#define ALT_TRANSFER_CLASS_TABLE \
{ \
	MMC_TC_COMMAND,      /* 0 (res) */   \
	MMC_TC_COMMAND,      /* 1 (res) */   \
	MMC_TC_COMMAND,      /* 2 (res) */   \
	MMC_TC_COMMAND,      /* 3 (res) */   \
	MMC_TC_COMMAND,      /* 4 (res) */   \
	MMC_TC_COMMAND,      /* 5 (res) */   \
	MMC_TC_COMMAND,      /* ACMD6 (SET BUS WIDTH) */   \
	MMC_TC_COMMAND,      /* 7 (res) */   \
	MMC_TC_COMMAND,      /* 8 (res) */   \
	MMC_TC_COMMAND,      /* 9 (res) */   \
	MMC_TC_COMMAND,      /* 10 (res) */   \
	MMC_TC_COMMAND,      /* 11 (res) */   \
	MMC_TC_COMMAND,      /* 12 (res) */   \
	MMC_TC_COMMAND,      /* ACMD13 (SD STATUS) */ \
	MMC_TC_COMMAND,      /* 14 (res) */   \
	MMC_TC_COMMAND,      /* 15 (res) */   \
	MMC_TC_COMMAND,      /* 16 (res) */   \
	MMC_TC_COMMAND,      /* 17 (res) */   \
	MMC_TC_COMMAND,      /* ACMD18 (secure read multi block) */   \
	MMC_TC_COMMAND,      /* 19 (res) */   \
	MMC_TC_COMMAND,      /* 20 (res) */   \
	MMC_TC_COMMAND,      /* 21 (res) */   \
	MMC_TC_COMMAND,      /* ACMD22 (send num wr blocks) */   \
	MMC_TC_COMMAND,      /* ACMD23 (set wr block erase count) */   \
	MMC_TC_COMMAND,      /* 24 (res) */      \
	MMC_TC_COMMAND,      /* ACMD25 ( secure write multi block) */ \
	MMC_TC_COMMAND,      /* ACMD26 ( secure write mkb) */ \
	MMC_TC_COMMAND,      /* 27 (res) */   \
	MMC_TC_COMMAND,      /* 28 (res) */   \
	MMC_TC_COMMAND,      /* 29 (res) */   \
	MMC_TC_COMMAND,      /* 30 (res) */   \
	MMC_TC_COMMAND,      /* 31 (res) */   \
	MMC_TC_COMMAND,      /* 32 (res) */   \
	MMC_TC_COMMAND,      /* 33 (res) */   \
	MMC_TC_COMMAND,      /* 34 (res) */   \
	MMC_TC_COMMAND,      /* 35 (res) */  \
	MMC_TC_COMMAND,      /* 36 (res) */   \
	MMC_TC_COMMAND,      /* 37 (res) */   \
	MMC_TC_COMMAND,      /* ACMD38 (secure erase) */   \
	MMC_TC_COMMAND,      /* 39 (res) */   \
	MMC_TC_COMMAND,      /* 40 (res) */   \
	MMC_TC_COMMAND,      /* ACMD41 (sd send op code */   \
	MMC_TC_COMMAND,      /* ACMD42 (set clr card detect pu resistor*/ \
	MMC_TC_COMMAND,      /* ACMD43 (get mkb) */   \
	MMC_TC_COMMAND,      /* ACMD44 (get mid) */   \
	MMC_TC_COMMAND,      /* ACMD45 (set cer RN1) */   \
	MMC_TC_COMMAND,      /* ACMD46 (get_cer_RN2) */   \
	MMC_TC_COMMAND,      /* ACMD47 (set cer_rs2) */   \
	MMC_TC_COMMAND,      /* ACMD48 (get cer_rs2) */   \
	MMC_TC_COMMAND,      /* ACMD49 (change secure area) */   \
	MMC_TC_COMMAND,      /* 50 (res) */   \
	/*MMC_TC_COMMAND*/ MMC_TC_READ,     /* ACMD51 (send scr) */ \
	MMC_TC_COMMAND,      /* 52 (padding to make table */   \
	MMC_TC_COMMAND,      /* 53 (same length as base table */   \
	MMC_TC_COMMAND,      /* 54 */   \
	MMC_TC_COMMAND,      /* 55 */   \
	MMC_TC_COMMAND       /* 56 */   \
}


// instantiate command to response lookup tables Normal commands
static const BYTE CommandToResponse[] = RESPONSE_TABLE;
static const BYTE CommandToTransferClass[] = TRANSFER_CLASS_TABLE;

// Alternate commands (previous command was CMD55)
static const BYTE AlternateCommandToResponse[] = ALT_RESPONSE_TABLE;
static const BYTE AlternateCommandToTransferClass[] = ALT_TRANSFER_CLASS_TABLE;

static SD_BUS_REQUEST Request;
static BOOL bAlternateCommandMode = FALSE;

//----------------------------------------------------------------------------
//
//  __zeromemory
//
void __zeromemory(void *pdst, size_t size)
{
    UINT8 *pch = (UINT8*)pdst;
    while (size-- > 0) *pch++ = 0;
}

//==============================================================================
// INTF_xxx functions
//==============================================================================

static SD_API_STATUS WaitForCommandResult(SDHC_Port_t* pPort, SD_BUS_REQUEST * pRequest)
{
    SD_API_STATUS ResultCode = SD_API_STATUS_PENDING;

    while (ResultCode == SD_API_STATUS_PENDING)
    {
        ResultCode = SdhcControllerIstThread(pPort, pRequest);
    }

    return ResultCode;
}

//----------------------------------------------------------------------------
//
// INTF_MMCSendCommand()
//
//----------------------------------------------------------------------------

#define CONVERT_TC(tc)                    ( \
    (tc) == MMC_TC_COMMAND ? SD_COMMAND : ( \
    (tc) == MMC_TC_READ ? SD_READ :       ( \
    SD_WRITE)))

#define CONVERT_RESPONSE_TYPE(rt)            ( \
    (rt) == MMC_RESPONSE_NONE ? NoResponse : ( \
    (rt) == MMC_RESPONSE_R1 ? ResponseR1   : ( \
    (rt) == MMC_RESPONSE_R2 ? ResponseR2   : ( \
    (rt) == MMC_RESPONSE_R3 ? ResponseR3   : ( \
    NoResponse)))))

unsigned int INTF_MMCSendCommand(SDHC_Port_t* pPort, struct MMC_command * pMMC_command)
{
    // ignore init flag, init clocks are requested by card insert...

    Request.CommandCode = pMMC_command->command;
    Request.CommandArgument = pMMC_command->argument;
    Request.BlockSize = pMMC_command->block_len;
    Request.NumBlocks = pMMC_command->num_blocks;
    Request.HCParam = 0;
    pPort->dwFlags = pMMC_command->flags;

    if (bAlternateCommandMode)
    {
        Request.CommandResponse.ResponseType = CONVERT_RESPONSE_TYPE(AlternateCommandToResponse[Request.CommandCode]);
        Request.TransferClass = CONVERT_TC(AlternateCommandToTransferClass[Request.CommandCode]);
    }
    else
    {
        Request.CommandResponse.ResponseType = CONVERT_RESPONSE_TYPE(CommandToResponse[Request.CommandCode]);
        Request.TransferClass = CONVERT_TC(CommandToTransferClass[Request.CommandCode]);
    }

    // check for commands with R1b response
    if (Request.CommandResponse.ResponseType == ResponseR1)
        if (Request.CommandCode == 12 || Request.CommandCode == 28 || Request.CommandCode == 29 || Request.CommandCode == 38)
            Request.CommandResponse.ResponseType = ResponseR1b;
    
    if( (Request.CommandCode == MMC_SEND_EXT_CSD) && (pMMC_command->card_type == CARDTYPE_MMC ) )
    {
        // We're trying to send the get ExtCSD request; in MMC that requires a different
        // response type, so change it here.  This overrides the 1 disconnect in the unified
        // response table (mmcdisk.h) as opposed to creating an entirely new table for 1 
        // command

        //OEMWriteDebugString(L"INTF_MMCSendCommand: Changing response type for MMC_SEND_EXT_CSD \r\n ");
        Request.CommandResponse.ResponseType = ResponseR1;
        // Pick a non-zero value to trigger that this is MMC so don't have to tweak the global SD_BUS_REQUEST define
        // for just 1 command
        Request.HCParam = 0x5;
    }

    if ( (pMMC_command->card_type == CARDTYPE_MMC ) && ( (Request.CommandCode == SEND_WRITE_PROT) || (Request.CommandCode == SEND_WRITE_PROT_TYPE) ) )
    {
        // MMC send write protect & send write protect type use data lines to send status
        Request.TransferClass = SD_READ;
    }

    Request.pBlockBuffer = pMMC_command->pBuffer;

    // send command
    if (!SD_API_SUCCESS(SdhcBusRequestHandler(pPort, &Request)))
        return 1;

    // wait for command done, check result code
    if (!SD_API_SUCCESS(WaitForCommandResult(pPort, &Request)))
        return 1;

    bAlternateCommandMode = FALSE;
    return 0;
}

unsigned int INTF_MMCReadResponse(struct MMC_command * pMMC_command)
{
    int i;

    // change response byte order from SDHC to match format expected by MMC/bootloader driver
    //  response[0] = Request.CommandResponse.ResponseBuffer[MSB]
    //  response[1] = Request.CommandResponse.ResponseBuffer[MSB-1]
    //  response[2] = Request.CommandResponse.ResponseBuffer[MSB-2]
    //  response[1] = Request.CommandResponse.ResponseBuffer[MSB-3]
    //  etc.

    __zeromemory(pMMC_command->response, 17);

    memset(pMMC_command->response, 0, 17);

    if (NoResponse != Request.CommandResponse.ResponseType) 
    {
        if (ResponseR2 == Request.CommandResponse.ResponseType)
        {
            // 17 byte response
            for (i = 0; i < 17; i++)
            {
                pMMC_command->response[i] = Request.CommandResponse.ResponseBuffer[16 - i];
            }
        }
        else
        {
            // 6 byte response
            for (i = 0; i < 6; i++)
            {
                pMMC_command->response[i] = Request.CommandResponse.ResponseBuffer[5 - i];
            }
        }
    }

    // for some commands parse the response
    if (pMMC_command->command == SEND_CSD)
    {
        if (pMMC_command->card_type == CARDTYPE_SD || pMMC_command->card_type == CARDTYPE_SDHC)
        {
            pMMC_command->csd.sd_csd.sdcsd_struct = (pMMC_command->response[1] >> 6) & 0x3;
            pMMC_command->csd.sd_csd.sdtacc = pMMC_command->response[2];
            pMMC_command->csd.sd_csd.sdnsac = pMMC_command->response[3];
            pMMC_command->csd.sd_csd.sdtr_speed = pMMC_command->response[4];
            pMMC_command->csd.sd_csd.sdccc = (pMMC_command->response[5] << 4) | ((pMMC_command->response[6] >> 4) & 0xF);
            pMMC_command->csd.sd_csd.sdrd_bl_len = pMMC_command->response[6] & 0xF;
            pMMC_command->csd.sd_csd.sdrd_bl_part = (pMMC_command->response[7] >> 7) & 0x1;
            pMMC_command->csd.sd_csd.sdwr_bl_msalign = (pMMC_command->response[7] >> 6) & 0x1;
            pMMC_command->csd.sd_csd.sddsr_imp = (pMMC_command->response[7] >> 5) & 0x1;
            if (pMMC_command->card_type == CARDTYPE_SDHC)
            {
                pMMC_command->csd.sd_csd.sdhcc_size = ((pMMC_command->response[8] & 0x3f) << 16) |
                    (pMMC_command->response[9] << 8) | (pMMC_command->response[10]);

                pMMC_command->csd.sd_csd.sdvdd_r_min = 0;
                pMMC_command->csd.sd_csd.sdvdd_r_max = 0;
                pMMC_command->csd.sd_csd.sdvdd_w_min = 0;
                pMMC_command->csd.sd_csd.sdvdd_w_max = 0;
                pMMC_command->csd.sd_csd.sdc_size_mult = 0;
            }
            else
            {
                pMMC_command->csd.sd_csd.sdc_size = ((pMMC_command->response[7] & 0x3)<<10) |
                    (pMMC_command->response[8]<<2) | ((pMMC_command->response[9]>>6)  & 0x3);
                pMMC_command->csd.sd_csd.sdvdd_r_min = (pMMC_command->response[9] >> 3) & 0x7;
                pMMC_command->csd.sd_csd.sdvdd_r_max = pMMC_command->response[9] & 0x7;
                pMMC_command->csd.sd_csd.sdvdd_w_min = (pMMC_command->response[9] >> 5) & 0x7;
                pMMC_command->csd.sd_csd.sdvdd_w_max = (pMMC_command->response[10] >> 2) & 0x7;
                pMMC_command->csd.sd_csd.sdc_size_mult = (pMMC_command->response[10] & 0x3) << 1 |
                    ((pMMC_command->response[11] >> 7) & 0x1);
            }
            pMMC_command->csd.sd_csd.sderase_bk_en = (pMMC_command->response[11] >> 6) & 0x1;
            pMMC_command->csd.sd_csd.sdsector_size = ((pMMC_command->response[11] << 1) & 0x7E) | ((pMMC_command->response[12] >> 7) & 0x1);
            pMMC_command->csd.sd_csd.sderase_grp_size = pMMC_command->response[12] & 0x7F;
            pMMC_command->csd.sd_csd.sdwp_grp_en = (pMMC_command->response[13] >> 7) & 0x1;
            pMMC_command->csd.sd_csd.sdr2w_factor = (pMMC_command->response[13] >> 2) & 0x7;
            pMMC_command->csd.sd_csd.sdwr_blk_len = ((pMMC_command->response[13] & 0x3) << 2) | ((pMMC_command->response[14] >> 6) & 0x3);
            pMMC_command->csd.sd_csd.sdwr_blk_part = (pMMC_command->response[14] >> 5) & 0x1;
            pMMC_command->csd.sd_csd.sdfile_fmt_grp = (pMMC_command->response[15] >> 7) & 0x1;
            pMMC_command->csd.sd_csd.sdcopy = (pMMC_command->response[15] >> 6) & 0x1;
            pMMC_command->csd.sd_csd.sdperm_wr_prot = (pMMC_command->response[15] >> 5) & 0x1;
            pMMC_command->csd.sd_csd.sdtmp_wr_prot = (pMMC_command->response[15] >> 4) & 0x1;
            pMMC_command->csd.sd_csd.sdfile_fmt = (pMMC_command->response[15] >> 2) & 0x3;
            pMMC_command->csd.sd_csd.sdcrc = 0;  //no computed crc provided
            pMMC_command->crc = 0;
        }
        else
        {
            pMMC_command->csd.mmc_csd.csd_struct = (pMMC_command->response[1] >> 6) & 0x3;
            pMMC_command->csd.mmc_csd.spec_vers = (pMMC_command->response[1] >> 2) & 0xF;
            pMMC_command->csd.mmc_csd.tacc = pMMC_command->response[2];
            pMMC_command->csd.mmc_csd.nsac = pMMC_command->response[3];
            pMMC_command->csd.mmc_csd.tr_speed = pMMC_command->response[4];
            pMMC_command->csd.mmc_csd.ccc = (pMMC_command->response[5] << 4) | ((pMMC_command->response[6] >> 4) & 0xF);
            pMMC_command->csd.mmc_csd.rd_bl_len = pMMC_command->response[6] & 0xF;
            pMMC_command->csd.mmc_csd.c_size = ((pMMC_command->response[7] & 0x3)<<10) | (pMMC_command->response[8]<<2) | ((pMMC_command->response[9]>>6)  & 0x3);
            pMMC_command->csd.mmc_csd.rd_bl_part = (pMMC_command->response[7] >> 7) & 0x1;
            pMMC_command->csd.mmc_csd.wr_bl_msalign = (pMMC_command->response[7] >> 6) & 0x1;
            pMMC_command->csd.mmc_csd.dsr_imp = (pMMC_command->response[7] >> 5) & 0x1;
            pMMC_command->csd.mmc_csd.vdd_r_min = (pMMC_command->response[9] >> 3) & 0x7;
            pMMC_command->csd.mmc_csd.vdd_r_max = pMMC_command->response[9] & 0x7;
            pMMC_command->csd.mmc_csd.vdd_w_min = (pMMC_command->response[10] >> 5) & 0x7;
            pMMC_command->csd.mmc_csd.vdd_w_max = (pMMC_command->response[10] >> 2) & 0x7;
            pMMC_command->csd.mmc_csd.c_size_mult = (pMMC_command->response[10] & 0x3) << 1 | ((pMMC_command->response[11] >> 7) & 0x1);
            pMMC_command->csd.mmc_csd.sector_size = (pMMC_command->response[11] >> 2) & 0x1F;
            pMMC_command->csd.mmc_csd.erase_grp_size = ((pMMC_command->response[11] & 0x3) << 3) | ((pMMC_command->response[12] >> 5) & 0x7);
            pMMC_command->csd.mmc_csd.wp_grp_size = pMMC_command->response[12] & 0x1F;
            pMMC_command->csd.mmc_csd.wp_grp_en = (pMMC_command->response[13] >> 7) & 0x1;
            pMMC_command->csd.mmc_csd.default_ecc = (pMMC_command->response[13] >> 5) & 0x3;
            pMMC_command->csd.mmc_csd.r2w_factor = (pMMC_command->response[13] >> 2) & 0x7;
            pMMC_command->csd.mmc_csd.wr_blk_len = ((pMMC_command->response[13] & 0x3) << 2) | ((pMMC_command->response[14] >> 6) & 0x3);
            pMMC_command->csd.mmc_csd.wr_blk_part = (pMMC_command->response[14] >> 5) & 0x1;
            pMMC_command->csd.mmc_csd.file_fmt_grp = (pMMC_command->response[15] >> 7) & 0x1;
            pMMC_command->csd.mmc_csd.copy = (pMMC_command->response[15] >> 6) & 0x1;
            pMMC_command->csd.mmc_csd.perm_wr_prot = (pMMC_command->response[15] >> 5) & 0x1;
            pMMC_command->csd.mmc_csd.tmp_wr_prot = (pMMC_command->response[15] >> 4) & 0x1;
            pMMC_command->csd.mmc_csd.file_fmt = (pMMC_command->response[15] >> 2) & 0x3;
            pMMC_command->csd.mmc_csd.ecc = pMMC_command->response[15] & 0x3;
            pMMC_command->csd.mmc_csd.crc = 0;
            pMMC_command->crc = 0;
        }
    }
    
    if (pMMC_command->command == SEND_CID)
    {
        if (pMMC_command->card_type == CARDTYPE_SD || pMMC_command->card_type == CARDTYPE_SDHC)
        {
            pMMC_command->cid.sd_cid.sdmid = pMMC_command->response[1];
            pMMC_command->cid.sd_cid.sdoid = (pMMC_command->response[2] << 8) |
                pMMC_command->response[3];
            for (i=0;i<5; i++)
                pMMC_command->cid.sd_cid.sdpnm[i] = pMMC_command->response[4+i];
            pMMC_command->cid.sd_cid.sdprv = pMMC_command->response[9];
            pMMC_command->cid.sd_cid.sdpsn = (pMMC_command->response[10]<<24) |
                (pMMC_command->response[11]<<16) | (pMMC_command->response[12]<<8) |
                 pMMC_command->response[13];
            pMMC_command->cid.sd_cid.sdmdt = ((pMMC_command->response[14] & 0xF) << 8) |
                pMMC_command->response[15];
            // pMMC_command->cid.sd_cid.sdcrc = pMMC_command->response[16];
            pMMC_command->cid.sd_cid.sdcrc = 0;
            pMMC_command->crc = 0;
        }
        else
        {
            pMMC_command->cid.mmc_cid.mid = pMMC_command->response[1];
            pMMC_command->cid.mmc_cid.oid = (pMMC_command->response[2] << 8) |
                pMMC_command->response[3];
            for (i=0; i<6; i++)
                pMMC_command->cid.mmc_cid.pnm[i] = pMMC_command->response[4+i];
            pMMC_command->cid.mmc_cid.prv = pMMC_command->response[10];
            pMMC_command->cid.mmc_cid.psn = (pMMC_command->response[11]<<24) |
                (pMMC_command->response[12]<<16) | (pMMC_command->response[13]<<8) |
                 pMMC_command->response[14];
            pMMC_command->cid.mmc_cid.mdt = pMMC_command->response[15];
            // pMMC_command->cid.mmc_cid.crc = pMMC_command->response[16];
            pMMC_command->cid.mmc_cid.crc = 0;
            pMMC_command->crc = 0;
        }
    }
        
    // format status register if needed
    if ((pMMC_command->card_type == CARDTYPE_SD || pMMC_command->card_type == CARDTYPE_SDHC) && pMMC_command->command == SEND_RELATIVE_ADDRESS)
    {
        pMMC_command->relative_address = (pMMC_command->response[1] << 8) |
                                         (pMMC_command->response[2]);
        pMMC_command->status = 0;
        pMMC_command->ocr = 0;
    }
    else if (Request.CommandResponse.ResponseType == ResponseR1 || Request.CommandResponse.ResponseType == ResponseR1b)
    {
        pMMC_command->status = (pMMC_command->response[4]) |
                               (pMMC_command->response[3] << 8) |
                               (pMMC_command->response[2] << 16) |
                               (pMMC_command->response[1] << 24);
        pMMC_command->ocr = 0;

        /*
        // This test fails for command 18 which reports 16...
        if (pMMC_command->command != (pMMC_command->response[0] & 0x3f))
        {
            OALMSGX(OAL_ERROR, (TEXT("MMC: command in response %d != command %d\r\n"), pMMC_command->response[0], pMMC_command->command));
        }
        */
    }
    else if (Request.CommandResponse.ResponseType == ResponseR3)
    {
        pMMC_command->status = 0;
        pMMC_command->ocr = (pMMC_command->response[4]) |
                            (pMMC_command->response[3] << 8) |
                            (pMMC_command->response[2] << 16) |
                            (pMMC_command->response[1] << 24);
    }
    else if (Request.CommandResponse.ResponseType == ResponseR7)
    {
        pMMC_command->status = 0;
        pMMC_command->ocr = (pMMC_command->response[4]) |
            (pMMC_command->response[3] << 8) |
            (pMMC_command->response[2] << 16) |
            (pMMC_command->response[1] << 24) |
            (pMMC_command->response[0] << 28);
    }
    else
    {
        pMMC_command->status = 0;
        pMMC_command->ocr = 0;
    }

    // check for alternate command mode entry
    if (Request.CommandCode == APP_CMD)
        bAlternateCommandMode = TRUE;
    else
        bAlternateCommandMode = FALSE;

    return 0;
}