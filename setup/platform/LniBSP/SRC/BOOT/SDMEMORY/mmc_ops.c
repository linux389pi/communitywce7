#include <SDCardDDK.h>
#include <diskio.h>
#include <blcommon.h>
#include "SDHCD.h"
#include "oal.h"
#include "oal_alloc.h"

#include "sdcard.h"
#include "sdcard_defs.h"

// set to true to enable more OALMSGs
// Caution: leave FALSE for XLOADER builds to keep size down
#define OALMSG_ENABLE   FALSE
//#define OALMSG_ENABLE   TRUE

#ifndef OAL_FUNC
#define OAL_FUNC 1
#endif

#ifndef OAL_INFO
#define OAL_INFO 1
#endif

#ifndef OAL_ERROR
#define OAL_ERROR 1
#endif

#if OALMSG_ENABLE
    #define OALMSGX(z, m)   OALMSG(z, m)
#else
    #define OALMSGX(z, m)   {}
#endif

//----------------------------------------------------------------------------

//----------------------------------------------------------------------------
//
// Send CMD6 (SWITCH) to eMMC to modify Ext Csd register
//
//----------------------------------------------------------------------------
DWORD MMCUpdateExtCsd(SDCARD_t* pCard, BYTE index, BYTE value)
{
    pCard->MMCcmd.command = MMC_SWITCH;
    pCard->MMCcmd.argument = (MMC_SWITCH_MODE_WRITE_BYTE << 24) 
                        | (index << 16) 
                        | (value << 8) 
                        | EXT_CSD_CMD_SET_NORMAL;
    /* Special case 
     Need to set Data present, Read (set in next if block) 
     and Block Count Enable bits in Cmd Register*/
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48B;
    pCard->MMCcmd.card_type = CARDTYPE_MMC;
    pCard->MMCcmd.num_blocks = 1;
    pCard->MMCcmd.block_len = SECTOR_SIZE; 
    return MMCCommandResponse(pCard);
}

//----------------------------------------------------------------------------
//
// On eMMC, get EXT_CSD register
//
//----------------------------------------------------------------------------
DWORD MMCGetExtCsd(SDCARD_t* pCard)
{
    // build command
    pCard->MMCcmd.command = MMC_SEND_EXT_CSD;
    pCard->MMCcmd.argument = pCard->Disk.d_RelAddress<<16;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48 | MMCHS_CMD_DP | MMCHS_CMD_BCE  | MMCHS_CMD_DDIR;
    pCard->MMCcmd.num_blocks = 1;
    pCard->MMCcmd.block_len = SECTOR_SIZE;     
    pCard->MMCcmd.pBuffer = (void *)&pCard->MMCcmd.mmc_extcsd;
    pCard->MMCcmd.card_type = CARDTYPE_MMC;

    return MMCCommandResponse(pCard);
}

//----------------------------------------------------------------------------
//
// Get card status
//
//----------------------------------------------------------------------------
DWORD MMCGetStatus(SDCARD_t* pCard)
{
    pCard->MMCcmd.command = SEND_STATUS;
    pCard->MMCcmd.argument = pCard->Disk.d_RelAddress<<16;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48;
    pCard->MMCcmd.pBuffer = NULL;
    pCard->MMCcmd.card_type = pCard->Disk.d_CardType;
    pCard->MMCcmd.block_len = SECTOR_SIZE;
    if(pCard->MMCcmd.card_type == CARDTYPE_SD)
    {
        //ACMD13 has associated data, but the CMD13 doesnt have data
        //Hence we need to set the Data Present Select (CMD:DP- bit 21) for ACMD13 alone
        pCard->MMCcmd.flags |= MMCHS_CMD_DP | MMCHS_CMD_DDIR;
        pCard->MMCcmd.num_blocks = 1;
    }
    else
    {
        pCard->MMCcmd.num_blocks = 0;
    }

    return MMCCommandResponse(pCard);
}

//----------------------------------------------------------------------------
//
// Configure block len
//
//----------------------------------------------------------------------------
DWORD MMCSetBlockLen(SDCARD_t* pCard)
{
    pCard->MMCcmd.card_type = pCard->Disk.d_CardType;
    pCard->MMCcmd.command = SET_BLOCKLEN;
    pCard->MMCcmd.argument = pCard->Disk.d_DiskInfo.di_bytes_per_sect;
    pCard->MMCcmd.flags = MMCHS_CMD_CCCE | MMCHS_CMD_CICE | MMCHS_RSP_LEN48;
    pCard->MMCcmd.num_blocks = 1;
    pCard->MMCcmd.block_len = pCard->Disk.d_DiskInfo.di_bytes_per_sect;

    return MMCCommandResponse(pCard);
}

//----------------------------------------------------------------------------
//
// Asks eMMC to go to Idle state
//
//----------------------------------------------------------------------------
DWORD MMCGoIdle(SDCARD_t* pCard)
{
    DWORD result = 1;
    pCard->MMCcmd.command = GO_IDLE_STATE;
    pCard->MMCcmd.argument = 0;
    pCard->MMCcmd.flags = MMCHS_RSP_NONE;
    pCard->MMCcmd.num_blocks = 1;
    pCard->MMCcmd.block_len = SECTOR_SIZE;
    pCard->MMCcmd.card_type = pCard->Disk.d_CardType;
    
    result = MMCCommandResponse(pCard);
    OALStall(1 * 1000);
    return result;
}

//----------------------------------------------------------------------------
//
// eMMC SEND_OP_COND
//
//----------------------------------------------------------------------------
DWORD MMCSendOpCond(SDCARD_t* pCard)
{
    DWORD result = 1;
	pCard->MMCcmd.command = SEND_OP_COND;
    pCard->MMCcmd.argument = 0x40FF8000;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48;
    pCard->MMCcmd.card_type = CARDTYPE_MMC;
    pCard->MMCcmd.ocr = 0;
    result = MMCCommandResponse(pCard);
    OALStall(1 * 1000);
    return result;
}

//----------------------------------------------------------------------------
//
// Ask for all cards' CID (SD & MMC compatible)
//
//----------------------------------------------------------------------------
DWORD MMCAllSendCID(SDCARD_t* pCard)
{
    // CMD2: ALL_SEND_CID - all cards send CID data
    pCard->MMCcmd.command = ALL_SEND_CID;
    pCard->MMCcmd.argument = 0;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN136 |MMCHS_CMD_CCCE;
    pCard->MMCcmd.card_type = pCard->Disk.d_CardType;

    return MMCCommandResponse(pCard);
}

//----------------------------------------------------------------------------
//
// Ask for card CID (Device Specific Data)
// (SD & MMC compatible)
//
//----------------------------------------------------------------------------
DWORD MMCSendCID(SDCARD_t* pCard)
{
    /*
     * CMD10: SEND_CID - send card identification
     */
    pCard->MMCcmd.command = SEND_CID;
    pCard->MMCcmd.argument = (pCard->Disk.d_RelAddress) << 16;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN136;
    pCard->MMCcmd.card_type = pCard->Disk.d_CardType;

    return MMCCommandResponse(pCard);
}

//----------------------------------------------------------------------------
//
// Ask for card CSD ()
//
//----------------------------------------------------------------------------
DWORD MMCSendCSD(SDCARD_t* pCard)
{
    /*
	 * CMD9: SEND_CSD - send card specific data
	 */
	pCard->MMCcmd.command = SEND_CSD;
	pCard->MMCcmd.argument = (pCard->Disk.d_RelAddress) << 16;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN136;
    pCard->MMCcmd.card_type = pCard->Disk.d_CardType;

	return MMCCommandResponse(pCard);
}

//----------------------------------------------------------------------------
//
// Set eMMC relative address
//
//----------------------------------------------------------------------------
DWORD MMCSetRelativeAddress(SDCARD_t* pCard)
{
    /*
     * CMD3: SET_RELATIVE_ADDR - set relative address
     * relative address should probably be interface number
     *
     * note: this command is SEND_RELATIVE_ADDRESS for SD cards
     */
    pCard->MMCcmd.command = SET_RELATIVE_ADDR;
    pCard->MMCcmd.argument = (pCard->Disk.d_RelAddress) << 16;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48;
    pCard->MMCcmd.card_type = CARDTYPE_MMC;

    return MMCCommandResponse(pCard);
}

//----------------------------------------------------------------------------
//
// Wait until card is ready
//
//----------------------------------------------------------------------------
DWORD MMCWaitForReady(SDCARD_t* pCard)
{
    UINT32 StartTime;
    int card_state;
    int bTimeout;

    if (MMCGetStatus(pCard))
        return 1;

    StartTime = OALGetTickCount();
    card_state = MMC_STATUS_STATE(pCard->MMCcmd.status);
    bTimeout = FALSE;

    // make sure the card is ready to accept data and we are in the proper
    // state.  If an error occured on the last transaction, the card might still
    // be in the prg state, etc.
        
    while ( (!(MMC_STATUS_READY(pCard->MMCcmd.status))) ||
           ( (card_state != MMC_STATUS_STATE_STBY) && (card_state != MMC_STATUS_STATE_TRAN) ) )
    {
        if (bTimeout)
        {
            return 1;
        }

        if ( (card_state != MMC_STATUS_STATE_STBY) && (card_state != MMC_STATUS_STATE_TRAN) )
        {
            OALMSGX(OAL_FUNC, (TEXT("SDMem: wait for card state = %i\r\n"), card_state));
            OALMSGX(OAL_FUNC, (TEXT("SDMem: MMCcmd.status = 0x%X\r\n"), pCard->MMCcmd.status));
        }
        
        if (MMCCommandResponse(pCard)) 
            return 1;

        card_state = MMC_STATUS_STATE(pCard->MMCcmd.status);

        if (OALGetTickCount() - StartTime > 1000)
            bTimeout = TRUE;

        OALStall(10 * 1000);
    }
    
    return 0;
}

//----------------------------------------------------------------------------
//
// Read Multiple sectors
//
//----------------------------------------------------------------------------
DWORD MMCReadMultiSectors(SDCARD_t* pCard, UINT32 LogicalSector, void *pBuffer, UINT16 numSectors)
{
    DWORD Status = MMC_FAILURE;

    // Make sure we don't access beyond end of disk
    if ((LogicalSector + numSectors) >= pCard->Disk.d_DiskInfo.di_total_sectors)
    {
        Status = ERROR_INVALID_PARAMETER;
        OALMSGX(OAL_FUNC, (TEXT("MMCReadMultiSectorsExit: ERROR_INVALID_PARAMETER\r\n")));
        goto MMCReadMultiSectorsExit;
    }
    
    if (pCard->Disk.d_DiskCardState != STATE_OPENED && pCard->Disk.d_DiskCardState != STATE_CLOSED)
    {
        Status = MMC_FAILURE;
        OALMSGX(OAL_FUNC, (TEXT("MMCReadMultiSectorsExit: incorrect disk state\r\n")));
        goto MMCReadMultiSectorsExit;
    }

    if (MMCWaitForReady(pCard))
    {
        Status = MMC_FAILURE;
        OALMSGX(OAL_ERROR, (TEXT("MMCReadMultiSectors: MMCWaitForReady error\r\n")));
        goto MMCReadMultiSectorsExit;
    }

    // build command
    pCard->MMCcmd.command = READ_MULTIPLE_BLOCK;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48 | MMCHS_CMD_MSBS | MMCHS_CMD_BCE | MMCHS_CMD_READ |MMCHS_CMD_DP | MMCHS_CMD_DDIR;
    pCard->MMCcmd.num_blocks = numSectors;
    pCard->MMCcmd.block_len = pCard->Disk.d_DiskInfo.di_bytes_per_sect;
    pCard->MMCcmd.pBuffer = pBuffer;
    
    // starting address
    if (pCard->MMCcmd.card_type == CARDTYPE_SDHC)
        pCard->MMCcmd.argument = LogicalSector;
    else
        pCard->MMCcmd.argument = LogicalSector * pCard->Disk.d_DiskInfo.di_bytes_per_sect;

    OALMSGX(OAL_FUNC, (TEXT("READ_MULTIPLE_BLOCK\r\n")));
    if (MMCCommandResponse(pCard))
    {
        Status = MMC_FAILURE;
        OALMSGX(OAL_ERROR, (TEXT("MMCReadMultiSectors: MMCCommandResponse error on READ_MULTIPLE_BLOCK!\r\n")));
        goto MMCReadMultiSectorsExit;
    }

    // send STOP_TRANSMISSION
    pCard->MMCcmd.command = STOP_TRANSMISSION;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48 | MMCHS_CMD_ABORT;
    pCard->MMCcmd.argument = 0;
    pCard->MMCcmd.num_blocks = 0;
    pCard->MMCcmd.block_len = 0;
    pCard->MMCcmd.pBuffer = 0;
    if (MMCCommandResponse(pCard))
    {
        Status = MMC_FAILURE;
        OALMSGX(OAL_ERROR, (TEXT("MMCReadMultiSectors: MMCCommandResponse error on STOP_TRANSMISSION!\r\n")));
        goto MMCReadMultiSectorsExit;
    }

    Status = MMC_SUCCESS;

MMCReadMultiSectorsExit:
    
    if (Status != MMC_SUCCESS)
    {
        OALMSGX(OAL_ERROR, (TEXT("read multi sectors error\r\n")));
    }

    return Status;
}

//----------------------------------------------------------------------------
//
// Read single sector
//
//----------------------------------------------------------------------------
DWORD MMCRead(SDCARD_t* pCard, UINT32 LogicalSector, void *pSectorBuffer)
{
    DWORD Status = MMC_FAILURE;

    // Make sure we don't access beyond end of disk
    if (LogicalSector >= pCard->Disk.d_DiskInfo.di_total_sectors)
    {
        Status = ERROR_INVALID_PARAMETER;
        OALMSGX(OAL_FUNC, (TEXT("DoDiskIO: ERROR_INVALID_PARAMETER\r\n")));
        goto MMCReadExit;
    }
    
    if (pCard->Disk.d_DiskCardState != STATE_OPENED && pCard->Disk.d_DiskCardState != STATE_CLOSED)
    {
        Status = MMC_FAILURE;
        OALMSGX(OAL_FUNC, (TEXT("DoDiskIO: incorrect disk state\r\n")));
        goto MMCReadExit;
    }

    if (MMCWaitForReady(pCard))
    {
        Status = MMC_FAILURE;
        goto MMCReadExit;
    }

    // build command
    pCard->MMCcmd.command = READ_SINGLE_BLOCK;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48 | MMCHS_CMD_READ | MMCHS_CMD_DP | MMCHS_CMD_DDIR;
    pCard->MMCcmd.num_blocks = 1;
    pCard->MMCcmd.block_len = pCard->Disk.d_DiskInfo.di_bytes_per_sect;
    pCard->MMCcmd.pBuffer = pSectorBuffer;
    
    // starting address
    if (pCard->MMCcmd.card_type == CARDTYPE_SDHC || (pCard->MMCcmd.card_type == CARDTYPE_MMC && pCard->Disk.d_MMC_HC))
        pCard->MMCcmd.argument = LogicalSector;
	else
        pCard->MMCcmd.argument = LogicalSector * pCard->Disk.d_DiskInfo.di_bytes_per_sect;

    OALMSGX(OAL_FUNC, (TEXT("READ_SINGLE_BLOCK\r\n")));
    if (MMCCommandResponse(pCard))
    {
        Status = MMC_FAILURE;
        OALMSGX(OAL_ERROR, (TEXT("MMCRead: MMCCommandResponse error on READ_SINGLE_BLOCK!\r\n")));
        goto MMCReadExit;
    }

    Status = MMC_SUCCESS;

MMCReadExit:
    
    if (Status != MMC_SUCCESS)
    {
        OALMSGX(OAL_ERROR, (TEXT("read error\r\n")));
    }

    return Status;
}

//----------------------------------------------------------------------------
//
// Write multiple sectors
//
//----------------------------------------------------------------------------
DWORD MMCWriteMultiSectors(SDCARD_t* pCard, UINT32 LogicalSector, void *pBuffer, UINT16 numSectors)
{
    DWORD Status = MMC_FAILURE;

    // Make sure we don't access beyond end of disk
    if ((LogicalSector + numSectors) >= pCard->Disk.d_DiskInfo.di_total_sectors)
    {
        Status = ERROR_INVALID_PARAMETER;
        OALMSGX(OAL_FUNC, (TEXT("MMCWriteMultiSectors: ERROR_INVALID_PARAMETER\r\n")));
        goto MMCWriteMultiSectorsExit;
    }
    
    if (pCard->Disk.d_DiskCardState != STATE_OPENED && pCard->Disk.d_DiskCardState != STATE_CLOSED)
    {
        Status = MMC_FAILURE;
        OALMSGX(OAL_FUNC, (TEXT("MMCWriteMultiSectors: incorrect disk state\r\n")));
        goto MMCWriteMultiSectorsExit;
    }

    if (MMCWaitForReady(pCard))
    {
        Status = MMC_FAILURE;
        OALMSGX(OAL_ERROR, (TEXT("MMCWriteMultiSectors: MMCWaitForReady error\r\n")));
        goto MMCWriteMultiSectorsExit;
    }

    // build command
    pCard->MMCcmd.command = WRITE_MULTIPLE_BLOCK;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48 | MMCHS_CMD_DP | MMCHS_CMD_MSBS | MMCHS_CMD_BCE;
    pCard->MMCcmd.num_blocks = numSectors;
    pCard->MMCcmd.block_len = pCard->Disk.d_DiskInfo.di_bytes_per_sect;
    pCard->MMCcmd.pBuffer = pBuffer;
    
    // starting address
    if (pCard->MMCcmd.card_type == CARDTYPE_SDHC)
        pCard->MMCcmd.argument = LogicalSector;
    else
        pCard->MMCcmd.argument = LogicalSector * pCard->Disk.d_DiskInfo.di_bytes_per_sect;

    OALMSGX(OAL_FUNC, (TEXT("WRITE_MULTIPLE_BLOCK\r\n")));
    if (MMCCommandResponse(pCard))
    {
        Status = MMC_FAILURE;
        OALMSGX(OAL_ERROR, (TEXT("MMCWriteMultiSectors: MMCCommandResponse error on WRITE_MULTIPLE_BLOCK!\r\n")));
        goto MMCWriteMultiSectorsExit;
    }

    // send STOP_TRANSMISSION
    pCard->MMCcmd.command = STOP_TRANSMISSION;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48B | MMCHS_CMD_ABORT;
    pCard->MMCcmd.argument = 0;
    pCard->MMCcmd.num_blocks = 0;
    pCard->MMCcmd.block_len = 0;
    pCard->MMCcmd.pBuffer = 0;
    if (MMCCommandResponse(pCard))
    {
        Status = MMC_FAILURE;
        OALMSGX(OAL_ERROR, (TEXT("MMCWriteMultiSectors: MMCCommandResponse error on STOP_TRANSMISSION!\r\n")));
        goto MMCWriteMultiSectorsExit;
    }

    Status = MMC_SUCCESS;

MMCWriteMultiSectorsExit:
    
    if (Status != MMC_SUCCESS)
    {
        OALMSGX(OAL_ERROR, (TEXT("write multi sectors error\r\n")));
    }

    return Status;
}

//----------------------------------------------------------------------------
//
// Write single sector
//
//----------------------------------------------------------------------------
DWORD MMCWrite(SDCARD_t* pCard, UINT32 LogicalSector, void *pSectorBuffer)
{
    DWORD Status = MMC_FAILURE;

    // Make sure we don't access beyond end of disk
    if (LogicalSector >= pCard->Disk.d_DiskInfo.di_total_sectors)
    {
        Status = ERROR_INVALID_PARAMETER;
        OALMSGX(OAL_FUNC, (TEXT("DoDiskIO: ERROR_INVALID_PARAMETER\r\n")));
        goto MMCWriteExit;
    }
    
    if (pCard->Disk.d_DiskCardState != STATE_OPENED && pCard->Disk.d_DiskCardState != STATE_CLOSED)
    {
        Status = MMC_FAILURE;
        OALMSGX(OAL_FUNC, (TEXT("DoDiskIO: incorrect disk state\r\n")));
        goto MMCWriteExit;
    }

    if (MMCWaitForReady(pCard))
    {
        Status = MMC_FAILURE;
        goto MMCWriteExit;
    }

    // build command
    pCard->MMCcmd.command = WRITE_BLOCK;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48 | MMCHS_CMD_DP;
    pCard->MMCcmd.num_blocks = 1;
    pCard->MMCcmd.block_len = pCard->Disk.d_DiskInfo.di_bytes_per_sect;
    pCard->MMCcmd.pBuffer = pSectorBuffer;
    
    /*
	* Data address for media =< 2GB is byte address, and data
    * address for media > 2GB is sector address.		 
    */
    if (pCard->MMCcmd.card_type == CARDTYPE_SDHC || (pCard->MMCcmd.card_type == CARDTYPE_MMC && pCard->Disk.d_MMC_HC))
        pCard->MMCcmd.argument = LogicalSector;
	else
        pCard->MMCcmd.argument = LogicalSector * pCard->Disk.d_DiskInfo.di_bytes_per_sect;

    OALMSG(1, (TEXT("MMC: Write (%d) bytes\r\n"), pCard->MMCcmd.block_len));
    if (MMCCommandResponse(pCard))
    {
        Status = MMC_FAILURE;
        goto MMCWriteExit;
    }

    Status = MMC_SUCCESS;

MMCWriteExit:
    
    if (Status != MMC_SUCCESS)
    {
        OALMSGX(OAL_ERROR, (TEXT("write error\r\n")));
    }

    return Status;
}

//----------------------------------------------------------------------------
//
// Select/Deselect card
// Use rel_addr = 0 to deselect
//
//----------------------------------------------------------------------------
DWORD MMCSelectCard(SDCARD_t* pCard)
{
    pCard->MMCcmd.card_type = pCard->Disk.d_CardType;
    pCard->MMCcmd.command = SELECT_DESELECT_CARD;
    pCard->MMCcmd.argument = (pCard->Disk.d_RelAddress << 16); // @@CSo flags shall be RSP_NONE | MMC_CMD_AC if address is 0
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48;
    return MMCCommandResponse(pCard);
}

//----------------------------------------------------------------------------
//
// (APPCMD) Get SCR (configuration register)
// SD application specific command
//----------------------------------------------------------------------------
DWORD SDGetConfigRegister(SDCARD_t* pCard, BYTE* scr)
{
    DWORD result = 1;
    pCard->MMCcmd.command = APP_CMD;
    pCard->MMCcmd.argument = (pCard->Disk.d_RelAddress) << 16;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48;
    
    if ( !MMCCommandResponse(pCard) )
    {
        // SEND_SCR - send configuration register
        pCard->MMCcmd.command = SEND_SCR;
        pCard->MMCcmd.argument = 0;
        pCard->MMCcmd.flags = MMCHS_RSP_LEN48 | MMCHS_CMD_DP | MMCHS_CMD_DDIR;
        pCard->MMCcmd.num_blocks = 1;
        pCard->MMCcmd.block_len = 8;
        pCard->MMCcmd.pBuffer = scr;
        OALMSGX(OAL_FUNC, (TEXT("SEND_SCR\r\n")));
        result = MMCCommandResponse(pCard);
    }

    return result;
}

//----------------------------------------------------------------------------
//
// (APPCMD) Set Bus width
// SD application specific command
//----------------------------------------------------------------------------
DWORD SDSetBusWidth(SDCARD_t* pCard, int width)
{
    DWORD result = 1;
    pCard->MMCcmd.command = APP_CMD;
    pCard->MMCcmd.argument = (pCard->Disk.d_RelAddress) << 16;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48;
    
    if ( !MMCCommandResponse(pCard) )
    {
        pCard->MMCcmd.command = SET_BUS_WIDTH;
        pCard->MMCcmd.argument = 0x2;
        pCard->MMCcmd.flags = MMCHS_RSP_LEN48;
        OALMSGX(OAL_FUNC, (TEXT("SET_BUS_WIDTH\r\n")));
        result = MMCCommandResponse(pCard);
    }

    return result;
}

//----------------------------------------------------------------------------
//
// SD specific : SEND_IF_COND
//
//----------------------------------------------------------------------------
DWORD SDSendIfCond(SDCARD_t* pCard)
{
    pCard->MMCcmd.command = SD_SEND_IF_COND;
    // specify VSH = 2.7-3.6V (bit 8), check pattern = 0xaa (bits 7:0)
    pCard->MMCcmd.argument = 0x000001aa;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48;
    pCard->MMCcmd.num_blocks = 0;
    pCard->MMCcmd.block_len = SECTOR_SIZE;
    pCard->MMCcmd.card_type = CARDTYPE_SD;
    pCard->MMCcmd.ocr = 0;

    return MMCCommandResponse(pCard);
}

//----------------------------------------------------------------------------
//
// SD specific : SEND_OP_COND
//
//----------------------------------------------------------------------------
DWORD SDSendOpCode(SDCARD_t* pCard, BOOL isHC)
{
    DWORD result = 1;
    pCard->MMCcmd.command = APP_CMD;
    pCard->MMCcmd.argument = (pCard->Disk.d_RelAddress) << 16;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48;
    pCard->MMCcmd.card_type = CARDTYPE_SD;

    if ( !MMCCommandResponse(pCard) )
    {
        pCard->MMCcmd.command = SD_SEND_OP_CODE;
        pCard->MMCcmd.flags = MMCHS_RSP_LEN48;
        if (isHC)
        {
            // specify 3.3V, HC
            pCard->MMCcmd.argument = 0x40200000;
        }
        else
        {
            // specify 3.3V
            pCard->MMCcmd.argument = 0x00200000;
        }
        result = MMCCommandResponse(pCard);
        OALStall(10 * 1000);
    }
    return result;
}

//----------------------------------------------------------------------------
//
// Ask SD to send its relative address
//
//----------------------------------------------------------------------------
DWORD SDSendRelativeAddress(SDCARD_t* pCard)
{
    pCard->MMCcmd.command = SEND_RELATIVE_ADDRESS;
    pCard->MMCcmd.argument = 0;
    pCard->MMCcmd.flags = MMCHS_RSP_LEN48;
    pCard->MMCcmd.card_type = CARDTYPE_SD;

    return MMCCommandResponse(pCard);
}