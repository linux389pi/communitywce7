/*
===============================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
===============================================================================
*/
//
//  File:  board_detect.c
//
#include "bsp.h"
#include "oal_i2c.h"
#include "sdk_i2c.h"
#include "bsp_padcfg.h"
#include "am33x_clocks.h"


#if defined(BUILDING_XLDR_SD)
	#undef  OALMSG(cond, exp)
	#define OALMSG(cond, exp)   ((void)FALSE)
#endif

//-----------------------------------------------------------------------------
//
//  Global:  g_dwBoardId
//
//  Board identifier (hardcoded)
//

const DWORD g_dwBoardId = (DWORD)BOARDID_PERMEATION_CONTROL_BOARD;

//-----------------------------------------------------------------------------
//
//  Global:  g_dwBoardProfile
//
//  Set during OEMInit to indicate Board Profile read from CPLD.
//  Note: Do not modifiy this setting. PROFILE_0 is mandatory and will be ORed later 
//

DWORD g_dwBoardProfile = (DWORD)PROFILE_0;

//-----------------------------------------------------------------------------
//
//  Global:  g_dwBoardHasDcard
//
//  Set during OEMInit to indicate if board has daugther board.
//  Bit field masks:
//  

DWORD g_dwBoardHasDcard = (DWORD)(0); 

//-----------------------------------------------------------------------------
//
//  Global:  g_dwBaseBoardVersion
//
//  Set during OEMInit to indicate the hardware version of the base board.
//

const DWORD g_dwBaseBoardVersion = (DWORD)BOARDVERSION_PERMEATION_CONTROL_PROTOTYPE;

