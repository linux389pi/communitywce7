//
//  File: rtc_ds1307.c
//
//  This file implements device driver for RTC DS1307. 
//
#include "rtc_ds1307_defs.h"
#include "rtc_ds1307_cfg.h"

#include "omap.h"
#include "ceddkex.h"
#include "sdk_padcfg.h"
#include "bsp_padcfg.h"
#include "bsp.h"
#include "sdk_i2c.h"
#include "sdk_gpio.h"

#include <windows.h>
#include <nkintr.h>
#include <winuserm.h>
#include <pmpolicy.h>

// ==== Debug Zones ================================================================================
// simple handling: debug zones are forced with TRUE or FALSE and need a recompile to be activated
// for sophisticated handling: use macro DEBUGZONE(), DBGPARAM dpCurSettings etc.
// (see MSDN e.g. Win CE 5.0, Debug Zones)
#ifndef SHIP_BUILD

#undef ZONE_ERROR
#undef ZONE_WARN
#undef ZONE_FUNCTION
#undef ZONE_INFO
#undef ZONE_TIPSTATE

#define ZONE_ERROR          DEBUGZONE(0)
#define ZONE_WARN           DEBUGZONE(1)
#define ZONE_FUNCTION       DEBUGZONE(2)
#define ZONE_INFO           DEBUGZONE(3)
#define ZONE_TIPSTATE       DEBUGZONE(4)

static DBGPARAM dpCurSettings = {
    L"RTC_DS1307", {
        L"Errors",      L"Warnings",    L"Function",    L"Info",
            L"IST",         L"Undefined",   L"Undefined",   L"Undefined",
            L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined",
            L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined"
    },
    // Bitfield controlling the zones.  1 means the zone is enabled, 0 disabled
    // We'll enable the Error, Warning, and Info zones by default here
    1 | 1 << 1 | 1 << 2 | 1 << 3
};

#endif

// I2C handle
static HANDLE                           m_hI2C;

//----- privat methods definition -----------------------------------------------------------------
// @internal
/// I2C Burst Read - Read Data from one register of an I2C device
/// @param[in]    uSlaveAddress The device's i2c address (0..127) if uSlaveAddress=0xff, no slaveAddress byte will be transmitted.
/// @param[out]   pBuffer Pointer to the data bytes to Receive
/// @param[in]    ucSubAddr The device's register address (offset). This is one byte transmitted after the slave address.
///                Set to any negative value if no offset byte should be transmitted
/// @param[in]    iNumberBytes Number of Bytes to Receive
/// @retval       TRUE success
/// @retval       FALSE Error during Transmission
/// @caveats      none
static BOOL burst_read(UCHAR uSlaveAddress, UCHAR* pBuffer, UCHAR ucSubAddr, DWORD iNumberBytes)
{
    BOOL bResult = FALSE;
	LONG lErr;
    
	if(I2CSetSlaveAddress(m_hI2C, (UINT16)uSlaveAddress))
    {
        // Read bytes
        if((UINT)-1 != I2CRead(m_hI2C,
                               (UCHAR)ucSubAddr,
                               pBuffer,
                               iNumberBytes))
        {
            bResult = TRUE;
        }
        else
        {
            lErr = GetLastError();
        RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in burst_read()->I2CRead %d\r\n"),lErr));
        }
    }
    else
    {
        lErr = GetLastError();
        RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in burst_read()->SetSlaveAddress %d\r\n"),lErr));
    }
    
    return bResult;
}

// @internal
/// I2C Burst Write - Write Data to one register of an I2C device
/// @param[in]    uSlaveAddress The device's i2c address (0..127)
/// @param[in]    pBuffer Pointer to the data bytes to Transmit
/// @param[in]    ucSubAddr The device's register address (offset). This is one byte transmitted after the slave address.
///               Set to any negative value if no offset byte should be transmitted
/// @param[in]    Number of Bytes to Transmit
/// @param[out]   none
/// @retval       TRUE success
/// @retval       FALSE Error during Transmission
/// @caveats      none
static BOOL burst_write(UCHAR uSlaveAddress, UCHAR* pBuffer, UCHAR ucSubAddr, DWORD iNumberBytes)
{
    BOOL bResult = FALSE;
	LONG lErr;
    
	if(I2CSetSlaveAddress(m_hI2C, (UINT16)uSlaveAddress))
    {
        // Read bytes
        if((UINT)-1 != I2CWrite(m_hI2C,
                               (UCHAR)ucSubAddr,
                               pBuffer,
                               iNumberBytes))
        {
            bResult = TRUE;
        }
        else
        {
            lErr = GetLastError();
            RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in burst_write() @ write data %d\r\n"),lErr));
        }
    }
    else
    {
        lErr = GetLastError();
        RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in burst_write() -> SetSlaveAddress %d\r\n"),lErr));
    }
    
    return bResult;
}

// Convert from BCD to binary
static UCHAR bcd_to_bin(UCHAR bcd)
{
    // Formula is : 1st nibble * 10 + 2nd nibble
    // Note 1 : first nibble values are in [0-5]
    // Note 2 : 2nd nibble values are in [0-9]
    // Note 3 : bit 7 is never used, that's why we mask with 0x70
    return ( (bcd & 0x0F) + (((bcd & 0x70) >> 4) * 10) );
}

// Convert from binary to BCD
static UCHAR bin_to_bcd(UCHAR bin)
{
    UCHAR temp; 
    UCHAR retval; 

    temp = bin; 
    retval = 0; 

    while(1) 
    { 
        // Get the tens digit by doing multiple subtraction 
        // of 10 from the binary value. 
        if(temp >= 10) 
        { 
            temp -= 10; 
            retval += 0x10; 
        } 
        else // Get the ones digit by adding the remainder. 
        { 
            retval += temp; 
            break; 
        } 
    } 

    return(retval);
}

//------------------------------------------------------------------------------
DWORD RTC_Init(LPCTSTR szContext, LPCVOID pBusContext)
//  Called by device manager to initialize device.
{
    DWORD rc = (DWORD)NULL;
    UCHAR dataByte = 0x00;

	UNREFERENCED_PARAMETER(pBusContext);

    DEBUGMSG(ZONE_FUNCTION, (L"+RTC_Init(%s, 0x%08x)\r\n", szContext, pBusContext));
    RETAILMSG(1, (L"+RTC_Init\r\n"));

    // Open I2C0 
    m_hI2C = I2COpen(DS1307_I2C_INDEX);

    if (m_hI2C != INVALID_HANDLE_VALUE && m_hI2C != NULL)
    {
        I2CSetBaudIndex(m_hI2C, SLOWSPEED_MODE); // 400 kHz
		I2CSetSubAddressMode(m_hI2C, I2C_SUBADDRESS_MODE_8); // 8 bits address mode

        //
        // Initialize DS1307 chip
        //
        // Read seconds registry first
        if(burst_read(DS1307_I2C_ADDRESS,&dataByte, DS1307_REG_SECONDS, 1U))
        {
            // Now write it back with oscillator bit forced to enable state
            DS1307_ENABLE_OSCILLATOR(dataByte);
            if(burst_write(DS1307_I2C_ADDRESS, &dataByte, DS1307_REG_SECONDS, 1U))
            {
                // Finally, disable output signal (not used)
                dataByte = 0x00;
                DS1307_DISABLE_OUTPUT(dataByte);
                if(burst_write(DS1307_I2C_ADDRESS, &dataByte, DS1307_REG_CONTROL, 1U))
                {
                    // Init successful
                    rc = (DWORD)m_hI2C; // non null value
                }
            }
        }
    }
    else
    {
        DEBUGMSG(ZONE_ERROR, (L"\r\RTC : Failed to open I2C"));
    }

    if (rc == 0)
	{
		RTC_Deinit((DWORD)0);
	}
    DEBUGMSG(ZONE_FUNCTION, (L"-RTC_Init(rc = %d\r\n", rc));

    return rc;
}

//------------------------------------------------------------------------------
BOOL RTC_Deinit(DWORD context)
{
    BOOL rc = TRUE;

	UNREFERENCED_PARAMETER(context);

    DEBUGMSG(ZONE_FUNCTION, (L"+RTC_Deinit()\r\n"));

    // I2C driver
    I2CClose(m_hI2C);

    DEBUGMSG(ZONE_FUNCTION, (L"-RTC_Deinit(rc = %d)\r\n", rc));
    return rc;
}

//------------------------------------------------------------------------------
DWORD RTC_Open(DWORD context, DWORD accessCode, DWORD shareMode)
{
    UNREFERENCED_PARAMETER(context);
    UNREFERENCED_PARAMETER(accessCode);
    UNREFERENCED_PARAMETER(shareMode);
    return context;
}

//------------------------------------------------------------------------------
BOOL RTC_Close(DWORD context)
{
    UNREFERENCED_PARAMETER(context);
    return TRUE;
}

//------------------------------------------------------------------------------
DWORD RTC_Read(DWORD context, LPVOID pBuffer, DWORD Count)
{
    DWORD dwRet = 0;
    SYSTEMTIME* pSysTime = (SYSTEMTIME*)pBuffer;
    UCHAR baRegs[DS1307_TOTAL_REGS_NB];
	DWORD i = 0;

    UNREFERENCED_PARAMETER(context);

    DEBUGMSG(ZONE_FUNCTION, (L"+RTC_Read(0x%08x, 0x%08x, %d)\r\n", context, pBuffer, Count));

    // Ensure that buffer size is correct
    if( pBuffer != NULL
        && Count == sizeof(SYSTEMTIME))
    {
		// Read DS1307 registers as a block
        if(burst_read(DS1307_I2C_ADDRESS, baRegs, DS1307_REG_SECONDS, sizeof(baRegs)))
        {
            // Now extracts all values with conversion from BCD to decimal
            pSysTime->wYear = (2000 + bcd_to_bin(baRegs[DS1307_REG_YEAR]));
            pSysTime->wMonth = bcd_to_bin(baRegs[DS1307_REG_MONTH]);
            pSysTime->wDayOfWeek = baRegs[DS1307_REG_DAY];
            pSysTime->wDay = bcd_to_bin(baRegs[DS1307_REG_DATE]);
            pSysTime->wHour = bcd_to_bin(baRegs[DS1307_REG_HOURS]);
            pSysTime->wMinute = bcd_to_bin(baRegs[DS1307_REG_MINUTES]);
            pSysTime->wSecond = bcd_to_bin(baRegs[DS1307_REG_SECONDS]);
            pSysTime->wMilliseconds = 0;  
            dwRet = sizeof(SYSTEMTIME);
        }
    }
    else
    {
        DEBUGMSG(ZONE_ERROR, (L"\r\RTC_Read :Invalid buffer size"));
    }

    DEBUGMSG(ZONE_FUNCTION, (L"-RTC_Read(dwRet = %d\r\n", dwRet));

    return dwRet;
}

//------------------------------------------------------------------------------
DWORD RTC_Write(DWORD context, LPCVOID pSourceBytes, DWORD NumberOfBytes)
{
    DWORD dwRet = 0;
    SYSTEMTIME* pSysTime = (SYSTEMTIME*)pSourceBytes;
    UCHAR baRegs[DS1307_TOTAL_REGS_NB - 1]; // Do not write control register
	DWORD i = 0;

    UNREFERENCED_PARAMETER(context);

    DEBUGMSG(ZONE_FUNCTION, (L"+RTC_Write(0x%08x, 0x%08x, %d)\r\n", context, pSourceBytes, NumberOfBytes));

    // Ensure that buffer size is correct
    if( pSourceBytes != NULL
        && NumberOfBytes == sizeof(SYSTEMTIME))
    {
        // Prepare all registry values in BCD
        baRegs[DS1307_REG_YEAR] = bin_to_bcd((UCHAR)(pSysTime->wYear - 2000));
        baRegs[DS1307_REG_MONTH] = bin_to_bcd((UCHAR)pSysTime->wMonth);
        baRegs[DS1307_REG_DAY] = (UCHAR)pSysTime->wDayOfWeek;
        baRegs[DS1307_REG_DATE] = bin_to_bcd((UCHAR)pSysTime->wDay);
        baRegs[DS1307_REG_HOURS] = bin_to_bcd((UCHAR)pSysTime->wHour);
        baRegs[DS1307_REG_MINUTES] = bin_to_bcd((UCHAR)pSysTime->wMinute);
        baRegs[DS1307_REG_SECONDS] = bin_to_bcd((UCHAR)pSysTime->wSecond);

        // Write DS1307 registers as a block
        if(burst_write(DS1307_I2C_ADDRESS, baRegs, DS1307_REG_SECONDS, sizeof(baRegs)))
        { 
            dwRet = sizeof(SYSTEMTIME);
        }
    }
    else
    {
        DEBUGMSG(ZONE_ERROR, (L"\r\RTC_Write :Invalid buffer size"));
    }

    DEBUGMSG(ZONE_FUNCTION, (L"-RTC_Write(dwRet = %d\r\n", dwRet));

    return dwRet;
}

//------------------------------------------------------------------------------
BOOL RTC_IOControl(
    DWORD context, DWORD code, BYTE *pInBuffer, DWORD inSize, BYTE *pOutBuffer,
    DWORD outSize, DWORD *pOutSize
) {
    UNREFERENCED_PARAMETER(context);
    UNREFERENCED_PARAMETER(code);
    UNREFERENCED_PARAMETER(pInBuffer);
    UNREFERENCED_PARAMETER(inSize);
    UNREFERENCED_PARAMETER(pOutBuffer);
    UNREFERENCED_PARAMETER(outSize);
    UNREFERENCED_PARAMETER(pOutSize);
    DEBUGMSG(ZONE_INIT, (L"RTC_IOControl"));
    return FALSE;
}

//------------------------------------------------------------------------------
BOOL __stdcall DllMain(HANDLE hDLL, DWORD reason, VOID *pReserved)
{
    UNREFERENCED_PARAMETER(pReserved);
    switch (reason) {
    case DLL_PROCESS_ATTACH:
        DEBUGREGISTER(hDLL);
        DisableThreadLibraryCalls((HMODULE)hDLL);
        break;
    }
    return TRUE;
}