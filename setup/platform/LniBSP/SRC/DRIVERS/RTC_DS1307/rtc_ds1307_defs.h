#ifndef __RTC_DS1307_DEFS_H
#define __RTC_DS1307_DEFS_H

/*--------------------------------------------------------------------------
 * DS1307 I2C SETTINGS
 *-------------------------------------------------------------------------*/
#define DS1307_I2C_ADDRESS      0x68 /* 1101000 */
/*--------------------------------------------------------------------------
 * REGISTER DEFINITIONS
 *-------------------------------------------------------------------------*/
 #define DS1307_REG_SECONDS     0x00
 #define DS1307_REG_MINUTES     0x01
 #define DS1307_REG_HOURS       0x02
 #define DS1307_REG_DAY         0x03
 #define DS1307_REG_DATE        0x04
 #define DS1307_REG_MONTH       0x05
 #define DS1307_REG_YEAR        0x06
 #define DS1307_REG_CONTROL     0x07

 #define DS1307_TOTAL_REGS_NB   0x08
 /*--------------------------------------------------------------------------
 * REGISTER SPECIAL BITS
 *-------------------------------------------------------------------------*/
 #define DS1307_SECONDS_CH       0x80    /* Clock halt bit. If Low, oscillator is enabled. (located in SECONDS registry) */
 #define DS1307_HOURS_12HR       0x40    /* 12/24H select mode in REG_HOUR. If High, 12h mode selected */
 #define DS1307_HOURS_12HR_PM    0x20    /* AM/PM select mode in 12h mode. If high, PM is selected. No use in 24h mode. */
 
 #define DS1307_CONTROL_OUT      0x80    /* Output level of OUT pin when SQWE is disabled. */
 #define DS1307_CONTROL_SQWE     0x10    /* Output oscillator enable bit. If High, enabled. */

 /*--------------------------------------------------------------------------
 * HELPER MACROS
 *-------------------------------------------------------------------------*/
 // Enable oscillator in SECONDS registry
 #define DS1307_ENABLE_OSCILLATOR(seconds_byte) (seconds_byte) = ((seconds_byte) & (~DS1307_SECONDS_CH))
 // Disable output pin
 #define DS1307_DISABLE_OUTPUT(control_byte) (control_byte) = (control_byte) & (~(DS1307_CONTROL_SQWE | DS1307_CONTROL_OUT))

#endif // __RTC_DS1307_DEFS_H