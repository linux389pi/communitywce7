#ifndef __RTC_DS1307_CFG_H
#define __RTC_DS1307_CFG_H

/*--------------------------------------------------------------------------
 * DRIVER CONFIGURATION SETTINGS
 *-------------------------------------------------------------------------*/
#define DS1307_I2C_INDEX    AM_DEVICE_I2C0 // Use I2C0

#endif // __RTC_DS1307_CFG_H