/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/

#pragma warning(push)
#pragma warning(disable: 4127 4201 28251 6054 6388)
//------------------------------------------------------------------------------
// Public
//
#include <windows.h>
#include <nkintr.h>
#include <creg.hxx>
#include <pm.h>

//------------------------------------------------------------------------------
// Platform
//
#include "omap.h"
#include <ceddk.h>
#include <ceddkex.h>
#include <oal.h>
#include <oal_clock.h>
#include <oalex.h>
#include <initguid.h>
#include "ft5x06_cfg.h"
#include "ft5x06Pdd.h"
#include "am33x_clocks.h"
#include "bsp_def.h"
#include "am33x_oal_prcm.h"

//------------------------------------------------------------------------------
// Defines
//


//------------------------------------------------------------------------------
// Debug zones
//
#ifndef SHIP_BUILD

#undef ZONE_ERROR
#undef ZONE_WARN
#undef ZONE_FUNCTION
#undef ZONE_INFO
#undef ZONE_TIPSTATE

#define ZONE_ERROR          DEBUGZONE(0)
#define ZONE_WARN           DEBUGZONE(1)
#define ZONE_FUNCTION       DEBUGZONE(2)
#define ZONE_INFO           DEBUGZONE(3)
#define ZONE_TIPSTATE       DEBUGZONE(4)
#define ZONE_CALIBRATE		DEBUGZONE(5)

static DBGPARAM dpCurSettings = {
    L"FT5X06", {
        L"Errors",      L"Warnings",    L"Function",    L"Info",
            L"IST",         L"Calibrate",   L"Undefined",   L"Undefined",
            L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined",
            L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined"
    },
    // Bitfield controlling the zones.  1 means the zone is enabled, 0 disabled
    // We'll enable the Error, Warning, and Info zones by default here
    1 | 1 << 1 | 1 << 2 | 1 << 3 | 1 << 5
};

#endif

//------------------------------------------------------------------------------
//  Device registry parameters
static const DEVICE_REGISTRY_PARAM s_deviceRegParams[] = {
    {
        L"SampleRate", PARAM_DWORD, FALSE, offset(TOUCH_DEVICE, nSampleRate),
        fieldsize(TOUCH_DEVICE, nSampleRate), (VOID*)DEFAULT_SAMPLE_RATE
    },
    {
        L"Priority256", PARAM_DWORD, FALSE, offset(TOUCH_DEVICE, dwISTPriority),
        fieldsize(TOUCH_DEVICE, dwISTPriority), (VOID*)DEFAULT_THREAD_PRIORITY
    },
    {
        L"SysIntr", PARAM_DWORD, FALSE, offset(TOUCH_DEVICE, dwSysIntr),
        fieldsize(TOUCH_DEVICE, dwSysIntr), (VOID*)SYSINTR_NOP
    },
};

//------------------------------------------------------------------------------
// global variables
//
static TOUCH_DEVICE s_TouchDevice =  {
    FALSE,                                          //bInitialized
	NULL,
    DEFAULT_SAMPLE_RATE,                            //nSampleRate
    0,                                              //nInitialSamplesDropped
    0,                                              //PenUpDebounceMS
    SYSINTR_NOP,                                    //dwSysIntr
    0,                                              //dwSamplingTimeOut
    FALSE,                                          //bTerminateIST
    0,                                              //hTouchPanelEvent
    D0,                                             //dwPowerState
    0,                                              //hIST
    0,                                              //nPenIRQ
    DEFAULT_THREAD_PRIORITY                         //dwISTPriority
};

static DWORD s_mddContext;
static PFN_TCH_MDD_REPORTSAMPLESET    s_pfnMddReportSampleSet;

//==============================================================================
// Function Name: TchPdd_Init
//
// Description: PDD should always implement this function. MDD calls it during
//              initialization to fill up the function table with rest of the
//              Helper functions.
//
// Arguments:
//              [IN] pszActiveKey - current active touch driver key
//              [IN] pMddIfc - MDD interface info
//              [OUT]pPddIfc - PDD interface (the function table to be filled)
//              [IN] hMddContext - mdd context (send to MDD in callback fn)
//
// Ret Value:   pddContext.
//==============================================================================
extern "C" DWORD WINAPI TchPdd_Init(LPCTSTR pszActiveKey,
    TCH_MDD_INTERFACE_INFO* pMddIfc,
    TCH_PDD_INTERFACE_INFO* pPddIfc,
    DWORD hMddContext
    )
{
    DWORD pddContext = 0;

    RETAILMSG(ZONE_FUNCTION, (TEXT("TchPdd_Init+\r\n")));

    // Initialize once only
    if (s_TouchDevice.bInitialized)
    {
        pddContext = (DWORD) &s_TouchDevice;
        goto cleanUp;
    }

    // Remember the callback function pointer
    s_pfnMddReportSampleSet = pMddIfc->pfnMddReportSampleSet;

    // Remember the mdd context
    s_mddContext = hMddContext;

    s_TouchDevice.nSampleRate = DEFAULT_SAMPLE_RATE;
    s_TouchDevice.dwPowerState = D0;
    s_TouchDevice.dwSamplingTimeOut = SAMPLING_TIMEOUT; /* @todo was INFINITE. Make it configurable from registry */
    s_TouchDevice.bTerminateIST = FALSE;
    s_TouchDevice.hTouchPanelEvent = NULL;

    // Initialize HW
    if (!PDDInitializeHardware(pszActiveKey))
    {
        RETAILMSG(ZONE_ERROR,  (TEXT("ERROR: TOUCH: Failed to initialize touch PDD.\r\n")));
        goto cleanUp;
    }
    
    // Save the Sys Interrupt ID for later
    s_TouchDevice.dwSysIntr = FT5X06_GetSysIntr();
    // Save the OS Event that will be raised on touch inputs
    s_TouchDevice.hTouchPanelEvent = FT5X06_GetEvent();

    //Calibrate the screen, if the calibration data is not already preset in the registry
    //PDDStartCalibrationThread();

    pddContext = (DWORD) &s_TouchDevice;

    // fill up pddifc table
    pPddIfc->version        = 1;
    pPddIfc->pfnDeinit      = TchPdd_Deinit;
    pPddIfc->pfnIoctl       = TchPdd_Ioctl;
    pPddIfc->pfnPowerDown   = TchPdd_PowerDown;
    pPddIfc->pfnPowerUp     = TchPdd_PowerUp;

	//Initialization of the h/w is done
    s_TouchDevice.bInitialized = TRUE;

cleanUp:
    RETAILMSG(ZONE_FUNCTION, (TEXT("TchPdd_Init-\r\n")));
    return pddContext;
}


//==============================================================================
// Function Name: TchPdd_DeInit
//
// Description: MDD calls it during deinitialization. PDD should deinit hardware
//              and deallocate memory etc.
//
// Arguments:   [IN] hPddContext. pddcontext returned in TchPdd_Init
//
// Ret Value:   None
//
//==============================================================================
void WINAPI TchPdd_Deinit(DWORD hPddContext)
{
    RETAILMSG(ZONE_FUNCTION, (TEXT("TchPdd_Deinit+\r\n")));

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hPddContext);

    // Close the IST and release the resources before
    // de-initializing.
    PDDTouchPanelDisable();

    // Release interrupt
    if (s_TouchDevice.dwSysIntr != 0)
        {
        KernelIoControl(
            IOCTL_HAL_RELEASE_SYSINTR,
            &s_TouchDevice.dwSysIntr,
            sizeof(s_TouchDevice.dwSysIntr),
            NULL,
            0,
            NULL
            );
        }

    s_TouchDevice.bInitialized = FALSE;


    RETAILMSG(ZONE_FUNCTION, (TEXT("TchPdd_Deinit-\r\n")));
}


//==============================================================================
// Function Name: TchPdd_Ioctl
//
// Description: The MDD controls the touch PDD through these IOCTLs.
//
// Arguments:   [IN] hPddContext. pddcontext returned in TchPdd_Init
//              DOWRD dwCode. IOCTL code
//              PBYTE pBufIn. Input Buffer pointer
//              DWORD dwLenIn. Input buffer length
//              PBYTE pBufOut. Output buffer pointer
//              DWORD dwLenOut. Output buffer length
//              PWORD pdwAcutalOut. Actual output buffer length.
//
// Ret Value:   TRUE if success else FALSE. SetLastError() if FALSE.
//==============================================================================
BOOL WINAPI TchPdd_Ioctl(DWORD hPddContext,
      DWORD dwCode,
      PBYTE pBufIn,
      DWORD dwLenIn,
      PBYTE pBufOut,
      DWORD dwLenOut,
      PDWORD pdwActualOut
)
{
    DWORD dwResult = ERROR_INVALID_PARAMETER;

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hPddContext);

    switch (dwCode)
    {
        //  Enable touch panel
        case IOCTL_TOUCH_ENABLE_TOUCHPANEL:
            RETAILMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_TOUCH_ENABLE_TOUCHPANEL\r\n"));
            PDDTouchPanelEnable();
            dwResult = ERROR_SUCCESS;
            break;

        //  Disable touch panel
        case IOCTL_TOUCH_DISABLE_TOUCHPANEL:
            RETAILMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_TOUCH_DISABLE_TOUCHPANEL\r\n"));
            PDDTouchPanelDisable();
            dwResult = ERROR_SUCCESS;
            break;


        //  Get current sample rate
        case IOCTL_TOUCH_GET_SAMPLE_RATE:
            RETAILMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_TOUCH_GET_SAMPLE_RATE\r\n"));

            //  Check parameter validity
            if ((pBufOut != NULL) && (dwLenOut == sizeof(DWORD)))
            {
                if (pdwActualOut)
                    *pdwActualOut = sizeof(DWORD);

                //  Get the sample rate
                *((DWORD*)pBufOut) = s_TouchDevice.nSampleRate;
                dwResult = ERROR_SUCCESS;
            }
            break;

        //  Set the current sample rate
        case IOCTL_TOUCH_SET_SAMPLE_RATE:
            RETAILMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_TOUCH_SET_SAMPLE_RATE\r\n"));

            //  Check parameter validity
            if ((pBufIn != NULL) && (dwLenIn == sizeof(DWORD)))
            {
                //  Set the sample rate
                s_TouchDevice.nSampleRate = *((DWORD*)pBufIn);
                dwResult = ERROR_SUCCESS;

            }
            break;

        //  Get touch properties
        case IOCTL_TOUCH_GET_TOUCH_PROPS:
            //  Check parameter validity
            if ((pBufOut != NULL) && (dwLenOut == sizeof(TCH_PROPS)))
            {
                if (pdwActualOut)
                    *pdwActualOut = sizeof(TCH_PROPS);\

                //  Fill out the touch driver properties
                ((TCH_PROPS*)pBufOut)->minSampleRate            = TOUCHPANEL_SAMPLE_RATE_LOW;
                ((TCH_PROPS*)pBufOut)->maxSampleRate            = TOUCHPANEL_SAMPLE_RATE_HIGH;
                ((TCH_PROPS*)pBufOut)->minCalCount              = 5;
                ((TCH_PROPS*)pBufOut)->maxSimultaneousSamples   = 1;
                ((TCH_PROPS*)pBufOut)->touchType                = TOUCHTYPE_SINGLETOUCH; /* @todo switch to TOUCHTYPE_MULTITOUCH */
                ((TCH_PROPS*)pBufOut)->calHoldSteadyTime        = CAL_HOLD_STEADY_TIME;
                ((TCH_PROPS*)pBufOut)->calDeltaReset            = CAL_DELTA_RESET;
                ((TCH_PROPS*)pBufOut)->xRangeMin                = RANGE_MIN;
                ((TCH_PROPS*)pBufOut)->xRangeMax                = RANGE_MAX;
                ((TCH_PROPS*)pBufOut)->yRangeMin                = RANGE_MIN;
                ((TCH_PROPS*)pBufOut)->yRangeMax                = RANGE_MAX;

                dwResult = ERROR_SUCCESS;
            }
            break;

        //  Power management IOCTLs
        case IOCTL_POWER_CAPABILITIES:
            RETAILMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_POWER_CAPABILITIES\r\n"));
            if (pBufOut != NULL && dwLenOut == sizeof(POWER_CAPABILITIES))
            {
                PPOWER_CAPABILITIES ppc = (PPOWER_CAPABILITIES) pBufOut;
                memset(ppc, 0, sizeof(*ppc));
                ppc->DeviceDx = DX_MASK(D0) | DX_MASK(D1) | DX_MASK(D2) | DX_MASK(D3) | DX_MASK(D4);

                if (pdwActualOut)
                    *pdwActualOut = sizeof(POWER_CAPABILITIES);

                dwResult = ERROR_SUCCESS;
            }
            break;

        case IOCTL_POWER_GET:
            RETAILMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_POWER_GET\r\n"));
            if(pBufOut != NULL && dwLenOut == sizeof(CEDEVICE_POWER_STATE))
            {
                *(PCEDEVICE_POWER_STATE) pBufOut = (CEDEVICE_POWER_STATE) s_TouchDevice.dwPowerState;

                if (pdwActualOut)
                    *pdwActualOut = sizeof(CEDEVICE_POWER_STATE);

                dwResult = ERROR_SUCCESS;
            }
            break;

        case IOCTL_POWER_SET:
            RETAILMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_POWER_SET\r\n"));
            if(pBufOut != NULL && dwLenOut == sizeof(CEDEVICE_POWER_STATE))
            {
                CEDEVICE_POWER_STATE dx = *(CEDEVICE_POWER_STATE*)pBufOut;
                if( VALID_DX(dx) )
                {
                    RETAILMSG(ZONE_FUNCTION, (L"TchPdd_Ioctl: IOCTL_POWER_SET = to D%u\r\n", dx));

                    if (pdwActualOut)
                        *pdwActualOut = sizeof(CEDEVICE_POWER_STATE);

                    //  Enable touchscreen for D0-D2; otherwise disable
                    switch( dx )
                    {
                        case D0:
                        case D1:
                        case D2:
                            //  Enable touchscreen
                            if(s_TouchDevice.dwPowerState==D3 ||
								s_TouchDevice.dwPowerState==D4 )
                                PDDTouchPanelPowerHandler(FALSE);    //Enable Touch panel
                            break;

                        case D3:
                        case D4:
                            //  Disable touchscreen
                            if(s_TouchDevice.dwPowerState==D0 ||
            				   s_TouchDevice.dwPowerState==D1 ||
            				   s_TouchDevice.dwPowerState==D2 )
                                PDDTouchPanelPowerHandler(TRUE); //Disable Touch panel
                            break;

                        default:
                            //  Ignore
                            break;
                    }
                    s_TouchDevice.dwPowerState = dx;

                    dwResult = ERROR_SUCCESS;
                }
            }
            break;

        default:
            dwResult = ERROR_NOT_SUPPORTED;
            break;
    }

    if (dwResult != ERROR_SUCCESS)
    {
        SetLastError(dwResult);
        return FALSE;
    }
    return TRUE;
}


//==============================================================================
// Function Name: TchPdd_PowerUp
//
// Description: MDD passes xxx_PowerUp stream interface call to PDD.
//
// Arguments:   [IN] hPddContext. pddcontext returned in TchPdd_Init
//
// Ret Value:   None
//==============================================================================
void WINAPI TchPdd_PowerUp(
    DWORD hPddContext
    )
{
    RETAILMSG(ZONE_FUNCTION, (TEXT("TchPdd_PowerUp+\r\n")));
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hPddContext);
    RETAILMSG(ZONE_FUNCTION, (TEXT("TchPdd_PowerUp-\r\n")));
}

//==============================================================================
// Function Name: TchPdd_PowerDown
//
// Description: MDD passes xxx_PowerDown stream interface call to PDD.
//
// Arguments:   [IN] hPddContext. pddcontext returned in TchPdd_Init
//
// Ret Value:   None
//==============================================================================
void WINAPI TchPdd_PowerDown(
    DWORD hPddContext
    )
{
    RETAILMSG(ZONE_FUNCTION, (TEXT("TchPdd_PowerDown+\r\n")));
    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(hPddContext);
    RETAILMSG(ZONE_FUNCTION, (TEXT("TchPdd_PowerDown-\r\n")));
}

//==============================================================================
//Internal Functions
//==============================================================================
//==============================================================================
// Function Name: PDDCalibrationThread
//
// Description: This function is called from the PDD Init to calibrate the screen.
//              If the calibration data is already present in the registry,
//              this step is skipped, else a call is made into the GWES for calibration.
//
// Arguments:   None.
//
// Ret Value:   Success(1), faliure(0)
//==============================================================================
static HRESULT PDDCalibrationThread()
{
    HKEY hKey;
    DWORD dwType;
    LONG lResult;
    HANDLE hAPIs;
	BOOL ret;

    RETAILMSG(ZONE_FUNCTION, (TEXT("CalibrationThread+\r\n")));

    // try to open [HKLM\hardware\devicemap\touch] key
    if (ERROR_SUCCESS != RegOpenKeyEx(HKEY_LOCAL_MACHINE, RK_HARDWARE_DEVICEMAP_TOUCH, 0, KEY_ALL_ACCESS, &hKey))
    {
        RETAILMSG(ZONE_CALIBRATE, (TEXT("CalibrationThread: calibration: Can't find [HKLM/%s]\r\n"), RK_HARDWARE_DEVICEMAP_TOUCH));
        return E_FAIL;
    }

    // check for calibration data (query the type of data only)
    lResult = RegQueryValueEx(hKey, RV_CALIBRATION_DATA, 0, &dwType, NULL, NULL);
    RegCloseKey(hKey);
    if (lResult == ERROR_SUCCESS)
    {
        // registry contains calibration data, return
        return S_OK;
    }

    hAPIs = OpenEvent(EVENT_ALL_ACCESS, FALSE, TEXT("SYSTEM/GweApiSetReady"));
    if (hAPIs)
    {
        WaitForSingleObject(hAPIs, INFINITE);
        CloseHandle(hAPIs);
    }

    // Perform calibration
    ret = TouchCalibrate();

    // try to open [HKLM\hardware\devicemap\touch] key
    if (ERROR_SUCCESS != RegOpenKeyEx(HKEY_LOCAL_MACHINE, RK_HARDWARE_DEVICEMAP_TOUCH, 0, KEY_ALL_ACCESS, &hKey))
    {
        RETAILMSG(ZONE_CALIBRATE, (TEXT("CalibrationThread: calibration: Can't find [HKLM/%s]\r\n"), RK_HARDWARE_DEVICEMAP_TOUCH));
        return E_FAIL;
    }

    // display new calibration data
    lResult = RegQueryValueEx(hKey, RV_CALIBRATION_DATA, 0, &dwType, NULL, NULL);
    if (lResult == ERROR_SUCCESS)
    {
        TCHAR szCalibrationData[100];
        DWORD Size = sizeof(szCalibrationData);

        RegQueryValueEx(hKey, RV_CALIBRATION_DATA, 0, &dwType, (BYTE *) szCalibrationData, (DWORD *) &Size);
        RETAILMSG(ZONE_CALIBRATE, (TEXT("touchp: calibration: new calibration data is \"%s\"\r\n"), szCalibrationData));
    }
    RegCloseKey(hKey);

    RETAILMSG(ZONE_FUNCTION, (TEXT("CalibrationThread-\r\n")));

    return S_OK;
}


//==============================================================================
// Function Name: PDDStartCalibrationThread
//
// Description: This function is creates the calibration thread with
//              PDDCalibrationThread as entry.
//
// Arguments:   None.
//
// Ret Value:   None
//==============================================================================
void PDDStartCalibrationThread()
{
    HANDLE hThread;

    hThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)PDDCalibrationThread, NULL, 0, NULL);
    // We don't need the handle, close it here
    CloseHandle(hThread);
}

//==============================================================================
// Function: PDDTouchPanelPowerHandler
//
// This function indicates to the driver that the system is entering
// or leaving the suspend state.
//
// Parameters:
//      bOff
//          [in] TRUE indicates that the system is turning off. FALSE
//          indicates that the system is turning on.
//
// Returns:
//      None.
//==============================================================================
void PDDTouchPanelPowerHandler(BOOL boff)
{
    RETAILMSG(ZONE_FUNCTION, (_T("PDDTouchPanelPowerHandler+\r\n")));

    if (s_TouchDevice.dwSysIntr != SYSINTR_NOP)
	{
        InterruptMask(s_TouchDevice.dwSysIntr, boff);
	}

    RETAILMSG(ZONE_FUNCTION, (_T("PDDTouchPanelPowerHandler-\r\n")));

    return;
}

//==============================================================================
// Function Name: PDDTouchIST
//
// Description: This is the IST which waits on the touch event or Time out value.
//              Normally the IST waits on the touch event infinitely, but once the
//              pen down condition is recognized the time out interval is changed
//              to dwSamplingTimeOut.
//
// Arguments:
//                  PVOID - Reseved not currently used.
//
// Ret Value:   None
//==============================================================================
ULONG PDDTouchIST(PVOID reserved)
{
    CETOUCHINPUT                input[2]; /* @todo change to array to support multitouch ! */
    INT                         inputCount; // Number of touch inputs

    // Remove-W4: Warning C4100 workaround
    UNREFERENCED_PARAMETER(reserved);

    RETAILMSG(ZONE_TOUCH_IST, (L"PDDTouchIST: IST thread started\r\n"));

    //  Loop until told to stop
    while(!s_TouchDevice.bTerminateIST)
    {
		//  Wait for touch event, timeout or terminate flag
       if(TRUE == FT5X06_WaitTouchInterrupt(s_TouchDevice.dwSamplingTimeOut))
       {
            // get the touch events
            FT5X06_GetPoint((PTCHINPUT)input, &inputCount);

            // Filter invalid events TouchSampleDownFlag TouchSampleValidFlag
            if(inputCount && input[0].dwFlags != 0)
            {
                // @todo the following line adds a filter to take 1 touch only in account
                //send this 1 sample to mdd
                s_pfnMddReportSampleSet(s_mddContext, 1, (const CETOUCHINPUT *)&input[0]);
            }
            else
            {
                continue; // Ignore this event
            }
       }
       // else : Timeout !
       else
       {
		   if (FALSE==FT5x06_TestCommunication())                  // timeout, test the communication to the touch controller
           {
               RETAILMSG(ZONE_TOUCH_IST,(TEXT("PDDTouchIST: restarts communication with touch controller...\r\n")));

               FT5X06_Exit();                                                // stop (reset/powerdown) the Touch Controller

               Sleep(400);                                                     // wait before an new try starts

			   FT5X06_Init();													// re-init chip
           }
       }
    }

    RETAILMSG(ZONE_TOUCH_IST, (L"PDDTouchIST: IST thread ending\r\n"));

    return ERROR_SUCCESS;
}

//==============================================================================
// Function Name: PDDInitializeHardware
//
// Description: This routine configures the FT5X06 driver.
//
// Arguments:  None
//
// Ret Value:   TRUE - Success
//                   FAIL - Failure
//==============================================================================
BOOL PDDInitializeHardware(LPCTSTR pszActiveKey)
{
    BOOL   rc = FALSE;

    UNREFERENCED_PARAMETER(pszActiveKey); /* @todo Unused for now */
    
    RETAILMSG(ZONE_FUNCTION, (TEXT("PDDInitializeHardware+\r\n")));

    // Read the and setup the configuration
    FT5X06_ReadConfiguration();                                           // read static global configuration parameters (default or set in the regitry)
    
    // Init the I2C interface
    if (FALSE==FT5X06_HardwareInit())                                             // init I2C communication and setup interrupt handling
    {
        RETAILMSG(ZONE_ERROR,(TEXT("aborted\r\n")));                      // FT5X06_HardwareInit() printed the detail of the failure already
        goto cleanUp;
    }
    
    //------ Startup Procedure --------------------------------------------------------------------
    RETAILMSG(ZONE_INFO,(TEXT("MultiTchHwAdapt: Start communication with the touch controller FT5x06\r\n")));
    
    // Init FT5X06 chip
    if (FALSE==FT5X06_Init())                                       // reset and start touch controller
    {
        RETAILMSG(ZONE_ERROR,(TEXT("aborted\r\n")));
        goto cleanUp;
    }

	// Add interrupts to wakeup sources in ftx506 ???
	/*if(!KernelIoControl(
		IOCTL_HAL_ENABLE_WAKE,
		&s_TouchDevice.dwSysIntr,
		sizeof(s_TouchDevice.dwSysIntr),
		NULL,
		0,
		NULL ))
	{
		RETAILMSG(ZONE_ERROR, (TEXT("ERROR: TOUCH: Failed to register sysintr as wake-up source!\r\n")));
		goto cleanUp;
	}*/

    // Done
    RETAILMSG(ZONE_INFO,(TEXT("done\r\n")));
    rc = TRUE;

cleanUp:

    RETAILMSG(ZONE_FUNCTION, (TEXT("PDDInitializeHardware-\r\n")));
    if( rc == FALSE )
    {
        PDDDeinitializeHardware();
    }

    return rc;
}


//==============================================================================
// Function Name: PDDDeinitializeHardware
//
// Description: This routine Deinitializes the h/w by closing the touch channels
//
// Arguments:  None
//
// Ret Value:   None
//==============================================================================
VOID PDDDeinitializeHardware()
{
    RETAILMSG(ZONE_FUNCTION, (TEXT("PDDDeinitializeHardware+\r\n")));
    
    FT5X06_HardwareExit();                                                  // release anything active already
    
    RETAILMSG(ZONE_FUNCTION, (TEXT("PDDDeinitializeHardware-\r\n")));
}


//==============================================================================
// Function Name: PDDTouchPanelEnable
//
// Description: This routine creates the touch thread(if it is not already created)
//              initializes the interrupt and unmasks it.
//
// Arguments:  None
//
// Ret Value:   TRUE - Success
//==============================================================================
BOOL  PDDTouchPanelEnable()
{
    RETAILMSG(ZONE_FUNCTION, (TEXT("PDDTouchPanelEnable+\r\n")));

    //  Check if already running
    if( s_TouchDevice.hIST == NULL )
    {
        //  Clear terminate flag
        s_TouchDevice.bTerminateIST = FALSE;

        //  Create IST thread
       s_TouchDevice.hIST = CreateThread( NULL, 0, PDDTouchIST, 0, 0, NULL );
        if( s_TouchDevice.hIST == NULL )
        {
            RETAILMSG(ZONE_ERROR, (TEXT("PDDTouchPanelEnable: Failed to create IST thread\r\n")));
            return FALSE;
        }

        // set IST thread priority
        CeSetThreadPriority (s_TouchDevice.hIST, s_TouchDevice.dwISTPriority);

        if (s_TouchDevice.dwSysIntr != SYSINTR_NOP)
		{
            InterruptMask(s_TouchDevice.dwSysIntr, FALSE);
		}
    }

    RETAILMSG(ZONE_FUNCTION, (TEXT("PDDTouchPanelEnable-\r\n")));
    return TRUE;
}

//==============================================================================
// Function Name: PDDTouchPanelDisable
//
// Description: This routine closes the IST and releases other resources.
//
// Arguments:  None
//
// Ret Value:   TRUE - Success
//==============================================================================
VOID  PDDTouchPanelDisable()
{
    RETAILMSG(ZONE_FUNCTION, (TEXT("PDDTouchPanelDisable+\r\n")));

    //  Disable touch interrupt service thread
    if( s_TouchDevice.hTouchPanelEvent )
    {
        s_TouchDevice.bTerminateIST = TRUE;
        SetEvent( s_TouchDevice.hTouchPanelEvent );
        
        PDDDeinitializeHardware();
        s_TouchDevice.hTouchPanelEvent = NULL;
    }

    //Closing IST handle
    if(s_TouchDevice.hIST)
    {
        CloseHandle(s_TouchDevice.hIST);
        s_TouchDevice.hIST=NULL;
     }

    RETAILMSG(ZONE_FUNCTION, (TEXT("PDDTouchPanelDisable-\r\n")));
}

BOOL WINAPI DllEntry(HANDLE hinstDll, DWORD dwReason, LPVOID lpvReserved)
{
	switch(dwReason)
	{
		case DLL_PROCESS_ATTACH:
		{
			RETAILREGISTERZONES((HINSTANCE)hinstDll);
			RETAILMSG(ZONE_INIT, (_T("FT5X06 Process attach\r\n")));
			DisableThreadLibraryCalls((HMODULE)hinstDll);
		}
		break;
		case DLL_PROCESS_DETACH:
		{
			RETAILMSG(ZONE_INIT, (_T("FT5X06 Process detach\r\n")));
		}
		break;
	}
	return TRUE;
}
