//------------------------------------------------------------------------------------------------------------------------------------
// @copyright Copyright (c) 2012 Toradex AG \n
//             All information contained herein is, and remains the property of Toradex Ag and its suppliers, if any. The intellectual\n
//             and technical concepts contained herein are proprietary to Toradex AG and its suppliers and may be covered by Swiss and\n
//             foreign patents, patents in process, and are protected by trade secret or copyright law. Dissemination of this information\n
//             or reproduction of this material is strictly forbidden unless prior written permission is obtained from Toradex AG.\n
//
// do not change any thing in this file !!
// (in case of an modification the Unified Multi-Touch Driver must be modified and tested too !)

#ifndef __UNFD_MULTITCHDRV_H
#define __UNFD_MULTITCHDRV_H

//------------------------------------------------------------------------------
// Touch Input defines
// Because of the lack of the header file "tchddi.h" CETOUCHINPUT and PCETOUCHINPUT
// are missing. To avoid type conflicts TCHINPUT, PTCHINPUT are defined and used
// instead CETOUCHINPUT and PCETOUCHINPUT.
// TCHINPUT,PTCHINPUT must be identical to CETOUCHINPUT,PCETOUCHINPUT
typedef struct {
    // Specifies the x coordinate of the touch point in 4ths of a pixel.
    LONG x;

    // Specifies the y coordinate of the touch point in 4ths of a pixel.
    LONG y;

    // Identifier of the touch input source. Reserved for use by the touch driver.
    HANDLE hSource;

    // Touch point identifier - this is the touch contact index. This ID must be
    // maintained for a contact from the time it goes down until the time it
    // goes up.
    DWORD dwID;

    // A set of bit flags that specify various aspects of touch point press /
    // release and motion.
    DWORD dwFlags;

    // A set of bit flags that specify which of the optional fields in the
    // structure contain valid values.
    DWORD dwMask;

    // Time stamp for the event, in milliseconds. If this parameter is 0,
    // the sample will be timestamped by GWES enroute to the gesture engine
    // and the TOUCHEVENTMASKF_TIMEFROMSYSTEM flag will be set in dwMask.
    DWORD dwTime;

    // Specifies the width of the touch contact area in 4ths of a pixel.
    DWORD cxContact;

    // Specifies the height of the touch contact area in 4ths of a pixel.
    DWORD cyContact;

    // Offset to an additional property structure which is associated
    // with this specific touch point. The offset is in bytes from
    // the begining of this TCHINPUT structure.
    DWORD dwPropertyOffset;

    // Size in bytes of the additional property structure.
    DWORD cbProperty;
} TCHINPUT, *PTCHINPUT;


//------------------------------------------------------------------------------
// custom IOCTL codes
//
#define IOCTL_FIRST                4000                                    // First custom IOCTL code
#define IOCTL_LAST                4095                                    // Last custom IOCTL code
#define IOCTL_SET_TOUCH_EVENT    CTL_CODE(FILE_DEVICE_HAL, IOCTL_FIRST+0,  METHOD_NEITHER, FILE_ANY_ACCESS)
//------------------------------------------------------------------------------

// IOCTL struct for touch sample
#define MAXTOUCHES                5

typedef struct
{
    TCHINPUT    TchInp[MAXTOUCHES];
    INT            iCnt;
}CETCHINP_IOCTL;


#endif
