/*
================================================================================
*             Texas Instruments OMAP(TM) Platform Software
* (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
*
* Use of this software is controlled by the terms and conditions found
* in the license agreement under which this software has been supplied.
*
================================================================================
*/

#ifndef __FT5X06_PDD_H
#define __FT5X06_PDD_H

#include <tchddsi.h>
#include <tchstream.h>
#include <tchstreamddsi.h>
#include "ft5x06.h"

//------------------------------------------------------------------------------
// local data structures
//

typedef struct
{
    BOOL        bInitialized;
	HANDLE		hReadTouchSampleBufferQueue;
    DWORD       nSampleRate;
    DWORD       nInitialSamplesDropped;
    DWORD       PenUpDebounceMS;
    DWORD       dwSysIntr;
    DWORD       dwSamplingTimeOut;
    BOOL        bTerminateIST;
    HANDLE      hTouchPanelEvent;
    DWORD       dwPowerState;
    HANDLE      hIST;
    LONG        nPenIRQ;
    DWORD       dwISTPriority;
}TOUCH_DEVICE;

// Internal functions.
static HRESULT PDDCalibrationThread();
void PDDStartCalibrationThread();

BOOL PDDGetTouchIntPinState( VOID );
BOOL PDDGetTouchData(UINT32 * pxPos, UINT32 * pyPos);
BOOL PDDGetRegistrySettings( PDWORD );
BOOL PDDInitializeHardware(LPCTSTR pszActiveKey );
VOID PDDDeinitializeHardware( VOID );
VOID PDDTouchPanelDisable();
BOOL PDDTouchPanelEnable();
ULONG PDDTouchIST(PVOID   reserved);
void PDDTouchPanelPowerHandler(BOOL boff);

//TCH PDD DDSI functions
extern "C" DWORD WINAPI TchPdd_Init(
    LPCTSTR pszActiveKey,
    TCH_MDD_INTERFACE_INFO* pMddIfc,
    TCH_PDD_INTERFACE_INFO* pPddIfc,
    DWORD hMddContext
    );

void WINAPI TchPdd_Deinit(DWORD hPddContext);
void WINAPI TchPdd_PowerUp(DWORD hPddContext);
void WINAPI TchPdd_PowerDown(DWORD hPddContext);
BOOL WINAPI TchPdd_Ioctl(
    DWORD hPddContext,
    DWORD dwCode,
    PBYTE pBufIn,
    DWORD dwLenIn,
    PBYTE pBufOut,
    DWORD dwLenOut,
    PDWORD pdwActualOut
    );

#endif // __FT5X06_PDD_H
