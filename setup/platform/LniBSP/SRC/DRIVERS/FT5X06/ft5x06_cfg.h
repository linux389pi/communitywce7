/// @file       ft5x06_cfg.h
/// @brief      Static configuration for FT5X06 driver
/// @caveats

#ifndef __FT5X06_CFG_H__
#define __FT5X06_RCFG_H__

/*== Hardware Settings =================================================*/

#define FT5X06_I2C_INDEX                  AM_DEVICE_I2C1                  // Use I2C0 for the touch management 
#define FT5X06_I2C_ADDR                   0x38                            // I2C address of the FT5x06 chip

// Interrup Pin
#define FT5X06_INT_GPIO_PIN               GPIO0_27                        // default GPIO pin for the "INT Line" @todo move to config !
#define FT5X06_INT_SIG_INV                0                               // 0 => INT signal, Falling Edge is interrupt 
// Reset Pin                                                                        // >0 => INT signal Rising Edge is interrupt
#define FT5X06_RESET_GPIO_PIN             GPIO3_21                        // default GPIO pin for the "RESET Line" @todo move to config !
#define FT5X06_RESET_POSTDLY              300                             // delay after the "RESET Line" is released
#define FT5X06_RESET_LIN_INV              1                               // 0 => "RESET Line" reset active is High and reset off is Low
                                                                        // >0 => "RESET line" reset active is Low and reset off is High

/*== Pdd Settings =================================================*/
//------------------------------------------------------------------------------
// Sample rate during polling.
#define DEFAULT_SAMPLE_RATE                 200               // Hz.
#define TOUCHPANEL_SAMPLE_RATE_LOW          DEFAULT_SAMPLE_RATE
#define TOUCHPANEL_SAMPLE_RATE_HIGH         DEFAULT_SAMPLE_RATE
#define DEFAULT_THREAD_PRIORITY             40 // @@CSo was 109 before

// timeout wait for communication test (ms)
#define SAMPLING_TIMEOUT                    5000                

#define RK_HARDWARE_DEVICEMAP_TOUCH     (TEXT("HARDWARE\\DEVICEMAP\\TOUCH"))
#define RV_CALIBRATION_DATA             (TEXT("CalibrationData"))

#define CAL_DELTA_RESET             20
#define CAL_HOLD_STEADY_TIME        1500
#define RANGE_MIN                   0
#define RANGE_MAX                   4096                                                                        

#endif //  __FT5X06_CFG_H__

