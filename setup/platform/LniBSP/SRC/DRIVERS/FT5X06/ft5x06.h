/// @file       TchContr_FT5x06.h
/// @copyright  Copyright (c) 2012 Toradex AG \n
///             All information contained herein is, and remains the property of Toradex Ag and its suppliers, if any. The intellectual\n
///             and technical concepts contained herein are proprietary to Toradex AG and its suppliers and may be covered by Swiss and\n
///             foreign patents, patents in process, and are protected by trade secret or copyright law. Dissemination of this information\n
///             or reproduction of this material is strictly forbidden unless prior written permission is obtained from Toradex AG.\n
/// @author     $Author$
/// @version    $Rev$
/// @date       $Date$
/// @brief      Function library for the touch controller FT5x06
///             GeneralError concept:\n
///             Every function, witch can detect errors, has as a return value BOOL ->  TRUE = ok, FALSE = error
///             The Details of the error itself is logged to the debug port.
/// @target     Colibri PXA270, PX3xx, T20 WinCE6, WinEC7
/// @caveats

#ifndef __TCHCONTRFT5X06_H__
#define __TCHCONTRFT5X06_H__

#include "unfd_multitchdrv.h"

//----- public methods declaration ------------------------------------------------------------------
/// Set the configuration data according to the registry entry or to the default value
/// These data are "INT port number", "RESET port number", "I2C slave address", "I2C speed",
/// The values are set to default or set by the entry in the registry (if a valid entry exists)
/// @param[in]    none
/// @param[out]   none
/// @retval       none
/// @caveats      none
void FT5X06_ReadConfiguration(void);

/// Init the I2C communication and the GIPO for the "INT" and "RST" pin of the touch controller
/// @param[in]    none
/// @param[out]   none
/// @retval       TRUE  success
/// @retval       FALSE error, a message to the console shows the reason
/// @caveats      vI2C_ReadConfiguration() has to be executed
BOOL FT5X06_HardwareInit(void);

/// Stop the I2C communication (release interrupt, mutex, stop the I2C driver etc)
/// @param[in]    none
/// @param[out]   none
/// @retval       none
/// @retval       none
/// @caveats      none
void FT5X06_HardwareExit(void);

/// Returns the configured Sys interrupt for the touch driver
DWORD FT5X06_GetSysIntr(void);

/// Returns the configured OS event to manage touch inputs
HANDLE FT5X06_GetEvent(void);

/// Test the level of the INT line
/// @param[in]    none
/// @param[out]   none
/// @retval       TRUE  INT line active
/// @retval       FALSE INT line passive
/// @caveats      none
BOOL FT5X06_CheckInterruptLine(void);

/// Release the current interrupt and wait for the next interrupt with timeout
/// @param[in]    none
/// @param[out]   none
/// @retval       TRUE  Interrupt
/// @retval       FALSE Timeout
/// @caveats      none
BOOL FT5X06_WaitTouchInterrupt(DWORD dwTimeout);

/// Start the touch controller (release the RST/PWR line) start the communication and and setup the\n
/// necessary registers in the touch controller
/// @param[in]    none
/// @param[out]   none
/// @retval       TRUE  success
/// @retval       FALSE error, a message to the console shows the reason
/// @caveats      none
BOOL FT5X06_Init();

/// Reset on or power off the touch controller\n
/// Because the touch controller is mounted very closed to the display an ESD pulse could disturb the controller in a way that his is not working any more (not broken for ever)
/// @param[in]    none
/// @param[out]   none
/// @retval       none
/// @caveats      none
void FT5X06_Exit(void);

/// Read out the count of touches (TD_STATUS) and the data for the 2 touch from the  touch controller\n
/// @param[in]    none
/// @param[out]   none
/// @retval       TRUE  success
/// @retval       FALSE communication error with the touch controller, a message to the console shows the reason
/// @caveats      none
BOOL FT5X06_ReadTouchInput(void);

/// Get the touch events from the controller
/// @param[in]    pInput = pointer to an array[2] of TCHINPUT, pCnt = pointer to count of touches
/// @param[out]   pInput = pointer to an array[2] of TCHINPUT, pCnt = pointer to count of touches
/// @retval       TRUE  success
/// @retval       FALSE communication error wit the touch controller,
/// @caveats      none
void FT5X06_GetPoint(PTCHINPUT pInput,INT *pCnt);

/// Reset off or Power on
/// @param[in]    none
/// @param[out]   none
/// @retval       none
/// @caveats      none
void FT5x06_PowerOn(void);

/// Test the communication by reading out the vendor's chip id
/// @param[in]    none
/// @param[out]   none
/// @retval       TRUE  success
/// @retval       FALSE communication error wit the touch controller,
/// @caveats      none
BOOL FT5x06_TestCommunication(void);

#endif //  __TCHCONTRFT5X06_H__

