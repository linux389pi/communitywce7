/// @file       TchContr_FT5x06.c
/// @copyright  Copyright (c) 2012 Toradex AG \n
///             All information contained herein is, and remains the property of Toradex Ag and its suppliers, if any. The intellectual\n
///             and technical concepts contained herein are proprietary to Toradex AG and its suppliers and may be covered by Swiss and\n
///             foreign patents, patents in process, and are protected by trade secret or copyright law. Dissemination of this information\n
///             or reproduction of this material is strictly forbidden unless prior written permission is obtained from Toradex AG.\n
/// @author     $Author$
/// @version    $Rev$
/// @date       $Date$
/// @brief      Function library for the touch controller FT5x06

// Public
#include <windows.h>
#include <limits.h>
#include <nkintr.h>
#include <tchstream.h>

// Platform
#include "omap.h"
#include <ceddk.h>
#include <ceddkex.h>
#include <oal.h>
#include <oal_clock.h>
#include <oalex.h>
#include <initguid.h>
#include "bsp_def.h"
#include "sdk_i2c.h"
#include "sdk_gpio.h"
#include "ft5x06_regs.h"
#include "ft5x06_cfg.h"
#include "ft5x06Pdd.h"
#include "am33x_clocks.h"
#include "am33x_gpio.h"
#include "sdk_padcfg.h"
#include "bsp_padcfg.h"


// ==== Debug Zones ================================================================================
// simple handling: debug zones are forced with TREU or FALSE and need a recompile to be activated
// for sophisticated handling: use macro DEBUGZONE(), DBGPARAM dpCurSettings etc.
// (see MSDN e.g. Win CE 5.0, Debug Zones)
#ifndef SHIP_BUILD

#undef ZONE_ERROR
#undef ZONE_WARN
#undef ZONE_FUNCTION
#undef ZONE_INFO
#undef ZONE_TIPSTATE

#define ZONE_ERROR          DEBUGZONE(0)
#define ZONE_WARN           DEBUGZONE(1)
#define ZONE_FUNCTION       DEBUGZONE(2)
#define ZONE_INFO           DEBUGZONE(3)
#define ZONE_TIPSTATE       DEBUGZONE(4)

static DBGPARAM dpCurSettings = {
    L"FT5X06", {
        L"Errors",      L"Warnings",    L"Function",    L"Info",
            L"IST",         L"Undefined",   L"Undefined",   L"Undefined",
            L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined",
            L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined"
    },
    // Bitfield controlling the zones.  1 means the zone is enabled, 0 disabled
    // We'll enable the Error, Warning, and Info zones by default here
    1 | 1 << 1 | 1 << 2 | 1 << 3
};

#endif

//== General ======================================================================================
//----- private preprocessor definitions -----------------------------------------------------------

//----- private members ----------------------------------------------------------------------------

//----- private preprocessor definitions -----------------------------------------------------------
// I2C settings                                                         
#define FT5X06_I2C_100KB                  1                               // 100kb I2C speed
#define FT5X06_I2C_400KB                  2                               // 400kb I2C speed
#define FT5X06_IST_PRIORITY               40                              // task priority hardware adapter

//----- private preprocessor definitions -----------------------------------------------------------

#define CHECK_OPERATING_MODE()      ((m_WorkModeRegister[DEVICE_MODE_ADR]&0x70)==0x00) // access macros
#define CHECK_FACTORY_MODE()        ((m_WorkModeRegister[DEVICE_MODE_ADR]&0x70)==0x40)
#define GET_GEST_ID()               m_WorkModeRegister[GEST_ID_ADR]       // access macros
#define GET_TD_STATUS()             (m_WorkModeRegister[TD_STATUS_ADR]&0x0F) // access macros
#define GET_TOUCHx_EVENT_FLG(tnr)   (m_WorkModeRegister[TOUCH1_XH+((tnr-1)*TOUCHx_SIZE)]>>6) // access macros for Event Flags
#define GET_TOUCHx_ID(tnr)          (m_WorkModeRegister[TOUCH1_YH+((tnr-1)*TOUCHx_SIZE)]>>4) // access macros for Touch ID
#define GET_TOUCHx_X(tnr)           (UINT16)((m_WorkModeRegister[ TOUCH1_XH + ( (tnr - 1) * TOUCHx_SIZE) ] & 0x0F) << 8) | m_WorkModeRegister[ TOUCH1_XL + ( (tnr - 1) * TOUCHx_SIZE)] 
#define GET_TOUCHx_Y(tnr)           (UINT16)((m_WorkModeRegister[ TOUCH1_YH + ( (tnr - 1) * TOUCHx_SIZE) ] & 0x0F) << 8) | m_WorkModeRegister[ TOUCH1_YL + ( (tnr - 1) * TOUCHx_SIZE)]
#define GET_ID_G_MODE()             m_WorkModeRegister[ID_G_MODE_ADR]     // access macros
#define SET_ID_G_MODE(mode)         m_WorkModeRegister[ID_G_MODE_ADR]=mode // access macros

//----- priver members ----------------------------------------------------------------------------
UCHAR                                   m_WorkModeRegister[FT5X06_WORKMODE_REG_SIZE];        // copy of Work Mode Registers

//==== privat type definitions ====================================================================
typedef struct
{
    UINT8   fingerNumber;                                                 // Number of Fingers

    // first touch point information
    UINT16  positionX1;                                                     // X Position
    UINT16  positionY1;                                                     // Y Position
    BOOL    tipSwitch1;                                                // Tip Switch
    UINT8   touchID1;                                                 // Touch ID

    // second touch point information
    UINT16  positionX2;                                                     // X Position
    UINT16  positionY2;                                                     // Y Position
    BOOL    tipSwitch2;                                                // Tip Switch
    UINT8   touchID2;                                                 // Touch ID
}I2C_DEVICE;

static HANDLE                           m_hI2C;
static HANDLE                           m_hGPIO;
static DWORD                            m_I2C_Speed;
static DWORD                            m_I2C_Addr;
static DWORD                            m_Interrupt_Sig_Inv;
static DWORD                            m_Reset_Post_Delay;
static DWORD                            m_Reset_Sig_Inv;
static DWORD                            m_Interrupt_Pin;
static DWORD                            m_Reset_Pin;
static HANDLE                           m_hI2C_Event;                     // for "interrupts"
static DWORD                            m_I2C_SysIntr;
static DWORD                            m_IST_Prio;                  // task priority


//----- privat methods definition -----------------------------------------------------------------
// @internal
/// I2C Burst Read - Read Data from one register of an I2C device
/// @param[in]    uSlaveAddress The device's i2c address (0..127) if uSlaveAddress=0xff, no slaveAddress byte will be transmitted.
/// @param[out]   pBuffer Pointer to the data bytes to Receive
/// @param[in]    ucSubAddr The device's register address (offset). This is one byte transmitted after the slave address.
///                Set to any negative value if no offset byte should be transmitted
/// @param[in]    iNumberBytes Number of Bytes to Receive
/// @retval       TRUE success
/// @retval       FALSE Error during Transmission
/// @caveats      none
static BOOL ft5x06_burst_read(UCHAR uSlaveAddress, UCHAR* pBuffer, UCHAR ucSubAddr, DWORD iNumberBytes)
{
    BOOL bResult = FALSE;
	LONG lErr;
    
	if(I2CSetSlaveAddress(m_hI2C, (UINT16)uSlaveAddress))
    {
        // Read bytes
        if((UINT)-1 != I2CRead(m_hI2C,
                               (UCHAR)ucSubAddr,
                               pBuffer,
                               iNumberBytes))
        {
            bResult = TRUE;
        }
        else
        {
            lErr = GetLastError();
        RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in ft5x06_burst_read()->I2CRead %d"),lErr));
        }
    }
    else
    {
        lErr = GetLastError();
        RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in ft5x06_burst_read()->SetSlaveAddress %d"),lErr));
    }
    
    return bResult;
}

// @internal
/// I2C Burst Write - Write Data to one register of an I2C device
/// @param[in]    uSlaveAddress The device's i2c address (0..127)
/// @param[in]    pBuffer Pointer to the data bytes to Transmit
/// @param[in]    ucSubAddr The device's register address (offset). This is one byte transmitted after the slave address.
///               Set to any negative value if no offset byte should be transmitted
/// @param[in]    Number of Bytes to Transmit
/// @param[out]   none
/// @retval       TRUE success
/// @retval       FALSE Error during Transmission
/// @caveats      none
static BOOL ft5x06_burst_write(UCHAR uSlaveAddress, UCHAR* pBuffer, UCHAR ucSubAddr, DWORD iNumberBytes)
{
    BOOL bResult = FALSE;
	LONG lErr;
    
	if(I2CSetSlaveAddress(m_hI2C, (UINT16)uSlaveAddress))
    {
        // Read bytes
        if((UINT)-1 != I2CWrite(m_hI2C,
                               (UCHAR)ucSubAddr,
                               pBuffer,
                               iNumberBytes))
        {
            bResult = TRUE;
        }
        else
        {
            lErr = GetLastError();
            RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in ft5x06_burst_read() @ write data %d"),lErr));
        }
    }
    else
    {
        lErr = GetLastError();
        RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in ft5x06_burst_write() -> SetSlaveAddress %d"),lErr));
    }
    
    return bResult;
}

// @internal
/// Read out parts of "Work Mode Registers" out of the touch controller FT5x06 and place in the internal copy (array).\n
/// The index of this array is equal to the addresses of the "Work Mode Registers" in the touch controller.
/// @param[in]    sStartRegister  The device's i2c address (0..127)
/// @param[in]    iNumber  Number of bytes to read
/// @param[out]   none
/// @retval       TRUE  success
/// @retval       FALSE error, a message to the console shows the reason
/// @caveats      none
static BOOL ft5x06_read_register(SHORT sStartRegister,UINT iNumber)
{
    BOOL bSuccess = FALSE;
    LONG lErr;

    bSuccess = ft5x06_burst_read((UCHAR)m_I2C_Addr,&m_WorkModeRegister[sStartRegister],sStartRegister,iNumber);

    if (FALSE == bSuccess)
    {
        lErr = GetLastError();
        RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in ft5x06_burst_read() %d"),lErr));
    }

    return bSuccess;
}

// @internal
/// Write out parts of internal register copy (array) to the "Work Mode Registers" of the touch controller FT5x06
/// The index of this array is equal to the adrresses of the "Work Mode Registers" in the touch controller
/// @param[in]    sStartRegister    The device's i2c address (0..127)
/// @param[in]    iNumber   Number of bytes to read
/// @param[out]   none
/// @retval       TRUE  success
/// @retval       FALSE error, a message to the console shows the reason
/// @caveats      none
static BOOL ft5x06_write_register(SHORT sStartRegister,UINT iNumber)
{
    BOOL bSuccess = FALSE;
    LONG lErr;

    bSuccess = ft5x06_burst_write((UCHAR)m_I2C_Addr,&m_WorkModeRegister[sStartRegister],sStartRegister,iNumber);

    if (FALSE == bSuccess)
    {
        lErr = GetLastError();
        RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in ft5x06_burst_write() %d"),lErr));
    }

    return bSuccess;
}

// @internal
/// Read out all the registers containing touch data of the touch controller
/// @param[in]    psI2CDat = pointer to the touch events data
/// @param[out]   psI2CDat = pointer to the touch events data (in case of error the data are set to no touches)
/// @retval       none
/// @caveats      none
static void FT5X06_ExtractTouchInfo(I2C_DEVICE *psI2CDat)
{
    RETAILMSG(ZONE_FUNCTION, ( TEXT("FT5X06_ExtractTouchInfo()+\r\n" )) );

    if (FALSE==FT5X06_ReadTouchInput())                // update the local copy of the registers
    {
        ERRORMSG(ZONE_ERROR, (TEXT("FT5X06_ExtractTouchInfo() I2C read failed\r\n")));
        psI2CDat->fingerNumber    = 0;                                    // clean all data
        psI2CDat->positionX1        = 0;
        psI2CDat->positionY1        = 0;
        psI2CDat->tipSwitch1   = FALSE;
        psI2CDat->touchID1    = 0;
        psI2CDat->positionX2        = 0;
        psI2CDat->positionY2        = 0;
        psI2CDat->tipSwitch2   = FALSE;
        psI2CDat->touchID2    = 0;
     }
     else
     {
        psI2CDat->fingerNumber  = (INT)GET_TD_STATUS();                   // Number of Fingers

        psI2CDat->positionX1    = GET_TOUCHx_X(1);
        psI2CDat->positionY1    = GET_TOUCHx_Y(1);

        psI2CDat->tipSwitch1    = GET_TOUCHx_EVENT_FLG(1);                // Tip Switch (TRUE = finger on touch)
        psI2CDat->tipSwitch1    = psI2CDat->tipSwitch1 ^ 0x01;             //alter bit 0 state, as we are UP as O and Down as 1.
        psI2CDat->touchID1      = GET_TOUCHx_ID(1);                       // Touch ID

        psI2CDat->positionX2    = GET_TOUCHx_X(2);
        psI2CDat->positionY2    = GET_TOUCHx_Y(2);

        psI2CDat->tipSwitch2    = GET_TOUCHx_EVENT_FLG(2);                // Tip Switch
        psI2CDat->tipSwitch2    = psI2CDat->tipSwitch2 ^ 0x01;             //alter bit 0 state, as we are calculating UP as O and Down as 1.
        psI2CDat->touchID2      = GET_TOUCHx_ID(2);                       //Touch ID

        //Touch up event is missing, when touch up fingerNumber number is wrong. So Counting number of fingers by detecting valid touch ID.
        if(psI2CDat->touchID1 != 15)
        {
            psI2CDat->fingerNumber = 1;
        }

        if(psI2CDat->touchID2 != 15)
        {
            psI2CDat->fingerNumber = 2;
        }
     }

    RETAILMSG(ZONE_FUNCTION,(TEXT("FT5X06_ExtractTouchInfo() -\r\n" )));
    return;
}



//----- public methods definition -----------------------------------------------------------------
/// Set the configuration data according to the registry entry or to the default value
/// These data are "INT port number", "RESET port number", "I2C slave address", "I2C speed",
/// The values are set to default or set by the entry in the registry (if a valid entry exists)
/// @param[in]    none
/// @param[out]   none
/// @retval       none
/// @caveats      none
void FT5X06_ReadConfiguration(void)
{
    HKEY    hKey;
    DWORD   dwSize;
    LONG    lError;

    // set default registry values
    m_I2C_Speed             = FT5X06_I2C_400KB;                                  // I2C speed @to be confirmed
    m_I2C_Addr              = FT5X06_I2C_ADDR;                                // I2C address of the FT5x06 chip

    m_Interrupt_Sig_Inv     = FT5X06_INT_SIG_INV;                                // Int signal invertetd
    m_Reset_Post_Delay      = FT5X06_RESET_POSTDLY;                              // delay after the "RESET Line" is released
    m_Reset_Sig_Inv         = FT5X06_RESET_LIN_INV;                              // Reset line inverted
    m_IST_Prio              = FT5X06_IST_PRIORITY;                                  // Task priority
    m_Interrupt_Pin         = FT5X06_INT_GPIO_PIN;                               // GPIO pin for Int
    m_Reset_Pin             = FT5X06_RESET_GPIO_PIN;                             // GPIO pin for Reset
    m_I2C_SysIntr           = (DWORD)SYSINTR_NOP;                           // Set default sys interrupt

    // @todo check settings in the registry
}

/// Returns the configured Sys interrupt for the touch driver
DWORD FT5X06_GetSysIntr(void)
{
    return m_I2C_SysIntr;
}

/// Returns the configured OS event to manage touch inputs
HANDLE FT5X06_GetEvent(void)
{
    return m_hI2C_Event;
}

/// Init the I2C communication and the GIPO for the "INT" and "RST" pin of the touch controller
/// @param[in]    none
/// @param[out]   none
/// @retval       TRUE  success
/// @retval       FALSE error, a message to the console shows the reason
/// @caveats      FT5X06_ReadConfiguration() has to be executed
BOOL FT5X06_HardwareInit(void)
{
    BOOL        bSuccess;
    DWORD       dwIRq;
    DWORD       dwEdge;

    RETAILMSG(ZONE_FUNCTION | ZONE_INFO, (TEXT("FT5X06_HardwareInit+\r\n")));
    //------ I2C init ----------------------------------------------------------------------------
    bSuccess = FALSE;
    
    m_hI2C = I2COpen(FT5X06_I2C_INDEX);

    if (m_hI2C != INVALID_HANDLE_VALUE && m_hI2C != NULL)
    {
        switch (m_I2C_Speed)
        {
            case FT5X06_I2C_100KB:
                I2CSetBaudIndex(m_hI2C, SLOWSPEED_MODE);
                bSuccess = TRUE;
                break;

            case FT5X06_I2C_400KB:
                I2CSetBaudIndex(m_hI2C, FULLSPEED_MODE);
                bSuccess = TRUE;
                break;

            default:
                break;
        }
		I2CSetSubAddressMode(m_hI2C, I2C_SUBADDRESS_MODE_8);
    }
	else
	{
		RETAILMSG(ZONE_ERROR,(TEXT("\r\nFT5x06 : Failed to open I2C")));
	}

    //------ GPIO init ----------------------------------------------------------------------------
    if (TRUE==bSuccess)
    {
        bSuccess = FALSE;

		m_hGPIO = GPIOOpen();

        if (m_hGPIO != INVALID_HANDLE_VALUE)
        {
            // If interrupt input is not inverted : falling edge. Otherwise rising edge
            dwEdge = (0 == m_Interrupt_Sig_Inv ? GPIO_INT_HIGH_LOW : GPIO_INT_LOW_HIGH); 
            
            // Set m_Interrupt_Pin as input
            GPIOSetMode(m_hGPIO, m_Interrupt_Pin, GPIO_DIR_INPUT | dwEdge );
            // Set m_Reset_Pin as output
            GPIOSetMode(m_hGPIO, m_Reset_Pin, GPIO_DIR_OUTPUT );
            
            // Ensure that the settings have been set properly
            bSuccess = ( (GPIO_DIR_INPUT | dwEdge ) == GPIOGetMode(m_hGPIO, m_Interrupt_Pin)
                       &&(GPIO_DIR_OUTPUT) == GPIOGetMode(m_hGPIO, m_Reset_Pin) );
            
            if(!bSuccess)
            {
                RETAILMSG(ZONE_ERROR,(TEXT("\r\nFailed to set GPIOs modes")));
            }
        }
        else
        {
            RETAILMSG(ZONE_ERROR,(TEXT("\r\nFailed to get a handle on GPIOs")));
        }
    }

    if (TRUE==bSuccess)
    {
        bSuccess = FALSE;

        // Reset the interrupt line 
        if (0==m_Reset_Sig_Inv)                                         // Reset not inverted
        {
            GPIOSetBit (m_hGPIO, m_Reset_Pin);                        // Reset active / no power (vInitFT5x06())
        }
        else
        {
            GPIOClrBit (m_hGPIO, m_Reset_Pin);                        // Reset active / no power (vInitFT5x06())
        }

        // Get IRQ for the interrupt GPIO 
        m_I2C_SysIntr =     (DWORD) SYSINTR_UNDEFINED;
        // Create an event to wait on
        m_hI2C_Event = CreateEvent(NULL, FALSE, FALSE, NULL);
        if (m_hI2C_Event!= NULL)
        {
                if (GPIOInterruptInitialize(m_hGPIO, m_Interrupt_Pin, &m_I2C_SysIntr, m_hI2C_Event))
                {
                    // Set the interrupt line as a wake up source for the system
                    //GPIOInterruptWakeUp(m_hGPIO, m_Interrupt_Pin, m_I2C_SysIntr, TRUE);
                    GPIOInterruptDone(m_hGPIO, m_Interrupt_Pin, m_I2C_SysIntr);
                    bSuccess = TRUE;
                }
                else
                {
                    RETAILMSG(ZONE_ERROR, (TEXT("ERROR: TOUCH: Failed to initialize the FT5x06 interrupt line.\r\n")));
                }
        }
        else
        {
            RETAILMSG(ZONE_ERROR, (TEXT("ERROR: TOUCH: Failed to create an event for the FT5x06 interrupt line.\r\n")));
        }
    }
    if (FALSE==bSuccess)
    {
        RETAILMSG(ZONE_ERROR, (TEXT("\r\nFT5X06_HardwareInit() had failed")));
    }
    RETAILMSG(ZONE_FUNCTION | ZONE_INFO, (TEXT("FT5X06_HardwareInit-\r\n")));
    
    return bSuccess;
}

/// Stop the I2C communication (release interrupt, mutex, stop the I2C driver etc)
/// @param[in]    none
/// @param[out]   none
/// @retval       none
/// @retval       none
/// @caveats      none
void FT5X06_HardwareExit(void)
{
    RETAILMSG(ZONE_FUNCTION, (TEXT("FT5X06_HardwareExit+\r\n")));
    // GPIO disable
    if(m_I2C_SysIntr != SYSINTR_UNDEFINED)
	{
		GPIOInterruptDone(m_hGPIO, m_Interrupt_Pin, m_I2C_SysIntr);
		GPIOInterruptDisable(m_hGPIO, m_Interrupt_Pin, m_I2C_SysIntr);
	}
	if (m_hI2C_Event != NULL)
	{
		CloseHandle(m_hI2C_Event);
	}
	GPIOClose(m_hGPIO);

    // I2C driver
    I2CClose(m_hI2C);
    RETAILMSG(ZONE_FUNCTION, (TEXT("FT5X06_HardwareExit-\r\n")));
}

/// Test the level of the INT line
/// @param[in]    none
/// @param[out]   none
/// @retval       TRUE  INT line active
/// @retval       FALSE INT line passive
/// @caveats      none
BOOL FT5X06_CheckInterruptLine(void)
{
    if (0==m_Interrupt_Sig_Inv)                                           // interrupt input is not inverted
    {
        return (0==GPIOGetBit(m_hGPIO, m_Interrupt_Pin));                 // interrupt input already low means there are pending touches
    }
    else
    {
        return (1==GPIOGetBit(m_hGPIO, m_Interrupt_Pin));                 // interrupt input already high means there are pending touches
    }
}

/// Release the current interrupt and wait for the next interrupt with timeout
/// @param[in]    DWORD dwTimeout
/// @param[out]   none
/// @retval       TRUE  Interrupt
/// @retval       FALSE Timeout
/// @caveats      none
BOOL FT5X06_WaitTouchInterrupt(DWORD dwTimeout)
{
    GPIOInterruptDone(m_hGPIO, m_Interrupt_Pin, m_I2C_SysIntr);
	if (WAIT_TIMEOUT == WaitForSingleObject(m_hI2C_Event,dwTimeout))  // wait for input interrupt or timeout
    {
		return FALSE;
    }
    else
    {
		return TRUE;
    }
}

/// Start the touch controller (release the RST/PWR line) start the communication and and setup the\n
/// necessary registers in the touch controller
/// @param[in]    none
/// @param[out]   none
/// @retval       TRUE  success
/// @retval       FALSE error, a message to the console shows the reason
/// @caveats      none
BOOL FT5X06_Init()
{
    UCHAR ucByte1;

    RETAILMSG(ZONE_FUNCTION, (TEXT("FT5X06_Init+\r\n")));
    
    // Reset the touch chip remotely
    FT5x06_PowerOn();

    // get the interrupt status
    if (FALSE==ft5x06_read_register(ID_G_MODE_ADR,ID_G_MODE_SIZE))
    {
        return FALSE;
    }

    // get the interrupt status/mode
    ucByte1 = GET_ID_G_MODE();

    SET_ID_G_MODE(ID_G_MODE_Trigger);                                  // set Interrupt mode
    if (FALSE==ft5x06_write_register(ID_G_MODE_ADR,ID_G_MODE_SIZE))
    {
        return FALSE;
    }

    if (TRUE==ft5x06_read_register(ID_G_THGROUP,5))
    {
        RETAILMSG(ZONE_INFO,(TEXT("ID_G_THGROUP(0x%X) ID_G_THPEAK(0x%X) ID_G_THCAL(0x%X) ID_G_THWATER(0x%X) ID_G_THTEMP(0x%X)\r\n" ),m_WorkModeRegister[ID_G_THGROUP],m_WorkModeRegister[ID_G_THPEAK],m_WorkModeRegister[ID_G_THCAL],m_WorkModeRegister[ID_G_THWATER],m_WorkModeRegister[ID_G_THTEMP]));
    }
    //User may need to find optimum value to get precise and valid touch points using below touch controller registers
    /* m_WorkModeRegister[ID_G_THGROUP] = 280/4;
    m_WorkModeRegister[ID_G_THPEAK] = 60;
    m_WorkModeRegister[ID_G_THCAL] = 128;
    m_WorkModeRegister[ID_G_THWATER] = 60;
    m_WorkModeRegister[ID_G_THTEMP] = 10;

    if(TRUE == ft5x06_write_register(ID_G_THCAL,1))
    {
        if (TRUE==ft5x06_read_register(ID_G_THGROUP,5))
        {
            RETAILMSG(1,(TEXT("ID_G_THGROUP(0x%X) ID_G_THPEAK(0x%X) ID_G_THCAL(0x%X) ID_G_THWATER(0x%X) ID_G_THTEMP(0x%X)\r\n" ),m_WorkModeRegister[ID_G_THGROUP],m_WorkModeRegister[ID_G_THPEAK],m_WorkModeRegister[ID_G_THCAL],m_WorkModeRegister[ID_G_THWATER],m_WorkModeRegister[ID_G_THTEMP]));
        }
    }  */
    //GPIOInterruptDone(m_hGPIO, m_Interrupt_Pin, m_I2C_SysIntr);
    
    RETAILMSG(ZONE_FUNCTION, (TEXT("FT5X06_Init-\r\n")));
    
    return TRUE;
}

/// Reset on or power off the touch controller\n
/// Because the touch controller is mounted very closed to the display an ESD pulse could disturb the controller in a way that his is not working any more (not broken for ever)
/// @param[in]    none
/// @param[out]   none
/// @retval       none
/// @caveats      none
void FT5X06_Exit(void)                                                // stop (reset/powerdown) the Touch Controller
{
    RETAILMSG(ZONE_FUNCTION, (TEXT("FT5X06_Exit+\r\n")));
    
    if (0==m_Reset_Sig_Inv)                                             // Reset not inverted
    {
        GPIOSetBit (m_hGPIO, m_Reset_Pin);                            // Reset active or no power
    }
    else
    {
        GPIOClrBit (m_hGPIO, m_Reset_Pin);                            // Reset active or no power
    }
    
    RETAILMSG(ZONE_FUNCTION, (TEXT("FT5X06_Exit-\r\n")));
}

/// Read out the count of touches (TD_STATUS) and the data for the 2 touch from the  touch controller\n
/// @param[in]    none
/// @param[out]   none
/// @retval       TRUE  success
/// @retval       FALSE communication error with the touch controller, a message to the console shows the reason
/// @caveats      none
BOOL FT5X06_ReadTouchInput(void)
{
    // get the status (number of touch points) and TOUCH1 and TOUCH2
    if (FALSE==ft5x06_read_register(TD_STATUS_ADR,TD_STATUS_SIZE+(TOUCHx_SIZE*2)))
    {
        return FALSE;
    }
    return TRUE;
}

/// Get the touch events from the controller
/// @param[in]    pInput = pointer to an array[2] of TCHINPUT, pCnt = pointer to count of touches
/// @param[out]   pInput = pointer to an array[2] of TCHINPUT, pCnt = pointer to count of touches
/// @retval       TRUE  success
/// @retval       FALSE communication error wit the touch controller,
/// @caveats      none
void FT5X06_GetPoint(PTCHINPUT pInput,INT *pCnt)
{
    // static variables for previous evnts
    static BOOL bPrevTipSwitchID1   = FALSE;
    static BOOL bPrevTipSwitchID2   = FALSE;
    static INT  s_xPrevious;
    static INT  s_yPrevious;
    I2C_DEVICE sI2CDat;                                                 // data structure of the controller data
    BOOL bINTReady;
    INT     iFiCnt;
    BOOL bPrevTipSwitch_tmp;
    PTCHINPUT pInput_tmp;                                           // only for debug

    pInput_tmp = pInput;

    RETAILMSG(ZONE_FUNCTION||ZONE_INFO, (TEXT("MultiTchAppDriv:FT5X06_GetPoint() +\r\n")));

    // reset pointer for TOUCHEVENTF_PRIMARY flag handling
    *pCnt = 0;

    // clear all data for the call back to the MDD
    memset(pInput, 0, 2*sizeof(TCHINPUT));                          // clear array for call back function

    // Check if pen data are available If so, get the data.
    // Note that we always return data from the previous sample to avoid returning data nearest
    // the point where the pen is going up.  Data during light touches is not accurate and should
    // normally be rejected.  Without the ability to do pressure measurement, we are limited to
    // rejecting points near the beginning and end of the pen down period.
    bINTReady  = FT5X06_CheckInterruptLine();
    if (bINTReady)
    {
        // get the data from the touch Controller
        FT5X06_ExtractTouchInfo(&sI2CDat);

        // get the count of fingers
        iFiCnt = sI2CDat.fingerNumber;

        // count of finges range vaid ?
        if ((0<iFiCnt)&&(3>iFiCnt))
        {
            //-------- generate the first data for the call back --------------------------------------------
            // choose the right previous state
            if (TOUCH_ID_1 == sI2CDat.touchID1)
            {
                bPrevTipSwitch_tmp = bPrevTipSwitchID1;
            }
            else
            {
                bPrevTipSwitch_tmp = bPrevTipSwitchID2;
            }

            // Touch controller reports finger on screen
            if (sI2CDat.tipSwitch1)
            {
                pInput->x       = (sI2CDat.positionX1 * TOUCH_SCALING_FACTOR); // X & Y are given in fourth pixels. Convert them to dpi
                pInput->y       = (sI2CDat.positionY1 * TOUCH_SCALING_FACTOR); // X & Y are given in fourth pixels. Convert them to dpi
                s_xPrevious     = pInput->x;
                s_yPrevious     = pInput->y;

                pInput->dwID    = sI2CDat.touchID1;

                // previous already a finger on the screen
                if (bPrevTipSwitch_tmp)
                {
                    pInput->dwFlags = TOUCHEVENTF_MOVE|TOUCHEVENTF_INRANGE;
                }
                else
                {
                    // previous finger up, finger down detected
                    pInput->dwFlags = TOUCHEVENTF_DOWN|TOUCHEVENTF_INRANGE;
                    bPrevTipSwitch_tmp  = TRUE;
                }

                *pCnt = 1;
                // prepared for the next entry
                pInput++;
            }
            else
            {
                // previous already a finger on the screen, finger up detected
                if (bPrevTipSwitch_tmp)
                {
                    pInput->x       = s_xPrevious; // already converted to dpi
                    pInput->y       = s_yPrevious; // already converted to dpi

                    pInput->dwID    = sI2CDat.touchID1;

                    pInput->dwFlags = TOUCHEVENTF_UP;
                    bPrevTipSwitch_tmp  = FALSE;

                    *pCnt = 1;
                    // prepared for the next entry
                    pInput++;
                }
            }
            // save the previous state
            if (TOUCH_ID_1 == sI2CDat.touchID1)
            {
                bPrevTipSwitchID1 = bPrevTipSwitch_tmp;
            }
            else
            {
                bPrevTipSwitchID2 = bPrevTipSwitch_tmp;
            }
            //-------- generate the second data for the call back -------------------------------------
            // choose the right previous state
            if (TOUCH_ID_1 == sI2CDat.touchID2)
            {
                bPrevTipSwitch_tmp = bPrevTipSwitchID1;
            }
            else
            {
                bPrevTipSwitch_tmp = bPrevTipSwitchID2;
            }

            // count of fingers range valid ?
            if (2==iFiCnt)
            {
                // Touch Controller reports finger on screen
                if (sI2CDat.tipSwitch2)
                {
                    pInput->x       = (sI2CDat.positionX2 * TOUCH_SCALING_FACTOR); // X & Y are given in fourth pixels. Convert them to dpi
                    pInput->y       = (sI2CDat.positionY2 * TOUCH_SCALING_FACTOR); // X & Y are given in fourth pixels. Convert them to dpi
                    s_xPrevious     = pInput->x;
                    s_yPrevious     = pInput->y;

                    pInput->dwID    = sI2CDat.touchID2;

                    // previous already a finger on the screen
                    if (bPrevTipSwitch_tmp)
                    {
                        pInput->dwFlags = TOUCHEVENTF_MOVE|TOUCHEVENTF_INRANGE;
                    }
                    else
                    {
                        // previous finger up, Finger down detected Now
                        pInput->dwFlags = TOUCHEVENTF_DOWN|TOUCHEVENTF_INRANGE;
                        bPrevTipSwitch_tmp  = TRUE;
                    }

                    *pCnt = 2;
                }
                else
                {
                    // previous already a finger on the screen, touch up detected
                    if (bPrevTipSwitch_tmp)
                    {
                        pInput->x       = s_xPrevious; // already converted to dpi
                        pInput->y       = s_yPrevious; // already converted to dpi

                        pInput->dwID    = sI2CDat.touchID2;
                        pInput->dwFlags = TOUCHEVENTF_UP;
                        bPrevTipSwitch_tmp  = FALSE;

                        *pCnt = 2;
                    }
                    // else no finger on screen and no touch
                }

            }

            // save the previous state
            if (TOUCH_ID_1 == sI2CDat.touchID2)
            {
                bPrevTipSwitchID1 = bPrevTipSwitch_tmp;
            }
            else
            {
                bPrevTipSwitchID2 = bPrevTipSwitch_tmp;
            }
        }

#if 0
        //-------- debug print ------------------------------------------------------------
        switch (sI2CDat.fingerNumber)
        {
            case 0:
                RETAILMSG(ZONE_INFO,(TEXT("MultiTchAppDriv:FT5X06_GetPoint() FT5x06 no touches\r\n")));
                break;
            case 1:
                RETAILMSG(ZONE_INFO,(TEXT("MultiTchAppDriv:FT5X06_GetPoint() FT5x06 FirstTouch TipSwich(%d),TouchID(%d), X(%d), Y(%d)\r\n" ),sI2CDat.tipSwitch1,sI2CDat.touchID1,sI2CDat.positionX1,sI2CDat.positionY1));
                break;
            case 2:
                RETAILMSG(ZONE_INFO,(TEXT("MultiTchAppDriv:FT5X06_GetPoint() FT5x06 FirstTouch TipSwich(%d),TouchID(%d), X(%d), Y(%d)\r\n" ),sI2CDat.tipSwitch1,sI2CDat.touchID1,sI2CDat.positionX1,sI2CDat.positionY1));
                RETAILMSG(ZONE_INFO,(TEXT("MultiTchAppDriv:FT5X06_GetPoint() FT5x06 SecondTouch TipSwich(%d),TouchID(%d), X(%d), Y(%d)\r\n" ),sI2CDat.tipSwitch2,sI2CDat.touchID2,sI2CDat.positionX2,sI2CDat.positionY2));
                break;
            default:
                RETAILMSG(ZONE_INFO,( TEXT("MultiTchAppDriv:FT5X06_GetPoint() FT5x06 illegal  of fingers (%d)"),sI2CDat.fingerNumber));
                break;
        }

        switch (*pCnt)
        {
            case 1:
                RETAILMSG(ZONE_INFO,(TEXT("MultiTchAppDriv:FT5X06_GetPoint() Input[0] dwID(%d), dwFlags(%x), X(%d), Y(%d)\r\n" ),pInput_tmp->dwID, pInput_tmp->dwFlags, pInput_tmp->x, pInput_tmp->y));
                break;

            case 2:
                RETAILMSG(ZONE_INFO,(TEXT("MultiTchAppDriv:FT5X06_GetPoint() Input[0] dwID(%d), dwFlags(%x), X(%d), Y(%d)\r\n" ),pInput_tmp->dwID, pInput_tmp->dwFlags, pInput_tmp->x, pInput_tmp->y));
                pInput_tmp++;
                RETAILMSG(ZONE_INFO,(TEXT("MultiTchAppDriv:FT5X06_GetPoint() Input[1] dwID(%d), dwFlags(%x), X(%d), Y(%d)\r\n" ),pInput_tmp->dwID, pInput_tmp->dwFlags, pInput_tmp->x, pInput_tmp->y));
                break;

            default:
                RETAILMSG(ZONE_INFO,(TEXT("MultiTchAppDriv:FT5X06_GetPoint() illegal pCint (%d)\r\n" ),*pCnt));
        }
#endif
    }
    RETAILMSG(ZONE_FUNCTION||ZONE_INFO,(TEXT("MultiTchAppDriv:FT5X06_GetPoint() -\r\n")));
    return;
}

/// Reset off or Power on
/// @param[in]    none
/// @param[out]   none
/// @retval       none
/// @caveats      none
void FT5x06_PowerOn(void)
{
    if (0==m_Reset_Sig_Inv)                                             // Reset not inverted
    {
        GPIOClrBit (m_hGPIO, m_Reset_Pin);                            // Reset off or power on
    }
    else
    {
        GPIOSetBit (m_hGPIO, m_Reset_Pin);                            // Reset off / Power on
    }
    Sleep(m_Reset_Post_Delay);                                             // wait until the reset sequence is over
 }

/// Test the communication by reading out the vendor's chip id
/// @param[in]    none
/// @param[out]   none
/// @retval       TRUE  success
/// @retval       FALSE communication error wit the touch controller,
/// @caveats      none
BOOL FT5x06_TestCommunication(void)
{
    // attention: it is possible that a fast power on and off sequence can reset the touch controller and therefore the interrupt mode bit changed to recognize that, the interrupt mode is checked
    if (FALSE==ft5x06_read_register(ID_G_MODE_ADR,ID_G_MODE_SIZE))
    {
        return FALSE;
    }

    // get the interrupt status/mode
    if (ID_G_MODE_Polling == GET_ID_G_MODE())
    {
        return FALSE;
    }
    else
    {
        return TRUE;
    }
}

