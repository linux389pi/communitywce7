/// @file       ft5x06_regs.h
/// @brief      Registry definition for FT5X06
/// @caveats

#ifndef __FT5X06_REGS_H__
#define __FT5X06_REGS_H__

//== "CTP Register Copy" ==========================================================================

// Work Mode Registers  (Register definition see FTS_AN_CTPM_Standard_eng_ver1.1.pdf)
#define FT5X06_WORKMODE_REG_SIZE        0x100                           // size of register block

// DEVIDE_MODE
#define DEVICE_MODE_ADR                 0x00                            // register address
#define DEVICE_MODE_SIZE                1                               // count of bytes

//GEST_ID
#define GEST_ID_ADR                     0x01                            // register address
#define GEST_ID_SIZE                    1                               // count of bytes

#define GEST_ID_NoGesture               0x00
#define GEST_ID_MoveUp                  0x10
#define GEST_ID_MoveLeft                0x14
#define GEST_ID_MoveDown                0x18
#define GEST_ID_MoveRight               0x1C
#define GEST_ID_ZoomIn                  0x48
#define GEST_ID_ZoomOut                 0x49

// TD_STATUS
#define TD_STATUS_ADR                   0x02                            // register address
#define TD_STATUS_SIZE                  1                               // count of bytes

// TOUCHx (tnr = touch number 1 to 10)
#define TOUCHx_SIZE                     0x6                               // count of bytes
#define TOUCHx_PutDown                  0x00
#define TOUCHx_PutUP                    0x01
#define TOUCHx_Contact                  0x02

#define TOUCH1_XH                       0x03
#define TOUCH1_XL                       0x04
#define TOUCH1_YH                       0x05
#define TOUCH1_YL                       0x06

#define TOUCH2_XH                       0x09
#define TOUCH2_XL                       0x0A
#define TOUCH2_YH                       0x0B
#define TOUCH2_YL                       0x0C

#define TOUCH3_XH                       0x0F
#define TOUCH3_XL                       0x10
#define TOUCH3_YH                       0x11
#define TOUCH3_YL                       0x12

#define TOUCH4_XH                       0x15
#define TOUCH4_XL                       0x16
#define TOUCH4_YH                       0x17
#define TOUCH4_YL                       0x18

#define TOUCH5_XH                       0x1B
#define TOUCH5_XL                       0x1C
#define TOUCH5_YH                       0x1D
#define TOUCH5_YL                       0x1E

#define TOUCH6_XH                       0x21
#define TOUCH6_XL                       0x22
#define TOUCH6_YH                       0x23
#define TOUCH6_YL                       0x24

#define TOUCH7_XH                       0x27
#define TOUCH7_XL                       0x28
#define TOUCH7_YH                       0x29
#define TOUCH7_YL                       0x2A

#define TOUCH8_XH                       0x2D
#define TOUCH8_XL                       0x2E
#define TOUCH8_YH                       0x2F
#define TOUCH8_YL                       0x30

#define TOUCH9_XH                       0x33
#define TOUCH9_XL                       0x34
#define TOUCH9_YH                       0x35
#define TOUCH9_YL                       0x36

#define TOUCH10_XH                      0x39
#define TOUCH10_XL                      0x3A
#define TOUCH10_YH                      0x3B
#define TOUCH10_YL                      0x3C

#define ID_G_THGROUP                    0x80
#define ID_G_THPEAK                     0x81
#define ID_G_THCAL                      0x82
#define ID_G_THWATER                    0x83
#define ID_G_THTEMP                     0x84

#define ID_G_CTRL                       0x86
#define ID_G_TIME_ENTER                 0x87
#define ID_G_PERIODACTIVE               0x88
#define ID_G_PERIOD                     0x89

#define ID_G_AUTO_CLB                   0xA0
#define ID_G_LIB_                       0xA1
#define ID_G_LIB                        0xA2
#define ID_G_CIPHER                     0xA3


// ID_G_MODE
#define ID_G_MODE_ADR                   0xA4                            // register address
#define ID_G_MODE_SIZE                  1                               // count of bytes
#define ID_G_MODE_Polling               0                               // polling mode
#define ID_G_MODE_Trigger               1                               // trigger mode

#define ID_G_PMODE                      0xA5
#define ID_G_FIRMID                     0xA6
#define ID_G_STATE                      0xA7
#define ID_G_FT5201ID                   0xA8
#define ID_G_ERR                        0xA9
#define ID_G_CLB                        0xAA

#define ID_G_B_AREA_TH                  0xAE

#define LOG_MSG_CNT                     0xFE
#define LOG_CUR_CHA                     0xFF

// Register sizes (see FTS_AN_CTPM_Standard_eng_ver1.1.pdf)
#define TOUCHREGBLK_SIZE                 4                               // register block size for touches

#define     TOUCH_ID_1      0
#define     TOUCH_ID_2      1

#endif //  __FT5X06_REGS_H__

