#ifndef __HUB_251X_CFG_H
#define __HUB_251X_CFG_H

#include "am33x_gpio.h"

//**********************************************************************************************************
// I2C CONFIGURATION
//**********************************************************************************************************
#define USB251X_I2C_INDEX       (AM_DEVICE_I2C0)
#define USB251X_I2C_ADDR        (0x2C)

//**********************************************************************************************************
// DEFAULT CONFIGURATION
//**********************************************************************************************************
#define USB251X_DEFAULT_CONFIG_BYTE_1   (0x02)
#define USB251X_DEFAULT_CONFIG_BYTE_2   (0x00)
#define USB251X_DEFAULT_CONFIG_BYTE_3   (0x00)
#define USB251X_DEFAULT_PORTMAP_12      (0x21)
#define USB251X_DEFAULT_NR_DEVICE       (0x08)
#define USB251X_DEFAULT_STATUS          (0x01)
//**********************************************************************************************************
// User Configuration
//**********************************************************************************************************
typedef struct 
{
    BYTE CONFIG_DATA_1;
    BYTE CONFIG_DATA_2;
    BYTE CONFIG_DATA_3;
    BYTE PORTMAP_12;
    BYTE NR_DEVICE;
    BYTE STATUS;
}USBHUB_USERCONF;

#endif // __HUB_251X_CFG_H