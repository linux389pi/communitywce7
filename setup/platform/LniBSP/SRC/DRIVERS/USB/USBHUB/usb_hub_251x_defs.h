#ifndef __USB_HUB_251X_DEFS_H
#define __USB_HUB_251X_DEFS_H

//**********************************************************************************************************
// Registers
//**********************************************************************************************************
#define USB251X_VENDOR_ID_LSB                       (0x00)
#define USB251X_VENDOR_ID_MSB                       (0x01)
#define USB251X_PRODUCT_ID_LSB                      (0x02)
#define USB251X_PRODUCT_ID_MSB                      (0x03)
#define USB251X_DEVICE_ID_LSB                       (0x04)
#define USB251X_DEVICE_ID_MSB                       (0x05)
#define USB251X_CONFIGURATION_DATA_BYTE_1           (0x06)
#define USB251X_CONFIGURATION_DATA_BYTE_2           (0x07)
#define USB251X_CONFIGURATION_DATA_BYTE_3           (0x08)
#define USB251X_NON_REMOVABLE_DEVICES               (0x09)
#define USB251X_PORT_DISABLE_SELF                   (0x0A)
#define USB251X_PORT_DISABLE_BUS                    (0x0B)
#define USB251X_MAX_POWER_SELF                      (0x0C)
#define USB251X_MAX_POWER_BUS                       (0x0D)
#define USB251X_HUB_CONTROLLER_MAX_CURRENT_SELF     (0x0E)
#define USB251X_HUB_CONTROLLER_MAX_CURRENT_BUS      (0x0F)
#define USB251X_POWER_ON_TIME                       (0x10)
#define USB251X_LANGUAGE_ID_HIGH                    (0x11)
#define USB251X_LANGUAGE_ID_LOW                     (0x12)
#define USB251X_MANUFACTURER_STRING_LENGTH          (0x13)
#define USB251X_PRODUCT_STRING_LENGTH               (0x14)
#define USB251X_SERIAL_STRING_LENGTH                (0x15)
#define USB251X_MANUFACTURER_STRING                 (0x16)
#define USB251X_PRODUCT_STRING                      (0x54)
#define USB251X_SERIAL_STRING                       (0x92)
#define USB251X_BATTERY_CHARGING_ENABLE             (0xD0)
#define USB251X_BOOST_UP                            (0xF6)
#define USB251X_BOOST_X                             (0xF8)
#define USB251X_PORT_SWAP                           (0xFA)
#define USB251X_PORT_MAP_12                         (0xFB)
#define USB251X_PORT_MAP_34                         (0xFC)
#define USB251X_PORT_MAP_56                         (0xFD)
#define USB251X_PORT_MAP_7                          (0xFE)
#define USB251X_STATUS_COMMAND                      (0xFF)

//**********************************************************************************************************
// Others
//**********************************************************************************************************
// Address space of the device
#define USB251X_ADDR_SPACE_SIZE                     (256)	
// Max number of bytes that can be written to chip at once
#define USB251X_I2C_WRITE_SIZE                      (32)

#endif // __USB_HUB_251X_DEFS_H