//
//  File: usb_hub_251x.c
//
//  This file implements device driver for USB Hub USB251x. 
//
#include "omap.h" 
#include "ceddkex.h" 
#include "sdk_padcfg.h" 
#include "bsp_padcfg.h" 
#include "bsp.h" 
#include "sdk_i2c.h"
#include "usb_hub_251x.h"
#include "usb_hub_251x_defs.h"
#include "usb_hub_251x_cfg.h"

#include <nkintr.h>
#include <winuserm.h>
#include <pmpolicy.h>

// ==== Debug Zones ================================================================================
// simple handling: debug zones are forced with TRUE or FALSE and need a recompile to be activated
// for sophisticated handling: use macro DEBUGZONE(), DBGPARAM dpCurSettings etc.
// (see MSDN e.g. Win CE 5.0, Debug Zones)
#ifndef SHIP_BUILD

#undef ZONE_ERROR
#undef ZONE_WARN
#undef ZONE_FUNCTION
#undef ZONE_INFO
#undef ZONE_TIPSTATE

#define ZONE_ERROR          DEBUGZONE(0)
#define ZONE_WARN           DEBUGZONE(1)
#define ZONE_FUNCTION       DEBUGZONE(2)
#define ZONE_INFO           DEBUGZONE(3)
#define ZONE_TIPSTATE       DEBUGZONE(4)

static DBGPARAM dpCurSettings = {
    L"HUB_DS1307", {
        L"Errors",      L"Warnings",    L"Function",    L"Info",
            L"IST",         L"Undefined",   L"Undefined",   L"Undefined",
            L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined",
            L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined" 
    },
    // Bitfield controlling the zones.  1 means the zone is enabled, 0 disabled
    // We'll enable the Error, Warning, and Info zones by default here
    1 | 1 << 1 | 1 << 2 | 1 << 3
};

#endif
//------------------------------------------------------------------------------
// User-configured settings
static USBHUB_USERCONF m_userConfiguration = 
{
    // CONFIG_DATA_1
    USB251X_DEFAULT_CONFIG_BYTE_1,
    // CONFIG_DATA_2
    USB251X_DEFAULT_CONFIG_BYTE_2,
    // CONFIG_DATA_3
    USB251X_DEFAULT_CONFIG_BYTE_3,
    // PORTMAP_12
    USB251X_DEFAULT_PORTMAP_12,
    // NR_DEVICE
    USB251X_DEFAULT_NR_DEVICE,
    // STATUS
    USB251X_DEFAULT_STATUS
};

// Default USB251x memory map
static BYTE m_defaultInitTable[USB251X_ADDR_SPACE_SIZE] = {
	0x24, 0x04, 0x12, 0x25, 0xb3, 0x0b, 0x9b, 0x20,	/* 00 - 07 */
	0x02, 0x00, 0x00, 0x00, 0x01, 0x32, 0x01, 0x32,	/* 08 - 0F */
	0x32, 0x00, 0x00, 0x03, 0x30, 0x00, 'L', 0x00,	/* 10 - 17 */
	'N', 0x00, 'I', 0x00, 0x00, 0x00, 0x00, 0x00,	/* 18 - 1F */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* 20 - 27 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* 28 - 2F */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* 30 - 37 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* 38 - 3F */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* 40 - 47 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* 48 - 4F */
	0x00, 0x00, 0x00, 0x00, 'U', 0x00, 'S', 0x00,	/* 50 - 57 */
	'B', 0x00, ' ', 0x00, '2', 0x00, '.', 0x00,	/* 58 - 5F */
	'0', 0x00, ' ', 0x00, 'H', 0x00, 'i', 0x00,	/* 60 - 67 */
	'-', 0x00, 'S', 0x00, 'p', 0x00, 'e', 0x00,	/* 68 - 6F */
	'e', 0x00, 'd', 0x00, ' ', 0x00, 'H', 0x00,	/* 70 - 77 */
	'u', 0x00, 'b', 0x00, ' ', 0x00, 'C', 0x00,	/* 78 - 7F */
	'o', 0x00, 'n', 0x00, 't', 0x00, 'r', 0x00,	/* 80 - 87 */
	'o', 0x00, 'l', 0x00, 'l', 0x00, 'e', 0x00,	/* 88 - 8F */
	'r', 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* 90 - 97 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* 98 - 9F */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* A0 - A7 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* A8 - AF */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* B0 - B7 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* B8 - BF */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* C0 - C7 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* C8 - CF */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* D0 - D7 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* D8 - DF */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* E0 - E7 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* E8 - EF */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,	/* F0 - F7 */
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00	/* F8 - FF */
};

//  Device registry parameters
static const DEVICE_REGISTRY_PARAM s_deviceRegParams[] = {
    {
        L"CFG_DATA_1", PARAM_DWORD, FALSE, offset(USBHUB_USERCONF, CONFIG_DATA_1),
        fieldsize(USBHUB_USERCONF, CONFIG_DATA_1), (VOID*)USB251X_DEFAULT_CONFIG_BYTE_1
    },
    {
        L"CFG_DATA_2", PARAM_DWORD, FALSE, offset(USBHUB_USERCONF, CONFIG_DATA_2),
        fieldsize(USBHUB_USERCONF, CONFIG_DATA_2), (VOID*)USB251X_DEFAULT_CONFIG_BYTE_2
    },
    {
        L"CFG_DATA_3", PARAM_DWORD, FALSE, offset(USBHUB_USERCONF, CONFIG_DATA_3),
        fieldsize(USBHUB_USERCONF, CONFIG_DATA_3), (VOID*)USB251X_DEFAULT_CONFIG_BYTE_3
    },
    {
        L"PORTMAP_12", PARAM_DWORD, FALSE, offset(USBHUB_USERCONF, PORTMAP_12),
        fieldsize(USBHUB_USERCONF, PORTMAP_12), (VOID*)USB251X_DEFAULT_PORTMAP_12
    },
    {
        L"NR_DEVICE", PARAM_DWORD, FALSE, offset(USBHUB_USERCONF, NR_DEVICE),
        fieldsize(USBHUB_USERCONF, NR_DEVICE), (VOID*)USB251X_DEFAULT_NR_DEVICE
    },
    {
        L"STATUS", PARAM_DWORD, FALSE, offset(USBHUB_USERCONF, STATUS),
        fieldsize(USBHUB_USERCONF, STATUS), (VOID*)USB251X_DEFAULT_STATUS
    },
};

// I2C handle
static HANDLE m_hI2C;

//------------------------------------------------------------------------------
// @internal
/// I2C Burst Write - Write Data to one register of an I2C device
/// @param[in]    uSlaveAddress The device's i2c address (0..127)
/// @param[in]    pBuffer Pointer to the data bytes to Transmit
/// @param[in]    bSubAddr Register address
/// @param[in]    Number of Bytes to Transmit
/// @param[out]   none
/// @retval       TRUE success
/// @retval       FALSE Error during Transmission
/// @caveats      none
static BOOL I2C_burst_write(UCHAR uSlaveAddress, UCHAR* pBuffer, UCHAR bSubAddr, DWORD iNumberBytes)
{
    BOOL bResult = FALSE;
	LONG lErr;
    
	if(I2CSetSlaveAddress(m_hI2C, (UINT16)uSlaveAddress))
    {
        // Read bytes
        if((UINT)-1 != I2CWrite(m_hI2C,
                               (UCHAR)bSubAddr,
                               pBuffer,
                               iNumberBytes))
        {
            bResult = TRUE;
        }
        else
        {
            lErr = GetLastError();
            RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in USB HOST SMBus @ write data %d"),lErr));
        }
    }
    else
    {
        lErr = GetLastError();
        RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in USB HOST SMBus -> SetSlaveAddress %d"),lErr));
    }
    
    return bResult;
}
/*
// Prepares the command according to SMbus specification and sends data blocks to the chip
static BOOL USB251X_WriteFullConfiguration()
{
	DWORD dwCnt = 0;
    BOOL isOk = FALSE;
	BYTE baWriteBuffer[USB251X_I2C_WRITE_SIZE + 1];

	for (dwCnt = 0; dwCnt < USB251X_ADDR_SPACE_SIZE / USB251X_I2C_WRITE_SIZE; dwCnt++) 
    {
		baWriteBuffer[0] = USB251X_I2C_WRITE_SIZE;
		memcpy(&baWriteBuffer[1], &m_defaultInitTable[dwCnt * USB251X_I2C_WRITE_SIZE], USB251X_I2C_WRITE_SIZE);
		
        isOk = I2C_burst_write(USB251X_I2C_ADDR,baWriteBuffer, sizeof(baWriteBuffer));

		if (!isOk) 
        {
            DEBUGMSG(ZONE_ERROR, (L"ERROR: USB HUB_Init: failed writings to 0x%02x\r\n", (dwCnt * USB251X_I2C_WRITE_SIZE)));
            break;
		}
	}
	return isOk;
}
*/
// Writes configuration bytes to chip registers from start address
static BOOL USB251X_WriteConfigurationBlock(BYTE bStartAddr, BYTE* pbBuffer, BYTE bLen)
{
	BYTE baWriteBuffer[USB251X_I2C_WRITE_SIZE + 1];
	BOOL isOk = FALSE;

	// Limit data size to be written
	if(bLen > USB251X_I2C_WRITE_SIZE)
	{
		bLen = USB251X_I2C_WRITE_SIZE;
	}

	// Set byte count
	baWriteBuffer[0] = bLen;
	// Fill with the data bytes
	memcpy(&baWriteBuffer[1], pbBuffer, bLen);

	isOk = I2C_burst_write(USB251X_I2C_ADDR, baWriteBuffer, bStartAddr, (bLen + 1));

	if (!isOk) 
    {
        DEBUGMSG(ZONE_ERROR, (L"ERROR: USB HUB_Init: failed writings block to 0x%02x\r\n", bStartAddr));
	}

	return isOk;
}

// Writes CONFIG_DATA bytes and NR_DEVICE blocks
static BOOL USB251X_WriteConfigurationData()
{
	BYTE baBuf[4];
	BOOL isOk = FALSE;

	// Prepare Config bytes
	baBuf[0] = m_defaultInitTable[USB251X_CONFIGURATION_DATA_BYTE_1];
	baBuf[1] = m_defaultInitTable[USB251X_CONFIGURATION_DATA_BYTE_2];
	baBuf[2] = m_defaultInitTable[USB251X_CONFIGURATION_DATA_BYTE_3];
	baBuf[3] = m_defaultInitTable[USB251X_NON_REMOVABLE_DEVICES];

	isOk = USB251X_WriteConfigurationBlock(USB251X_CONFIGURATION_DATA_BYTE_1, baBuf, sizeof(baBuf));

	return isOk;
}

//------------------------------------------------------------------------------
DWORD HUB_Init(LPCTSTR szContext, LPCVOID pBusContext)
//  Called by device manager to initialize device.
{
    DWORD rc = (DWORD)NULL;
    BOOL isOk = FALSE;

    UNREFERENCED_PARAMETER(pBusContext);

    // Init I2C0
    m_hI2C = I2COpen(USB251X_I2C_INDEX);

    if (m_hI2C != INVALID_HANDLE_VALUE && m_hI2C != NULL)
    {
		I2CSetBaudIndex(m_hI2C, SLOWSPEED_MODE); // SMBus only works with slow speeds
        I2CSetSubAddressMode(m_hI2C, I2C_SUBADDRESS_MODE_8);
        // Read parameters from registry
        if (GetDeviceRegistryParams(szContext, &m_userConfiguration, dimof(s_deviceRegParams), s_deviceRegParams) != ERROR_SUCCESS)
        {
            DEBUGMSG(ZONE_ERROR, (L"ERROR: USB HUB_Init: Failed read HUB driver registry parameters\r\n"));
            DEBUGMSG(ZONE_ERROR, (L"ERROR: USB HUB_Init: Default parameters applied\r\n"));
        }
        // Apply configuration
        // Set configuration of data byte 1
        m_defaultInitTable[USB251X_CONFIGURATION_DATA_BYTE_1] = m_userConfiguration.CONFIG_DATA_1;
        // Set configuration of data byte 2
        m_defaultInitTable[USB251X_CONFIGURATION_DATA_BYTE_2] = m_userConfiguration.CONFIG_DATA_2;
        // Set configuration of data byte 3
        m_defaultInitTable[USB251X_CONFIGURATION_DATA_BYTE_3] = m_userConfiguration.CONFIG_DATA_3;
		// Set non removable device resgister
        m_defaultInitTable[USB251X_NON_REMOVABLE_DEVICES] = m_userConfiguration.NR_DEVICE;
        // Set port mapping for ports 1 and 2
        m_defaultInitTable[USB251X_PORT_MAP_12] = m_userConfiguration.PORTMAP_12;
        // Set SMBus configuration
        m_defaultInitTable[USB251X_STATUS_COMMAND] = m_userConfiguration.STATUS;

/* @todo Uncomment for final version 
        // Write configuration to chipID
        // CONFIG DATA bytes + NR DEVICE
		isOk = USB251X_WriteConfigurationData();
		if(isOk)
		{
			// PORTMAP
			isOk = USB251X_WriteConfigurationBlock(USB251X_PORT_MAP_12, &m_defaultInitTable[USB251X_PORT_MAP_12], 1);
		}
		if(isOk)
		{
			// STATUS/COMMAND
			isOk = USB251X_WriteConfigurationBlock(USB251X_STATUS_COMMAND, &m_defaultInitTable[USB251X_STATUS_COMMAND], 1);
		}
        
		if(isOk)
        {
            // Return a non null value
            rc = (DWORD) m_hI2C;
        }
*/
        rc = (DWORD) m_hI2C;
    }
	else
	{
		RETAILMSG(ZONE_ERROR,(TEXT("\r\nUSB251x : Failed to open I2C")));
	}

    DEBUGMSG(ZONE_FUNCTION, (L"+HUB_Init(%s, 0x%08x)\r\n", szContext, pBusContext));
    RETAILMSG(1, (L"+HUB_Init\r\n"));

    DEBUGMSG(ZONE_FUNCTION, (L"-HUB_Init(rc = %d\r\n", rc));

    return rc;
}

//------------------------------------------------------------------------------
BOOL HUB_Deinit(DWORD context)
{
    BOOL rc = FALSE;

    DEBUGMSG(ZONE_FUNCTION, (L"+HUB_Deinit(0x%08x)\r\n", context));

    rc = TRUE;

    DEBUGMSG(ZONE_FUNCTION, (L"-HUB_Deinit(rc = %d)\r\n", rc));
    return rc;
}

//------------------------------------------------------------------------------
DWORD HUB_Open(DWORD context, DWORD accessCode, DWORD shareMode)
{
    DWORD rc = (DWORD)NULL;
    BOOL isOk = FALSE;
    UNREFERENCED_PARAMETER(context);
    UNREFERENCED_PARAMETER(accessCode);
    UNREFERENCED_PARAMETER(shareMode);

/* @todo remove the following lines for final version */
    // Write configuration to chipID
    // CONFIG DATA bytes + NR DEVICE
    isOk = USB251X_WriteConfigurationData();
    if(isOk)
    {
        // PORTMAP
        isOk = USB251X_WriteConfigurationBlock(USB251X_PORT_MAP_12, &m_defaultInitTable[USB251X_PORT_MAP_12], 1);
    }
    if(isOk)
    {
        // STATUS/COMMAND
        isOk = USB251X_WriteConfigurationBlock(USB251X_STATUS_COMMAND, &m_defaultInitTable[USB251X_STATUS_COMMAND], 1);
    }
    
    if(isOk)
    {
        // Return a non null value
        rc = (DWORD) m_hI2C;
    }

    return context;
}

//------------------------------------------------------------------------------
BOOL HUB_Close(DWORD context)
{
    UNREFERENCED_PARAMETER(context);
    return TRUE;
}

//------------------------------------------------------------------------------
DWORD HUB_Read(DWORD context, LPVOID pBuffer, DWORD Count)
{
    UNREFERENCED_PARAMETER(context);
    UNREFERENCED_PARAMETER(pBuffer);
    UNREFERENCED_PARAMETER(Count);
    return 0;
}

//------------------------------------------------------------------------------
DWORD HUB_Write(DWORD context, LPCVOID pSourceBytes, DWORD NumberOfBytes)
{
    UNREFERENCED_PARAMETER(context);
    UNREFERENCED_PARAMETER(pSourceBytes);
    UNREFERENCED_PARAMETER(NumberOfBytes);
    return 0;
}

//------------------------------------------------------------------------------
BOOL HUB_IOControl(
    DWORD context, DWORD code, BYTE *pInBuffer, DWORD inSize, BYTE *pOutBuffer,
    DWORD outSize, DWORD *pOutSize
) {
    BOOL isOk = FALSE;

	UNREFERENCED_PARAMETER(context);
    UNREFERENCED_PARAMETER(pOutBuffer);
    UNREFERENCED_PARAMETER(outSize);
    UNREFERENCED_PARAMETER(pOutSize);

	switch(code)
	{
		case USB251X_IOCTL_SET_CONFIG_DATA_1:
			isOk = USB251X_WriteConfigurationBlock(USB251X_CONFIGURATION_DATA_BYTE_1, pInBuffer, 1);
			break;
		case USB251X_IOCTL_SET_CONFIG_DATA_2:
			isOk = USB251X_WriteConfigurationBlock(USB251X_CONFIGURATION_DATA_BYTE_2, pInBuffer, 1);
			break;
		case USB251X_IOCTL_SET_CONFIG_DATA_3:
			isOk = USB251X_WriteConfigurationBlock(USB251X_CONFIGURATION_DATA_BYTE_3, pInBuffer, 1);
			break;
		case USB251X_IOCTL_SET_NR_DEVICE:
			isOk = USB251X_WriteConfigurationBlock(USB251X_NON_REMOVABLE_DEVICES, pInBuffer, 1);
			break;
		case USB251X_IOCTL_SET_PORTMAP_12:
			isOk = USB251X_WriteConfigurationBlock(USB251X_PORT_MAP_12, pInBuffer, 1);
			break;
		case USB251X_IOCTL_SET_STATUS:
			isOk = USB251X_WriteConfigurationBlock(USB251X_STATUS_COMMAND, pInBuffer, 1);
			break;
		default:
			break;
	}
    DEBUGMSG(ZONE_INIT, (L"HUB_IOControl"));
    return FALSE;
}

//------------------------------------------------------------------------------
BOOL __stdcall DllMain(HANDLE hDLL, DWORD reason, VOID *pReserved)
{
    UNREFERENCED_PARAMETER(pReserved);
    switch (reason) {
    case DLL_PROCESS_ATTACH:
        DEBUGREGISTER(hDLL);
        DisableThreadLibraryCalls((HMODULE)hDLL);
        break;
    }
    return TRUE;
}
