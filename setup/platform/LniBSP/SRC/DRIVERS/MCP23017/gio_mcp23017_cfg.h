#ifndef __MCP23017_CFG_H
#define __MCP23017_CFG_H
/*--------------------------------------------------------------------------
 * DRIVER CONFIGURATION SETTINGS
 *-------------------------------------------------------------------------*/
#define MCP23017_I2C_INDEX    AM_DEVICE_I2C0 // Use I2C0

#endif // __MCP23017_CFG_H
