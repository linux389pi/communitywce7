//
//  File: gio_mcp23017.c
//
//  This file implements device driver for External GPIO on MCP23017 
//
#include "gio_mcp23017.h"
#include "gio_mcp23017_cfg.h"
#include "gio_mcp23017_defs.h"

#include "omap.h" 
#include "ceddkex.h" 
#include "sdk_padcfg.h" 
#include "bsp_padcfg.h" 
#include "bsp.h"
#include "sdk_i2c.h"
#include "sdk_gpio.h"

#include <nkintr.h>
#include <winuserm.h>
#include <pmpolicy.h>

// ==== Debug Zones ================================================================================
// simple handling: debug zones are forced with TRUE or FALSE and need a recompile to be activated
// for sophisticated handling: use macro DEBUGZONE(), DBGPARAM dpCurSettings etc.
// (see MSDN e.g. Win CE 5.0, Debug Zones)
#ifndef SHIP_BUILD

#undef ZONE_ERROR
#undef ZONE_WARN
#undef ZONE_FUNCTION
#undef ZONE_INFO
#undef ZONE_TIPSTATE

#define ZONE_ERROR          DEBUGZONE(0)
#define ZONE_WARN           DEBUGZONE(1)
#define ZONE_FUNCTION       DEBUGZONE(2)
#define ZONE_INFO           DEBUGZONE(3)
#define ZONE_TIPSTATE       DEBUGZONE(4)

static DBGPARAM dpCurSettings = {
    L"GPIO_23017", {
        L"Errors",      L"Warnings",    L"Function",    L"Info",
            L"IST",         L"Undefined",   L"Undefined",   L"Undefined",
            L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined",
            L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined" 
    },
    // Bitfield controlling the zones.  1 means the zone is enabled, 0 disabled
    // We'll enable the Error, Warning, and Info zones by default here
    1 | 1 << 1 | 1 << 2 | 1 << 3
};

#endif

// MCP23017 GPIO register port identifier
typedef enum
 {
     Gio_PortA = 0x00,
     Gio_PortB = MCP23017_REGISTER_PORT_OFFSET   // Shift between portA and portB registers
 }Mcp23017_Register_Port_t;

// MCP23017 Configuration registers
typedef struct
{
    // Direction register
	BYTE IODIR;
    // Polarity register
	BYTE IPOL;
	// Port output register
    BYTE GPIO;
	// Chip mode configuration register
	BYTE IOCON;
}Mcp23017_Register_Cfg_t;

// I2C handle
static HANDLE                           m_hI2C;
// Local copy of GPIO statuses for all pins controlled by MCP23017
// Avoids the necessity to read a register before updating it
static Mcp23017_Register_Cfg_t          m_MCP23017_Regs[2]; // for PortA and PortB

//----- privat methods definition -----------------------------------------------------------------
// @internal
/// I2C Burst Read - Read Data from one register of an I2C device
/// @param[in]    uSlaveAddress The device's i2c address (0..127) if uSlaveAddress=0xff, no slaveAddress byte will be transmitted.
/// @param[out]   pBuffer Pointer to the data bytes to Receive
/// @param[in]    ucSubAddr The device's register address (offset). This is one byte transmitted after the slave address.
///                Set to any negative value if no offset byte should be transmitted
/// @param[in]    iNumberBytes Number of Bytes to Receive
/// @retval       TRUE success
/// @retval       FALSE Error during Transmission
/// @caveats      none
static BOOL burst_read(UCHAR uSlaveAddress, UCHAR* pBuffer, UCHAR ucSubAddr, DWORD iNumberBytes)
{
    BOOL bResult = FALSE;
	LONG lErr;
    
	if(I2CSetSlaveAddress(m_hI2C, (UINT16)uSlaveAddress))
    {
        // Read bytes
        if((UINT)-1 != I2CRead(m_hI2C,
                               (UCHAR)ucSubAddr,
                               pBuffer,
                               iNumberBytes))
        {
            bResult = TRUE;
        }
        else
        {
            lErr = GetLastError();
        RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in burst_read()->I2CRead %d\r\n"),lErr));
        }
    }
    else
    {
        lErr = GetLastError();
        RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in burst_read()->SetSlaveAddress %d\r\n"),lErr));
    }
    
    return bResult;
}
// @internal
/// I2C Burst Write - Write Data to one register of an I2C device
/// @param[in]    uSlaveAddress The device's i2c address (0..127)
/// @param[in]    pBuffer Pointer to the data bytes to Transmit
/// @param[in]    ucSubAddr The device's register address (offset). This is one byte transmitted after the slave address.
///               Set to any negative value if no offset byte should be transmitted
/// @param[in]    Number of Bytes to Transmit
/// @param[out]   none
/// @retval       TRUE success
/// @retval       FALSE Error during Transmission
/// @caveats      none
static BOOL burst_write(UCHAR uSlaveAddress, UCHAR* pBuffer, UCHAR ucSubAddr, DWORD iNumberBytes)
{
    BOOL bResult = FALSE;
	LONG lErr;
    
	if(I2CSetSlaveAddress(m_hI2C, (UINT16)uSlaveAddress))
    {
        // Read bytes
        if((UINT)-1 != I2CWrite(m_hI2C,
                               (UCHAR)ucSubAddr,
                               pBuffer,
                               iNumberBytes))
        {
            bResult = TRUE;
        }
        else
        {
            lErr = GetLastError();
            RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in burst_write() @ write data %d\r\n"),lErr));
        }
    }
    else
    {
        lErr = GetLastError();
        RETAILMSG(ZONE_ERROR,(TEXT("\r\nillegal response in burst_write() -> SetSlaveAddress %d\r\n"),lErr));
    }

    return bResult;
}

//------------------------------------------------------------------------------
DWORD GIO_Init(LPCTSTR szContext, LPCVOID pBusContext)
//  Called by device manager to initialize device.
{
    DWORD rc = (DWORD)FALSE;

    UNREFERENCED_PARAMETER(pBusContext);

    DEBUGMSG(ZONE_FUNCTION, (L"+GIO_Init(%s, 0x%08x)\r\n", szContext, pBusContext));
    RETAILMSG(1, (L"+GIO_Init\r\n"));

    // Init internal vars
    memset(m_MCP23017_Regs, 0, sizeof(m_MCP23017_Regs));
	// Force all pins to be LOW when cleared at boot
	m_MCP23017_Regs[0].IPOL = 0xFF;
	m_MCP23017_Regs[1].IPOL = 0xFF;

    // Open I2C0 
    m_hI2C = I2COpen(MCP23017_I2C_INDEX);
    if (m_hI2C != INVALID_HANDLE_VALUE && m_hI2C != NULL)
    {
        I2CSetBaudIndex(m_hI2C, HIGHSPEED_MODE_1P6); // 1.6 MHz
		I2CSetSubAddressMode(m_hI2C, I2C_SUBADDRESS_MODE_8); // 8 bits address mode
        //
        // Initialize MCP23017 chip
        //
        // Specify the work mode : Byte mode, not sequential
		m_MCP23017_Regs[0].IOCON = MCP23017_IOCON_SETTINGS;
		rc = burst_write(MCP23017_I2C_ADDRESS, &m_MCP23017_Regs[0].IOCON, MCP23017_IOCON, 1U);
		// Write default values to the chip
		if(rc != FALSE)
        {
			rc = burst_write(MCP23017_I2C_ADDRESS, &m_MCP23017_Regs[0].IODIR, MCP23017_IODIRA, 1U);
		}
        if(rc != FALSE)
        {
            rc = burst_write(MCP23017_I2C_ADDRESS, &m_MCP23017_Regs[1].IODIR, MCP23017_IODIRB, 1U);
        }
        if(rc != FALSE)
        {
            rc = burst_write(MCP23017_I2C_ADDRESS, &m_MCP23017_Regs[0].IPOL, MCP23017_IPOLA, 1U);
        }
        if(rc != FALSE)
        {
            rc = burst_write(MCP23017_I2C_ADDRESS, &m_MCP23017_Regs[1].IPOL, MCP23017_IPOLB, 1U);
        }
        if(rc != FALSE)
        {
            rc = burst_write(MCP23017_I2C_ADDRESS, &m_MCP23017_Regs[0].GPIO, MCP23017_GPIOA, 1U);
        }
        if(rc != FALSE)
        {
            rc = burst_write(MCP23017_I2C_ADDRESS, &m_MCP23017_Regs[1].GPIO, MCP23017_GPIOB, 1U);
        }
    }
    else
    {
        DEBUGMSG(ZONE_ERROR, (L"\r\nGPIO MCP23017 : Failed to open I2C"));
    }

    if (rc == FALSE) GIO_Deinit((DWORD)0);
    DEBUGMSG(ZONE_FUNCTION, (L"-GIO_Init(rc = %d\r\n", rc));

    return rc;
}

//------------------------------------------------------------------------------
BOOL GIO_Deinit(DWORD context)
{
    BOOL rc = FALSE;

    DEBUGMSG(ZONE_FUNCTION, (L"+GIO_Deinit()\r\n"));
	RETAILMSG(1, (L"+GIO_Deinit\r\n"));

    // I2C driver
    I2CClose(m_hI2C);

    rc = TRUE;

    DEBUGMSG(ZONE_FUNCTION, (L"-GIO_Deinit(rc = %d)\r\n", rc));
    return rc;
}

//------------------------------------------------------------------------------
DWORD GIO_Open(DWORD context, DWORD accessCode, DWORD shareMode)
{
    UNREFERENCED_PARAMETER(context);
    UNREFERENCED_PARAMETER(accessCode);
    UNREFERENCED_PARAMETER(shareMode);
    return context;
}

//------------------------------------------------------------------------------
BOOL GIO_Close(DWORD context)
{
    UNREFERENCED_PARAMETER(context);
    return TRUE;
}

//------------------------------------------------------------------------------
DWORD GIO_Read(DWORD context, LPVOID pBuffer, DWORD Count)
{
    UNREFERENCED_PARAMETER(context);
    UNREFERENCED_PARAMETER(pBuffer);
    UNREFERENCED_PARAMETER(Count);
    return 0;
}

//------------------------------------------------------------------------------
DWORD GIO_Write(DWORD context, LPCVOID pSourceBytes, DWORD NumberOfBytes)
{
    UNREFERENCED_PARAMETER(context);
    UNREFERENCED_PARAMETER(pSourceBytes);
    UNREFERENCED_PARAMETER(NumberOfBytes);
    return 0;
}

//------------------------------------------------------------------------------
BOOL GIO_IOControl(
    DWORD context, DWORD code, BYTE *pInBuffer, DWORD inSize, BYTE *pOutBuffer,
    DWORD outSize, DWORD *pOutSize
) {
    BOOL res = FALSE;
    BYTE bData = 0x00;
    BYTE bPinIndex = 0x00;
    BYTE bPortOffset = 0x00;
    Mcp23017_Register_Cfg_t* xpRegConfig = &m_MCP23017_Regs[0]; // default;  
    
    UNREFERENCED_PARAMETER(context);
    
    DEBUGMSG(ZONE_FUNCTION, (L"+GIO_IOControl()\r\n"));

    if(inSize == 1U
        && pInBuffer != NULL)
    {
        // Marshal input parameter
        bPinIndex = *pInBuffer; // default
        // Post-process bPin to identify the register Port and Pin index
        if(bPinIndex > (BYTE)GIO_A7)
        {
            bPortOffset = MCP23017_REGISTER_PORT_OFFSET;
            bPinIndex = (BYTE)(bPinIndex - (BYTE)GIO_B0);
            xpRegConfig = &m_MCP23017_Regs[1]; 
        }

        switch(code)
        {
            case MCP23017_IOCTL_SET_DIR_INPUT :
                bData = MCP23017_SET_PIN_AS_INPUT(xpRegConfig->IODIR, bPinIndex);
                res = burst_write(MCP23017_I2C_ADDRESS, &bData, (MCP23017_IODIR + bPortOffset), 1U);
                if(res != FALSE)
                {
                    // Save the new value locally
                    xpRegConfig->IODIR = bData;
                }
                break;
            case MCP23017_IOCTL_SET_DIR_OUTPUT :
                bData = MCP23017_SET_PIN_AS_OUTPUT(xpRegConfig->IODIR, bPinIndex);
                res = burst_write(MCP23017_I2C_ADDRESS, &bData, (MCP23017_IODIR + bPortOffset), 1U);
                if(res != FALSE)
                {
                    // Save the new value locally
                    xpRegConfig->IODIR = bData;
                }
                break;
            case MCP23017_IOCTL_SET_POL_NORMAL :
                bData = MCP23017_SET_POL_NORMAL(xpRegConfig->IPOL, bPinIndex);
                res = burst_write(MCP23017_I2C_ADDRESS, &bData, (MCP23017_IPOL + bPortOffset), 1U);
                if(res != FALSE)
                {
                    // Save the new value locally
                    xpRegConfig->IPOL = bData;
                }
                break;
            case MCP23017_IOCTL_SET_POL_REVERSED :
                bData = MCP23017_SET_POL_REVERSED(xpRegConfig->IPOL, bPinIndex);
                res = burst_write(MCP23017_I2C_ADDRESS, &bData, (MCP23017_IPOL + bPortOffset), 1U);
                if(res != FALSE)
                {
                    // Save the new value locally
                    xpRegConfig->IPOL = bData;
                }
                break;
            case MCP23017_IOCTL_SET_PIN :
                bData = MCP23017_SET_PIN(xpRegConfig->GPIO, bPinIndex);
                res = burst_write(MCP23017_I2C_ADDRESS, &bData, (MCP23017_GPIO + bPortOffset), 1U);
                if(res != FALSE)
                {
                    // Save the new value locally
                    xpRegConfig->GPIO = bData;
                }
                break;
            case MCP23017_IOCTL_CLR_PIN :
                bData = MCP23017_CLR_PIN(xpRegConfig->GPIO, bPinIndex);
                res = burst_write(MCP23017_I2C_ADDRESS, &bData, (MCP23017_GPIO + bPortOffset), 1U);
                if(res != FALSE)
                {
                    // Save the new value locally
                    xpRegConfig->GPIO = bData;
                }
                break;
            case MCP23017_IOCTL_GET_PIN :// force remote read
                res = burst_read(MCP23017_I2C_ADDRESS, &xpRegConfig->GPIO, (MCP23017_GPIO + bPortOffset), 1U);
                if(res != FALSE)
                {
                    // Return the value
                    *pOutBuffer = (((xpRegConfig->GPIO) & ((0x00U) | (1 << bPinIndex))) ? 0x01 : 0x00);
                    *pOutSize = (BYTE) 1U;
                }
                break;
            case MCP23017_IOCTL_GET_DIR:
                res = burst_read(MCP23017_I2C_ADDRESS, &xpRegConfig->IODIR, (MCP23017_IODIR + bPortOffset), 1U);
                if(res != FALSE)
                {
                    // Return the value
                    *pOutBuffer = (((xpRegConfig->IODIR) & ((0x00U) | (1 << bPinIndex))) ? 0x01 : 0x00);
                    *pOutSize = (BYTE) 1U;
                }
                break;
            case MCP23017_IOCTL_GET_POL:
                res = burst_read(MCP23017_I2C_ADDRESS, &xpRegConfig->IODIR, (MCP23017_IODIR + bPortOffset), 1U);
                if(res != FALSE)
                {
                    // Return the value
                    *pOutBuffer = (((xpRegConfig->IODIR) & ((0x00U) | (1 << bPinIndex))) ? 0x01 : 0x00);
                    *pOutSize = (BYTE) 1U;
                }
                break;
            default:
                break;
        }
    }
    else
    {
        DEBUGMSG(ZONE_ERROR, (L"GIO_IOControl : Bad parameters (pInBuffer=0x%08x, inSize=%d)\r\n", pInBuffer, inSize));
    }

    DEBUGMSG(ZONE_FUNCTION, (L"-GIO_IOControl()\r\n"));

    return res;
}

//------------------------------------------------------------------------------
BOOL __stdcall DllMain(HANDLE hDLL, DWORD reason, VOID *pReserved)
{
    UNREFERENCED_PARAMETER(pReserved);
    switch (reason) {
    case DLL_PROCESS_ATTACH:
        DEBUGREGISTER(hDLL);
        DisableThreadLibraryCalls((HMODULE)hDLL);
        break;
    }
    return TRUE;
}