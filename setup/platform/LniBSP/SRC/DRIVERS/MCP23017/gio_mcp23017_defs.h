#ifndef __MCP23017_DEFS_H
#define __MCP23017_DEFS_H

/*--------------------------------------------------------------------------
 * MCP23017 I2C SETTINGS
 *-------------------------------------------------------------------------*/
#define MCP23017_I2C_ADDRESS      0x27 // (00100111)

/*--------------------------------------------------------------------------
 * REGISTER DEFINITIONS
 *-------------------------------------------------------------------------*/
// Offset addres between portA and portB registers
#define MCP23017_REGISTER_PORT_OFFSET       0x01

// Direction register 
#define MCP23017_IODIR     0x00
// Polarity register 
#define MCP23017_IPOL      0x02
// Configuration register
#define MCP23017_IOCON     0x0A // Shared betwwen A and B
// Pull up register 
#define MCP23017_GPPU      0x0C
// Port value 
#define MCP23017_GPIO      0x12
// Latch values (not port real value)
#define MCP23017_OLAT      0x14

// Direction register A
#define MCP23017_IODIRA     MCP23017_IODIR
// Direction register B
#define MCP23017_IODIRB     (MCP23017_IODIRA + MCP23017_REGISTER_PORT_OFFSET)
// Polarity register A
#define MCP23017_IPOLA      MCP23017_IPOL
// Polarity register B
#define MCP23017_IPOLB      (MCP23017_IPOLA + MCP23017_REGISTER_PORT_OFFSET)
// Pull up register A
#define MCP23017_GPPUA      MCP23017_GPPU
// Pull up register B
#define MCP23017_GPPUB      (MCP23017_GPPUA + MCP23017_REGISTER_PORT_OFFSET)
// Port value A
#define MCP23017_GPIOA      MCP23017_GPIO
// Port value B
#define MCP23017_GPIOB      (MCP23017_GPIOA + MCP23017_REGISTER_PORT_OFFSET)
// Latch values A (not port real value)
#define MCP23017_OLATA      MCP23017_OLAT
// Latch values B (not port real value)
#define MCP23017_OLATB      (MCP23017_OLATA + MCP23017_REGISTER_PORT_OFFSET)

// Chip mode configuration (the driver only works with these settings)
#define MCP23017_IOCON_SEQOP_DISABLED  0x20 // Sequential mode disabled. Registers are not auto-incrementing
#define MCP23017_IOCON_SETTINGS		   (MCP23017_IOCON_SEQOP_DISABLED)

/*--------------------------------------------------------------------------
 * HELPER MACROS
 *-------------------------------------------------------------------------*/
 // Set pin direction
 #define MCP23017_SET_PIN_AS_INPUT(dir_register, bit) ((dir_register) | (1 << bit))
 #define MCP23017_SET_PIN_AS_OUTPUT(dir_register, bit) ( (dir_register) & (~(1 << bit)) ) 
 // Set pin polarity
 #define MCP23017_SET_POL_NORMAL(pol_register, bit) ( (pol_register) & (~(1 << bit)) )
 #define MCP23017_SET_POL_REVERSED(pol_register, bit) ((pol_register) | (1 << bit))
 // Set pin state to HIGH
 #define MCP23017_SET_PIN(gpio_register, bit) ((gpio_register) | (1 << bit))
 #define MCP23017_CLR_PIN(gpio_register, bit) ((gpio_register) & (~(1 << bit)) )
 



#endif // __MCP23017_DEFS_H
