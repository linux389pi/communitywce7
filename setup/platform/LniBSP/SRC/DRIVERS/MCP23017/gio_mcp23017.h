#ifndef __MCP23017__H
#define __MCP23017__H

#include "winioctl.h"

/*--------------------------------------------------------------------------
 * GPIO Public IDs
 *-------------------------------------------------------------------------*/
 typedef enum
 {
     // PortA
     GIO_A0 = 0,
     GIO_A1,
     GIO_A2,
     GIO_A3,
     GIO_A4,
     GIO_A5,
     GIO_A6,
     GIO_A7,
                    // Do not add anything here !
     // PortB
     GIO_B0 = 8,
     GIO_B1,
     GIO_B2,
     GIO_B3,
     GIO_B4,
     GIO_B5,
     GIO_B6,
     GIO_B7,
 }Mcp23017_Gio_Id_t;

/*--------------------------------------------------------------------------
 * IOCTL CODES
 *-------------------------------------------------------------------------*/
#define MCP23017_IOCTL_FILE_DEVICE      (2048) // The first 2047 are reserved by WinCE

#define MCP23017_IOCTL_SET_DIR_INPUT \
    CTL_CODE(MCP23017_IOCTL_FILE_DEVICE, 0x100, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define MCP23017_IOCTL_SET_DIR_OUTPUT \
    CTL_CODE(MCP23017_IOCTL_FILE_DEVICE, 0x101, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define MCP23017_IOCTL_SET_POL_NORMAL \
    CTL_CODE(MCP23017_IOCTL_FILE_DEVICE, 0x102, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define MCP23017_IOCTL_SET_POL_REVERSED \
    CTL_CODE(MCP23017_IOCTL_FILE_DEVICE, 0x103, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define MCP23017_IOCTL_SET_PIN \
    CTL_CODE(MCP23017_IOCTL_FILE_DEVICE, 0x104, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define MCP23017_IOCTL_CLR_PIN \
    CTL_CODE(MCP23017_IOCTL_FILE_DEVICE, 0x105, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define MCP23017_IOCTL_GET_PIN \
    CTL_CODE(MCP23017_IOCTL_FILE_DEVICE, 0x106, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define MCP23017_IOCTL_GET_DIR \
    CTL_CODE(MCP23017_IOCTL_FILE_DEVICE, 0x107, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define MCP23017_IOCTL_GET_POL \
    CTL_CODE(MCP23017_IOCTL_FILE_DEVICE, 0x108, METHOD_BUFFERED, FILE_ANY_ACCESS)    

#endif // __MCP23017__H
