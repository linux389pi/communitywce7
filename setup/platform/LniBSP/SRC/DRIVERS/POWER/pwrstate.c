//
//  File: pwrstate.c
//
//  This file implements device driver for RTC DS1307. 
//
#include "omap.h" 
#include "ceddkex.h" 
#include "sdk_padcfg.h" 
#include "bsp_padcfg.h" 
#include "bsp.h" 
#include "pwrstate.h"
#include "pwrstate_defs.h"

#include <nkintr.h>
#include <winuserm.h>
#include <pmpolicy.h>

// ==== Debug Zones ================================================================================
// simple handling: debug zones are forced with TRUE or FALSE and need a recompile to be activated
// for sophisticated handling: use macro DEBUGZONE(), DBGPARAM dpCurSettings etc.
// (see MSDN e.g. Win CE 5.0, Debug Zones)
#ifndef SHIP_BUILD

#undef ZONE_ERROR
#undef ZONE_WARN
#undef ZONE_FUNCTION
#undef ZONE_INFO
#undef ZONE_TIPSTATE

#define ZONE_ERROR          DEBUGZONE(0)
#define ZONE_WARN           DEBUGZONE(1)
#define ZONE_FUNCTION       DEBUGZONE(2)
#define ZONE_INFO           DEBUGZONE(3)
#define ZONE_IST            DEBUGZONE(4)

static DBGPARAM dpCurSettings = {
    L"PWS_DS1307", {
        L"Errors",      L"Warnings",    L"Function",    L"Info",
            L"IST",         L"Undefined",   L"Undefined",   L"Undefined",
            L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined",
            L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined" 
    },
    // Bitfield controlling the zones.  1 means the zone is enabled, 0 disabled
    // We'll enable the Error, Warning, and Info zones by default here
    1 | 1 << 1 | 1 << 2 | 1 << 3
};

#endif

//------------------------------------------------------------------------------
//  Local Definitions

#define PWS_DEVICE_COOKIE           'PWRM'

//------------------------------------------------------------------------------
//  Local Structures

typedef struct {
    DWORD Cookie;
    DWORD priority256;

    HANDLE hGpio;
    HANDLE hUserEvent;

    // Power states
    BOOL isPrimaryPresent;
    BOOL isCigarLighterPresent;
    BOOL isExternalBatteryPresent;
    PWS_PowerStates_te currentPowerState;

    // Primary interrupt object
    HANDLE hPrimaryIntrEvent;
    DWORD dwSysintrPrimary;
    HANDLE hPrimaryIntrThread;

    // Cigar lighter interrupt objects
    HANDLE hCigarLIntrEvent;
    DWORD dwSysintrCigarL;
    HANDLE hCigarLIntrThread;

    // External battery interrupt objects
    HANDLE hExtBatteryIntrEvent;
    DWORD dwSysintrExtBattery;
    HANDLE hExtBatteryIntrThread;

    BOOL bIntrThreadExit;
	DWORD timeout;
} PwrMonitorDevice_t;

//------------------------------------------------------------------------------
//  Device registry parameters

static const DEVICE_REGISTRY_PARAM s_deviceRegParams[] = {
    {
        L"Priority256", PARAM_DWORD, FALSE, 
        offset(PwrMonitorDevice_t, priority256),
        fieldsize(PwrMonitorDevice_t, priority256), (VOID*)100
    }};

//------------------------------------------------------------------------------
//  Local variables

// Global and unique handle
static PwrMonitorDevice_t m_Device = {0,};

//------------------------------------------------------------------------------
//  Local function definitions

BOOL PWS_Deinit(DWORD context);
DWORD PWS_PrimaryIntrThread(VOID *pContext);
DWORD PWS_CigarLIntrThread(VOID *pContext);
DWORD PWS_ExtBatteryIntrThread(VOID *pContext);

//------------------------------------------------------------------------------
//  Static methods

// Re-computes the power supply state engine 
static VOID ComputePowerStatus(PwrMonitorDevice_t *pDevice)
{
    //pDevice->currentPowerState

    if(pDevice->isPrimaryPresent)
    {
        pDevice->currentPowerState = Normal;
    }
    else if(pDevice->isCigarLighterPresent)
    {
        pDevice->currentPowerState = Transport;
    }
    else if(pDevice->isExternalBatteryPresent)
    {
        pDevice->currentPowerState = Standalone;
    }
    else
    {
        // No power source left. Either we are already dead or a secure battery
        // is connected to TPS65217 and we will be alive for a short time
        pDevice->currentPowerState = Shutdown;
    }

    // Set public event
    SetEvent(pDevice->hUserEvent);
}

//------------------------------------------------------------------------------
DWORD PWS_Init(LPCTSTR szContext, LPCVOID pBusContext)
//  Called by device manager to initialize device.
{
    DWORD rc = (DWORD)NULL;
    PwrMonitorDevice_t *pDevice = &m_Device;

    UNREFERENCED_PARAMETER(pBusContext);
    DEBUGMSG(ZONE_FUNCTION, (L"+PWS_Init(%s, 0x%08x)\r\n", szContext, pBusContext));

    memset(pDevice, 0, sizeof(PwrMonitorDevice_t));

    // Set Cookie
    pDevice->Cookie = PWS_DEVICE_COOKIE;

    // Read device parameters
    if (GetDeviceRegistryParams(szContext, pDevice, dimof(s_deviceRegParams), s_deviceRegParams) != ERROR_SUCCESS)
    {
        DEBUGMSG(ZONE_ERROR, (L"ERROR: PWS_Init: Failed read PWR driver registry parameters\r\n"));
        goto cleanUp;
    }

    // Create interrupt events
    pDevice->hPrimaryIntrEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (pDevice->hPrimaryIntrEvent == NULL)
    {
        DEBUGMSG(ZONE_ERROR, (L"ERROR: PWS_Init: Failed create interrupt event for primary supply\r\n"));
        goto cleanUp;
    }
    pDevice->hCigarLIntrEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (pDevice->hCigarLIntrEvent == NULL)
    {
        DEBUGMSG(ZONE_ERROR, (L"ERROR: PWS_Init: Failed create interrupt event for cigar lighter\r\n"));
        goto cleanUp;
    }
    pDevice->hExtBatteryIntrEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
    if (pDevice->hExtBatteryIntrEvent == NULL)
    {
        DEBUGMSG(ZONE_ERROR, (L"ERROR: PWS_Init: Failed create interrupt event for external battery\r\n"));
        goto cleanUp;
    }
    // Create public event to notify applications about a power status change
    pDevice->hUserEvent = CreateEventW(NULL, FALSE, FALSE, POWER_STATE_PUBLIC_EVENT_NAME);
    if (pDevice->hUserEvent == NULL)
    {
        DEBUGMSG(ZONE_ERROR, (L"ERROR: PWS_Init: Failed create public event\r\n"));
        goto cleanUp;
    }

    // Open GPIO driver
    pDevice->hGpio = GPIOOpen();
    if(!pDevice->hGpio)
    {
        DEBUGMSG(ZONE_ERROR, (L"ERROR: PWS_Init: Failed to init GPIO handler\r\n"));
        goto cleanUp;
    }
    // Set mode for all input pins      
    GPIOSetMode(pDevice->hGpio,PWS_PRIMARY_SUPPLY_SIGNAL,GPIO_DIR_INPUT | GPIO_INT_LOW_HIGH | GPIO_INT_HIGH_LOW);
    GPIOSetMode(pDevice->hGpio,PWS_CIGAR_LIGHTER_SIGNAL,GPIO_DIR_INPUT | GPIO_INT_LOW_HIGH | GPIO_INT_HIGH_LOW);
    GPIOSetMode(pDevice->hGpio,PWS_EXT_BATTERY_SIGNAL,GPIO_DIR_INPUT | GPIO_INT_LOW_HIGH | GPIO_INT_HIGH_LOW);
    // Enable interrupt for each one of them
    // Primary supply
    if (!GPIOInterruptInitialize(pDevice->hGpio,PWS_PRIMARY_SUPPLY_SIGNAL,&pDevice->dwSysintrPrimary,pDevice->hPrimaryIntrEvent))
    {
        DEBUGMSG(ZONE_ERROR, (L"ERROR: PWS_Init: Failed to init interrupt for primary supply\r\n"));
        goto cleanUp;
    }    
    GPIOInterruptDone(pDevice->hGpio,PWS_PRIMARY_SUPPLY_SIGNAL,pDevice->dwSysintrPrimary);
    // Cigar lighter
    if (!GPIOInterruptInitialize(pDevice->hGpio,PWS_CIGAR_LIGHTER_SIGNAL,&pDevice->dwSysintrCigarL,pDevice->hCigarLIntrEvent))
    {
        DEBUGMSG(ZONE_ERROR, (L"ERROR: PWS_Init: Failed to init interrupt for cigar lighter\r\n"));
        goto cleanUp;
    }    
    GPIOInterruptDone(pDevice->hGpio,PWS_CIGAR_LIGHTER_SIGNAL,pDevice->dwSysintrCigarL);
    // External battery
    if (!GPIOInterruptInitialize(pDevice->hGpio,PWS_EXT_BATTERY_SIGNAL,&pDevice->dwSysintrExtBattery,pDevice->hExtBatteryIntrEvent))
    {
        DEBUGMSG(ZONE_ERROR, (L"ERROR: PWS_Init: Failed to init interrupt for external battery\r\n"));
        goto cleanUp;
    }    
    GPIOInterruptDone(pDevice->hGpio,PWS_EXT_BATTERY_SIGNAL,pDevice->dwSysintrExtBattery);

    // Set IST timeouts
    pDevice->timeout = PWS_IST_TIMEOUT_MS;
    // Set default power statuses
    pDevice->isPrimaryPresent = (GPIOGetBit(pDevice->hGpio, PWS_PRIMARY_SUPPLY_SIGNAL) == 1);
    pDevice->isCigarLighterPresent = (GPIOGetBit(pDevice->hGpio, PWS_CIGAR_LIGHTER_SIGNAL) == 1);
    pDevice->isExternalBatteryPresent = (GPIOGetBit(pDevice->hGpio, PWS_EXT_BATTERY_SIGNAL) == 1);

    // Start interrupt service threads
    pDevice->bIntrThreadExit = FALSE;
    // Primary supply
    pDevice->hPrimaryIntrThread = CreateThread(NULL, 0, PWS_PrimaryIntrThread, pDevice, 0, NULL);
    if (!pDevice->hPrimaryIntrThread)
    {
        DEBUGMSG (ZONE_ERROR, (L"ERROR: PWS_Init: Failed create interrupt thread for primary\r\n"));
        goto cleanUp;
    }
    // Cigar lighter
    pDevice->hCigarLIntrThread = CreateThread(NULL, 0, PWS_CigarLIntrThread, pDevice, 0, NULL);
    if (!pDevice->hCigarLIntrThread)
    {
        DEBUGMSG (ZONE_ERROR, (L"ERROR: PWS_Init: Failed create interrupt thread for cigar lighter\r\n"));
        goto cleanUp;
    }
    // External battery
    pDevice->hExtBatteryIntrThread = CreateThread(NULL, 0, PWS_ExtBatteryIntrThread, pDevice, 0, NULL);
    if (!pDevice->hExtBatteryIntrThread)
    {
        DEBUGMSG (ZONE_ERROR, (L"ERROR: PWS_Init: Failed create interrupt thread for external battery\r\n"));
        goto cleanUp;
    }

    // Set thread priorities
    CeSetThreadPriority(pDevice->hPrimaryIntrThread, pDevice->priority256);
    CeSetThreadPriority(pDevice->hCigarLIntrThread, pDevice->priority256);
    CeSetThreadPriority(pDevice->hExtBatteryIntrThread, pDevice->priority256);

    // Return non-null value
    rc = (DWORD)pDevice;

cleanUp:
   if (rc == 0)
    {
        PWS_Deinit((DWORD)pDevice);
    }
    DEBUGMSG(ZONE_FUNCTION, (L"-PWS_Init(rc = %d\r\n", rc));

    return rc;
}

//------------------------------------------------------------------------------
BOOL PWS_Deinit(DWORD context)
{
    BOOL rc = FALSE;
    PwrMonitorDevice_t *pDevice = (PwrMonitorDevice_t*)context;

    DEBUGMSG(ZONE_FUNCTION, (L"+PWS_Deinit(0x%08x)\r\n", context));

    pDevice->bIntrThreadExit = TRUE; // Stop threads
	if (pDevice->hPrimaryIntrThread != NULL)
    {
        WaitForSingleObject(pDevice->hPrimaryIntrEvent, INFINITE);
		CloseHandle(pDevice->hPrimaryIntrThread);
    }
    if (pDevice->hCigarLIntrThread != NULL)
    {
        WaitForSingleObject(pDevice->hCigarLIntrThread, INFINITE);
		CloseHandle(pDevice->hCigarLIntrThread);
    }
    if (pDevice->hExtBatteryIntrThread != NULL)
    {
        WaitForSingleObject(pDevice->hExtBatteryIntrThread, INFINITE);
		CloseHandle(pDevice->hExtBatteryIntrThread);
    }
    if (!pDevice->hPrimaryIntrEvent)
    {
        CloseHandle(pDevice->hPrimaryIntrEvent);
    }
    if (!pDevice->hCigarLIntrEvent)
    {
        CloseHandle(pDevice->hCigarLIntrEvent);
    }
    if (!pDevice->hExtBatteryIntrEvent)
    {
        CloseHandle(pDevice->hExtBatteryIntrEvent);
    }
    if (!pDevice->hUserEvent)
    {
        CloseHandle(pDevice->hUserEvent);
    }
    if(pDevice->hGpio != NULL)
    {
        GPIOInterruptDone(pDevice->hGpio, PWS_PRIMARY_SUPPLY_SIGNAL, pDevice->dwSysintrPrimary);
        GPIOInterruptDisable(pDevice->hGpio, PWS_PRIMARY_SUPPLY_SIGNAL,pDevice->dwSysintrPrimary);
        GPIOInterruptDone(pDevice->hGpio, PWS_CIGAR_LIGHTER_SIGNAL, pDevice->dwSysintrCigarL);
        GPIOInterruptDisable(pDevice->hGpio, PWS_CIGAR_LIGHTER_SIGNAL, pDevice->dwSysintrCigarL);
        GPIOInterruptDone(pDevice->hGpio, PWS_EXT_BATTERY_SIGNAL, pDevice->dwSysintrExtBattery);
        GPIOInterruptDisable(pDevice->hGpio, PWS_EXT_BATTERY_SIGNAL, pDevice->dwSysintrExtBattery);
        CloseHandle(pDevice->hGpio);
    }

    rc = TRUE;

    DEBUGMSG(ZONE_FUNCTION, (L"-PWS_Deinit(rc = %d)\r\n", rc));
    return rc;
}

//------------------------------------------------------------------------------
DWORD PWS_Open(DWORD context, DWORD accessCode, DWORD shareMode)
{
    UNREFERENCED_PARAMETER(context);
    UNREFERENCED_PARAMETER(accessCode);
    UNREFERENCED_PARAMETER(shareMode);
    return context;
}

//------------------------------------------------------------------------------
BOOL PWS_Close(DWORD context)
{
    UNREFERENCED_PARAMETER(context);
    return TRUE;
}

//------------------------------------------------------------------------------
DWORD PWS_Read(DWORD context, LPVOID pBuffer, DWORD Count)
{
    UNREFERENCED_PARAMETER(context);
    UNREFERENCED_PARAMETER(pBuffer);
    UNREFERENCED_PARAMETER(Count);
    return 0;
}

//------------------------------------------------------------------------------
DWORD PWS_Write(DWORD context, LPCVOID pSourceBytes, DWORD NumberOfBytes)
{
    UNREFERENCED_PARAMETER(context);
    UNREFERENCED_PARAMETER(pSourceBytes);
    UNREFERENCED_PARAMETER(NumberOfBytes);
    return 0;
}

//------------------------------------------------------------------------------
BOOL PWS_IOControl(
    DWORD context, DWORD code, BYTE *pInBuffer, DWORD inSize, BYTE *pOutBuffer,
    DWORD outSize, DWORD *pOutSize
) {
    BOOL res = FALSE;
    PwrMonitorDevice_t *pDevice = (PwrMonitorDevice_t*)context;

	UNREFERENCED_PARAMETER(context);

    DEBUGMSG(ZONE_INFO, (L"+PWS_IOControl(code=0x%08x, pInBuffer=0x%08x, inSize=%d, pOutBuffer=0x%08x, outSize=%d)", code, pInBuffer, inSize, pOutBuffer, outSize));

    switch(code)
    {
        case PWS_IOCTL_READ_STATUS :
            if(pOutBuffer != NULL)
            {
                (BYTE)*pOutBuffer = pDevice->currentPowerState;
                *pOutSize = sizeof(PWS_PowerStates_te);
                res = TRUE;
            }
            break;
        case PWS_IOCTL_READ_PRIM_SIGNAL:
            if(pOutBuffer != NULL)
            {
                (BYTE)*pOutBuffer = pDevice->isPrimaryPresent;
                *pOutSize = sizeof(BOOL);
                res = TRUE;
            }
            break;
        case PWS_IOCTL_READ_CIG_SIGNAL:
            if(pOutBuffer != NULL)
            {
                (BYTE)*pOutBuffer = pDevice->isCigarLighterPresent;
                *pOutSize = sizeof(BOOL);
                res = TRUE;
            }
            break;
        case PWS_IOCTL_READ_BATT_SIGNAL:
            if(pOutBuffer != NULL)
            {
                (BYTE)*pOutBuffer = pDevice->isExternalBatteryPresent;
                *pOutSize = sizeof(BOOL);
                res = TRUE;
            }
            break;
        default:
            break;
    }
    return res;
}

//------------------------------------------------------------------------------
//
//  Function:  PWS_PrimaryIntrThread
//  This function acts as the IST for primary supply signal intterupt.
DWORD PWS_PrimaryIntrThread(VOID *pContext)
{
    PwrMonitorDevice_t *pDevice = (PwrMonitorDevice_t*)pContext;
    
    DEBUGMSG(ZONE_IST, (L"+PWS_PrimaryIntrThread\r\n"));

    // Loop until we are stopped...
    while (!pDevice->bIntrThreadExit)
    {
        // Wait for interrupt
        if (WaitForSingleObject(pDevice->hPrimaryIntrEvent, pDevice->timeout) != WAIT_TIMEOUT)
		{
            // Interrupt occured. Refresh pin state
            pDevice->isPrimaryPresent = (GPIOGetBit(pDevice->hGpio, PWS_PRIMARY_SUPPLY_SIGNAL) == 1);
            ComputePowerStatus(pDevice);
        }
        GPIOInterruptDone(pDevice->hGpio, PWS_PRIMARY_SUPPLY_SIGNAL, pDevice->dwSysintrPrimary);
    }

    DEBUGMSG(ZONE_IST, (L"-PWS_PrimaryIntrThread\r\n"));
    return ERROR_SUCCESS;
}

//
//  Function:  PWS_CigarLIntrThread
//  This function acts as the IST for cigar lighter signal intterupt.
DWORD PWS_CigarLIntrThread(VOID *pContext)
{
    PwrMonitorDevice_t *pDevice = (PwrMonitorDevice_t*)pContext;
    
    DEBUGMSG(ZONE_IST, (L"+PWS_CigarLIntrThread\r\n"));

    // Loop until we are stopped...
    while (!pDevice->bIntrThreadExit)
    {
        // Wait for interrupt
        if (WaitForSingleObject(pDevice->hCigarLIntrEvent, pDevice->timeout) != WAIT_TIMEOUT)
        {
            // Interrupt occured. Refresh pin state
            pDevice->isCigarLighterPresent = (GPIOGetBit(pDevice->hGpio, PWS_CIGAR_LIGHTER_SIGNAL) == 1);
            ComputePowerStatus(pDevice);
        }
        GPIOInterruptDone(pDevice->hGpio, PWS_CIGAR_LIGHTER_SIGNAL, pDevice->dwSysintrCigarL);
    }

    DEBUGMSG(ZONE_IST, (L"-PWS_CigarLIntrThread\r\n"));
    return ERROR_SUCCESS;
}

//
//  Function:  PWS_ExtBatteryIntrThread
//  This function acts as the IST for external battery signal intterupt.
DWORD PWS_ExtBatteryIntrThread(VOID *pContext)
{
    PwrMonitorDevice_t *pDevice = (PwrMonitorDevice_t*)pContext;
    
    DEBUGMSG(ZONE_IST, (L"+PWS_ExtBatteryIntrThread\r\n"));

    // Loop until we are stopped...
    while (!pDevice->bIntrThreadExit)
    {
        // Wait for interrupt
        if (WaitForSingleObject(pDevice->hExtBatteryIntrEvent, pDevice->timeout) != WAIT_TIMEOUT)
        {
            // Interrupt occured. Refresh pin state
            pDevice->isExternalBatteryPresent = (GPIOGetBit(pDevice->hGpio, PWS_EXT_BATTERY_SIGNAL) == 1);
            ComputePowerStatus(pDevice);
        }
        GPIOInterruptDone(pDevice->hGpio, PWS_EXT_BATTERY_SIGNAL, pDevice->dwSysintrExtBattery);
    }

    DEBUGMSG(ZONE_IST, (L"-PWS_ExtBatteryIntrThread\r\n"));
    return ERROR_SUCCESS;
}   

//------------------------------------------------------------------------------
BOOL __stdcall DllMain(HANDLE hDLL, DWORD reason, VOID *pReserved)
{
    UNREFERENCED_PARAMETER(pReserved);
    switch (reason) {
    case DLL_PROCESS_ATTACH:
        DEBUGREGISTER(hDLL);
        DisableThreadLibraryCalls((HMODULE)hDLL);
        break;
    }
    return TRUE;
}