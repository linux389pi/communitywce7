#ifndef __PWR_STATE_DEFS_H_
#define __PWR_STATE_DEFS_H_

/*--------------------------------------------------
 *  INPUTS SIGNAL ASSIGNMENTS
 * ------------------------------------------------*/
 /* Do not modify these settings unless PADS CONFIG is modified too (and of course if the
  * board is as well) */
 #define PWS_PRIMARY_SUPPLY_SIGNAL  (GPIO2_1)
 #define PWS_CIGAR_LIGHTER_SIGNAL   (GPIO1_17)
 #define PWS_EXT_BATTERY_SIGNAL     (GPIO1_14)

 /*--------------------------------------------------
 *  IST Configuration
 * ------------------------------------------------*/
 #define PWS_IST_TIMEOUT_MS          (500)

#endif // __PWR_STATE_DEFS_H_