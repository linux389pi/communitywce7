#ifndef __PWR_STATE__H_
#define __PWR_STATE__H_

#include "pm.h"

/// Device power states 
typedef enum
{
    // Device is powered by primary power supply
    Normal, 
    // Device is powered by external battery
    Standalone,
    // Device is powered by cigar lighter
    Transport,
    // Main supplies are all down. Product enters low power mode
    // if it can (if a secure battery is connected to TPS65217)
    // Application needs to start shut down process
    Shutdown,
} PWS_PowerStates_te;

// Public event name
// Do not modify this name unless applications are modified as well
// @todo move to registry
#define POWER_STATE_PUBLIC_EVENT_NAME L"Global\\PowerStateEvt"

/*--------------------------------------------------------------------------
 * IOCTL CODES
 *-------------------------------------------------------------------------*/
#define PWS_IOCTL_FILE_DEVICE      (2048) // The first 2047 are reserved by WinCE \

#define PWS_IOCTL_READ_STATUS \
    CTL_CODE(PWS_IOCTL_FILE_DEVICE, 0x200, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define PWS_IOCTL_READ_PRIM_SIGNAL \
    CTL_CODE(PWS_IOCTL_FILE_DEVICE, 0x201, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define PWS_IOCTL_READ_CIG_SIGNAL \
    CTL_CODE(PWS_IOCTL_FILE_DEVICE, 0x202, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define PWS_IOCTL_READ_BATT_SIGNAL \
    CTL_CODE(PWS_IOCTL_FILE_DEVICE, 0x203, METHOD_BUFFERED, FILE_ANY_ACCESS)
 

#endif // __PWR_STATE__H_