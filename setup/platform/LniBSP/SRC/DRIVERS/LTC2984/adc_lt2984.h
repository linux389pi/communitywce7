#ifndef __ADC_LT2984_H
#define __ADC_LT2984_H

#include "windows.h"
#include "winioctl.h"

/*--------------------------------------------------------------------------
 * PUBLIC TYPES
 *-------------------------------------------------------------------------*/
// Channel configuration structure
typedef struct
{
    // Channel identifier (1-20)
    BYTE channelId;
    // Channel 32 bits configuration
    // See defs.h file for settings
    DWORD configuration;
}LT2984_ChannelConf_ts;

/*--------------------------------------------------------------------------
 * PUBLIC DEFINITIONS
 *-------------------------------------------------------------------------*/
// LT2984_IOCTL_SELECT_ADC : selection of ADC 1 
#define LT2984_ADC1_ID  (0x00)
// LT2984_IOCTL_SELECT_ADC : selection of ADC 2 (if mounted) 
#define LT2984_ADC2_ID  (0x01)
/*--------------------------------------------------------------------------
 * IOCTL CODES
 *-------------------------------------------------------------------------*/
#define LT2984_IOCTL_FILE_DEVICE      (2048) // The first 2047 are reserved by WinCE \

#define LT2984_IOCTL_SELECT_ADC \
    CTL_CODE(LT2984_IOCTL_FILE_DEVICE, 0x200, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define LT2984_IOCTL_ASSIGN_CHANNEL \
    CTL_CODE(LT2984_IOCTL_FILE_DEVICE, 0x201, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define LT2984_IOCTL_MEASURE_CHANNEL_TEMPERATURE \
    CTL_CODE(LT2984_IOCTL_FILE_DEVICE, 0x202, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define LT2984_IOCTL_MEASURE_CHANNEL_VOLTAGE \
    CTL_CODE(LT2984_IOCTL_FILE_DEVICE, 0x203, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define LT2984_IOCTL_START_CHANNEL_CONVERSION \
    CTL_CODE(LT2984_IOCTL_FILE_DEVICE, 0x204, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define LT2984_IOCTL_WAIT_END_OF_CONVERSION \
    CTL_CODE(LT2984_IOCTL_FILE_DEVICE, 0x205, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define LT2984_IOCTL_EEPROM_WRITE \
    CTL_CODE(LT2984_IOCTL_FILE_DEVICE, 0x206, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define LT2984_IOCTL_EEPROM_READ \
    CTL_CODE(LT2984_IOCTL_FILE_DEVICE, 0x207, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define LT2984_IOCTL_SLEEP \
    CTL_CODE(LT2984_IOCTL_FILE_DEVICE, 0x208, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define LT2984_IOCTL_READ_FAULT_STATUS \
    CTL_CODE(LT2984_IOCTL_FILE_DEVICE, 0x209, METHOD_BUFFERED, FILE_ANY_ACCESS)

#endif // __ADC_LT2984_H