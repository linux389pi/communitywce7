//
//  File: ADC_LT2984.c
//
//  This file implements device driver for RTC LT2984. 
//
#include "adc_lt2984.h"
#include "adc_lt2984_defs.h"
#include "adc_lt2984_cfg.h"

#include "omap.h" 
#include "ceddkex.h" 
#include "sdk_padcfg.h" 
#include "bsp_padcfg.h" 
#include "bsp.h"
#include <sdk_spi.h>
#include "am33x_mcspi_regs.h" 

#include <nkintr.h>
#include <winuserm.h>
#include <pmpolicy.h>

// ==== Debug Zones ================================================================================
// simple handling: debug zones are forced with TRUE or FALSE and need a recompile to be activated
// for sophisticated handling: use macro DEBUGZONE(), DBGPARAM dpCurSettings etc.
// (see MSDN e.g. Win CE 5.0, Debug Zones)
#ifndef SHIP_BUILD

#undef ZONE_ERROR
#undef ZONE_WARN
#undef ZONE_FUNCTION
#undef ZONE_INFO
#undef ZONE_TIPSTATE

#define ZONE_ERROR          DEBUGZONE(0)
#define ZONE_WARN           DEBUGZONE(1)
#define ZONE_FUNCTION       DEBUGZONE(2)
#define ZONE_INFO           DEBUGZONE(3)
#define ZONE_TIPSTATE       DEBUGZONE(4)

static DBGPARAM dpCurSettings = {
    L"ADC_LT2984", {
        L"Errors",      L"Warnings",    L"Function",    L"Info",
            L"IST",         L"Undefined",   L"Undefined",   L"Undefined",
            L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined",
            L"Undefined",   L"Undefined",   L"Undefined",   L"Undefined" 
    },
    // Bitfield controlling the zones.  1 means the zone is enabled, 0 disabled
    // We'll enable the Error, Warning, and Info zones by default here
    1 | 1 << 1 | 1 << 2 | 1 << 3
};

#endif

// Get High byte of 16 bits word
#define HIGH_BYTE(word)  ((word >> 8) & 0x00FF)
// Get low byte of 16 bits word
#define LOW_BYTE(word)   ((word) & 0x00FF)
// Get Channel resgiter's start adress 
#define LT2984_GET_CHANNEL_START_ADDRESS(base,channel) ((base) + 4 * (channel-1))

//------------------------------------------------------------------------------

// ADC device type
typedef struct
{
    // Interrupt pin
    DWORD intrPin;
    // System Interrupt IRQ
    DWORD sysIntr;
    // System Ubterrupt Event
    HANDLE hIntrEvent;
    // Signals if the ADC entered sleep mode
    BOOL isInSleepMode;
    // Last recovered fault status
    BYTE faultStatus;
}DEVICE_ADC_TS;

//------------------------------------------------------------------------------
static BOOL ADC_ConfigureADC(DWORD index, DWORD interruptPin);
static BOOL ADC_Read8(BYTE bCmd, WORD wStartAddress, BYTE* OutBuffer);
static BOOL ADC_Write8(BYTE bCmd, WORD wStartAddress, BYTE InBuffer);
static BOOL ADC_Read32(BYTE bCmd, WORD wStartAddress, DWORD* OutBuffer);
static BOOL ADC_Write32(BYTE bCmd, WORD wStartAddress, DWORD InBuffer);
static BOOL ADC_SPI_Read(BYTE* pOutBuffer, DWORD size);
static BOOL ADC_SPI_Write(BYTE* pInBuffer, DWORD size);
static BYTE LT2984_GetSelectedADC();
static void LT2984_WakeUpIfRequired();
static BOOL LT2984_AssignChannel(LT2984_ChannelConf_ts* channelConfig);
static BOOL LT2984_StartChannelConversion(BYTE channelId);
static BOOL LT2984_WaitCompletion();
static BOOL LT2984_WriteEEPROM();
static BOOL LT2984_ReadEEPROM();
static BYTE LT2984_GetLastFaultStatus();
static BOOL LT2984_ReadRawConversion(BYTE channelId, int* ipRawConversion);
static BOOL LT2984_ReadTemperature(BYTE channelId, FLOAT* fpTemperature);
static BOOL LT2984_ReadVoltage(BYTE channelId, FLOAT* fpVoltage);
static BOOL LT2984_EnterSleepMode();
BOOL ADC_Deinit(DWORD context);
//------------------------------------------------------------------------------
// SPI handle
static HANDLE m_hSPI = INVALID_HANDLE_VALUE;
// GPIO handle
static HANDLE m_hGPIO = INVALID_HANDLE_VALUE;
// ADC instances
static DEVICE_ADC_TS m_hADC[LT2984_ADC_NB];

// Configure an ADC instance 
static BOOL ADC_ConfigureADC(DWORD index, DWORD interruptPin)
{
    BOOL bSuccess = FALSE;

    m_hADC[index].intrPin = interruptPin;
    m_hADC[index].sysIntr = (DWORD) SYSINTR_UNDEFINED;
    m_hADC[index].hIntrEvent = INVALID_HANDLE_VALUE;
    m_hADC[index].isInSleepMode = FALSE;
    m_hADC[index].faultStatus = 0;

    // Configure interrupt pins
    GPIOSetMode(m_hGPIO, m_hADC[index].intrPin, GPIO_DIR_INPUT | GPIO_INT_LOW_HIGH);
    // Verify
    if((GPIO_DIR_INPUT | GPIO_INT_HIGH_LOW) != GPIOGetMode(m_hGPIO, m_hADC[index].intrPin))
    {
        // Configure interrupt event
        m_hADC[index].hIntrEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
        if (m_hADC[index].hIntrEvent != NULL)
        {
            if (GPIOInterruptInitialize(
                    m_hGPIO, 
                    m_hADC[index].intrPin, 
                    &m_hADC[index].sysIntr, 
                    m_hADC[index].hIntrEvent))
            {
                // Set the interrupt line as a wake up source for the system
                //GPIOInterruptWakeUp(m_hGPIO, m_Interrupt_Pin, m_I2C_SysIntr, TRUE);
                GPIOInterruptDone(m_hGPIO, m_hADC[index].intrPin, (DWORD)m_hADC[index].hIntrEvent);
                bSuccess = TRUE;
            }
            else
            {
                RETAILMSG(ZONE_ERROR, (L"Failed to initialize the interrupt line for ADC %d\r\n", index));
            }
        }
        else
        {
            RETAILMSG(ZONE_ERROR, (L"Failed to initialize the interrupt Event for ADC %d\r\n", index));
        }
    }
    else
    {
        RETAILMSG(ZONE_ERROR, (L"Failed to initialize the interrupt Pin for ADC %d\r\n", index));
    }

    return bSuccess;
}

static BOOL ADC_SPI_Write(BYTE* pInBuffer, DWORD size)
{
    DWORD spiConfig = LT2984_SPI_CONFIG_TX | MCSPI_CHCONF_FORCE;
    BOOL res = FALSE;

    // Set SPI as master
    if(SPIConfigure(m_hSPI, LT2984_SPI_CHANNEL, spiConfig))
    {
        // Write packet
        if(0U == SPIWrite(m_hSPI, size, pInBuffer))
        {
            RETAILMSG(ZONE_ERROR, (L"ADX : SPI write data failed.\r\n"));
        }
        else
        {
            res = TRUE;
        }
    }
    else
    {
        RETAILMSG(ZONE_ERROR, (L"ADX : Can't configure SPI1 in TX mode\r\n"));
    }

    return res;
}

static BOOL ADC_SPI_Read(BYTE* pOutBuffer, DWORD size)
{
    DWORD spiConfig = LT2984_SPI_CONFIG_RX | MCSPI_CHCONF_FORCE; // force cs bit on 
    BOOL res = FALSE;

    // Set SPI as master
    if(SPIConfigure(m_hSPI, LT2984_SPI_CHANNEL, spiConfig))
    {
        // Write packet
        if(size != SPIRead(m_hSPI, size, pOutBuffer))
        {
            RETAILMSG(ZONE_ERROR, (L"ADX : SPI read data failed.\r\n"));
        }
        else
        {
            res = TRUE;
        }
    }
    else
    {
        RETAILMSG(ZONE_ERROR, (L"ADX : Can't configure SPI1 in RX mode\r\n"));
    }
    return res;
}

// SPI write transaction for an 8 bytes data
// @param bCmd Command identifier
// @param wStartAddress register/ram start address
// @param InBuffer input data to be sent
static BOOL ADC_Write8(BYTE bCmd, WORD wStartAddress, BYTE InBuffer)
{
    BOOL res = FALSE;
    BYTE bWriteDataPacket[4] = {0,};

    LT2984_WakeUpIfRequired();
	
	SPILockController(m_hSPI, 0xFFFFF);
	SPIEnableChannel(m_hSPI);

    // Prepare data packet
	bWriteDataPacket[0] = bCmd;
    bWriteDataPacket[1] = HIGH_BYTE(wStartAddress);
    bWriteDataPacket[2] = LOW_BYTE(wStartAddress);
    bWriteDataPacket[3] = (BYTE) InBuffer;

    // Transfer data
    res = ADC_SPI_Write(bWriteDataPacket, sizeof(bWriteDataPacket));

	SPIDisableChannel(m_hSPI);
	SPIUnlockController(m_hSPI);

    return res;
}

// SPI read transaction for an 8 bytes data
// @param bCmd Command identifier
// @param wStartAddress register/ram start address
// @param OutBuffer output data to be sent
static BOOL ADC_Read8(BYTE bCmd, WORD wStartAddress, BYTE* OutBuffer)
{
    BOOL res = FALSE;
    BYTE bWriteDataPacket[3] = {0,};

    LT2984_WakeUpIfRequired();
	
	SPILockController(m_hSPI, 0xFFFFF);
	SPIEnableChannel(m_hSPI);

    // Prepare data packet
	bWriteDataPacket[0] = bCmd;
    bWriteDataPacket[1] = HIGH_BYTE(wStartAddress);
    bWriteDataPacket[2] = LOW_BYTE(wStartAddress);

    // Transfer data
    res = ADC_SPI_Write(bWriteDataPacket, sizeof(bWriteDataPacket));
    if(res)
    {
        // Read result
        res = ADC_SPI_Read(OutBuffer, 1);
    }

	SPIDisableChannel(m_hSPI);
	SPIUnlockController(m_hSPI);

    return res;
}

// SPI write transaction for an 32 bytes data
// @param bCmd Command identifier
// @param wStartAddress register/ram start address
// @param InBuffer input data to be sent
static BOOL ADC_Write32(BYTE bCmd, WORD wStartAddress, DWORD InBuffer)
{
    BOOL res = FALSE;
    BYTE baWriteDataPacket[7] = {0,};

    LT2984_WakeUpIfRequired();
	
	SPILockController(m_hSPI, 0xFFFFF);
	SPIEnableChannel(m_hSPI);

    // Prepare data packet
	baWriteDataPacket[0] = bCmd;
    baWriteDataPacket[1] = HIGH_BYTE(wStartAddress);
    baWriteDataPacket[2] = LOW_BYTE(wStartAddress);
	baWriteDataPacket[3] = (BYTE) ((InBuffer >> 24) & 0x000000FF);
	baWriteDataPacket[4] = (BYTE) ((InBuffer >> 16) & 0x000000FF);
	baWriteDataPacket[5] = (BYTE) ((InBuffer >> 8) & 0x000000FF);
	baWriteDataPacket[6] = (BYTE) (InBuffer & 0x000000FF);

    // Transfer data
    res = ADC_SPI_Write(baWriteDataPacket, sizeof(baWriteDataPacket));

	SPIDisableChannel(m_hSPI);
	SPIUnlockController(m_hSPI);

    return res;
}

// SPI read transaction for an 32 bytes data
// @param bCmd Command identifier
// @param wStartAddress register/ram start address
// @param OutBuffer output data to be received
static BOOL ADC_Read32(BYTE bCmd, WORD wStartAddress, DWORD* pOutBuffer)
{
    BOOL res = FALSE;
    BYTE baWriteDataPacket[3] = {0,};
    BYTE baReadDataPacket[4] = {0,};

    LT2984_WakeUpIfRequired();
	
	SPILockController(m_hSPI, 0xFFFFF);
	SPIEnableChannel(m_hSPI);

    // Prepare data packet
	baWriteDataPacket[0] = bCmd;
    baWriteDataPacket[1] = HIGH_BYTE(wStartAddress);
    baWriteDataPacket[2] = LOW_BYTE(wStartAddress);

    // Transfer data
    res = ADC_SPI_Write(baWriteDataPacket, sizeof(baWriteDataPacket));
    if(res)
    {
        // Read result
        res = ADC_SPI_Read(baReadDataPacket, sizeof(baReadDataPacket));
        if(res)
        {
            // Copy read packet out
            *pOutBuffer = ((DWORD) (baReadDataPacket[0] << 24) |
                        (DWORD) (baReadDataPacket[1] << 16) |
                        (DWORD) (baReadDataPacket[2] << 8) |
                        (DWORD) (baReadDataPacket[3]) );
        }
    }

	SPIDisableChannel(m_hSPI);
	SPIUnlockController(m_hSPI);

    return res;
}

// Returns the currently selected ADC index
static BYTE LT2984_GetSelectedADC()
{
    BYTE selectedADC = 0; // Default ADC1 is selected
	if(GPIOGetBit(m_hGPIO, LT2984_ADC_SELECT_PIN) == 0x00)
	{
		// If cleared, ADC2 is selected
		selectedADC = 0x01;
	}
	return selectedADC; 
}

// Gets the ADC out of sleep mode
static void LT2984_WakeUpIfRequired()
{
    BYTE bSelectedADC = 0;

    // Get selected ADC
    bSelectedADC = LT2984_GetSelectedADC();

    if(m_hADC[bSelectedADC].isInSleepMode)
    {
        // Wake it up
        SPIEnableChannel(m_hSPI); // activate chip select
        Sleep(WAKE_UP_DELAY);
        SPIDisableChannel(m_hSPI); // deactivate chip select
    }    
}

// Set channel assignment
static BOOL LT2984_AssignChannel(LT2984_ChannelConf_ts* channelConfig)
{
  WORD wStartAddress;
  BOOL status = FALSE; 

  if(channelConfig->channelId >= LT2984_CHANNEL_MIN
    && channelConfig->channelId <= LT2984_CHANNEL_MAX)
  {
    // Compute start address
    wStartAddress = LT2984_GET_CHANNEL_START_ADDRESS(CH_ADDRESS_BASE, channelConfig->channelId);
    // Send assignment
    status = ADC_Write32(WRITE_TO_RAM, wStartAddress, channelConfig->configuration);
  }
  else
  {
      RETAILMSG(ZONE_ERROR, (TEXT("ERROR: ADC LT2984: Specified channel is out of range.\r\n")));
  }

  return status;
}

// Start conversion on a channel 
// Returns immediately
static BOOL LT2984_StartChannelConversion(BYTE channelId)
{
    BOOL status = FALSE;

    if(channelId >= LT2984_CHANNEL_MIN
        && channelId <= LT2984_CHANNEL_MAX)
    {
        // Start conversion
        status = ADC_Write8(WRITE_TO_RAM, COMMAND_STATUS_REGISTER, CONVERSION_CONTROL_BYTE | channelId);
    }
    else
    {
        RETAILMSG(ZONE_ERROR, (TEXT("ERROR: ADC LT2984: Specified channel is out of range.\r\n")));
    }

    return status;
}

// Waits for the ADC to be ready again
static BOOL LT2984_WaitCompletion()
{
    BOOL status = FALSE;
    BYTE bAdcIndex = 0;
    BYTE bAdcStatus = 0x00;

    // Ensure that the ADC is not already ready
    status = ADC_Read8(READ_FROM_RAM, COMMAND_STATUS_REGISTER, &bAdcStatus);
    if(status != FALSE)
    {
        if(bAdcStatus & ADC_READY)
        {
            // Adc is ready
            status = TRUE;
        }
        else
        {
            // Adc is busy
            // Get selected ADC
            bAdcIndex = LT2984_GetSelectedADC();
            // Release current interrupt and wait for the next one
            GPIOInterruptDone(m_hGPIO, m_hADC[bAdcIndex].intrPin, (DWORD)m_hADC[bAdcIndex].hIntrEvent);
            // Wait completion
            if (WAIT_TIMEOUT == WaitForSingleObject(m_hADC[bAdcIndex].hIntrEvent,LT2984_ADC_CONVERSION_TIMEOUT))  // wait for input interrupt or timeout
            {
                    RETAILMSG(ZONE_ERROR, (L"ADX WaitCompletion failed : timeour error\r\n"));
            }
            else
            {
                    status = TRUE;
            }
        }
    }

    return status;
}

// Forces ADC to mirror its RAM into EEPROM
static BOOL LT2984_WriteEEPROM()
{
    BOOL status = FALSE;
    BYTE bEEpromSuccess;

    // Set EEPROM key
    status = ADC_Write32(WRITE_TO_RAM, EEPROM_START_ADDRESS, EEPROM_KEY);
    if(status)
    {
        // Write to EEPROM
        status = ADC_Write8(WRITE_TO_RAM, COMMAND_STATUS_REGISTER, WRITE_TO_EEPROM); // output value is ignored
        if(status)
        {
            // Wait the end of processing
            status = LT2984_WaitCompletion();
            
            if(status)
            {
                // Check for success 
                status = ADC_Read8(READ_FROM_RAM, EEPROM_RESULT_CODE_ADDRESS, &bEEpromSuccess);
                if(status)
                {
                    status = (bEEpromSuccess == 0); // If 0, EEPROM transfer succeeded
                }
            }
        }
    }

    return status;
}

// Forces ADC to mirror its EEPROM into RAM
static BOOL LT2984_ReadEEPROM()
{
    BOOL status = FALSE;
    BYTE bEEpromSuccess;

    // Set EEPROM key
    status = ADC_Write32(WRITE_TO_RAM, EEPROM_START_ADDRESS, EEPROM_KEY);
    if(status)
    {
        // Write to EEPROM
        status = ADC_Write8(WRITE_TO_RAM, COMMAND_STATUS_REGISTER, READ_FROM_EEPROM);
        if(status)
        {
            // Wait the end of processing
            status = LT2984_WaitCompletion();

            if(status)
            {
                // Check for success 
                status = ADC_Read8(READ_FROM_RAM, EEPROM_RESULT_CODE_ADDRESS, &bEEpromSuccess);
                if(status)
                {
                    status = (bEEpromSuccess == 0); // If 0, EEPROM transfer succeeded
                }
            }
        }
    }

    return status;
}

// Returns the last fault status recovered by the ADC
static BYTE LT2984_GetLastFaultStatus()
{
    BYTE selectedADC = 0;
    selectedADC = LT2984_GetSelectedADC();
    return m_hADC[selectedADC].faultStatus;
}

// Start conversion on a channel 
// Returns immediately
static BOOL LT2984_ReadRawConversion(BYTE channelId, int* ipRawConversion)
{
    BOOL status = FALSE;
    int dwSignedConversionValue = 0;
    WORD wStartAddress = 0;
    BYTE selectedADC = 0;

    // Get start address
    wStartAddress = LT2984_GET_CHANNEL_START_ADDRESS(CONVERSION_RESULT_MEMORY_BASE, channelId);

    // Read measure
    status = ADC_Read32(READ_FROM_RAM, wStartAddress, (DWORD*)&dwSignedConversionValue);
    if(status)
    {
        // Save the fault status first
        selectedADC = LT2984_GetSelectedADC();
        m_hADC[selectedADC].faultStatus = dwSignedConversionValue >> 24;
        // Now filter the measure out of register value
        dwSignedConversionValue &= 0xFFFFFF; // conversion value is contained in 24 LSB's of register
        // Check if value is negative (bit sign on)
        if(dwSignedConversionValue & 0x800000)
        {
            dwSignedConversionValue |= (0xFF000000);
        }
        // Return output value
        *ipRawConversion = dwSignedConversionValue;
    }

    return status;
}

// Read measured temperature
static BOOL LT2984_ReadTemperature(BYTE channelId, FLOAT* fpTemperature)
{
    BOOL status = FALSE;
    int iRawValue = 0;

    if(channelId >= LT2984_CHANNEL_MIN
        && channelId <= LT2984_CHANNEL_MAX)
    {
        status = LT2984_ReadRawConversion(channelId, &iRawValue);
        if(status)
        {
            *fpTemperature = (FLOAT) (((FLOAT)iRawValue) / 1024);
        }
    }
    else
    {
        RETAILMSG(ZONE_ERROR, (TEXT("ERROR: ADC LT2984: Specified channel is out of range.\r\n")));
    }

    return status;
}

// Read measured voltage
static BOOL LT2984_ReadVoltage(BYTE channelId, FLOAT* fpVoltage)
{
    BOOL status = FALSE;
    int iRawValue = 0;

    if(channelId >= LT2984_CHANNEL_MIN
        && channelId <= LT2984_CHANNEL_MAX)
    {
        status = LT2984_ReadRawConversion(channelId, &iRawValue);
        if(status)
        {
            *fpVoltage = (FLOAT) (((FLOAT)iRawValue) / 2097152);
        }
    }
    else
    {
        RETAILMSG(ZONE_ERROR, (TEXT("ERROR: ADC LT2984: Specified channel is out of range.\r\n")));
    }

    return status;
}

// Force ADC to enter sleep mode
// It will wake up on next write operation 
// After wake up it will require 200 ms to start-up
static BOOL LT2984_EnterSleepMode()
{
    BOOL status = FALSE;
    BYTE bSelectedADC = 0;

    // Get selected ADC
    bSelectedADC = LT2984_GetSelectedADC();
    // Start conversion
    status = ADC_Write8(WRITE_TO_RAM, COMMAND_STATUS_REGISTER, CONVERSION_CONTROL_BYTE | ENTER_SLEEP_MODE);
    if(status)
    {
        // Update ADC handle 
        m_hADC[bSelectedADC].isInSleepMode = TRUE;  
    }

    return status;
}

//------------------------------------------------------------------------------
DWORD ADC_Init(LPCTSTR szContext, LPCVOID pBusContext)
//  Called by device manager to initialize device.
{
    DWORD rc = (DWORD)NULL;

    UNREFERENCED_PARAMETER(pBusContext);
    DEBUGMSG(ZONE_FUNCTION, (L"+ADC_Init(%s, 0x%08x)\r\n", szContext, pBusContext));
    RETAILMSG(1, (L"+ADC_Init\r\n"));

    // Initialize GPIOs
    m_hGPIO = GPIOOpen();
    if (m_hGPIO != INVALID_HANDLE_VALUE)
    {
        // Configure ADC selection pin
        GPIOSetMode(m_hGPIO, LT2984_ADC_SELECT_PIN, GPIO_DIR_OUTPUT);
        if( (GPIO_DIR_OUTPUT) != GPIOGetMode(m_hGPIO, LT2984_ADC_SELECT_PIN) )
        {
            RETAILMSG(ZONE_ERROR, (L"ADC selection pin configuration failed.\r\n"));
            goto cleanUp;       
        }

        // Configure ADCs
        if(FALSE == ADC_ConfigureADC(0, LT2984_ADC1_INT_PIN))
        {
            goto cleanUp;
        }
        if(FALSE == ADC_ConfigureADC(1, LT2984_ADC2_INT_PIN))
        {
            goto cleanUp;
        }   
    }

    // Initialize SPI
    m_hSPI = SPIOpen(_T("SPI2:")); // SPI2 is the ID of AM3355x SPI1
    if (m_hSPI == NULL
        || m_hSPI == INVALID_HANDLE_VALUE)
    {
		RETAILMSG(ZONE_ERROR, (L"ADX : Can't open SPI1: device driver!\r\n"));
        goto cleanUp;
    }
    else
    {
        rc = (DWORD)m_hSPI;
    }

cleanUp:
    if (rc == (DWORD)NULL)
	{
		ADC_Deinit((DWORD)0);
	}
    DEBUGMSG(ZONE_FUNCTION, (L"-ADC_Init(rc = %d\r\n", rc));
	RETAILMSG(1, (L"-ADC_Init\r\n"));

    return rc;
}

//------------------------------------------------------------------------------
BOOL ADC_Deinit(DWORD context)
{
    BOOL rc = TRUE;
    DWORD i = 0;

    DEBUGMSG(ZONE_FUNCTION, (L"+ADC_Deinit()\r\n"));

    // ADC release
    for(i = 0; i < LT2984_ADC_NB; i++)
    {
        if(m_hADC[i].sysIntr != (DWORD) SYSINTR_UNDEFINED)
        {
            GPIOInterruptDone(m_hGPIO, m_hADC[i].intrPin, m_hADC[i].sysIntr);
		    GPIOInterruptDisable(m_hGPIO, m_hADC[i].intrPin, m_hADC[i].sysIntr);
        }
        if(m_hADC[i].hIntrEvent != INVALID_HANDLE_VALUE)
        {
            CloseHandle(m_hADC[i].hIntrEvent);
        }
    }
    // GPIO release
    if(m_hGPIO != INVALID_HANDLE_VALUE)
    {
        GPIOClose(m_hGPIO);
    }
    // SPI release
    if (m_hSPI != NULL
        && m_hSPI != INVALID_HANDLE_VALUE)
    {
        SPIClose(m_hSPI);
    } 

    DEBUGMSG(ZONE_FUNCTION, (L"-ADC_Deinit(rc = %d)\r\n", rc));
    return rc;
}

//------------------------------------------------------------------------------
DWORD ADC_Open(DWORD context, DWORD accessCode, DWORD shareMode)
{
    UNREFERENCED_PARAMETER(context);
    UNREFERENCED_PARAMETER(accessCode);
    UNREFERENCED_PARAMETER(shareMode);
    return context;
}

//------------------------------------------------------------------------------
BOOL ADC_Close(DWORD context)
{
    UNREFERENCED_PARAMETER(context);
    return TRUE;
}

//------------------------------------------------------------------------------
DWORD ADC_Read(DWORD context, LPVOID pBuffer, DWORD Count)
{
    UNREFERENCED_PARAMETER(context);
    UNREFERENCED_PARAMETER(pBuffer);
    UNREFERENCED_PARAMETER(Count);
    return 0;
}

//------------------------------------------------------------------------------
DWORD ADC_Write(DWORD context, LPCVOID pSourceBytes, DWORD NumberOfBytes)
{
    UNREFERENCED_PARAMETER(context);
    UNREFERENCED_PARAMETER(pSourceBytes);
    UNREFERENCED_PARAMETER(NumberOfBytes);
    return 0;
}

//------------------------------------------------------------------------------
BOOL ADC_IOControl(
    DWORD context, DWORD code, BYTE *pInBuffer, DWORD inSize, BYTE *pOutBuffer,
    DWORD outSize, DWORD *pOutSize
) 
{
    BOOL res = FALSE;
	UNREFERENCED_PARAMETER(context);
    
    RETAILMSG(1, (L"+ADC_IOControl(code=0x%08x, pInBuffer=0x%08x, inSize=%d, pOutBuffer=0x%08x, outSize=%d)", code, pInBuffer, inSize, pOutBuffer, outSize));

    switch(code)
    {
        case LT2984_IOCTL_SELECT_ADC :
            if(*pInBuffer == LT2984_ADC1_ID) // Select ADC 1
            {
                RETAILMSG(1, (L"ADX Selecting ADC 1\r\n"));
				GPIOSetBit(m_hGPIO, LT2984_ADC_SELECT_PIN);
				res = TRUE;
            }
            else if(*pInBuffer == LT2984_ADC2_ID)
            {
                RETAILMSG(1, (L"ADX Selecting ADC 2\r\n"));
				GPIOClrBit(m_hGPIO, LT2984_ADC_SELECT_PIN);
				res = TRUE;
            }
            else
            {
                // Invalid parameter
                RETAILMSG(1, (L"ADX Select ADC : Invalid parameter pInBuffer (0x%08x)\r\n", pInBuffer));
            }
            break;
        case LT2984_IOCTL_ASSIGN_CHANNEL :
            if(pInBuffer != NULL)
            {
				RETAILMSG(1, (L"ADX Assign channel:(0x%08x) to channel %d", ((LT2984_ChannelConf_ts*)pInBuffer)->configuration,((LT2984_ChannelConf_ts*)pInBuffer)->channelId ));
				res = LT2984_AssignChannel((LT2984_ChannelConf_ts*)pInBuffer);
            }
            break;
        case LT2984_IOCTL_MEASURE_CHANNEL_TEMPERATURE :
            if(pInBuffer != NULL
                && pOutBuffer != NULL)
            {
                res = LT2984_ReadTemperature((BYTE)*pInBuffer,(FLOAT*)pOutBuffer);
                if(res)
                {
                    (DWORD)*pOutSize = sizeof(FLOAT);
                }
            }
            break;
        case LT2984_IOCTL_MEASURE_CHANNEL_VOLTAGE :
            
            if(pInBuffer != NULL
                && pOutBuffer != NULL)
            {
                res = LT2984_ReadVoltage((BYTE)*pInBuffer,(FLOAT*)pOutBuffer);
                if(res)
                {
                    (DWORD)*pOutSize = sizeof(FLOAT);
                }
            }
            break;
        case LT2984_IOCTL_START_CHANNEL_CONVERSION :
            if(pInBuffer != NULL)
            {
                // Returns immediately
                res = LT2984_StartChannelConversion((BYTE)*pInBuffer);
            }
            break;
        case LT2984_IOCTL_WAIT_END_OF_CONVERSION :
            res = LT2984_WaitCompletion();
            break;
        case LT2984_IOCTL_EEPROM_WRITE :
            res = LT2984_WriteEEPROM();
            break;
        case LT2984_IOCTL_EEPROM_READ :
            res = LT2984_ReadEEPROM();
            break;
        case LT2984_IOCTL_SLEEP :
            res = LT2984_EnterSleepMode();
            break;
        case LT2984_IOCTL_READ_FAULT_STATUS:
            if(pOutBuffer != NULL)
            {
                (BYTE)*pOutBuffer = LT2984_GetLastFaultStatus();
                *pOutSize = sizeof(BYTE);
                res = TRUE;
            }
            break;
        default:
            break;
    }
    return res;
}

//------------------------------------------------------------------------------
BOOL __stdcall DllMain(HANDLE hDLL, DWORD reason, VOID *pReserved)
{
    UNREFERENCED_PARAMETER(pReserved);
    switch (reason) {
    case DLL_PROCESS_ATTACH:
        DEBUGREGISTER(hDLL);
        DisableThreadLibraryCalls((HMODULE)hDLL);
        break;
    }
    return TRUE;
}