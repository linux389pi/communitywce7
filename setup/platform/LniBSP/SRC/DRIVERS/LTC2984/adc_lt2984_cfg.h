#ifndef __ADC_LT2984_CFG_H
#define __ADC_LT2984_CFG_H

#include "am33x_gpio.h"

//**********************************************************************************************************
// DRIVER CONFIGURATION
//**********************************************************************************************************
// Total number of ADCs
// Note : Cannot be modified
#define LT2984_ADC_NB   2
// Conversion timeout per channel (milliseconds)
#define LT2984_ADC_CONVERSION_TIMEOUT   260

//**********************************************************************************************************
// SPI CONFIGURATION
//**********************************************************************************************************
// SPI Master single channel
#define LT2984_SPI_CHANNEL 0

// SPI word length
#define LT2984_SPI_WORD_LENGTH 8

// Configures everything SPI in TX mode
#define LT2984_SPI_CONFIG_TX (MCSPI_PHA_ODD_EDGES | /* data is latched on odd edges of clock */ \
                MCSPI_POL_ACTIVEHIGH | /* polarity : active HIGH */ \
                MCSPI_CHCONF_CLKD(5) | /* Clock divider : 5 */ \
                MCSPI_CHCONF_WL(LT2984_SPI_WORD_LENGTH) | /* SPI Word length : 8,16,24,32 bits*/ \
                MCSPI_CSPOLARITY_ACTIVELOW | /* Chip select polarity : active LOW */ \
                MCSPI_CHCONF_TRM_TXONLY | /* Transmission mode : Send  */ \
                MCSPI_CHCONF_DPE0) /* Enable transmission on channel 1 (disable channel 0 for transmission) */

// Configures everything SPI in RX mode
#define LT2984_SPI_CONFIG_RX (MCSPI_PHA_ODD_EDGES | /* data is latched on odd edges of clock */ \
                MCSPI_POL_ACTIVEHIGH | /* polarity : active HIGH */ \
                MCSPI_CHCONF_CLKD(5) | /* Clock divider : 5 */ \
                MCSPI_CHCONF_WL(LT2984_SPI_WORD_LENGTH) | /* SPI Word length : 8,16,24,32 bits*/ \
                MCSPI_CSPOLARITY_ACTIVELOW | /* Chip select polarity : active LOW */ \
                MCSPI_CHCONF_TRM_RXONLY | /* Transmission mode : Receive */ \
                MCSPI_CHCONF_DPE0) /* Enable transmission on channel 1 (disable channel 0 for transmission) */ 

//**********************************************************************************************************
// PIN CONFIGURATION
//**********************************************************************************************************
// ADC selection pin
// If 0 : ADC2 is selected
// If 1 : ADC1 is selected
#define LT2984_ADC_SELECT_PIN GPIO2_5

// ADC interrupt lines
#define LT2984_ADC1_INT_PIN GPIO1_25
#define LT2984_ADC2_INT_PIN GPIO1_27

#endif // __ADC_LT2984_CFG_H