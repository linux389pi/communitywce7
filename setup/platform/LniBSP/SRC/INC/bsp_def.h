// All rights reserved ADENEO EMBEDDED 2010
// Copyright (c) 2007, 2008 BSQUARE Corporation. All rights reserved.

//
//=============================================================================
//            Texas Instruments OMAP(TM) Platform Software
// (c) Copyright Texas Instruments, Incorporated. All Rights Reserved.
//
//  Use of this software is controlled by the terms and conditions found
// in the license agreement under which this software has been supplied.
//
//=============================================================================
//

//------------------------------------------------------------------------------
//
//  File:  bsp_def.h
//
#ifndef __BSP_DEF_H
#define __BSP_DEF_H

#ifdef __cplusplus
extern "C" {
#endif

//------------------------------------------------------------------------------
// Default Boot ARGS (used by eboot)

#define BSP_EBOOT_DEFAULT_MAC_ADDRESS   {0x7B64,0x0CD4,0x55EA} /*TODO DO NOT USE THIS MAC FOR RELEASE*/
#define BSP_EBOOT_DEFAULT_MAC_ADDRESS1  {0x0000,0x0000,0x0000} /*TODO DO NOT USE THIS MAC FOR RELEASE*/

//------------------------------------------------------------------------------
// CPU Info

// Hard-coded CPU settings for our board 
#define BSP_CPU_FAMILY              CPU_FAMILY_AM33X
#define BSP_CPU_VENDOR              L"Texas Instruments"
#define BSP_CPU_NAME                L"AM33X"
#define BSP_CPU_CORE                L"Cortex-A8"

// Platform Info

#define BSP_PLATFORM_TYPE           L"BSP_AM33X_TYPE"
#define BSP_PLATFORM_NAME           L"BSP_AM33X"

// USB Info

#define BSP_USB_DEFAULT_DEVICE_ID   0x0B5D902F /* TODO : This is the hardcoded UID for USB.
                                                  Check if we should generate this ID or not */

//------------------------------------------------------------------------------
// Misc

#define BSP_DEVICE_AM33x_PREFIX       "LNI33X-"

#define AM33x_OPP_NUM    5

//------------------------------------------------------------------------------
// I2C slave addresses
//Note: We mostly use I2C0 on the base board (BUS 1)
#define I2C_BASE_BOARD_ADDR			(0x50)
#define I2C_BASE_BOARD_BUS			(1)
// TLV320AIC3106 (not used)
#define I2C_AUDIO_ADDR				(0x1b)
#define I2C_AUDIO_BUS				(1)
// TPS65217
#define I2C_PMIC_ADDR				(0x24)
#define I2C_PMIC_BUS				(1)
// TDA19988
#define I2C_HDMI_ADDR				(0x70)
#define I2C_HDMI_BUS				(1)
#define I2C_CEC_ADDR				(0x34)
#define I2C_CEC_BUS					(1)

//------------------------------------------------------------------------------
//
//  Define:  TPS65217_I2C_DEVICE_ID
//
//  i2c bus twl is on
//      AM_DEVICE_I2C0
//      AM_DEVICE_I2C1
//      
//
#define TPS65217_I2C_DEVICE_ID              (AM_DEVICE_I2C0)

//------------------------------------------------------------------------
// LCD/DVI Resolutions
typedef enum OMAP_LCD_DVI_RES {
	OMAP_RES_DEFAULT=0,
    OMAP_LCD_320W_240H,
    OMAP_LCD_480W_272H,
    OMAP_LCD_640W_480H,
    OMAP_LCD_800W_480H,
    OMAP_LCD_800W_480H4D,
    OMAP_LCD_800W_600H,
    OMAP_DVI_800W_600H,		// first DVI supported mode see IsDVIMode()
    OMAP_DVI_1024W_768H,
    OMAP_DVI_1280W_720H,
    OMAP_RES_INVALID
}OMAP_LCD_DVI_RES;


#ifdef BSP_TSADC_TOUCH

//------------------------------------------------------------------------
// Touch (modifies ADC settings)
#define XSTEPS    6
#define YSTEPS    6

#define TOUCH_SAMPLE_QUEUE		(TEXT("ADC_TOUCH_SAMPLES"))

typedef struct {
	UINT32 bufferX[XSTEPS];
	UINT32 bufferY[YSTEPS];
} TouchSample_t;

#endif

//------------------------------------------------------------------------
// ADC
#define IOCTL_ADC_GETCHANNEL            \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x300, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_ADC_SCANCHANNEL            \
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x301, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_ADC_AVAILABLECHANNELS		\
    CTL_CODE(FILE_DEVICE_UNKNOWN, 0x302, METHOD_BUFFERED, FILE_ANY_ACCESS)

//------------------------------------------------------------------------------
//  default timeout in tick count units (milli-seconds)
#define BSP_I2C_TIMEOUT_INIT            (500)

#define BSP_I2C0_OA_INIT                (0x0E)
#define BSP_I2C0_BAUDRATE_INIT          (1)
#define BSP_I2C0_MAXRETRY_INIT          (5)
#define BSP_I2C0_RX_THRESHOLD_INIT      (5)
#define BSP_I2C0_TX_THRESHOLD_INIT      (5)

#define BSP_I2C1_OA_INIT                (0x0E)
#define BSP_I2C1_BAUDRATE_INIT          (1)
#define BSP_I2C1_MAXRETRY_INIT          (5)
#define BSP_I2C1_RX_THRESHOLD_INIT      (5)
#define BSP_I2C1_TX_THRESHOLD_INIT      (5)

#define BSP_I2C2_OA_INIT                (0x0E)
#define BSP_I2C2_BAUDRATE_INIT          (1)
#define BSP_I2C2_MAXRETRY_INIT          (5)
#define BSP_I2C2_RX_THRESHOLD_INIT      (5)
#define BSP_I2C2_TX_THRESHOLD_INIT      (5)

#define BSP_I2C3_OA_INIT                (0x0E)
#define BSP_I2C3_BAUDRATE_INIT          (1)
#define BSP_I2C3_MAXRETRY_INIT          (5)
#define BSP_I2C3_RX_THRESHOLD_INIT      (5)
#define BSP_I2C3_TX_THRESHOLD_INIT      (5)

//------------------------------------------------------------------------------
//
//  Select initial XLDR CPU and IVA speed and VDD1 voltage using BSP_OPM_SELECT 
//
    // MPU[720hz @ 1.35V], IVA2[520Mhz @ 1.35V]
    #define BSP_SPEED_CPUMHZ                720
    #define BSP_SPEED_IVAMHZ                520
    #define VDD1_INIT_VOLTAGE_VALUE         0x3c

//------------------------------------------------------------------------------
//
//  Define:  BSP_DEVICE_PREFIX
//
//  This define is used as device name prefix when KITL creates device name.
//
#define BSP_DEVICE_PREFIX       BSP_DEVICE_AM33x_PREFIX

//------------------------------------------------------------------------------
//
//  Define:  BSP_GPMC_xxx
//
//  These constants are used to initialize general purpose memory configuration 
//  registers
//
// NOTE - Settings below are based on CORE DPLL = 332MHz, L3 = CORE/2 (166MHz)

//  NAND settings, not optimized
#define GPMC_SIZE_256M		0x0
#define GPMC_SIZE_128M		0x8
#define GPMC_SIZE_64M		0xC
#define GPMC_SIZE_32M		0xE
#define GPMC_SIZE_16M		0xF

#define GPMC_MAX_REG        7

#define GPMC_NAND_BASE     0x08000000
#define GPMC_NAND_SIZE     GPMC_SIZE_128M

#if 1 /*BSP_AM33X*/   /* SA 8-Bit Nand */
#define M_NAND_GPMC_CONFIG1	0x00000800
#else
#define M_NAND_GPMC_CONFIG1	0x00001810
#endif
#define M_NAND_GPMC_CONFIG2    0x001e1e00
#define M_NAND_GPMC_CONFIG3    0x001e1e00
#define M_NAND_GPMC_CONFIG4    0x16051807
#define M_NAND_GPMC_CONFIG5    0x00151e1e
#define M_NAND_GPMC_CONFIG6	   0x16000f80
#define M_NAND_GPMC_CONFIG7	   0x00000008

//------------------------------------------------------------------------------
//
//  Define:  BSP_UART_DSIUDLL & BSP_UART_DSIUDLH
//
//  This constants are used to initialize serial debugger output UART.
//  Serial debugger uses 115200-8-N-1
//
#define BSP_UART_LCR                   (0x03)
#define BSP_UART_DSIUDLL               (26)
#define BSP_UART_DSIUDLH               (0)

BOOL BSPInsertGpioDevice(UINT range,void* fnTbl,WCHAR* name);


// nand pin connection information
#define BSP_GPMC_NAND_CS            (0)      // NAND is on CHIP SELECT 0
#define BSP_GPMC_IRQ_WAIT_EDGE      (GPMC_IRQENABLE_WAIT0_EDGEDETECT)


#define BSP_WATCHDOG_TIMEOUTPERIOD_MILLISECONDS    (20000)
#define BSP_WATCHDOG_REFRESH_MILLISECONDS   (500)
#define BSP_WATCHDOG_THREAD_PRIORITY        (100)

/* NLED GPIO settings */
#define BSP_LED_NUMBER              (8)
// Mapping between NLEDs and GPIOs
#define BSP_CONFIG_ASSIGN_NLED_IO() \
    g_NLedPddParam[0].GPIOId = GPIO1_21; \
    g_NLedPddParam[1].GPIOId = GPIO1_22; \
    g_NLedPddParam[2].GPIOId = GPIO2_4;  \
    g_NLedPddParam[3].GPIOId = GPIO1_23; \
    g_NLedPddParam[4].GPIOId = GPIO1_24; \
    g_NLedPddParam[5].GPIOId = GPIO0_26; \
    g_NLedPddParam[6].GPIOId = GPIO1_28; \
    g_NLedPddParam[7].GPIOId = GPIO1_29 
    
// Notification leds assignment (used by other components)
#define NOTIFICATION_LED0_GPIO GPIO1_21 // OK
#define NOTIFICATION_LED1_GPIO GPIO1_22 // Warning
#define NOTIFICATION_LED2_GPIO GPIO2_4  // Error
#define NOTIFICATION_LED3_GPIO GPIO1_23 // Watchdog

#ifdef __cplusplus
}
#endif

#endif
