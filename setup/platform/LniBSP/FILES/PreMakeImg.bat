@REM
@REM Copy appropriate logo file to OSDesign
@REM
@echo off

pushd "%_TARGETPLATROOT%"\FILES

echo deleting old logo image

@REM Cleanup
if exist logo.bmp del /f /q logo.bmp

@REM Copy the logo image to release directory
if defined BSP_DISPLAY_LCD_RES_320_240 (
    if exist "%_TARGETPLATROOT%"\IMG\logo_320_240.bmp copy /y "%_TARGETPLATROOT%"\IMG\logo_320_240.bmp logo.bmp
) else if defined BSP_DISPLAY_LCD_RES_480_272 (
    if exist "%_TARGETPLATROOT%"\IMG\logo_480_272.bmp copy /y "%_TARGETPLATROOT%"\IMG\logo_480_272.bmp logo.bmp
) else if defined BSP_DISPLAY_LCD_RES_640_480 (
    if exist "%_TARGETPLATROOT%"\IMG\logo_640_480.bmp copy /y "%_TARGETPLATROOT%"\IMG\logo_640_480.bmp logo.bmp
) else if defined BSP_DISPLAY_LCD_RES_800_480 ( 
    if exist "%_TARGETPLATROOT%"\IMG\logo_800_480.bmp copy /y "%_TARGETPLATROOT%"\IMG\logo_800_480.bmp logo.bmp
) else if defined BSP_DISPLAY_LCD_RES_800_600 (
    if exist "%_TARGETPLATROOT%"\IMG\logo_800_600.bmp copy /y "%_TARGETPLATROOT%"\IMG\logo_800_600.bmp logo.bmp
) else if defined BSP_DISPLAY_DVI_RES_800_600 (
    if exist "%_TARGETPLATROOT%"\IMG\logo_800_600.bmp copy /y "%_TARGETPLATROOT%"\IMG\logo_800_600.bmp logo.bmp
) else if defined BSP_DISPLAY_DVI_RES_1024_768 (
    if exist "%_TARGETPLATROOT%"\IMG\logo_1024_768.bmp copy /y "%_TARGETPLATROOT%"\IMG\logo_1024_768.bmp logo.bmp
) else if defined BSP_DISPLAY_DVI_RES_1280_720 (
    if exist "%_TARGETPLATROOT%"\IMG\logo_1280_720.bmp copy /y "%_TARGETPLATROOT%"\IMG\logo_1280_720.bmp logo.bmp
)

popd


